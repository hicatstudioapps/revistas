package com.artifex.mupdf;

import java.io.File;

import java.util.HashMap;

import com.librelio.task.SafeAsyncTask;


import android.content.Context;
import android.graphics.Point;
import android.graphics.PointF;
import android.net.Uri;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;


public class MuPDFPageAdapter extends BaseAdapter {
	private static final String TAG = "MuPDFPageAdapter";

	private final Context context;
	private final MuPDFCore core;
	private final SparseArray<PointF> mPageSizes = new SparseArray<PointF>();
	private final String path;
	private Uri[] mthumb;

	public MuPDFPageAdapter(Context context, MuPDFCore core, String path) {
		this.context = context;
		this.core = core;
		this.path = path;
		System.out.println("Se hace esto");
		
		File folder = new File(path);
		File[] thumbList = folder.listFiles();

		HashMap<Integer, String> allFiles = new HashMap<Integer, String>();
		for (int i = 0; i < thumbList.length; i++) {
			String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_")+1, thumbList[i].getName().lastIndexOf("."));
			allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
		}

		mthumb = new Uri[allFiles.size()];
		for (int i = 0; i < allFiles.size(); i++) {
			mthumb[i] = Uri.parse(allFiles.get(i));
		}
	}

	public int getCount() {
		return core.countPages();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		Log.d(TAG,"getView "+position);
		final MuPDFPageView pageView;
		if (convertView == null) {
			pageView = new MuPDFPageView(context, core, new Point(parent.getWidth(), parent.getHeight()), mthumb[position]);
		} else {
			pageView = (MuPDFPageView)convertView;
		}

		PointF pageSize = mPageSizes.get(position);
		if (pageSize != null) {
			// We already know the page size. Set it up
			// immediately
			pageView.setPage(position, pageSize);
		} else {
			// Page size as yet unknown. Blank it for now, and
			// start a background task to find the size
			pageView.blank(position);
			SafeAsyncTask<Void,Void,PointF> sizingTask = new SafeAsyncTask<Void,Void,PointF>() {
				@Override
				protected PointF doInBackground(Void... arg0) {
					return core.getPageSize(position);
				}

				@Override
				protected void onPostExecute(PointF result) {
					if (isCancelled()) {
						return;
					}
					// We now know the page size
					mPageSizes.put(position, result);
					// Check that this view hasn't been reused for
					// another page since we started
					if (pageView.getPage() == position)
						pageView.setPage(position, result);
				}
			};

			sizingTask.safeExecute();
		}
		
		return pageView;
	}
}
