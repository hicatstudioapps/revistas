package com.artifex.mupdf;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import com.librelio.activity.SlideShowActivity;
import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.activity.MuPDFActivity;
import com.muevaelvolante.qiumagazine.adapter.DBAdapter;
import com.muevaelvolante.qiumagazine.utils.Constant;
import com.muevaelvolante.qiumagazine.utils.ManagerGoogleAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class MuPDFPageView extends PageView {
	private static final String TAG = "MuPDFPageView";
	public static final String PATH_KEY = "path";
	public static final String LINK_URI_KEY = "link_uri";
		
	private final MuPDFCore muPdfCore;
	private HashMap<String, FrameLayout> mediaHolders = new HashMap<String, FrameLayout>();
	
	private ArrayList<String> runningLinks;
	
	private Button []btnLink;
	private Button []btnVideo;
	private int numLinksInPage;
	private int numVideosInPage;
	private Button []btnAudio;
	private int numAudiosInPage;
	private Button []btnAudioPremium;
	private int numAudiosPremiumInPage;
	private WebView []webView;
	private int numHtml5InPage
            ;
	private Button []btnGallery;
	private int numGallerysInPage;
	
	private Boolean autoAudioPlaying=false;
    private Boolean autoVideoPlaying=false;
	
	private String pdfName;
	
	private DBAdapter dbAdapter = null;
	
	private Bitmap []bitmaps;
	
	private VideoView videoHolder;

	private MediaPlayer media;
	
	private Boolean mediaLoaded;

    private boolean mFirstTime;

	public MuPDFPageView(Context c, MuPDFCore muPdfCore, Point parentSize, Uri path) {
		super(c, parentSize, muPdfCore.getFileName(), path);
		this.muPdfCore = muPdfCore;
		runningLinks = new ArrayList<String>();
		
		String []split = muPdfCore.getFileName().split("/");
		pdfName= URLDecoder.decode(split[split.length-3]);
		
		System.out.println("Pdf name es: "+pdfName);
		mediaLoaded=false;
        this.mFirstTime = true;
	}

	public int hitLinkPage(float x, float y) {
		// Since link highlighting was implemented, the super class
		// PageView has had sufficient information to be able to
		// perform this method directly. Making that change would
		// make MuPDFCore.hitLinkPage superfluous.
		float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
		float docRelX = (x - getLeft()) / scale;
		float docRelY = (y - getTop()) / scale;

		return muPdfCore.hitLinkPage(mPageNumber, docRelX, docRelY);
	}

	public String hitLinkUri(float x, float y) {
		float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
		float docRelX = (x - getLeft()) / scale;
		float docRelY = (y - getTop()) / scale;

		final String uriString = muPdfCore
				.hitLinkUri(mPageNumber, docRelX, docRelY);

		if (uriString == null)
			return null;
		
		LinkInfo[] links = muPdfCore.getPageLinks(getPage());
		if (links == null)
			return null;
		LinkInfo linkInfo = null;
		for (int i = 0; i < links.length; i++) {
			if (links[i].uri != null && links[i].uri.equals(uriString)) {
				linkInfo = links[i];
				break;
			}
		}
		
		if(runningLinks.contains(linkInfo.uri)){
			Log.d(TAG,"Already running link: "+linkInfo.uri);
			return linkInfo.uri;
		} else if(!linkInfo.isFullScreen()){
			runningLinks.add(linkInfo.uri);
		}
		
		if (linkInfo.isMediaURI()) {
			try {
				final String basePath = muPdfCore.getFileDirectory();
				MediaHolder h = new MediaHolder(getContext(), linkInfo, basePath);
				h.setVisibility(View.VISIBLE);
				this.mediaHolders.put(uriString, h);
				addView(h);
			} catch (IllegalStateException e) {
				Log.e(TAG, "hitLinkUri failed", e);
				return null;
			}
		}
		return uriString;
	}

	@Override
	public void setPage(int page, PointF size) {
		super.setPage(page, size);	
		
		if(dbAdapter==null)
			dbAdapter = new DBAdapter(getContext());
	}
	
public void loadMedia(){

    mediaLoaded=true;
	
	System.out.println("el tamaño es: "+mSize.x+"-"+mSize.y +" y la pagina es: "+mPageNumber);
	
		if(dbAdapter==null)
			dbAdapter = new DBAdapter(getContext());
		dbAdapter.open();
		Cursor c;
		if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
			c = dbAdapter.getData(pdfName, ""+(mPageNumber*2), ""+((mPageNumber*2)+1));
		}
		else{
			c = dbAdapter.getData(pdfName, ""+(mPageNumber+1));
		}
		if(c.getCount() > 0){
			int index=0;
			btnLink = new Button[20];//initialize links array
			int indexVideo=0;
			btnVideo =  new Button[10];//initialize video array
			int indexAudio=0;
			btnAudio = new Button[10];//initialize audio array
			int indexAudioPremium=0;
			btnAudioPremium = new Button[10];//initialize audio array
			int indexHtml5 = 0;
			webView = new WebView[10];
			int indexGallery = 0;
			btnGallery = new Button[10];//initialize gallery buttons array
			while(c.moveToNext()){
				
				if(c.getString(2).equalsIgnoreCase("link")){
					System.out.println(c.getString(0)+"-"+c.getString(1)+"-"+c.getString(2)+"-"+c.getString(3)+"-"+c.getString(4)+"-"+c.getString(5)+"-"+c.getString(6)+"-"+c.getString(7)+"-"+c.getString(8)+"-"+c.getString(9)+"-"+c.getString(10)+"-"+c.getString(11)+"-"+c.getString(12)+"-");
					btnLink[index] = new Button(getContext());//initialize elements
					//btnLink[index].setText(c.getString(3));
					
					
					if(c.getString(11).equalsIgnoreCase("2")){
						btnLink[index].setBackgroundColor(Color.TRANSPARENT);
					}
					else if(c.getString(11).equalsIgnoreCase("3")){
						//Set black color for highlight
						btnLink[index].setBackgroundColor(Color.BLACK);
						
						//set the animation
						final Animation animation = new AlphaAnimation(0, Float.parseFloat("0.5")); // Change alpha from fully visible to invisible
					    animation.setDuration(1000); // duration - a second
					    animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
					    animation.setRepeatCount(Constant.blinks); // Repeat animation
					    animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
					    animation.setFillEnabled(true);
					    animation.setFillAfter(true);
					    btnLink[index].startAnimation(animation);
					}
					else {
						//center button
						
						if(c.getString(12).equalsIgnoreCase("3"))
							btnLink[index].setBackgroundResource(R.drawable.link_button);
						else if(c.getString(12).equalsIgnoreCase("4"))
							btnLink[index].setBackgroundResource(R.drawable.goto_page_button);
						else if(c.getString(12).equalsIgnoreCase("5"))
							btnLink[index].setBackgroundResource(R.drawable.link_webstore_button);
						
					}
					
					addView(btnLink[index]);
					System.out.println("Controls vale: "+c.getString(11));
					int centerX=0;
					int centerY=0;
					if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
						centerX=(int)(((c.getFloat(7)*mSize.x/2 - c.getFloat(5)*mSize.x/2)/2)+(c.getFloat(5)*mSize.x/2)-(mSize.x/2/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/2/20));
						if(Integer.parseInt(c.getString(4))==mPageNumber*2){
							if(Integer.parseInt(c.getString(11))<4){
								btnLink[index].layout((int)(Float.parseFloat(c.getString(5))*mSize.x/2), (int)(Float.parseFloat(c.getString(6))*mSize.y), (int)(Float.parseFloat(c.getString(7))*mSize.x/2), (int)(Float.parseFloat(c.getString(8))*mSize.y));
							}
							else{
								if(Integer.parseInt(c.getString(11))<=4)//center
									btnLink[index].layout(centerX, centerY, centerX+(mSize.x/20), centerY+(mSize.x/20));
								else if(Integer.parseInt(c.getString(11))==5)//bottom-left
									btnLink[index].layout((int)(c.getFloat(5)*mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
								else if(Integer.parseInt(c.getString(11))==6)//bottom-center
									btnLink[index].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20), (int)(c.getFloat(8)*mSize.y));
								else if(Integer.parseInt(c.getString(11))==7)//bottom-right
									btnLink[index].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2), (int)(c.getFloat(8)*mSize.y));
							}
							
						}else{
							//btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/2/10)+mSize.x/2, centerY+(mSize.x/2/10));
							if(Integer.parseInt(c.getString(11))<4){
								btnLink[index].layout((int)(Float.parseFloat(c.getString(5))*mSize.x/2+mSize.x/2), (int)(Float.parseFloat(c.getString(6))*mSize.y), (int)(Float.parseFloat(c.getString(7))*mSize.x/2+mSize.x/2), (int)(Float.parseFloat(c.getString(8))*mSize.y));
							}
							else{
								if(Integer.parseInt(c.getString(11))<=4)//center
									btnLink[index].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/20)+mSize.x/2, centerY+(mSize.x/20));
								else if(Integer.parseInt(c.getString(11))==5)//bottom-left
									btnLink[index].layout((int)(c.getFloat(5)*mSize.x/2+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2+mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
								else if(Integer.parseInt(c.getString(11))==6)//bottom-center
									btnLink[index].layout(centerX+mSize.x/2, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20+mSize.x/2), (int)(c.getFloat(8)*mSize.y));
								else if(Integer.parseInt(c.getString(11))==7)//bottom-right
									btnLink[index].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2+mSize.x/2), (int)(c.getFloat(8)*mSize.y));
							}
						}
							
					}
					else{
						centerX=(int)(((c.getFloat(7)*mSize.x - c.getFloat(5)*mSize.x)/2)+(c.getFloat(5)*mSize.x)-(mSize.x/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/20));
						if(Integer.parseInt(c.getString(11))<4){
							btnLink[index].layout((int)(Float.parseFloat(c.getString(5))*mSize.x), (int)(Float.parseFloat(c.getString(6))*mSize.y), (int)(Float.parseFloat(c.getString(7))*mSize.x), (int)(Float.parseFloat(c.getString(8))*mSize.y));
						}
						else{
							if(Integer.parseInt(c.getString(11))==4)//center
								btnLink[index].layout(centerX, centerY, centerX+(mSize.x/10), centerY+(mSize.x/10));
							else if(Integer.parseInt(c.getString(11))==5)//bottom-left
								btnLink[index].layout((int)c.getFloat(5)*mSize.x, (int)(c.getFloat(8)*mSize.y-mSize.x/10), (int)(c.getFloat(5)*mSize.x+(mSize.x/10)), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==6)//bottom-center
								btnLink[index].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/10), centerX+(mSize.x/10), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==7)//bottom-right
								btnLink[index].layout((int)(c.getFloat(7)*mSize.x-mSize.x/10), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)(c.getFloat(7)*mSize.x), (int)(c.getFloat(8)*mSize.y));
						}
						//btnLink[index].layout(l, t, r, b);
					}
					
					btnLink[index].setTag(c.getString(4));//save page number in tag
					System.out.println("a�ado el "+index);
					
					final String url = c.getString(3);
					final int type = Integer.parseInt(c.getString(12));
					
					btnLink[index].setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							MuPDFActivity pdf = (MuPDFActivity)getContext();
							if(type==3){
								Intent intent = new Intent(Intent.ACTION_VIEW);
								/*String aux=url;
								if(url.startsWith("http://"))
									aux=url.replace("http://", "");
								if(url.startsWith("https://"))
									aux=url.replace("https://", "");
								String composeURL = Constant.API_URL+"&key="+Constant.API_KEY+"&module=magazine&action=stats&stats=true&device="+Build.MODEL+"&software=Android "+Build.VERSION.RELEASE+"&page="+(String)v.getTag()+"&stat_type=3&id_issue="+pdf.getmEdition()+"&media="+aux;
								*/
								intent.setData(Uri.parse(url));
								getContext().startActivity(intent);

								ManagerGoogleAnalytics.getInstance().sendEvent((MuPDFActivity)getContext(),getResources().getString(R.string.readingTrackedName)+": "+pdf.getIntent().getStringExtra("name"), getResources().getString(R.string.linkTrackedName), url, null);
								
							}
							else if(type==4){
								System.out.println("Nos vamos a la pagina: "+url);
								if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
									pdf.getDocView().setDisplayedViewIndex(Integer.parseInt(url)/2);
								}
								else{
									pdf.getDocView().setDisplayedViewIndex(Integer.parseInt(url)-1);
								}
							}
						}
					});
					
					
					index++;
				}
				else if(c.getString(2).equalsIgnoreCase("audio")){
					System.out.println("AUDIOOOOOOOOO");
					System.out.println(c.getString(0)+"-"+c.getString(1)+"-"+c.getString(2)+"-"+c.getString(3)+"-"+c.getString(4)+"-"+c.getString(5)+"-"+c.getString(6)+"-"+c.getString(7)+"-"+c.getString(8)+"-"+c.getString(9)+"-"+c.getString(10)+"-"+c.getString(11)+"-"+c.getString(12)+"-");
					
					btnAudio[indexAudio] = new Button(getContext());
					
					if(c.getString(11).equalsIgnoreCase("2")){
						btnAudio[indexAudio].setBackgroundColor(Color.TRANSPARENT);
					}
					else if(c.getString(11).equalsIgnoreCase("3")){
						//Set black color for highlight
						btnAudio[indexAudio].setBackgroundColor(Color.BLACK);
						
						//set the animation
						final Animation animation = new AlphaAnimation(0, Float.parseFloat("0.5")); // Change alpha from fully visible to invisible
					    animation.setDuration(1000); // duration - a second
					    animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
					    animation.setRepeatCount(Constant.blinks); // Repeat animation
					    animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
					    animation.setFillEnabled(true);
					    animation.setFillAfter(true);
					    btnAudio[indexAudio].startAnimation(animation);
					}
					else{
						btnAudio[indexAudio].setBackgroundResource(R.drawable.audio_button);
					}
				
					addView(btnAudio[indexAudio]);
					
					int leftAux=0;
					int rightAux=0;
					int centerX=0;
					int centerY=0;
					if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
						centerX=(int)(((c.getFloat(7)*mSize.x/2 - c.getFloat(5)*mSize.x/2)/2)+(c.getFloat(5)*mSize.x/2)-(mSize.x/2/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/2/20));
						if(Integer.parseInt(c.getString(4))==mPageNumber*2){

							if(Integer.parseInt(c.getString(11))<=4)//center
								btnAudio[indexAudio].layout(centerX, centerY, centerX+(mSize.x/20), centerY+(mSize.x/20));
							else if(Integer.parseInt(c.getString(11))==5)//bottom-left
								btnAudio[indexAudio].layout((int)(c.getFloat(5)*mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==6)//bottom-center
								btnAudio[indexAudio].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==7)//bottom-right
								btnAudio[indexAudio].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2), (int)(c.getFloat(8)*mSize.y));
							
							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2);
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2);
						}else{
							//btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/2/10)+mSize.x/2, centerY+(mSize.x/2/10));
							
							if(Integer.parseInt(c.getString(11))<=4)//center
								btnAudio[indexAudio].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/20)+mSize.x/2, centerY+(mSize.x/20));
							else if(Integer.parseInt(c.getString(11))==5)//bottom-left
								btnAudio[indexAudio].layout((int)(c.getFloat(5)*mSize.x/2+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2+mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==6)//bottom-center
								btnAudio[indexAudio].layout(centerX+mSize.x/2, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20+mSize.x/2), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==7)//bottom-right
								btnAudio[indexAudio].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2+mSize.x/2), (int)(c.getFloat(8)*mSize.y));

							
							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2)+mSize.x/2;
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2)+mSize.x/2;
						}
							
					}
					else{
						centerX=(int)(((c.getFloat(7)*mSize.x - c.getFloat(5)*mSize.x)/2)+(c.getFloat(5)*mSize.x)-(mSize.x/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/20));
						
						if(Integer.parseInt(c.getString(11))<=4)//center
							btnAudio[indexAudio].layout(centerX, centerY, centerX+(mSize.x/10), centerY+(mSize.x/10));
						else if(Integer.parseInt(c.getString(11))==5)//bottom-left
							btnAudio[indexAudio].layout((int)(c.getFloat(5)*mSize.x), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)((c.getFloat(5)*mSize.x)+(mSize.x/10)), (int)(c.getFloat(8)*mSize.y));
						else if(Integer.parseInt(c.getString(11))==6)//bottom-center
							btnAudio[indexAudio].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/10), centerX+(mSize.x/10), (int)(c.getFloat(8)*mSize.y));
						else if(Integer.parseInt(c.getString(11))==7)//bottom-right
							btnAudio[indexAudio].layout((int)(c.getFloat(7)*mSize.x-mSize.x/10), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)(c.getFloat(7)*mSize.x), (int)(c.getFloat(8)*mSize.y));
						
						leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x);
						rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x);
					}
					
					btnAudio[indexAudio].setTag(c.getString(3));
					final String folder = c.getString(0);
					final int indexAux=indexAudio;
					
					if(c.getString(3).startsWith("http://"))
						btnAudio[indexAudio].setBackgroundResource(R.drawable.audio_premium_button_open);
					
					btnAudio[indexAudio].setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							
							//if(media==null){
							String filename = (String) v.getTag();
							if(filename.startsWith("http://")){
								String[] splited = filename.split("/");
								filename = splited[splited.length-1];
							}
								File audioFile = new File(Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
										+ "/Extra/"+filename);
								System.out.println("Direccion de audio: "+audioFile.toString());
								/*Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setDataAndType(Uri.fromFile(audioFile),"audio/*");
								getContext().startActivity(intent);*/
								
								/*media = MediaPlayer.create(getContext(), Uri.fromFile(audioFile));
								media.start();
								
								btnAudio[indexAux].setBackgroundResource(R.drawable.audio_pause_button);
								
								media.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
									
									@Override
									public void onCompletion(MediaPlayer mp) {
										// TODO Auto-generated method stub
										media.stop();
										media=null;
										
									}
								});*/

								MuPDFActivity pdf = (MuPDFActivity)getContext();
							ManagerGoogleAnalytics.getInstance().sendEvent((MuPDFActivity)getContext(),getResources().getString(R.string.readingTrackedName)+": "+pdf.getIntent().getStringExtra("name"), getResources().getString(R.string.audioTrackedName), (String)v.getTag(), null);
								
								pdf.showAudioControls(audioFile);
								//pdf.audioBar.setMax(media.getDuration()/1000);
							/*}
							else if(media.isPlaying()){
									media.pause();
									btnAudio[indexAux].setBackgroundResource(R.drawable.audio_button);
							}
								else{
									media.start();
									btnAudio[indexAux].setBackgroundResource(R.drawable.audio_pause_button);
							}*/
							
						}
					});
					
					if(Integer.parseInt(c.getString(9))==1){
						btnAudio[indexAudio].setBackgroundResource(0);
						btnAudio[indexAudio].performClick();
						autoAudioPlaying=true;
					}
					
					indexAudio++;
				}
				else if(c.getString(2).equalsIgnoreCase("audios_premium")){
					System.out.println("AUDIOOOOOOOOO PREMIUUUUUUUM");
					System.out.println(c.getString(0)+"-"+c.getString(1)+"-"+c.getString(2)+"-"+c.getString(3)+"-"+c.getString(4)+"-"+c.getString(5)+"-"+c.getString(6)+"-"+c.getString(7)+"-"+c.getString(8)+"-"+c.getString(9)+"-"+c.getString(10)+"-"+c.getString(11)+"-"+c.getString(12)+"-");
					
					btnAudioPremium[indexAudioPremium] = new Button(getContext());
					btnAudioPremium[indexAudioPremium].setBackgroundResource(R.drawable.audio_premium_button_closed);
					addView(btnAudioPremium[indexAudioPremium]);
					
					int leftAux=0;
					int rightAux=0;
					int centerX=0;
					int centerY=0;
					if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
						centerX=(int)(((c.getFloat(7)*mSize.x/2 - c.getFloat(5)*mSize.x/2)/2)+(c.getFloat(5)*mSize.x/2)-(mSize.x/2/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/2/20));
						if(Integer.parseInt(c.getString(4))==mPageNumber*2){
							btnAudioPremium[indexAudioPremium].layout(centerX, centerY, centerX+(mSize.x/2/10), centerY+(mSize.x/2/10));
							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2);
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2);
						}else{
							btnAudioPremium[indexAudioPremium].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/2/10)+mSize.x/2, centerY+(mSize.x/2/10));
							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2)+mSize.x/2;
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2)+mSize.x/2;
						}
							
					}
					else{
						centerX=(int)(((c.getFloat(7)*mSize.x - c.getFloat(5)*mSize.x)/2)+(c.getFloat(5)*mSize.x)-(mSize.x/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/20));
						btnAudioPremium[indexAudioPremium].layout(centerX, centerY, centerX+(mSize.x/10), centerY+(mSize.x/10));
						leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x);
						rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x);
					}
					
					btnAudioPremium[indexAudioPremium].setTag(c.getString(3));
					final String folder = c.getString(0);
					final int indexAux=indexAudioPremium;
					
					btnAudioPremium[indexAudioPremium].setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							
							//if(media==null){
								/*File audioFile = new File(Environment.getExternalStorageDirectory()
										+ "/" + Constant.PATH + "/" + URLEncoder.encode(folder)
										+ "/Extra/"+(String) v.getTag());
								System.out.println("Direccion de audio: "+audioFile.toString());*/
								/*Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setDataAndType(Uri.fromFile(audioFile),"audio/*");
								getContext().startActivity(intent);*/
								
								/*media = MediaPlayer.create(getContext(), Uri.fromFile(audioFile));
								media.start();
								
								btnAudio[indexAux].setBackgroundResource(R.drawable.audio_pause_button);
								
								media.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
									
									@Override
									public void onCompletion(MediaPlayer mp) {
										// TODO Auto-generated method stub
										media.stop();
										media=null;
										
									}
								});*/
								
								MuPDFActivity pdf = (MuPDFActivity)getContext();
								
								pdf.showBuyDialog();
								//pdf.audioBar.setMax(media.getDuration()/1000);
							/*}
							else if(media.isPlaying()){
									media.pause();
									btnAudio[indexAux].setBackgroundResource(R.drawable.audio_button);
							}
								else{
									media.start();
									btnAudio[indexAux].setBackgroundResource(R.drawable.audio_pause_button);
							}*/
							
						}
					});
					
					if(Integer.parseInt(c.getString(9))==1){
						btnAudioPremium[indexAudioPremium].setBackgroundResource(0);
						btnAudioPremium[indexAudioPremium].performClick();
						autoAudioPlaying=true;
					}
					
					indexAudioPremium++;
				}
				else if(c.getString(2).equalsIgnoreCase("video")){
					System.out.println(c.getString(0)+"-"+c.getString(1)+"-"+c.getString(2)+"-"+c.getString(3)+"-"+c.getString(4)+"-"+c.getString(5)+"-"+c.getString(6)+"-"+c.getString(7)+"-"+c.getString(8)+"-"+c.getString(9)+"-"+c.getString(10)+"-"+c.getString(11)+"-"+c.getString(12)+"-");
					
					btnVideo[indexVideo] = new Button(getContext());//initialize elements
					
					int leftAux=0;
					int rightAux=0;
					if(c.getInt(12)==2 || c.getInt(12)==1){
						
						if(c.getString(11).equalsIgnoreCase("2")){
							btnVideo[indexVideo].setBackgroundColor(Color.TRANSPARENT);
						}
						else if(c.getString(11).equalsIgnoreCase("3")){
							//Set black color for highlight
							btnVideo[indexVideo].setBackgroundColor(Color.BLACK);
							
							//set the animation
							final Animation animation = new AlphaAnimation(0, Float.parseFloat("0.5")); // Change alpha from fully visible to invisible
						    animation.setDuration(1000); // duration - a second
						    animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
						    animation.setRepeatCount(Constant.blinks); // Repeat animation
						    animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
						    animation.setFillEnabled(true);
						    animation.setFillAfter(true);
						    btnVideo[indexVideo].startAnimation(animation);
						}
						else{
							//Local video
							if(c.getInt(12)==2)
								btnVideo[indexVideo].setBackgroundResource(R.drawable.videoplay_button);
							else
								btnVideo[indexVideo].setBackgroundResource(R.drawable.videoplay_streaming_button);
						}
						
						
						
						addView(btnVideo[indexVideo]);
						int centerX=0;
						int centerY=0;
						
						if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
							centerX=(int)(((c.getFloat(7)*mSize.x/2 - c.getFloat(5)*mSize.x/2)/2)+(c.getFloat(5)*mSize.x/2)-(mSize.x/2/20));
							centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/2/20));
							if(Integer.parseInt(c.getString(4))==mPageNumber*2){

								if(Integer.parseInt(c.getString(11))<=4)//center
									btnVideo[indexVideo].layout(centerX, centerY, centerX+(mSize.x/20), centerY+(mSize.x/20));
								else if(Integer.parseInt(c.getString(11))==5)//bottom-left
									btnVideo[indexVideo].layout((int)(c.getFloat(5)*mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
								else if(Integer.parseInt(c.getString(11))==6)//bottom-center
									btnVideo[indexVideo].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20), (int)(c.getFloat(8)*mSize.y));
								else if(Integer.parseInt(c.getString(11))==7)//bottom-right
									btnVideo[indexVideo].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2), (int)(c.getFloat(8)*mSize.y));
								
								leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2);
								rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2);
							}else{
								//btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/2/10)+mSize.x/2, centerY+(mSize.x/2/10));
								
								if(Integer.parseInt(c.getString(11))<=4)//center
									btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/20)+mSize.x/2, centerY+(mSize.x/20));
								else if(Integer.parseInt(c.getString(11))==5)//bottom-left
									btnVideo[indexVideo].layout((int)(c.getFloat(5)*mSize.x/2+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2+mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
								else if(Integer.parseInt(c.getString(11))==6)//bottom-center
									btnVideo[indexVideo].layout(centerX+mSize.x/2, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20+mSize.x/2), (int)(c.getFloat(8)*mSize.y));
								else if(Integer.parseInt(c.getString(11))==7)//bottom-right
									btnVideo[indexVideo].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2+mSize.x/2), (int)(c.getFloat(8)*mSize.y));

								
								leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2)+mSize.x/2;
								rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2)+mSize.x/2;
							}
								
						}
						else{
							centerX=(int)(((c.getFloat(7)*mSize.x - c.getFloat(5)*mSize.x)/2)+(c.getFloat(5)*mSize.x)-(mSize.x/20));
							centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/20));
							
							if(Integer.parseInt(c.getString(11))<=4)//center
								btnVideo[indexVideo].layout(centerX, centerY, centerX+(mSize.x/10), centerY+(mSize.x/10));
							else if(Integer.parseInt(c.getString(11))==5)//bottom-left
								btnVideo[indexVideo].layout((int)(c.getFloat(5)*mSize.x), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)((c.getFloat(5)*mSize.x)+(mSize.x/10)), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==6)//bottom-center
								btnVideo[indexVideo].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/10), centerX+(mSize.x/10), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==7)//bottom-right
								btnVideo[indexVideo].layout((int)(c.getFloat(7)*mSize.x-mSize.x/10), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)(c.getFloat(7)*mSize.x), (int)(c.getFloat(8)*mSize.y));
							
							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x);
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x);
						//btnVideo[index].layout((int)(Float.parseFloat(c.getString(5))*mSize.x), (int)(Float.parseFloat(c.getString(6))*mSize.y), (int)(Float.parseFloat(c.getString(7))*mSize.x), (int)(Float.parseFloat(c.getString(8))*mSize.y));
						}
					}
					btnVideo[indexVideo].setTag(c.getString(3));
					final String folder = c.getString(0);
					final int left=leftAux;
					final int top=(int)(Float.parseFloat(c.getString(6))*mSize.y);
					final int right=rightAux;
					final int bottom=(int)(Float.parseFloat(c.getString(8))*mSize.y);
					final int fullscreen = Integer.parseInt(c.getString(10));
					final int type = c.getInt(12);
					final String owner = c.getString(13);
					final String title = c.getString(14);
					
					btnVideo[indexVideo].setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {

							if(type==2){
								System.out.println("El path es: " + Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
										+ "/Extra" + (String) v.getTag());
								File videoFile = new File(Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
										+ "/Extra/" + (String) v.getTag());
								


								
								if(videoHolder==null)
									videoHolder = new VideoView(getContext());
								videoHolder.setLayoutParams(new LayoutParams(right-left, bottom-top));

                                videoHolder.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        videoHolder.bringToFront();
                                    }
                                });
	
								// It works fine with .wmv, .3gp and .mp4 formats 
								//Here we have to specify the SD Card path 
                                videoHolder.setVideoURI(Uri.fromFile(videoFile));
								videoHolder.requestFocus();
                                videoHolder.start();

								
								videoHolder.setMediaController(new MediaController(videoHolder.getContext()));

								
								
								MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
								metaRetriever.setDataSource(Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
										+ "/Extra/" + (String) v.getTag());
								int height = Integer.parseInt(metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
								int width = Integer.parseInt(metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
								
								Log.d("RES","alto: "+height+" ancho: "+width);
								
								float ratioX,ratioY;
								int videoFrameX,videoFrameY,videoFrameStartX=0,videoFrameStartY=0;
								
								if(fullscreen==1){
									//Estamos en fullScreen

									videoFrameStartX = 0;
									videoFrameX=mSize.x;
									videoFrameY=videoFrameX*height/width;
									videoFrameStartY=(int)((mSize.y-videoFrameY)/2);
									
									Log.i("VIDEO FRAME", "alto: " + videoFrameY + " ancho: " + videoFrameX);
									videoHolder.layout(videoFrameStartX, videoFrameStartY, videoFrameStartX + videoFrameX, videoFrameStartY + videoFrameY);
									if(videoHolder.getParent() != null && videoHolder.getParent() instanceof ViewGroup)
										((ViewGroup)videoHolder.getParent()).removeView(videoHolder);

									addView(videoHolder);
								}
								else{
									videoHolder.layout(left, top, right, bottom);
									addView(videoHolder);

								}
								//}
									
								MuPDFActivity pdf = (MuPDFActivity)getContext();
								ManagerGoogleAnalytics.getInstance().sendEvent((MuPDFActivity) getContext(), getResources().getString(R.string.readingTrackedName) + ": " + pdf.getIntent().getStringExtra("name"), getResources().getString(R.string.videoOfflineTrackedName), (String) v.getTag(), null);

								videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
									
									@Override
									public void onCompletion(MediaPlayer mp) {

										videoHolder.layout(0, 0, 0, 0);
										videoHolder.setVideoURI(null);
										
										removeView(videoHolder);
										videoHolder=null;
									}
								});
							}
							else if(type==1){
								Intent intent = new Intent(Intent.ACTION_VIEW);
								String composeURL="";
								if(owner.equals("yt"))
									composeURL="http://www.youtube.com/watch?v="+(String)v.getTag();
								else if(owner.equals("vimeo"))
									composeURL="http://player.vimeo.com/video/"+(String)v.getTag();
								intent.setData(Uri.parse(composeURL));
								getContext().startActivity(intent);
								MuPDFActivity pdf = (MuPDFActivity)getContext();
								ManagerGoogleAnalytics.getInstance().sendEvent((MuPDFActivity)getContext(),getResources().getString(R.string.readingTrackedName)+": "+pdf.getIntent().getStringExtra("name"), getResources().getString(R.string.videoOnlineTrackedName), title, null);
							}
						}
					});

                    if(Integer.parseInt(c.getString(9))==1){
                        btnVideo[indexVideo].setBackgroundResource(0);
                        final int indexVideoAuto = indexVideo;
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                                if (btnVideo != null) {
                                    btnVideo[indexVideoAuto].performClick();
                                }

                            }

                        }, 400);
                        autoVideoPlaying=true;
                    }
					
					indexVideo++;
				}
				else if(c.getString(2).equalsIgnoreCase("html5")){
					//System.out.println("Aqui hay html, "+c.getString(3));
					webView[indexHtml5] = new WebView(getContext());
					webView[indexHtml5].setBackgroundColor(Color.TRANSPARENT);
					webView[indexHtml5].getSettings().setJavaScriptEnabled(true);
					
					webView[indexHtml5].setWebViewClient(new WebViewClient(){
						public void onReceivedError(WebView view, int errorCode,
				                String description, String failingUrl) {
				            // Handle the error
				        }

				        public boolean shouldOverrideUrlLoading(WebView view, String url) {
				            view.loadUrl(url);
				            return true;
				        }
					});
					
					webView[indexHtml5].loadUrl(c.getString(3));
					//web.loadDataWithBaseURL(baseUrl, data, "text/html", encoding, historyUrl)
					int leftAux=0;
					int rightAux=0;
					
					if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
						if(Integer.parseInt(c.getString(4))==mPageNumber*2){
							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2);
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2);
						}else{
							//btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/2/10)+mSize.x/2, centerY+(mSize.x/2/10));
							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2)+mSize.x/2;
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2)+mSize.x/2;
						}
							
					}
					else{
						leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x);
						rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x);
					//btnVideo[index].layout((int)(Float.parseFloat(c.getString(5))*mSize.x), (int)(Float.parseFloat(c.getString(6))*mSize.y), (int)(Float.parseFloat(c.getString(7))*mSize.x), (int)(Float.parseFloat(c.getString(8))*mSize.y));
					}
					
					final int left=leftAux;
					final int top=(int)(Float.parseFloat(c.getString(6))*mSize.y);
					final int right=rightAux;
					final int bottom=(int)(Float.parseFloat(c.getString(8))*mSize.y);
					
					webView[indexHtml5].layout(left, top, right, bottom);
					addView(webView[indexHtml5]);
					
					indexHtml5++;
				}
				else if(c.getString(2).equalsIgnoreCase("gallery")){
					
					System.out.println("Hay gallery aqui");
					
					btnGallery[indexGallery] = new Button(getContext());
					
					if(c.getString(11).equalsIgnoreCase("2")){
						btnGallery[indexGallery].setBackgroundColor(Color.TRANSPARENT);
					}
					else if(c.getString(11).equalsIgnoreCase("3")){
						//Set black color for highlight
						btnGallery[indexGallery].setBackgroundColor(Color.BLACK);
						
						//set the animation
						final Animation animation = new AlphaAnimation(0, Float.parseFloat("0.5")); // Change alpha from fully visible to invisible
					    animation.setDuration(1000); // duration - a second
					    animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
					    animation.setRepeatCount(Constant.blinks); // Repeat animation
					    animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
					    animation.setFillEnabled(true);
					    animation.setFillAfter(true);
					    btnGallery[indexGallery].startAnimation(animation);
					}
					else{
						btnGallery[indexGallery].setBackgroundResource(R.drawable.gallery_button);
					}
					
					addView(btnGallery[indexGallery]);
					
					int leftAux=0;
					int rightAux=0;
					int centerX=0;
					int centerY=0;
					if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
						centerX=(int)(((c.getFloat(7)*mSize.x/2 - c.getFloat(5)*mSize.x/2)/2)+(c.getFloat(5)*mSize.x/2)-(mSize.x/2/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/2/20));
						if(Integer.parseInt(c.getString(4))==mPageNumber*2){

							if(Integer.parseInt(c.getString(11))<=4)//center
								btnGallery[indexGallery].layout(centerX, centerY, centerX+(mSize.x/20), centerY+(mSize.x/20));
							else if(Integer.parseInt(c.getString(11))==5)//bottom-left
								btnGallery[indexGallery].layout((int)(c.getFloat(5)*mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==6)//bottom-center
								btnGallery[indexGallery].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==7)//bottom-right
								btnGallery[indexGallery].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2), (int)(c.getFloat(8)*mSize.y));
							
							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2);
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2);
						}else{
							//btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/2/10)+mSize.x/2, centerY+(mSize.x/2/10));
							
							if(Integer.parseInt(c.getString(11))<=4)//center
								btnGallery[indexGallery].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/20)+mSize.x/2, centerY+(mSize.x/20));
							else if(Integer.parseInt(c.getString(11))==5)//bottom-left
								btnGallery[indexGallery].layout((int)(c.getFloat(5)*mSize.x/2+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2+mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==6)//bottom-center
								btnGallery[indexGallery].layout(centerX+mSize.x/2, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20+mSize.x/2), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==7)//bottom-right
								btnGallery[indexGallery].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2+mSize.x/2), (int)(c.getFloat(8)*mSize.y));

							
							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2)+mSize.x/2;
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2)+mSize.x/2;
						}
							
					}
					else{
						centerX=(int)(((c.getFloat(7)*mSize.x - c.getFloat(5)*mSize.x)/2)+(c.getFloat(5)*mSize.x)-(mSize.x/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/20));
						
						if(Integer.parseInt(c.getString(11))<=4)//center
							btnGallery[indexGallery].layout(centerX, centerY, centerX+(mSize.x/10), centerY+(mSize.x/10));
						else if(Integer.parseInt(c.getString(11))==5)//bottom-left
							btnGallery[indexGallery].layout((int)(c.getFloat(5)*mSize.x), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)((c.getFloat(5)*mSize.x)+(mSize.x/10)), (int)(c.getFloat(8)*mSize.y));
						else if(Integer.parseInt(c.getString(11))==6)//bottom-center
							btnGallery[indexGallery].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/10), centerX+(mSize.x/10), (int)(c.getFloat(8)*mSize.y));
						else if(Integer.parseInt(c.getString(11))==7)//bottom-right
							btnGallery[indexGallery].layout((int)(c.getFloat(7)*mSize.x-mSize.x/10), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)(c.getFloat(7)*mSize.x), (int)(c.getFloat(8)*mSize.y));
						
						leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x);
						rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x);
					}
					
					final String folder = c.getString(0);
					final String data = c.getString(3);
					
					btnGallery[indexGallery].setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							
							try {
								JSONArray jsonArry = new JSONArray(data);
								
								//New activity only if parse result OK
								if(jsonArry != null || jsonArry.length() > 0){
									
									String []photos = new String[jsonArry.length()];
									
									for(int i=0; i<jsonArry.length(); i++){
										JSONObject jsonObject = jsonArry.getJSONObject(i);
										String photoURL = jsonObject.optString("url");
										photos[i] = Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
												+ "/images/"+photoURL.substring(photoURL.lastIndexOf("/")+1);
									}
									
									Intent intent = new Intent(getContext(),SlideShowActivity.class);
									intent.putExtra(MediaHolder.FULL_PATH_KEY, Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
											+ "/images");
									intent.putExtra("photos", photos);
									getContext().startActivity(intent);
									
									MuPDFActivity pdf = (MuPDFActivity)getContext();
									ManagerGoogleAnalytics.getInstance().sendEvent((MuPDFActivity)getContext(),getResources().getString(R.string.readingTrackedName)+": "+pdf.getIntent().getStringExtra("name"), getResources().getString(R.string.galleryTrackedName), ""+mPageNumber, null);
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}					
						}
					});
					
					indexGallery++;
				}
				
				numLinksInPage=index;
				numVideosInPage=indexVideo;
				numAudiosInPage=indexAudio;
				numAudiosPremiumInPage=indexAudioPremium;
				numHtml5InPage=indexHtml5;
				numGallerysInPage=indexGallery;
				System.out.println("En esta pagina hay: "+numLinksInPage);
			}
		}
		c.close();
		dbAdapter.close();
		
		//mediaLoaded=true;
	}

	public int currentAudioTime(){
		return media.getCurrentPosition()/1000;
	}

	public void deleteMedia(){
		System.out.println("Elimino media de la pagina: " + mPageNumber);
		if(numLinksInPage!=0){
			for(int s=0;s<numLinksInPage;s++){
				Log.i("deleteMEdia","elimino button["+s+"]");
				btnLink[s].layout(0, 0, 0, 0);
				removeView(btnLink[s]);
				btnLink[s]=null;
			}
			btnLink=null;
			numLinksInPage=0;
		}
		
		if(numAudiosInPage!=0){
			for(int s=0;s<numAudiosInPage;s++){
				btnAudio[s].layout(0, 0, 0, 0);
				removeView(btnAudio[s]);
				btnAudio[s]=null;
			}
			
			/*if(media!=null){
				if(!media.isPlaying()){
					media.stop();
					media=null;
				}
			}
			
			if(autoAudioPlaying && media!=null){
				media.stop();
				media=null;
			}*/

			autoAudioPlaying=false;
			btnAudio=null;
			numAudiosInPage=0;
		}
		
		if(numAudiosPremiumInPage!=0){
			for(int s=0;s<numAudiosPremiumInPage;s++){
				btnAudioPremium[s].layout(0, 0, 0, 0);
				removeView(btnAudioPremium[s]);
				btnAudioPremium[s]=null;
			}
			
			/*if(media!=null){
				if(!media.isPlaying()){
					media.stop();
					media=null;
				}
			}
			
			if(autoAudioPlaying && media!=null){
				media.stop();
				media=null;
			}*/

			autoAudioPlaying=false;
			btnAudioPremium=null;
			numAudiosPremiumInPage=0;
		}
		
		
		if(numVideosInPage!=0){
			for(int s=0;s<numVideosInPage;s++){
				Log.i("deleteMEdia","elimino button["+s+"]");
				btnVideo[s].layout(0, 0, 0, 0);
				removeView(btnVideo[s]);
				btnVideo[s]=null;
			}
            autoVideoPlaying = false;
			btnVideo=null;
			numVideosInPage=0;
		}
		
		if(videoHolder!=null){
			videoHolder.stopPlayback();
			videoHolder.layout(0, 0, 0, 0);
			videoHolder.setMediaController(null);
			videoHolder.setVideoURI(null);
			videoHolder=null;
		}
		
		mediaLoaded=false;
		
		if(numHtml5InPage!=0){
			for(int s=0;s<numHtml5InPage;s++){
				webView[s].layout(0, 0, 0, 0);
				removeView(webView[s]);
				webView[s]=null;
			}
			webView=null;
			numHtml5InPage=0;
		}
		
		if(numGallerysInPage!=0){
			for(int s=0;s<numGallerysInPage;s++){
				btnGallery[s].layout(0, 0, 0, 0);
				removeView(btnGallery[s]);
				btnGallery[s]=null;
			}
			btnGallery=null;
			numGallerysInPage=0;
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		for (Map.Entry<String, FrameLayout> entry : mediaHolders.entrySet()) {
			MediaHolder mLinkHolder = (MediaHolder) entry.getValue();
			LinkInfo currentLink = mLinkHolder.getLinkInfo();
			float scale = mSourceScale * (float) getWidth() / (float) mSize.x;

			int width = (int) ((currentLink.right - currentLink.left) * scale);
			int height = (int) ((currentLink.bottom - currentLink.top) * scale);
			// mLinkHolder.measure(widthMeasureSpec, heightMeasureSpec);
			mLinkHolder.measure(View.MeasureSpec.EXACTLY | width,
                    View.MeasureSpec.EXACTLY | height);

		}

	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		float scale = mSourceScale * (float) getWidth() / (float) mSize.x;

		for (Map.Entry<String, FrameLayout> entry : mediaHolders.entrySet()) {
			MediaHolder mLinkHolder = (MediaHolder) entry.getValue();
			LinkInfo currentLink = mLinkHolder.getLinkInfo();
			mLinkHolder.layout((int) (currentLink.left * scale),
					(int) (currentLink.top * scale),
					(int) (currentLink.right * scale),
					(int) (currentLink.bottom * scale));
		}
	}

	@Override
	public void blank(int page) {
		super.blank(page);
		Iterator<Entry<String, FrameLayout>> i = mediaHolders.entrySet()
				.iterator();
		while (i.hasNext()) {
			Entry<String, FrameLayout> entry = i.next();
			MediaHolder mLinkHolder = (MediaHolder) entry.getValue();
			mLinkHolder.recycle();
			i.remove();
			removeView(mLinkHolder);
			mLinkHolder = null;
		}
		
		System.out.println("Se libera la pagina: "+page);
		
		if(numLinksInPage!=0){
			
			for(int s=0;s<numLinksInPage;s++){
				btnLink[s].layout(0, 0, 0, 0);
				removeView(btnLink[s]);
				btnLink[s]=null;
			}
			btnLink=null;
			numLinksInPage=0;
		}
		
		if(numAudiosInPage!=0){
			for(int s=0;s<numAudiosInPage;s++){
				btnAudio[s].layout(0, 0, 0, 0);
				removeView(btnAudio[s]);
				btnAudio[s]=null;
			}
			if(media!=null){
				if(!media.isPlaying()){
					media.stop();
					media=null;
				}
			}
			
			btnAudio=null;
			numAudiosInPage=0;
		}
		
		if(numVideosInPage!=0){
			for(int s=0;s<numVideosInPage;s++){
				Log.i("deleteMEdia","elimino button["+s+"]");
				btnVideo[s].layout(0, 0, 0, 0);
				removeView(btnVideo[s]);
				btnVideo[s]=null;
			}
			btnVideo=null;
			numVideosInPage=0;
		}
		
		if(videoHolder!=null){
			videoHolder.stopPlayback();
			videoHolder.layout(0, 0, 0, 0);
			videoHolder.setMediaController(null);
			videoHolder.setVideoURI(null);
			videoHolder=null;
		}
		
		mediaLoaded=false;
		
		if(numHtml5InPage!=0){
			for(int s=0;s<numHtml5InPage;s++){
				webView[s].layout(0, 0, 0, 0);
				removeView(webView[s]);
				webView[s]=null;
			}
			webView=null;
			numHtml5InPage=0;
		}
		
		if(numGallerysInPage!=0){
			for(int s=0;s<numGallerysInPage;s++){
				btnGallery[s].layout(0, 0, 0, 0);
				removeView(btnGallery[s]);
				btnGallery[s]=null;
			}
			btnGallery=null;
			numGallerysInPage=0;
		}
	}

	@Override
	public void removeHq() {
		super.removeHq();
		Iterator<Entry<String, FrameLayout>> i = mediaHolders.entrySet()
				.iterator();
		while (i.hasNext()) {
			Entry<String, FrameLayout> entry = i.next();
			MediaHolder mLinkHolder = (MediaHolder) entry.getValue();
			i.remove();
			removeView(mLinkHolder);
			mLinkHolder = null;
		}
		
	}

	@Override
	public void addHq() {
		super.addHq();
		
		Rect viewArea = new Rect(getLeft(),getTop(),getRight(),getBottom());
		if(!(viewArea.width()-5<mSize.x && mSize.x<viewArea.width()+5) || !(viewArea.height()-5<mSize.y && mSize.y<viewArea.height()+5)){
			//System.out.println("Tama�o de pantalla: "+mSize.x+"-"+mSize.y+" y el tama�o del area: "+viewArea.width()+"-"+viewArea.height());
			deleteMedia();
		}
        else if(mediaLoaded!=true){
			deleteMedia();
			loadMedia();
		}

        if (this.mFirstTime && !mediaLoaded) {
            this.mFirstTime = false;
            loadMedia();
        }
			
		for (Map.Entry<String, FrameLayout> entry : mediaHolders.entrySet()) {
			MediaHolder mLinkHolder = (MediaHolder) entry.getValue();

			mLinkHolder.bringToFront();
		}
	}

	public void addMediaHolder(MediaHolder h,String uriString){
		this.mediaHolders.put(uriString, h);
	}
	
	public void cleanRunningLinkList(){
		runningLinks.clear();
	}
	@Override
	protected void drawPage(Bitmap bm, int sizeX, int sizeY, int patchX,
			int patchY, int patchWidth, int patchHeight) {
		if (null != bm) {
			muPdfCore.drawPage(mPageNumber, bm, sizeX, sizeY, patchX, patchY,
					patchWidth, patchHeight);
		} else {
			Log.w(TAG, "IGNORED drawPage");
		}

	}

	@Override
	protected LinkInfo[] getLinkInfo() {
		return muPdfCore.getPageLinks(mPageNumber);
	}

	protected LinkInfo[] getExternalLinkInfo() {
		return muPdfCore.getPageURIs(mPageNumber);
	}	
}
