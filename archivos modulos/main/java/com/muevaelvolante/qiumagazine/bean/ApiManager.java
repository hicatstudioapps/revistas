package com.muevaelvolante.qiumagazine.bean;

import android.content.Context;
import android.content.SharedPreferences;

import com.muevaelvolante.qiumagazine.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Created by idintel on 19/01/2016.
 *
 * Esta clase obtiene los valores del archivo info.json que se encuentra en Internet.
 * Oferta configuraciones de la aplicación
 */
public class ApiManager {

    private static ApiManager mInstance;

    public String last_update;
    public int onoff;
    public String min_version;
    public boolean subscribers_db_integration;
    public ArrayList<String> languages;
    public String url_phone;
    public String url_tablet;

    public String menu; //Fichero de menu.json
    public String homestyle;
    public String url_domain;

    private ApiManager(Context c)
    {
        loadFromDisc(c);
    }

    public static ApiManager getInstance(Context c)
    {
        if(mInstance == null)
        {
            mInstance = new ApiManager(c);
        }

        return  mInstance;
    }

    public void saveFromDisc(Context c)
    {
        SharedPreferences prefs = c.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastUpdate", last_update);
        editor.putInt("onoff", onoff);
        editor.putString("min_version", min_version);
        editor.putBoolean("subscribers_db_integration", subscribers_db_integration);
        editor.putStringSet("languages", new HashSet<String>(languages));
        editor.putString("url_phone", url_phone);
        editor.putString("url_tablet", url_tablet);
        editor.putString("menu", menu);
        editor.putString("url_domain", url_domain);
        editor.putString("homestyle", homestyle);

        editor.commit();
    }


    public void loadFromDisc(Context c)
    {
        SharedPreferences prefs = c.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        last_update = prefs.getString("last_update","00:00:00");
        onoff = prefs.getInt("onoff", 1);
        min_version = prefs.getString("onomin_versionff","0");
        subscribers_db_integration = prefs.getBoolean("subscribers_db_integration", true);
        languages = new ArrayList<>();
        Set<String> setLanguages = prefs.getStringSet("languages",null);
        if(setLanguages != null)
            languages.addAll(setLanguages);

        url_phone = prefs.getString("url_phone","");
        url_tablet = prefs.getString("url_tablet","");
        menu = prefs.getString("menu","/menu.json");
        url_domain = prefs.getString("url_domain","");
        homestyle = prefs.getString("homestyle","standard");
    }


    public void parseFromJSONandStore(JSONObject jsonObject, Context c)
    {
        last_update = jsonObject.optString("last_update");
        onoff = jsonObject.optInt("onoff");
        min_version = jsonObject.optString("min_version");
        subscribers_db_integration  = jsonObject.optBoolean("subscribers_db_integration");

        String languagesSTR = jsonObject.optString("languages");
        languages  = new ArrayList<>(Arrays.asList(languagesSTR.split(",")));

        url_phone  = jsonObject.optString("url_phone");
        url_tablet  = jsonObject.optString("url_phone");

        menu = jsonObject.optString("menu");
        homestyle = jsonObject.optString("homestyle");
        url_domain = jsonObject.optString("url_domain");

        saveFromDisc(c);
    }

    public String getDefaultLanguage()
    {
        String deviceLang = Locale.getDefault().getLanguage();
        if(languages == null || !languages.contains(deviceLang))
            deviceLang = "en";

        return deviceLang;
    }

    public String getURL(Context c)
    {
        String deviceLang = getDefaultLanguage();
        String urlSuffix;

        if(c.getResources().getBoolean(R.bool.isTablet))
            urlSuffix = url_tablet;
        else
            urlSuffix = url_phone;

        return url_domain + deviceLang +urlSuffix;
    }



    public String getMenuURL(Context c)
    {
        String deviceLang = getDefaultLanguage();
        String urlSuffix;

        //Ejemplo: "http://d1mtxouorzlm31.cloudfront.net/en/menu.json"
        return url_domain + deviceLang + menu;
    }
}
