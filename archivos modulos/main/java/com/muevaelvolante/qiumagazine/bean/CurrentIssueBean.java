package com.muevaelvolante.qiumagazine.bean;

import java.util.ArrayList;

public class CurrentIssueBean {

	String idPage;
	String sortNumber;
	String imageName;
	String imageURL;
	String thmbnailURL;
	String idParent;
	String summery;
	String template;
	String morePage;
	String link;
	String text;
	ArrayList<VideoBean> videoList;
	ArrayList<AudioBean> audioList;
	ArrayList<LinkBean> linkList;
	ArrayList<AudioPremiumBean> audioPremiumList;
	ArrayList<LinkBean> html5List;
	ArrayList<GalleryBean> galleryList;
	
	
	/**
	 * @return the idPage
	 */
	public String getIdPage() {
		return idPage;
	}
	/**
	 * @param idPage the idPage to set
	 */
	public void setIdPage(String idPage) {
		this.idPage = idPage;
	}
	/**
	 * @return the sortNumber
	 */
	public String getSortNumber() {
		return sortNumber;
	}
	/**
	 * @param sortNumber the sortNumber to set
	 */
	public void setSortNumber(String sortNumber) {
		this.sortNumber = sortNumber;
	}
	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}
	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	/**
	 * @return the imageURL
	 */
	public String getImageURL() {
		return imageURL;
	}
	/**
	 * @param imageURL the imageURL to set
	 */
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	/**
	 * @return the thmbnailURL
	 */
	public String getThmbnailURL() {
		return thmbnailURL;
	}
	/**
	 * @param thmbnailURL the thmbnailURL to set
	 */
	public void setThmbnailURL(String thmbnailURL) {
		this.thmbnailURL = thmbnailURL;
	}
	/**
	 * @return the idParent
	 */
	public String getIdParent() {
		return idParent;
	}
	/**
	 * @param idParent the idParent to set
	 */
	public void setIdParent(String idParent) {
		this.idParent = idParent;
	}
	/**
	 * @return the summery
	 */
	public String getSummery() {
		return summery;
	}
	/**
	 * @param summery the summery to set
	 */
	public void setSummery(String summery) {
		this.summery = summery;
	}
	/**
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}
	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template) {
		this.template = template;
	}
	/**
	 * @return the morePage
	 */
	public String getMorePage() {
		return morePage;
	}
	/**
	 * @param morePage the morePage to set
	 */
	public void setMorePage(String morePage) {
		this.morePage = morePage;
	}
	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}
	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}
	/**
	 * @return the videoList
	 */
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public ArrayList<VideoBean> getVideoList() {
		return videoList;
	}
	/**
	 * @param videoList the videoList to set
	 */
	public void setVideoList(ArrayList<VideoBean> videoList) {
		this.videoList = videoList;
	}
	/**
	 * @return the audioList
	 */
	public ArrayList<AudioBean> getAudioList() {
		return audioList;
	}
	/**
	 * @param audioList the audioList to set
	 */
	public void setAudioList(ArrayList<AudioBean> audioList) {
		this.audioList = audioList;
	}
	/**
	 * @return the linkList
	 */
	public ArrayList<LinkBean> getLinkList() {
		return linkList;
	}
	/**
	 * @param linkList the linkList to set
	 */
	public void setLinkList(ArrayList<LinkBean> linkList) {
		this.linkList = linkList;
	}
	
	/**
	 * @return the premium audioList
	 */
	public ArrayList<AudioPremiumBean> getAudioPremiumList() {
		return audioPremiumList;
	}
	/**

	 */
	public void setAudioPremiumList(ArrayList<AudioPremiumBean> audioPremiumList) {
		this.audioPremiumList = audioPremiumList;
	}
	/**
	 * @return the html5 list
	 */
	public ArrayList<LinkBean> getHtml5List() {
		return html5List;
	}
	/**

	 */
	public void setHtml5List(ArrayList<LinkBean> linkList) {
		this.html5List = linkList;
	}
	/**
	 * @return the html5 list
	 */
	public ArrayList<GalleryBean> getGalleryList() {
		return galleryList;
	}
	/**

	 */
	public void setGalleryList(ArrayList<GalleryBean> galleryList) {
		this.galleryList = galleryList;
	}
	
}
