package com.muevaelvolante.qiumagazine.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

public class DBAdapterIssues {

	public static final String EDITION = "edition";
    public static final String EDITION_ID = "edition_id";
    public static final String EDITION_NAME = "edition_name";
    public static final String DATE = "date";
    public static final String ORIENTATION = "orientation";
    public static final String LANG = "lang";
    public static final String PRICE = "price";
    public static final String PUBLISHER_PRICE = "publisher_price";
    public static final String DESCRIPTION = "description";
    public static final String COVER = "cover";
    public static final String COVER_URL = "cover_url";
    public static final String TOTAL_PAGES = "total_pages";
    public static final String SUBSCRIPTIONS = "subscriptions";
    public static final String ORIGINAL_PDF = "original_pdf";
    public static final String REFERENCE_NUMBER = "reference_number";
    public static final String MODE = "mode";
    public static final String KEY_ID = "_id";
    
    
    //For extra media
    public static final String ISSUE_ID = "issue_id";
    public static final String KEY_ISSUE_NAME = "issue_name";
    public static final String KEY_PAGE_NUMBER = "page_number";
    public static final String KEY_EXTRA_MEDIA = "extra_media_name";
    public static final String KEY_EXTRA_MEDIA_DATA = "extra_media_data";
    public static final String START_X = "start_x";
    public static final String START_Y = "start_y";
    public static final String END_X = "end_x";
    public static final String END_Y = "end_y";
    public static final String AUTO = "auto";
    public static final String FULLSCREEN = "fullscreen";
    public static final String CONTROLS = "controls";
    public static final String TYPE = "type";
    public static final String OWNER = "owner";
    public static final String TITLE = "title";
    
    private static final String DATABASE_NAME = "qiumagazine.db";
    private static final String DATABASE_TABLE = "issues";
    private static final String DATABASE_TABLE_EXTRA = "issue_media";
    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_CREATE =
        "create table " + DATABASE_TABLE + "("+KEY_ID+" integer primary key autoincrement, "
        + EDITION + " text, "+EDITION_ID+" text, "+EDITION_NAME+" text, "+DATE+" text, "+ORIENTATION+" text, "+LANG+" text, " +
        		PRICE+" text, "+DESCRIPTION+" text, "+COVER+" text, "+COVER_URL+" text, "+TOTAL_PAGES+" text, "+SUBSCRIPTIONS+" text, "+ORIGINAL_PDF+" text, "+PUBLISHER_PRICE+" text, "+REFERENCE_NUMBER+" text, "+MODE+" text);";
    
    private static final String DATABASE_CREATE_EXTRA =
            "create table " + DATABASE_TABLE_EXTRA + "("+KEY_ID+" integer primary key autoincrement, "
            + ISSUE_ID + " text, "+KEY_ISSUE_NAME+" text, "+KEY_PAGE_NUMBER+" text, "+KEY_EXTRA_MEDIA+" text, "+KEY_EXTRA_MEDIA_DATA+" text, "+START_X+" text, " +
            		START_Y+" text, "+END_X+" text, "+END_Y+" text, "+AUTO+" boolean, "+FULLSCREEN+" boolean, "+CONTROLS+" integer, "+TYPE+" integer, "+OWNER+" text, "+TITLE+" text);";
        
    private final Context context; 
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;
    
    
    public DBAdapterIssues(Context ctx){
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }
    
    public DBAdapterIssues open() throws SQLException{
        db = DBHelper.getWritableDatabase();
        return this;
    }
    public void close(){
        DBHelper.close();
        if (db != null)
			db.close();
    }
    
    public long inserData(String edition, String edition_id, String edition_name, String date, String orientation, String lang, String price,
    		String description, String cover, String cover_url, String total_pages, String subscriptions, String original_pdf, String publisher_price,
                          String reference_number, String mode){
    	    	
    	ContentValues cv = new ContentValues();
    	cv.put(EDITION, edition);
    	cv.put(EDITION_ID, edition_id);
    	cv.put(EDITION_NAME, edition_name);
    	cv.put(DATE, date);
    	cv.put(ORIENTATION, orientation);
    	cv.put(LANG, lang);
    	cv.put(PRICE, price);
    	cv.put(DESCRIPTION, description);
    	cv.put(COVER, cover);
    	cv.put(COVER_URL, cover_url);
    	cv.put(TOTAL_PAGES, total_pages);
    	cv.put(SUBSCRIPTIONS, subscriptions);
    	cv.put(ORIGINAL_PDF, original_pdf);
    	cv.put(PUBLISHER_PRICE, publisher_price);
    	cv.put(REFERENCE_NUMBER, reference_number);
        cv.put(MODE, mode);
    	
    	return db.insert(DATABASE_TABLE, null, cv);
    }
    
    public Cursor getData(String edition_id){
    	return db.query(DATABASE_TABLE, new String[]{EDITION, EDITION_ID, EDITION_NAME, DATE, ORIENTATION, 
    			LANG, PRICE, DESCRIPTION, COVER, COVER_URL, TOTAL_PAGES, SUBSCRIPTIONS, ORIGINAL_PDF, PUBLISHER_PRICE, REFERENCE_NUMBER, MODE},
    						EDITION_ID +" = '" +edition_id+"'", null, null, null, null, null);
    }
    
    public Cursor getAllData(){
    	return db.query(DATABASE_TABLE, new String[]{EDITION, EDITION_ID, EDITION_NAME, DATE, ORIENTATION, 
    			LANG, PRICE, DESCRIPTION, COVER, COVER_URL, TOTAL_PAGES, SUBSCRIPTIONS, ORIGINAL_PDF, PUBLISHER_PRICE, REFERENCE_NUMBER, MODE},
    						null, null, null, null, DATE + " DESC", null);
    }
    
    public Cursor getEditionByName(String issueName){
    	return db.query(DATABASE_TABLE, new String[]{EDITION, EDITION_ID},EDITION_NAME +" = '" +issueName+"'",null,null,null,null);
    }
    
    public Cursor getEditionById(String edition_id){
    	return db.query(DATABASE_TABLE, new String[]{EDITION, EDITION_ID},EDITION_ID +" = '" +edition_id+"'",null,null,null,null);
    }

    public void deleteEdition(String edition_id){
    	String where = EDITION_ID+"='"+edition_id+"'";
    	//System.out.println("el nombre es: "+issueName);
    	db.delete(DATABASE_TABLE, where, null);
    }
    
    public void updateEdition(String edition_id, String edition_name, String date, String orientation, String lang, String price,
    		String description, String subscriptions, String publisher_price, String reference_number, String mode){
    	String where = EDITION_ID+"='"+edition_id+"'";
    	ContentValues args = new ContentValues();
    	args.put(EDITION_NAME, edition_name);
    	args.put(DATE, date);
    	args.put(ORIENTATION, orientation);
    	args.put(LANG, lang);
    	args.put(PRICE, price);
    	args.put(DESCRIPTION, description);
    	args.put(SUBSCRIPTIONS, subscriptions);
    	args.put(PUBLISHER_PRICE, publisher_price);
    	args.put(REFERENCE_NUMBER, reference_number);
        args.put(MODE, mode);
    	db.update(DATABASE_TABLE, args, where, null);
    }
    
    public static byte[] getBytes(Object obj) {
		byte[] data = null;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(obj);
			oos.flush();
			oos.close();
			bos.close();
			data = bos.toByteArray();
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return data;
	}
   
    private static class DatabaseHelper extends SQLiteOpenHelper 
    {
        DatabaseHelper(Context context) 
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) 
        {
            db.execSQL(DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE_EXTRA);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
        	if (oldVersion < DATABASE_VERSION) {

                db.execSQL("ALTER TABLE "+DATABASE_TABLE+" ADD Column "+MODE);

            }
        }
    }
}
