package com.muevaelvolante.qiumagazine.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

public class DBAdapter {
	
	public static final String ISSUE_ID = "issue_id";
    public static final String KEY_ISSUE_NAME = "issue_name";
    public static final String KEY_PAGE_NUMBER = "page_number";
    public static final String KEY_EXTRA_MEDIA = "extra_media_name";
    public static final String KEY_EXTRA_MEDIA_DATA = "extra_media_data";
    public static final String KEY_ID = "_id";
    public static final String START_X = "start_x";
    public static final String START_Y = "start_y";
    public static final String END_X = "end_x";
    public static final String END_Y = "end_y";
    public static final String AUTO = "auto";
    public static final String FULLSCREEN = "fullscreen";
    public static final String CONTROLS = "controls";
    public static final String TYPE = "type";
    public static final String OWNER = "owner";
    public static final String TITLE = "title";
    
    private static final String DATABASE_NAME = "qiumagazine.db";
    private static final String DATABASE_TABLE = "issue_media";
    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_CREATE =
        "create table " + DATABASE_TABLE + "("+KEY_ID+" integer primary key autoincrement, "
        + ISSUE_ID + " text, "+KEY_ISSUE_NAME+" text, "+KEY_PAGE_NUMBER+" text, "+KEY_EXTRA_MEDIA+" text, "+KEY_EXTRA_MEDIA_DATA+" text, "+START_X+" text, " +
        		START_Y+" text, "+END_X+" text, "+END_Y+" text, "+AUTO+" boolean, "+FULLSCREEN+" boolean, "+CONTROLS+" integer, "+TYPE+" integer, "+OWNER+" text, "+TITLE+" text);";
        
    private final Context context; 
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;
    
    public DBAdapter(Context ctx){
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }
    
    public DBAdapter open() throws SQLException{
        db = DBHelper.getWritableDatabase();
        return this;
    }
    public void close(){
        DBHelper.close();
        if (db != null)
			db.close();
    }
    
    public long inserData(String id, String issueName, String extraMediaName, String mediaData, String sortNumber, String start_x, String start_y,
    		String end_x, String end_y, Boolean auto, Boolean fullscreen, Integer controls, Integer type, String owner, String title){
    	ContentValues cv = new ContentValues();
    	cv.put(ISSUE_ID, id);
    	cv.put(KEY_ISSUE_NAME, issueName);
    	cv.put(KEY_EXTRA_MEDIA, extraMediaName);
    	cv.put(KEY_EXTRA_MEDIA_DATA, mediaData);
    	cv.put(KEY_PAGE_NUMBER, sortNumber);
    	cv.put(START_X, start_x);
    	cv.put(START_Y, start_y);
    	cv.put(END_X, end_x);
    	cv.put(END_Y, end_y);
    	cv.put(AUTO, auto);
    	cv.put(FULLSCREEN, fullscreen);
    	cv.put(CONTROLS, controls);
    	cv.put(TYPE, type);
    	cv.put(OWNER, owner);
    	cv.put(TITLE, title);
    	
    	return db.insert(DATABASE_TABLE, null, cv);
    }
    
    public Cursor getData(String issueId){
    	return db.query(DATABASE_TABLE, new String[]{ISSUE_ID, KEY_ISSUE_NAME, KEY_EXTRA_MEDIA, KEY_EXTRA_MEDIA_DATA, KEY_PAGE_NUMBER, 
    			START_X, START_Y, END_X, END_Y, AUTO, FULLSCREEN, CONTROLS, TYPE}, 
    						ISSUE_ID +" = '" +issueId+"'", null, null, null, null, null);
    }
    
    public Cursor getData(String issueId, String pageNumber){
    	System.out.println("A buscar en "+issueId+" pagina: "+pageNumber);
    	return db.query(DATABASE_TABLE, new String[]{ISSUE_ID, KEY_ISSUE_NAME, KEY_EXTRA_MEDIA, KEY_EXTRA_MEDIA_DATA, KEY_PAGE_NUMBER, 
    			START_X, START_Y, END_X, END_Y, AUTO, FULLSCREEN, CONTROLS, TYPE, OWNER, TITLE}, 
    						ISSUE_ID +" = '" +issueId+"' AND "+KEY_PAGE_NUMBER+"='"+pageNumber+"'", null, null, null, null, null);
    }
    
    public Cursor getData(String issueId, String pageNumber, String pageNumber2){
    	System.out.println("A buscar en "+issueId+" pagina: "+pageNumber+"y página: "+pageNumber2);
    	return db.query(DATABASE_TABLE, new String[]{ISSUE_ID, KEY_ISSUE_NAME, KEY_EXTRA_MEDIA, KEY_EXTRA_MEDIA_DATA, KEY_PAGE_NUMBER, 
    			START_X, START_Y, END_X, END_Y, AUTO, FULLSCREEN, CONTROLS, TYPE, OWNER, TITLE}, 
    						ISSUE_ID +" = '" +issueId+"' AND ("+KEY_PAGE_NUMBER+"='"+pageNumber+"' OR "+KEY_PAGE_NUMBER+"='"+pageNumber2+"')", null, null, null, null, null);
    }
    
    public Cursor getPremiumData(String issueId){
    	return db.query(DATABASE_TABLE, new String[]{ISSUE_ID, KEY_ISSUE_NAME, KEY_EXTRA_MEDIA, KEY_EXTRA_MEDIA_DATA, KEY_PAGE_NUMBER, 
    			START_X, START_Y, END_X, END_Y, AUTO, FULLSCREEN, CONTROLS, TYPE, OWNER, TITLE}, ISSUE_ID +" = '" +issueId+"' AND "+KEY_EXTRA_MEDIA +" = 'audios_premium'", null, null, null, null);
    }
    
    public void makePremiumAvailable(String issueId){
    	String where = ISSUE_ID+"='"+issueId+"' AND "+KEY_EXTRA_MEDIA+" = 'audios_premium'";
    	ContentValues args = new ContentValues();
    	args.put(KEY_EXTRA_MEDIA, "audio");
    	db.update(DATABASE_TABLE, args, where, null);
    }
    
    public void deleteData(String issueId){
    	String where = ISSUE_ID+"='"+issueId+"'";
    	//System.out.println("el nombre es: "+issueName);
    	db.delete(DATABASE_TABLE, where, null);
    }

    public static byte[] getBytes(Object obj) {
		byte[] data = null;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(obj);
			oos.flush();
			oos.close();
			bos.close();
			data = bos.toByteArray();
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return data;
	}
   
    private static class DatabaseHelper extends SQLiteOpenHelper 
    {
        DatabaseHelper(Context context) 
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) 
        {
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, 
        int newVersion) 
        {
        	if (oldVersion >= newVersion)
    			return;
        }
    }
}
