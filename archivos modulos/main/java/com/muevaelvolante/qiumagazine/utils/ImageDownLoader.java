package com.muevaelvolante.qiumagazine.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.muevaelvolante.qiumagazine.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;

public abstract class ImageDownLoader {
	private static final String LOG_TAG = "ImageDownlader";
	private static Activity ctx;

	public static void download(String url, ImageView imageView, Activity activity, String path){
		Context context = imageView.getContext();
		Bitmap bitmap = getBitmapFromCache(context, url);
		
		ctx = activity;
		if (bitmap == null)
			forceDownload(url, imageView, path);
		else {
			cancelPotentialDownload(url, imageView);
			imageView.setImageBitmap(bitmap);
		}
	}

	private static void forceDownload(String url, ImageView imageView, String path){
		if (url == null) {
			imageView.setImageDrawable(null);
			return;
		}

		if (cancelPotentialDownload(url, imageView)) {
			BitmapDownloadTask task = new BitmapDownloadTask(url, imageView, ctx, path);
			DownloadedDrawable drawable = new DownloadedDrawable(task);
			imageView.setImageDrawable(drawable);
			imageView.setMinimumHeight(156);
			task.execute();
		}

	}

	private static boolean cancelPotentialDownload(String url, ImageView imageView){
		BitmapDownloadTask task = getBitmapDownloadTask(imageView);

		if (task != null) {
			String downloadURL = task.url;
			if (downloadURL == null || !downloadURL.equals(url))
				task.cancel(true);
			return false;
		}
		return true;
	}

	private static BitmapDownloadTask getBitmapDownloadTask(ImageView imageView){
		if (imageView != null){
			Drawable drawable = imageView.getDrawable();
			if (drawable instanceof DownloadedDrawable) {
				DownloadedDrawable ddrawable = (DownloadedDrawable)drawable;
				return ddrawable.getBitmapDownloadTask();
			}
		}

		return null;
	}

	static Bitmap downloadBitmap(Context ctx, String url, String path){

		final HttpClient client = new DefaultHttpClient();

		final HttpGet getRequest = new HttpGet(url);

		try {
			HttpResponse response = client.execute(getRequest);
			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				Log.w("ImageDownloader", "Error " + statusCode + " while retriving bitmap from " + url);
				return null;
			}
			
			final HttpEntity entity = response.getEntity();
			
			if (entity != null){
				InputStream inputStream = null;
				try{
					inputStream = entity.getContent();
					
					BitmapFactory.Options options=new BitmapFactory.Options();
					options.inSampleSize=2; //try to decrease decoded image
					options.inPurgeable=true; //purgeable to disk
				    Bitmap myBitmap = BitmapFactory.decodeStream(new FlushedInputStream(inputStream), null, options);
				    
				    if(!(new File(Constant.getAppFilepath(ctx)+"/"+path+"/cover/cover.jpg").exists())){
					    Log.i("save","guardando la imagen en la sd en path: "+path);
					    (new File(Constant.getAppFilepath(ctx)+"/"+path+"/cover")).mkdir();
					    OutputStream stream = new FileOutputStream(Constant.getAppFilepath(ctx)+"/"+path+"/cover/cover.jpg");
					    //Write bitmap to file using JPEG and 80% quality hint for JPEG.
					    myBitmap.compress(CompressFormat.JPEG, 100, stream);
				    }
					
					return myBitmap;
					
				} finally {
					if (inputStream != null)
						inputStream.close();
					entity.consumeContent();
				}

			}
		} catch(IOException e){
			getRequest.abort();
			Log.w(LOG_TAG, "I/O error while retriving from url :" + url, e);
		} catch (IllegalStateException e) {
            getRequest.abort();
            Log.w(LOG_TAG, "Incorrect URL: " + url);
        } catch (Exception e) {
            getRequest.abort();
            Log.w(LOG_TAG, "Error while retrieving bitmap from " + url, e);
        } finally {
        	//client.close();
        }
        return null;

	}

	static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break;  // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

	public static class BitmapDownloadTask extends AsyncTask<Void, Void, Bitmap> {

		private String url;
		private final WeakReference<ImageView> imageViewRef;
		private Context ctx;
		private ProgressDialog pDialog;
		private String path;

		public BitmapDownloadTask(String url, ImageView imageView, Context context, String path) {
			imageViewRef = new WeakReference<ImageView>(imageView);
			this.ctx = context;
			this.url = url;
			this.path = path;
			pDialog = new ProgressDialog(context);
		}

		protected void onPostExecute(Bitmap bitmap){
			if (this.isCancelled()) {
				bitmap = null;
			}
			ImageView imageView = imageViewRef.get();

			if (imageView != null) {
				Context ctx = imageView.getContext();
				addBitmapToCache(ctx, url, bitmap);

				if (imageViewRef != null) {

					BitmapDownloadTask task = getBitmapDownloadTask(imageView);
					if (this == task) {
						imageView.setImageBitmap(bitmap);
					}
				}
			}
			
			if(pDialog.isShowing())
				pDialog.dismiss();
		}

		@Override
		protected Bitmap doInBackground(Void... params) {
			
			Bitmap myBitmap;
			
			if(new File(Constant.getAppFilepath(ctx)+"/"+path+"/cover/cover.jpg").exists()){
				File f = new File(Constant.getAppFilepath(ctx)+"/"+path+"/cover/cover.jpg");
				myBitmap = BitmapFactory.decodeFile(f.toString());
				Log.i("info","covers from SD");
			}
			else{
				myBitmap=downloadBitmap(ctx,url, path);
				Log.i("info","covers from internet");
			}
			return myBitmap;

		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			 this.pDialog.setMessage(ctx.getResources().getString(R.string.loading));
		     this.pDialog.show();
		}
		
	}

	static class DownloadedDrawable extends ColorDrawable {
		private final WeakReference<BitmapDownloadTask> taskRef;

		public DownloadedDrawable(BitmapDownloadTask task){
			super();
			//super(Color.BLACK);
			taskRef = new WeakReference<BitmapDownloadTask>(task);
		}

		public BitmapDownloadTask getBitmapDownloadTask(){
			return taskRef.get();
		}
	}

	private static void addBitmapToCache(Context context, String url, Bitmap bitmap){
		if (bitmap != null) {

			if (context != null) {
				File dir = context.getCacheDir();
				String fileName = url.hashCode() + ".png";
				File f = new File(dir, fileName);
				FileOutputStream out = null;
				try {
					out = new FileOutputStream(f);
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
				} catch (FileNotFoundException e) {
					Log.w(LOG_TAG, "Error save bitmap from url: " + url + " to " + f);
				} finally {
					if (out != null) {
						try {
							out.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	private static Bitmap getBitmapFromCache(Context context, String url) {
		if (url != null) {

			if (context != null) {
				File dir = context.getCacheDir();
				String fileName = url.hashCode() + ".png";
				File f = new File(dir, fileName);
				if (f.exists()) {
					//return BitmapFactory.decodeFile(f.toString());
					Bitmap bm = BitmapFactory.decodeFile(f.toString());
					
					int height=context.getResources().getInteger(R.integer.bitmap_size_height);
					int width = height*bm.getWidth()/bm.getHeight();
					//int width=context.getResources().getInteger(R.integer.bitmap_size_width);
					if(context.getResources().getBoolean(R.bool.isTablet)){
						height = bm.getHeight();
						width = bm.getWidth();
					}
					
					Log.i("res","La resolucion es: "+width+"-"+height);
					
					return  Bitmap.createScaledBitmap(bm,width,height,true);
				}
			}
		}

		return null;
	}
}
