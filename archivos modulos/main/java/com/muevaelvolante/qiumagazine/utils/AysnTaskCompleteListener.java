package com.muevaelvolante.qiumagazine.utils;

public interface AysnTaskCompleteListener<T> {
	public void onTaskComplete(T result, String method);

	public void onTaskComplete(String response, String callTo);
}
