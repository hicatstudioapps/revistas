package com.muevaelvolante.qiumagazine.utils;

public interface DownloadTaskCompleteListener {
	public void onTaskComplete(Boolean result, String method);
}
