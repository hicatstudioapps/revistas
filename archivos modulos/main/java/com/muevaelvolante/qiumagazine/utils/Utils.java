package com.muevaelvolante.qiumagazine.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.muevaelvolante.qiumagazine.R;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by joseluis on 28/01/2016.
 */
public class Utils {


    public static long HOURS_24_MILLIS = 1000 * 60 * 60 * 24;


    /**
     * Comprueba si un usuario es de tipo admin o preview y por tanto puede acceder a todas las revistas de forma gratuita.
     * Incluso las que están marcadas como preview
     * @param context
     * @param user_
     * @param pass_
     * @return
     */
    public static boolean checkIfUserPreview(Context context, String user_, String pass_)
    {
        if(user_.equalsIgnoreCase("admin") || user_.equalsIgnoreCase("preview"))
        {
            if(pass_.equalsIgnoreCase(context.getString(R.string.API_KEY).substring(0,10)))
            {
                SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("preview", true);
                editor.apply();

                return true;
            }
        }

        return false;
    }


    public static boolean isUserAdmin(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        return preferences.getBoolean("preview",false);
    }




    public static long getCurrentTimeInMillis() {

        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("UTC"));
        return c.getTimeInMillis();

    }


}
