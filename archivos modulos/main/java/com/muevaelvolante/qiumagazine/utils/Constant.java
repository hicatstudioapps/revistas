package com.muevaelvolante.qiumagazine.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class Constant {
	
	//public final static String EDITION_URL = "http://www.qiusystem.com/api&key=Aq0jOZ79gGxNoL2uxcnbsQdG8xOc4Q5VmwHD&action=issues&module=magazine&preview=true"; 
	//Iphone
	//public final static String CURRENT_ISSUE_URL = "http://api.qiusystem.com/?apikey=12331233&list=pagesmd&edition=";
	
	//Android
	//public final static String CURRENT_ISSUE_URL = "http://www.qiusystem.com/api&key=Aq0jOZ79gGxNoL2uxcnbsQdG8xOc4Q5VmwHD&action=pagesmd&module=magazine&edition=";
    //public final static String API_URL = "http://api.qiumagazine.com/api";
    //public final static String API_KEY = "Aq0jOZ79gGxNoL2uxcnbsQdG8xOc4Q5VmwHD";//test

    public final static String S3_URL = "https://s3.amazonaws.com/qiumagazine-issues";
    public final static String CLOUD_S3_URL = "http://downloads.qiumagazine.com";
    public final static Integer test = 0;
    public final static int blinks = 5;
    public final static String RSAKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiC1WBQLey4AgNSJWlFjwUqJRdLGhsglJ9A4y5tNm2QBo7F3EW74ESqMl7B+QRoEGRmvqQkbRV8lgT6hj82DBhfFSUymDobBS2HJbDaJKuLQg+6u3Hs9XCHFcfXcM8UP/nvK+OV6zMb02ZbEMUxEAS7n6bOgH+Vu1khb1xyrmTBH9jeGHYA2yxzqG7kH8oWS1XgbZZsAbkC/v2ktnwX0sypfdHJURaNIG7M5yBH2RuLzZfV+l5akMDPdsaspHTe/4fyHqhWG3jACPfBl8dE0nzoJHdmYnwogsWUFd6IB2mX+13ypyeDMBUQvMBXv57Ucw06CVdcdKDsv9Ged7HOPh8QIDAQAB";


    public static String getAppFilepath(Context context)
    {

        File path = context.getExternalFilesDir(null);
        if(path != null)
            return path.getAbsolutePath();
        else
        {
            String pkg = context.getPackageName();

            return Environment.getExternalStorageDirectory().getPath()
                    .concat("/Android/data/")
                    .concat(pkg)
                    .concat("/file");
        }
    }
}