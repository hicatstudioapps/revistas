package com.muevaelvolante.qiumagazine.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.bean.CurrentIssueBean;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;


public class DownloadFileAsync extends AsyncTask<String, String, Boolean> {

	private Context context;
	private ArrayList<CurrentIssueBean> currentIssueBeanList = new ArrayList<CurrentIssueBean>();
	private String issueName;
	private String pathDownload;
	private ProgressDialog progressDialog;
	private DownloadTaskCompleteListener cb;
	private int filesize;
	
	public DownloadFileAsync(Context context, DownloadTaskCompleteListener callback,ArrayList<CurrentIssueBean> currentIssueBeanList, String issueName){
		this.context = context;
		this.currentIssueBeanList = currentIssueBeanList;
		this.issueName = issueName;
		this.cb = callback;
		progressDialog = new ProgressDialog(context);
	}
    
    protected Boolean doInBackground(String... aurl) {
        int count;
        boolean flag = false;
        pathDownload = Constant.getAppFilepath(context) + "/" + URLEncoder.encode(issueName);
        String selectionPath = "";
        Log.i("path","path es: "+pathDownload);
        for(int i=0; i<aurl.length; i++){
            try {
            	String[] urlWithImage = aurl[i].split("'");
            	Log.i("string","aurl: "+aurl[i]);
            	String dwnUrl = urlWithImage[0];
            	String imageName = urlWithImage[1];
                URL url = new URL(dwnUrl); 
                URLConnection conexion = url.openConnection();
                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();
                if(dwnUrl.contains("pdf")){
                	selectionPath = "/PDF";
                	Log.i("pdf","el path de pdf: "+dwnUrl);
                }else if(dwnUrl.contains("page_edit")){
                	selectionPath = "/PDF";
                }else if(dwnUrl.contains("small_thumbs")){
                	selectionPath = "/Thumb";
                }else if(dwnUrl.contains("big_thumbs")){
                	selectionPath = "/Thumb";
                }else if(dwnUrl.contains("media")){
                	String[] fileNameWithSort = imageName.split("!");
                	String imagename = fileNameWithSort[0];
                	selectionPath = "/Extra";
                	imageName = imagename;
                }		
                File path = new File(pathDownload + selectionPath);
                if(!path.isDirectory()){
                     if(path.mkdirs());
                }
                
                File outFile = new File(path, imageName);
                if (outFile.exists())
                	outFile.delete();
                
                int fileLength = conexion.getContentLength();
                filesize=fileLength;
                
                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(outFile);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    output.write(data, 0, count);
                    publishProgress(""+(int) (total * 100 / fileLength));
                }

                output.flush();
                output.close();
                input.close();
                
                //progressDialog.setMax(aurl.length);
                publishProgress(""+i);
                flag = true;
            } catch (Exception e) {flag=false;}
        }
        return true;

    }
  

	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@Override
	protected void onPreExecute() {
		progressDialog.setMessage(context.getResources().getString(R.string.downloading));
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
	 */
	@Override
	protected void onProgressUpdate(String... values) {
		// TODO Auto-generated method stub
		 progressDialog.setProgress(Integer.parseInt(values[0]));
	}

	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(Boolean result) {
		cb.onTaskComplete(result, ""+filesize);
		
		if(progressDialog.isShowing())
			progressDialog.dismiss();
	}

}
