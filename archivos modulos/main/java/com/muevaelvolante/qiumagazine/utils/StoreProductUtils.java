package com.muevaelvolante.qiumagazine.utils;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.adapter.DBAdapterIssues;
import com.muevaelvolante.qiumagazine.bean.AudioBean;
import com.muevaelvolante.qiumagazine.bean.AudioPremiumBean;
import com.muevaelvolante.qiumagazine.bean.CurrentIssueBean;
import com.muevaelvolante.qiumagazine.bean.GalleryBean;
import com.muevaelvolante.qiumagazine.bean.LinkBean;
import com.muevaelvolante.qiumagazine.bean.StoreProductBean;
import com.muevaelvolante.qiumagazine.bean.VideoBean;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;



public class StoreProductUtils {
	
	
	public static ArrayList<StoreProductBean> getAllStoreProduct(Context ctx, String result){
		
		//System.out.println("Edition URL : "+ result);
		ArrayList<StoreProductBean> getAllStoreProductList = new ArrayList<StoreProductBean>();
		
		try {
			
			JSONArray jsonArry = new JSONArray(result);
			if(jsonArry != null || jsonArry.length() > 0){
				
				for(int i=0; i<jsonArry.length(); i++){
					JSONObject jsonObject = jsonArry.getJSONObject(i);
					StoreProductBean storeProductBean = new StoreProductBean();
					storeProductBean.setEdition(jsonObject.optString("edition"));
					storeProductBean.setEditionId(jsonObject.optString("edition_id"));
					storeProductBean.setEditionName(jsonObject.optString("edition_name").replace("'", "�"));
					storeProductBean.setIssueDate(jsonObject.optString("date"));
					storeProductBean.setScreenOrientation(jsonObject.optString("orientation"));
					storeProductBean.setLang(jsonObject.optString("lang"));
					storeProductBean.setPrice(jsonObject.optString("price"));
					storeProductBean.setDescription(jsonObject.optString("description"));
					storeProductBean.setCoverName(jsonObject.optString("cover"));
					storeProductBean.setCoverUrl(jsonObject.optString("cover_url"));
					storeProductBean.setTotalPage(jsonObject.optString("total_pages"));
					storeProductBean.setSubscriptions(jsonObject.optString("subscriptions"));
					storeProductBean.setDownloadPDF(jsonObject.optString("original_pdf"));
					storeProductBean.setPublisher_price(jsonObject.optString("publisher_price"));
					storeProductBean.setReference_number(jsonObject.optString("reference_number"));
                    storeProductBean.setMode(jsonObject.optString("mode"));
					String[] pdfName = jsonObject.optString("original_pdf").split("/");//split by "/" to obtain pdf name
					storeProductBean.setPdfName(pdfName[pdfName.length-1]);//set the pdf name
					
					getAllStoreProductList.add(storeProductBean);
					
					File dir = new File(Constant.getAppFilepath(ctx)+"/"+URLEncoder.encode(jsonObject.optString("edition_id")));
					if(!dir.exists())
						dir.mkdir();
				}
				
			}
			
		} catch (Exception e) {}
		
		return getAllStoreProductList;
	}
	
	
	public static ArrayList<CurrentIssueBean> getCurrentIssueList(String result){
		ArrayList<CurrentIssueBean> currentIssueList = new ArrayList<CurrentIssueBean>();
		try {
			//System.out.println("Current Issue Result : "+ result);
			JSONArray jsonArry = new JSONArray(result);
			if(jsonArry != null || jsonArry.length() > 0){
				
				for(int i=0; i<jsonArry.length(); i++){
					
					JSONObject jsonObject = jsonArry.getJSONObject(i);
					Log.i("objeto", "aqui esta: " + jsonArry.getJSONObject(i).toString());
					CurrentIssueBean currentIssueBean = new CurrentIssueBean();
					currentIssueBean.setIdPage(jsonObject.optString("id_page"));
					currentIssueBean.setImageName(jsonObject.optString("image"));
					currentIssueBean.setSortNumber(jsonObject.optString("sort"));
					currentIssueBean.setImageURL(jsonObject.optString("image_url"));
					currentIssueBean.setThmbnailURL(jsonObject.optString("thumbnail_url"));
					currentIssueBean.setIdParent(jsonObject.optString("id_parent"));
					currentIssueBean.setLink(jsonObject.optString("link"));
					currentIssueBean.setMorePage(jsonObject.optString("more_pages"));
					currentIssueBean.setTemplate(jsonObject.getString("template"));
					currentIssueBean.setSummery(jsonObject.optString("summary"));
					currentIssueBean.setText(jsonObject.optString("text"));
					
					JSONObject mediaObject = jsonObject.optJSONObject("extra_media");
					if(mediaObject != null){
						if(mediaObject.optJSONArray("audios") != null){
							ArrayList<AudioBean> audioList = new ArrayList<AudioBean>();
							JSONArray audioArr = mediaObject.getJSONArray("audios");
							for(int a=0; a<audioArr.length();a++){
								AudioBean audioBean = new AudioBean();
								audioBean.setIdPage(jsonObject.optString("id_page"));
								audioBean.setSort(jsonObject.optString("sort"));
								audioBean.setFileName(audioArr.getJSONObject(a).getString("filename"));
								audioBean.setTitle(audioArr.getJSONObject(a).getString("title"));
								audioBean.setUrl(audioArr.getJSONObject(a).getString("url"));
								audioBean.setAuto(audioArr.getJSONObject(a).getBoolean("auto"));
								//audioBean.setControls(audioArr.getJSONObject(a).getInt("controls"));
								if(audioArr.getJSONObject(a).getString("controls").equalsIgnoreCase("false"))
									audioBean.setControls(0);
								else
									audioBean.setControls(audioArr.getJSONObject(a).getInt("controls"));
								audioBean.setType(audioArr.getJSONObject(a).getInt("type"));
								audioBean.setStartX((float)audioArr.getJSONObject(a).getDouble("start-x"));
								audioBean.setStartY((float)audioArr.getJSONObject(a).getDouble("start-y"));
								audioBean.setEndX((float)audioArr.getJSONObject(a).getDouble("end-x"));
								audioBean.setEndY((float)audioArr.getJSONObject(a).getDouble("end-y"));
								audioList.add(audioBean);
							}
							currentIssueBean.setAudioList(audioList);
						}
						if(mediaObject.optJSONArray("audios_premium") != null){
							ArrayList<AudioPremiumBean> audioList = new ArrayList<AudioPremiumBean>();
							JSONArray audioArr = mediaObject.getJSONArray("audios_premium");
							for(int a=0; a<audioArr.length();a++){
								AudioPremiumBean audioBean = new AudioPremiumBean();
								audioBean.setIdPage(jsonObject.optString("id_page"));
								audioBean.setSort(jsonObject.optString("sort"));
								audioBean.setFileName(audioArr.getJSONObject(a).getString("filename"));
								audioBean.setTitle(audioArr.getJSONObject(a).getString("title"));
								audioBean.setUrl(audioArr.getJSONObject(a).getString("url"));
								audioBean.setAuto(audioArr.getJSONObject(a).getBoolean("auto"));
								//audioBean.setControls(audioArr.getJSONObject(a).getInt("controls"));
								if(audioArr.getJSONObject(a).getString("controls").equalsIgnoreCase("false"))
									audioBean.setControls(0);
								else
									audioBean.setControls(audioArr.getJSONObject(a).getInt("controls"));
								audioBean.setType(audioArr.getJSONObject(a).getInt("type"));
								audioBean.setStartX((float)audioArr.getJSONObject(a).getDouble("start-x"));
								audioBean.setStartY((float)audioArr.getJSONObject(a).getDouble("start-y"));
								audioBean.setEndX((float)audioArr.getJSONObject(a).getDouble("end-x"));
								audioBean.setEndY((float)audioArr.getJSONObject(a).getDouble("end-y"));
								audioList.add(audioBean);
							}
							currentIssueBean.setAudioPremiumList(audioList);
						}
						if(mediaObject.optJSONArray("link") != null){
							ArrayList<LinkBean> linkList = new ArrayList<LinkBean>();
							JSONArray linkArr = mediaObject.getJSONArray("link");
							System.out.println("link: "+linkArr.toString());
							for(int a=0; a<linkArr.length();a++){
								LinkBean linkBean = new LinkBean();
								linkBean.setIdPage(jsonObject.optString("id_page"));
								linkBean.setSort(jsonObject.optString("sort"));
								linkBean.setUrl(linkArr.getJSONObject(a).getString("url"));
								
								if(linkArr.getJSONObject(a).getString("controls").equalsIgnoreCase("false"))
									linkBean.setControls(0);
								else
									linkBean.setControls(linkArr.getJSONObject(a).getInt("controls"));
								linkBean.setType(linkArr.getJSONObject(a).getInt("type"));
								linkBean.setStartX((float)linkArr.getJSONObject(a).getDouble("start-x"));
								linkBean.setStartY((float)linkArr.getJSONObject(a).getDouble("start-y"));
								linkBean.setEndX((float)linkArr.getJSONObject(a).getDouble("end-x"));
								linkBean.setEndY((float)linkArr.getJSONObject(a).getDouble("end-y"));
								linkList.add(linkBean);
							}
							System.out.println("Lenght : Link "+linkArr.length());
							currentIssueBean.setLinkList(linkList);
						}
						if(mediaObject.optJSONArray("videos") != null){
							ArrayList<VideoBean> videoList = new ArrayList<VideoBean>();
							JSONArray videoArr = mediaObject.getJSONArray("videos");
							System.out.println("video: "+videoArr.toString());
							for(int a=0; a<videoArr.length();a++){
								VideoBean videoBean = new VideoBean();
								videoBean.setIdPage(jsonObject.optString("id_page"));
								videoBean.setSort(jsonObject.optString("sort"));
								if(videoArr.getJSONObject(a).getInt("type")==2)
									videoBean.setFileName(videoArr.getJSONObject(a).getString("filename"));
								videoBean.setTitle(videoArr.getJSONObject(a).getString("title"));
								videoBean.setUrl(videoArr.getJSONObject(a).getString("url"));
								videoBean.setOwner(videoArr.getJSONObject(a).getString("owner"));
								videoBean.setAuto(videoArr.getJSONObject(a).getBoolean("auto"));
								videoBean.setFullscreen(videoArr.getJSONObject(a).getBoolean("fullscreen"));
								if(videoArr.getJSONObject(a).getString("controls").equalsIgnoreCase("false"))
									videoBean.setControls(0);
								else
									videoBean.setControls(videoArr.getJSONObject(a).getInt("controls"));
								videoBean.setType(videoArr.getJSONObject(a).getInt("type"));
								videoBean.setStartX((float)videoArr.getJSONObject(a).getDouble("start-x"));
								videoBean.setStartY((float)videoArr.getJSONObject(a).getDouble("start-y"));
								videoBean.setEndX((float)videoArr.getJSONObject(a).getDouble("end-x"));
								videoBean.setEndY((float)videoArr.getJSONObject(a).getDouble("end-y"));
								if(videoArr.getJSONObject(a).getString("owner")!=null){
									videoBean.setOwner(videoArr.getJSONObject(a).getString("owner"));
								}
								
								videoList.add(videoBean);
							}
							System.out.println("Lenght : Videos "+videoArr.length());
							currentIssueBean.setVideoList(videoList);
						}
						if(mediaObject.optJSONArray("html5") != null){
							//The object format of html is the same than links
							
							ArrayList<LinkBean> linkList = new ArrayList<LinkBean>();
							JSONArray linkArr = mediaObject.getJSONArray("html5");
							System.out.println("link: "+linkArr.toString());
							for(int a=0; a<linkArr.length();a++){
								LinkBean linkBean = new LinkBean();
								linkBean.setIdPage(jsonObject.optString("id_page"));
								linkBean.setSort(jsonObject.optString("sort"));
								linkBean.setUrl(linkArr.getJSONObject(a).getString("url"));
								
								if(linkArr.getJSONObject(a).getString("controls").equalsIgnoreCase("false"))
									linkBean.setControls(0);
								else
									linkBean.setControls(linkArr.getJSONObject(a).getInt("controls"));
								linkBean.setType(linkArr.getJSONObject(a).getInt("type"));
								linkBean.setStartX((float)linkArr.getJSONObject(a).getDouble("start-x"));
								linkBean.setStartY((float)linkArr.getJSONObject(a).getDouble("start-y"));
								linkBean.setEndX((float)linkArr.getJSONObject(a).getDouble("end-x"));
								linkBean.setEndY((float)linkArr.getJSONObject(a).getDouble("end-y"));
								linkList.add(linkBean);
							}
							System.out.println("Lenght : Link "+linkArr.length());
							currentIssueBean.setHtml5List(linkList);
						}
						if(mediaObject.optJSONArray("gallery") != null){
							ArrayList<GalleryBean> galleryList = new ArrayList<GalleryBean>();
							JSONArray galleryArr = mediaObject.getJSONArray("gallery");
							System.out.println("gallery: "+galleryArr.toString());
							for(int a=0; a<galleryArr.length();a++){
								GalleryBean galleryBean = new GalleryBean();
								galleryBean.setIdPage(jsonObject.optString("id_page"));
								galleryBean.setSort(jsonObject.optString("sort"));
								
								//Save array of images as string and then parse to maintain db structure
								galleryBean.setUrl(galleryArr.getJSONObject(a).getString("pages"));
								galleryBean.setAuto(galleryArr.getJSONObject(a).getBoolean("auto"));
								galleryBean.setFullscreen(galleryArr.getJSONObject(a).getBoolean("fullscreen"));
								if(galleryArr.getJSONObject(a).getString("controls").equalsIgnoreCase("false"))
									galleryBean.setControls(0);
								else
									galleryBean.setControls(galleryArr.getJSONObject(a).getInt("controls"));
								galleryBean.setType(galleryArr.getJSONObject(a).getInt("type"));
								galleryBean.setStartX((float)galleryArr.getJSONObject(a).getDouble("start-x"));
								galleryBean.setStartY((float)galleryArr.getJSONObject(a).getDouble("start-y"));
								galleryBean.setEndX((float)galleryArr.getJSONObject(a).getDouble("end-x"));
								galleryBean.setEndY((float)galleryArr.getJSONObject(a).getDouble("end-y"));
								
								galleryList.add(galleryBean);
							}
							System.out.println("Lenght : Gallery "+galleryArr.length());
							currentIssueBean.setGalleryList(galleryList);
						}
						
					}
					
					currentIssueList.add(currentIssueBean);
				}
			}	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return currentIssueList;
	}







	public static ArrayList<StoreProductBean> loadFromInternalDB(DBAdapterIssues dbAdapterIssues){

		ArrayList<StoreProductBean> getAllStoreProductList = new ArrayList<StoreProductBean>();
		//load from database
		dbAdapterIssues.open();
		Cursor c = dbAdapterIssues.getAllData();
		if(c.getCount()>0){
			while(c.moveToNext()){
				StoreProductBean storeProductBean = new StoreProductBean();
				storeProductBean.setEdition(c.getString(0));
				storeProductBean.setEditionId(c.getString(1));
				storeProductBean.setEditionName(c.getString(2));
				storeProductBean.setIssueDate(c.getString(3));
				storeProductBean.setScreenOrientation(c.getString(4));
				storeProductBean.setLang(c.getString(5));
				storeProductBean.setPrice(c.getString(6));
				storeProductBean.setDescription(c.getString(7));
				storeProductBean.setCoverName(c.getString(8));
				storeProductBean.setCoverUrl(c.getString(9));
				storeProductBean.setTotalPage(c.getString(10));
				storeProductBean.setSubscriptions(c.getString(11));
				storeProductBean.setDownloadPDF(c.getString(12));
				storeProductBean.setPublisher_price(c.getString(13));
				storeProductBean.setReference_number(c.getString(14));
                storeProductBean.setMode(c.getString(15));
				String[] pdfName = c.getString(12).split("/");//split by "/" to obtain pdf name
				storeProductBean.setPdfName(pdfName[pdfName.length-1]);//set the pdf name
				//storeProductBean.setPdfName(c.getString(13));//set the pdf name

				getAllStoreProductList.add(storeProductBean);
			}
		}

		c.close();
		dbAdapterIssues.close();

		return getAllStoreProductList;
	}




    public static int getModeDrawable(String mode) {

        if ( mode.equalsIgnoreCase(StoreProductBean.MODE_PUBLISHED) ) {

            return R.drawable.shape_circle_status_ok;
        }

        else if ( mode.equalsIgnoreCase(StoreProductBean.MODE_PREVIEW) ) {

            return R.drawable.shape_circle_status_preview;

        } else {
            return 0; // ¿Ó return R.drawable.shape_circle_status_ko?
        }

    }



}
