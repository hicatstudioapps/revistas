package com.muevaelvolante.qiumagazine.app;

import android.app.Application;

import com.muevaelvolante.qiumagazine.R;
import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseInstallation;

import java.util.Locale;


public class Magazine extends Application {

	
	@Override
    public void onCreate() {
        super.onCreate();
        
        System.out.println("Initializing parse...");
        ParseCrashReporting.enable(this);
        Parse.initialize(this, getString(R.string.parseAppId), getString(R.string.parseClientKey));
        
        String code = Locale.getDefault().getLanguage();
		// Search for "sublocale".
		int index = code.indexOf("_");
	
		if (index != -1) {
			// If sublocale. 
			// cut sublocale
			code =  code.substring(0, index);
		}
		
		ParseInstallation.getCurrentInstallation().put("deviceLanguage", code);
        ParseInstallation.getCurrentInstallation().saveEventually();
	}
	
}
