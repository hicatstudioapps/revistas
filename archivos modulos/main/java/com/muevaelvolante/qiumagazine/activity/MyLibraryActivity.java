package com.muevaelvolante.qiumagazine.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.adapter.DBAdapter;
import com.muevaelvolante.qiumagazine.adapter.DBAdapterIssues;
import com.muevaelvolante.qiumagazine.bean.AudioBean;
import com.muevaelvolante.qiumagazine.bean.CurrentIssueBean;
import com.muevaelvolante.qiumagazine.bean.GalleryBean;
import com.muevaelvolante.qiumagazine.bean.LinkBean;
import com.muevaelvolante.qiumagazine.bean.StoreProductBean;
import com.muevaelvolante.qiumagazine.bean.VideoBean;
import com.muevaelvolante.qiumagazine.utils.Constant;
import com.muevaelvolante.qiumagazine.utils.DownloadFileAsync;
import com.muevaelvolante.qiumagazine.utils.DownloadTaskCompleteListener;
import com.muevaelvolante.qiumagazine.utils.ImageDownLoader;
import com.muevaelvolante.qiumagazine.utils.ManagerGoogleAnalytics;
import com.muevaelvolante.qiumagazine.utils.StoreProductUtils;
import com.muevaelvolante.qiumagazine.utils.Utils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;

import net.lingala.zip4j.exception.ZipException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Date;
import java.util.ArrayList;


public class MyLibraryActivity extends Activity implements 
	OnClickListener,
		DownloadTaskCompleteListener {

	private File filePath;
	private String currentAppPath;
	private Button btnDeleteIssue, btnStore, btnMyLibrary;
	private GridView layoutForHorizontalList;
	private StoreProductBean storeProductBean;
	URL url;
	boolean isDownload = false;
	private DBAdapter dbAdapter = null;
	private DBAdapterIssues dbAdapterIssues = null;
	
	private String internalID;
	
	public ArrayList<StoreProductBean> issues;

	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_library);
		
		dbAdapter = new DBAdapter(this);
		dbAdapterIssues = new DBAdapterIssues(this);
		
		btnStore = (Button) findViewById(R.id.btn_store);
		btnMyLibrary = (Button) findViewById(R.id.btn_librarys);
		
		layoutForHorizontalList = (GridView) findViewById(R.id.layoutForHorizontalList);
		
		btnMyLibrary.setOnClickListener(this);
		btnStore.setOnClickListener(this);
				
		btnStore.setSelected(true);
		
		currentAppPath = Constant.getAppFilepath(MyLibraryActivity.this) + "/";
		filePath = new File(currentAppPath);
		issues = (ArrayList<StoreProductBean>) getIntent().getSerializableExtra("issues");
		isDownload = getIntent().getBooleanExtra("isDownload", false);
		if(isDownload){
			setMyLibraryUI();
			Bundle b = new Bundle();
			b =  getIntent().getExtras();
			storeProductBean = (StoreProductBean) b.getSerializable("storeProductBean");
			downloadIssue(storeProductBean);
		}else{
			setMyLibraryUI();
		}	
	}
	
	@Override
	  public void onStart() {
	    super.onStart();

		GoogleAnalytics.getInstance(this).reportActivityStart(this);
		ManagerGoogleAnalytics.getInstance().sendVisitScreen(this, getString(R.string.libraryTrackedName));
	  }
	
	@Override
	  public void onStop() {
	    super.onStop();

		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	  }
	
	
	public void downloadIssue(StoreProductBean storeProductBean) {
		try {
			String stat = "&stats=true&stat_type=2&device="+Build.MODEL+"&software=Android "+Build.VERSION.RELEASE+"&id_issue="+storeProductBean.getEdition()+"&payment_type=1";
			stat=stat.replaceAll(" ", "%20");
			url = new URL(getString(R.string.API_URL)+"&key="+getString(R.string.API_KEY) + "&action=pagesmd&module=magazine&edition=" + storeProductBean.getEdition() + stat);
			//"&stats=true&stat_type=2&device="+Build.MODEL+"&software=Android"+Build.VERSION.SDK_INT+"d&id_issue="+storeProductBean.getEdition()+"&payment_type=1"
			/*
			Log.i("url","la url es: "+url.toString());
			
			IssueSyncTask issuetask = new IssueSyncTask(this, "Test");
			ArrayList<CurrentIssueBean> currentIssueBeanList = issuetask.execute(url).get();*/
			
			internalID = ""+System.currentTimeMillis()+storeProductBean.getEditionId();
			
			//save download in google analytics
			ManagerGoogleAnalytics.getInstance().sendEvent(this,getString(R.string.downloadTrackedName), storeProductBean.getEditionName(), null, null);
			
			//Save download in parse
			ParseObject download = new ParseObject("downloads");
			download.put("app_version", Float.parseFloat(this.getPackageManager()
				    .getPackageInfo(this.getPackageName(), 0).versionName));
			download.put("device", Build.MODEL);
			download.put("installation_id", ParseInstallation.getCurrentInstallation().getInstallationId());
			download.put("download_start", (new Date(System.currentTimeMillis())));
			download.put("id_app", getResources().getInteger(R.integer.id_app));
			download.put("id_currency", getResources().getInteger(R.integer.id_currency));
			download.put("id_customer", getResources().getInteger(R.integer.id_customer));
			download.put("id_partner", getResources().getInteger(R.integer.id_partner));
			download.put("id_publication", getResources().getInteger(R.integer.id_publication));
			if(Constant.test==1)
				download.put("test", 1);
			download.put("internalID", internalID);
			download.put("id_issue", storeProductBean.getEditionId());
			download.put("platform", "Android");

			if(storeProductBean.isSubscriptionActive(this))
			{
				download.put("price_customer", (storeProductBean.getPrice().equalsIgnoreCase("null"))?0:Float.parseFloat(storeProductBean.getPrice()));
				download.put("price_publisher", (storeProductBean.getPublisher_price().equalsIgnoreCase("null"))?0: Float.parseFloat(storeProductBean.getPublisher_price()));
				//Recuperar el id_subscription y si existe:
					//download.put("id_subscription",id_subscription);
			}
			else
			{
				download.put("price_customer", (storeProductBean.getPrice().equalsIgnoreCase("null"))?0:Float.parseFloat(storeProductBean.getPrice()));
				download.put("price_publisher", (storeProductBean.getPublisher_price().equalsIgnoreCase("null"))?0: Float.parseFloat(storeProductBean.getPublisher_price()));
			}

			download.put("free_issue",storeProductBean.isFree());

			if(ParseUser.getCurrentUser()!=null)
				download.put("id_user", ParseUser.getCurrentUser().getObjectId());

			download.saveEventually();
			
			ArrayList<CurrentIssueBean> currentIssueBeanList = new ArrayList<CurrentIssueBean>();
			ArrayList<String> urlList = new ArrayList<String>();
			urlList.add(Constant.CLOUD_S3_URL+"/issues/"+storeProductBean.getEditionId()+"_standard.zip'"+storeProductBean.getEditionId()+"_standard.zip");
			
			Thread trd = new Thread(new Runnable(){
				  @Override
				  public void run(){
				    //code to do the HTTP request
						try {
							DefaultHttpClient client = new DefaultHttpClient();
							HttpGet httpGet = new HttpGet(url.toString());
							HttpResponse execute = client.execute(httpGet);
							
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				  }
				});
			trd.start();
			
			/*urlList.add(storeProductBean.getdownloadPDF()+"'"+storeProductBean.getPdfName());//Download the pdf
			Log.i("elemento","se van a descargar: "+currentIssueBeanList.size()+" elementos");
			for (int i = 0; i < currentIssueBeanList.size(); i++) {
				CurrentIssueBean currentIssuBean = (CurrentIssueBean) currentIssueBeanList.get(i);
				//urlList.add(currentIssuBean.getImageURL()+"'"+currentIssuBean.getImageName());//Images as pages
				urlList.add(currentIssuBean.getThmbnailURL()+"'"+currentIssuBean.getImageName());//Download thumbnails
				/*if(currentIssuBean.getSummery().equalsIgnoreCase("true")){
					dbAdapter.open();
					dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
							"summery", currentIssuBean.getSummery(), currentIssuBean.getSortNumber());
					dbAdapter.close();
				}*/
				/*Log.i("descarga","thumb: "+currentIssuBean.getThmbnailURL()+"'"+currentIssuBean.getImageName());
				if (currentIssuBean.getAudioList() != null) {
					ArrayList<AudioBean> audioBean = (ArrayList<AudioBean>) currentIssuBean.getAudioList();
					
					dbAdapter.open();
					for(int j=0;j<audioBean.size();j++){
						urlList.add(audioBean.get(j).getUrl()+"'"+audioBean.get(j).getFileName()+"!"+audioBean.get(j).getSort());
						dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
								"audio", audioBean.get(j).getFileName(), currentIssuBean.getSortNumber(), String.valueOf(audioBean.get(j).getStartX()),
								String.valueOf(audioBean.get(j).getStartY()), String.valueOf(audioBean.get(j).getEndX()), String.valueOf(audioBean.get(j).getEndY()),
								audioBean.get(j).getAuto(), false, 0, 2);
					}
					dbAdapter.close();
				}
				if(currentIssuBean.getAudioPremiumList() != null){
					ArrayList<AudioPremiumBean> audioPremiumBean = (ArrayList<AudioPremiumBean>)currentIssuBean.getAudioPremiumList();
					dbAdapter.open();
					for(int j=0;j<audioPremiumBean.size();j++){
						dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
								"audios_premium", audioPremiumBean.get(j).getUrl(), currentIssuBean.getSortNumber(), String.valueOf(audioPremiumBean.get(j).getStartX()),
								String.valueOf(audioPremiumBean.get(j).getStartY()), String.valueOf(audioPremiumBean.get(j).getEndX()), String.valueOf(audioPremiumBean.get(j).getEndY()),
								audioPremiumBean.get(j).getAuto(), false, 0, 2);
					}
					dbAdapter.close();
				}
				if (currentIssuBean.getVideoList() != null) {
					ArrayList<VideoBean> videoBean = (ArrayList<VideoBean>) currentIssuBean.getVideoList();
					
					dbAdapter.open();
					for(int j=0;j<videoBean.size();j++){
						
						if(videoBean.get(j).getType()==2){
							urlList.add(videoBean.get(j).getUrl()+"'"+videoBean.get(j).getFileName()+"!"+videoBean.get(j).getSort());
							dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
									"video", videoBean.get(j).getFileName(), currentIssuBean.getSortNumber(), String.valueOf(videoBean.get(j).getStartX()),
									String.valueOf(videoBean.get(j).getStartY()), String.valueOf(videoBean.get(j).getEndX()), String.valueOf(videoBean.get(j).getEndY()),
									videoBean.get(j).getAuto(), videoBean.get(j).getFullscreen(), videoBean.get(j).getControls(), videoBean.get(j).getType());
						}
						else if(videoBean.get(j).getType()==1){
							dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
									"video", videoBean.get(j).getUrl(), currentIssuBean.getSortNumber(), String.valueOf(videoBean.get(j).getStartX()),
									String.valueOf(videoBean.get(j).getStartY()), String.valueOf(videoBean.get(j).getEndX()), String.valueOf(videoBean.get(j).getEndY()),
									videoBean.get(j).getAuto(), videoBean.get(j).getFullscreen(), videoBean.get(j).getControls(), videoBean.get(j).getType());
						}
						
					}
					dbAdapter.close();
				}
				if (currentIssuBean.getLinkList() != null) {
					ArrayList<LinkBean> linkBean = (ArrayList<LinkBean>) currentIssuBean.getLinkList();
//					urlList.add(linkBean.get(0).getUrl());
					
					dbAdapter.open();
					for(int j=0;j<linkBean.size();j++){
						dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
								"link", linkBean.get(j).getUrl(), currentIssuBean.getSortNumber(), String.valueOf(linkBean.get(j).getStartX()),
								String.valueOf(linkBean.get(j).getStartY()), String.valueOf(linkBean.get(j).getEndX()), String.valueOf(linkBean.get(j).getEndY()),
								false, false, linkBean.get(j).getControls(), linkBean.get(j).getType());
					}
					dbAdapter.close();
				}
			}*/
			String[] downloadURLList = new String[urlList.size()]; 
			for (int i = 0; i < urlList.size(); i++) {
				downloadURLList[i] = urlList.get(i);
			}
			new DownloadFileAsync(MyLibraryActivity.this, this, currentIssueBeanList, storeProductBean.getEditionId()).execute(downloadURLList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	@Override
	public void onTaskComplete(Boolean result, String method) {

		//Send finish time to parse
		ParseObject download_log = new ParseObject("downloads_logs");
		download_log.put("download_finished", (new Date(System.currentTimeMillis())));
		download_log.put("internalID", internalID);
		download_log.put("filesize", Integer.parseInt(method));
		if(Constant.test==1)
			download_log.put("test", Constant.test);
		
		download_log.saveEventually();
		
		unzip();
		setMyLibraryUI();
	}
	
	public void unzip(){
		String source = Constant.getAppFilepath(MyLibraryActivity.this) + "/"+URLEncoder.encode(storeProductBean.getEditionId())+"/"+storeProductBean.getEditionId()+"_standard.zip";
		String destination = Constant.getAppFilepath(MyLibraryActivity.this) + "/" +URLEncoder.encode(storeProductBean.getEditionId())+"/";
		
		File pdfDir = new File(destination+"PDF");
		File extraDir = new File(destination+"Extra");
		
		pdfDir.mkdir();
		extraDir.mkdir();
		
		File jsonFile = null;//save the json file
		
		try{
			//ZipFile zipFile = new ZipFile(source);
			net.lingala.zip4j.core.ZipFile zipFile = new net.lingala.zip4j.core.ZipFile(source);
			if (zipFile.isEncrypted()) {
				//Log.i("pass",""+Constant.passZipA+storeProductBean.getEditionId()+Constant.passZipB);
				zipFile.setPassword(getString(R.string.passZipA)+storeProductBean.getEditionId()+getString(R.string.passZipB));
			}
			zipFile.extractAll(destination);
				
		} catch (ZipException e) {

			e.printStackTrace();
		}
		
		
		ArrayList<File> files = new ArrayList<File>();
		File file = new File(destination);
		for(int i=0;i<file.list().length;i++){
			File f = new File(file.listFiles()[i].getPath());
			files.add(f);
			Log.i("file",f.getName());
		}
			
		for(int i=0;i<files.size();i++){
			File f = files.get(i);
			if(f.isDirectory()){
				if(f.getName().equals("thumbnails")){
					String str = f.toString();
					Log.i("original",str);
					str=str.replace("thumbnails", "Thumb");
					Log.i("remplazado",str);
					File newName = new File(str);
					f.renameTo(newName);
				}
			}
			
			//if(!f.getName().equals("cover") && !f.getName().endsWith(".json") && !f.getName().equals("PDF") && !f.getName().equals("Extra")){
				/*if(f.getName().equals("thumbnails") && f.isDirectory()){
					String str = f.toString();
					Log.i("original",str);
					str=str.replace("thumbnails", "Thumb");
					Log.i("remplazado",str);
					File newName = new File(str);
					f.renameTo(newName);
				}*/
			else if(f.getName().endsWith(".json")){
				jsonFile=f;
			}
			else if(f.getName().endsWith(".pdf")){
				String str = f.toString();
				Log.i("original",str);
				str=str.replace(f.getName(), "PDF/"+f.getName());
				Log.i("remplazado",str);
				File newName = new File(str);
				f.renameTo(newName);
			}
			else if(f.getName().endsWith(".zip")){
				f.delete();
			}
			else{
				String str = f.toString();
				Log.i("original",str);
				str=str.replace(f.getName(), "Extra/"+f.getName());
				Log.i("remplazado",str);
				File newName = new File(str);
				f.renameTo(newName);
			}
			//}
		}
		
		String str = "";
		try {
			str = getStringFromFile(jsonFile.getPath());
			Log.i("str","leido de archivo: "+str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ArrayList<CurrentIssueBean> currentIssueList = StoreProductUtils.getCurrentIssueList(str);
		saveExtra(currentIssueList);
		jsonFile.delete();
	}
	
	public void saveExtra(ArrayList<CurrentIssueBean> currentIssueBeanList){
		
		
		for (int i = 0; i < currentIssueBeanList.size(); i++) {  
			CurrentIssueBean currentIssuBean = (CurrentIssueBean) currentIssueBeanList.get(i);
			//urlList.add(currentIssuBean.getImageURL()+"'"+currentIssuBean.getImageName());//Images as pages
			/*if(currentIssuBean.getSummery().equalsIgnoreCase("true")){
				dbAdapter.open();
				dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
						"summery", currentIssuBean.getSummery(), currentIssuBean.getSortNumber());
				dbAdapter.close();
			}*/
			if (currentIssuBean.getAudioList() != null) {
				ArrayList<AudioBean> audioBean = (ArrayList<AudioBean>) currentIssuBean.getAudioList();
				
				dbAdapter.open();
				for(int j=0;j<audioBean.size();j++){
					dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
							"audio", audioBean.get(j).getFileName(), currentIssuBean.getSortNumber(), String.valueOf(audioBean.get(j).getStartX()),
							String.valueOf(audioBean.get(j).getStartY()), String.valueOf(audioBean.get(j).getEndX()), String.valueOf(audioBean.get(j).getEndY()),
							audioBean.get(j).getAuto(), false, 0, 2, null, audioBean.get(j).getTitle());
				}
				dbAdapter.close();
			}
			if (currentIssuBean.getVideoList() != null) {
				ArrayList<VideoBean> videoBean = (ArrayList<VideoBean>) currentIssuBean.getVideoList();
				
				dbAdapter.open();
				for(int j=0;j<videoBean.size();j++){
					
					if(videoBean.get(j).getType()==2){
						dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
								"video", videoBean.get(j).getFileName(), currentIssuBean.getSortNumber(), String.valueOf(videoBean.get(j).getStartX()),
								String.valueOf(videoBean.get(j).getStartY()), String.valueOf(videoBean.get(j).getEndX()), String.valueOf(videoBean.get(j).getEndY()),
								videoBean.get(j).getAuto(), videoBean.get(j).getFullscreen(), videoBean.get(j).getControls(), videoBean.get(j).getType(), videoBean.get(j).getOwner(), videoBean.get(j).getTitle());
					}
					else if(videoBean.get(j).getType()==1){
						dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
								"video", videoBean.get(j).getUrl(), currentIssuBean.getSortNumber(), String.valueOf(videoBean.get(j).getStartX()),
								String.valueOf(videoBean.get(j).getStartY()), String.valueOf(videoBean.get(j).getEndX()), String.valueOf(videoBean.get(j).getEndY()),
								videoBean.get(j).getAuto(), videoBean.get(j).getFullscreen(), videoBean.get(j).getControls(), videoBean.get(j).getType(), videoBean.get(j).getOwner(), videoBean.get(j).getTitle());
					}
					
				}
				dbAdapter.close();
			}
			if (currentIssuBean.getLinkList() != null) {
				ArrayList<LinkBean> linkBean = (ArrayList<LinkBean>) currentIssuBean.getLinkList();
//				urlList.add(linkBean.get(0).getUrl());
				
				dbAdapter.open();
				for(int j=0;j<linkBean.size();j++){
					dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
							"link", linkBean.get(j).getUrl(), currentIssuBean.getSortNumber(), String.valueOf(linkBean.get(j).getStartX()),
							String.valueOf(linkBean.get(j).getStartY()), String.valueOf(linkBean.get(j).getEndX()), String.valueOf(linkBean.get(j).getEndY()),
							false, false, linkBean.get(j).getControls(), linkBean.get(j).getType(), null,"");
				}
				dbAdapter.close();
			}
			if (currentIssuBean.getHtml5List() != null) {
				ArrayList<LinkBean> linkBean = (ArrayList<LinkBean>) currentIssuBean.getHtml5List();
//				urlList.add(linkBean.get(0).getUrl());
				
				dbAdapter.open();
				for(int j=0;j<linkBean.size();j++){
					dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
							"html5", linkBean.get(j).getUrl(), currentIssuBean.getSortNumber(), String.valueOf(linkBean.get(j).getStartX()),
							String.valueOf(linkBean.get(j).getStartY()), String.valueOf(linkBean.get(j).getEndX()), String.valueOf(linkBean.get(j).getEndY()),
							false, false, linkBean.get(j).getControls(), linkBean.get(j).getType(), null,"");
				}
				dbAdapter.close();
			}
			if (currentIssuBean.getGalleryList() != null) {
				ArrayList<GalleryBean> galleryBean = (ArrayList<GalleryBean>) currentIssuBean.getGalleryList();
				
				dbAdapter.open();
				for(int j=0;j<galleryBean.size();j++){
					dbAdapter.inserData(storeProductBean.getEditionId(), storeProductBean.getEditionName(), 
							"gallery", galleryBean.get(j).getUrl(), currentIssuBean.getSortNumber(), String.valueOf(galleryBean.get(j).getStartX()),
							String.valueOf(galleryBean.get(j).getStartY()), String.valueOf(galleryBean.get(j).getEndX()), String.valueOf(galleryBean.get(j).getEndY()),
							galleryBean.get(j).getAuto(), galleryBean.get(j).getFullscreen(), galleryBean.get(j).getControls(), galleryBean.get(j).getType(), null, "");
					
				}
				dbAdapter.close();
			}
		}
	}
	
	public static String convertStreamToString(InputStream is) throws Exception {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	      sb.append(line).append("\n");
	    }
	    return sb.toString();
	}

	public static String getStringFromFile (String filePath) throws Exception {
	    File fl = new File(filePath);
	    FileInputStream fin = new FileInputStream(fl);
	    String ret = convertStreamToString(fin);
	    //Make sure you close all streams.
	    fin.close();        
	    return ret;
	}
	
	public void setMyLibraryUI(){
		try {
			
			
			
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if(filePath.exists()){
				
				//Filter only downloaded magazines
				ArrayList<StoreProductBean> downloadedFiles = new ArrayList<StoreProductBean>();
//				for(int j=0;j<filePath.list().length;j++){
//					File auxFiles= filePath.listFiles()[j];
//					int total=0;
//					for(int s=0;s<auxFiles.list().length;s++){
//						if(auxFiles.listFiles()[s].isDirectory()){
//							File f = new File(auxFiles.listFiles()[s].getPath());
//							total += f.list().length;
//						}
//						else{
//							total++;
//						}
//					}
//					if(total>10){
//						downloadedFiles.add(auxFiles);
//					}
//				}
				
				for(int i=0;i<issues.size();i++){
					StoreProductBean bean = issues.get(i);
					System.out.println("Issue: "+bean.getEditionName()+" id: "+bean.getEditionId());
					
					if(btnMyLibrary.isSelected()){
						if(bean.isDownloaded(MyLibraryActivity.this)){
							downloadedFiles.add(bean);
						}
					}
					else{
						downloadedFiles.add(bean);
					}
				}
				
				//File[] files = downloadedFiles.toArray(new File[downloadedFiles.size()]);
				layoutForHorizontalList.setAdapter(new ImageAdapter(this, 0, downloadedFiles));
				
			}else{
				Toast.makeText(MyLibraryActivity.this, "Record not Found...", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {e.printStackTrace();}
	}
	
	public class ImageAdapter extends ArrayAdapter<StoreProductBean> {
		private Context mContext;
		private ArrayList<StoreProductBean> filteredIssues;
		
	    public ImageAdapter(Context context, int textViewResourceId, ArrayList<StoreProductBean> objects) {
			super(context, textViewResourceId, objects);
			
			mContext = context;
			filteredIssues = objects;
		}

	    // create a new txtGrid for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
			View gridView = convertView;

			final Context context = this.mContext;
			
			if (gridView == null) {
				LayoutInflater vi = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				gridView = vi.inflate(R.layout.my_library_placeholder, null);
			}
			
			final StoreProductBean bean = filteredIssues.get(position);
			
			//String cover_path = Environment.getExternalStorageDirectory() + "/" + Constant.PATH + "/"+mImageIds[position].getName()+"/cover/cover.jpg";
				
			ImageView imgGrid = (ImageView) gridView.findViewById(R.id.imgMagazineIssue);
			//imgGrid.setImageURI(Uri.parse(cover_path));
			
			ImageDownLoader.download(bean.getCoverUrl(), imgGrid, MyLibraryActivity.this, URLEncoder.encode(bean.getEditionId()));

			btnDeleteIssue = (Button) gridView.findViewById(R.id.btnDelete);
			
			if(bean.isDownloaded(MyLibraryActivity.this)){
				btnDeleteIssue.setText(R.string.read);
				btnDeleteIssue.setBackgroundResource(R.drawable.btn_read);	
			}else{

				if (Utils.isUserAdmin(getApplicationContext()))
					btnDeleteIssue.setText(R.string.download);
				else if (bean.getPrice().equalsIgnoreCase("null")) {
					btnDeleteIssue.setText(R.string.download);
				}else{
					if(bean.isSubscriptionActive(MyLibraryActivity.this))
						btnDeleteIssue.setText(R.string.subscribe);
					else
						btnDeleteIssue.setText(R.string.check_price);
				}
				btnDeleteIssue.setBackgroundResource(R.drawable.btn_buy);
			}
			
			//btnDeleteIssue.setTag(mImageIds[position].getName());
			//imgGrid.setTag(mImageIds[position].getName());
			
			imgGrid.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					if(btnMyLibrary.isSelected()){
						if(bean.isDownloaded(MyLibraryActivity.this)){
							File []fileList = (new File(currentAppPath+"/"+(String)v.getTag()+"/PDF")).listFiles();
							try{
								String totalFilePath = filePath.getAbsolutePath()+"/"+URLEncoder.encode(URLEncoder.encode(bean.getEditionId()))+"/PDF/"+bean.getPdfName();
								Uri uri = Uri.parse(totalFilePath);
								Intent intent = new Intent(context,MuPDFActivity.class);
								intent.putExtra("edition_id", bean.getEditionId());
								intent.setAction(Intent.ACTION_VIEW);
								intent.setData(uri);
								context.startActivity(intent);
							} catch (Exception e) {
								Log.i("Error","Problem with starting PDF-activity, path: "+filePath,e);
							}
						}
					}
				}
			});
			btnDeleteIssue.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					if(btnStore.isSelected()){
						if(bean.isDownloaded(MyLibraryActivity.this)){
							String totalFilePath = filePath.getAbsolutePath()+"/"+URLEncoder.encode(URLEncoder.encode(bean.getEditionId()))+"/PDF/"+bean.getPdfName();
							Uri uri = Uri.parse(totalFilePath);
							Intent intent = new Intent(context,MuPDFActivity.class);
							intent.putExtra("edition_id", bean.getEditionId());
							intent.setAction(Intent.ACTION_VIEW);
							intent.setData(uri);
							context.startActivity(intent);
						}
						else{
							if (bean.getPrice().equalsIgnoreCase("null")) {
								storeProductBean=bean;
								downloadIssue(storeProductBean);
							}else{
								if(bean.isSubscriptionActive(MyLibraryActivity.this)){
									storeProductBean=bean;
									downloadIssue(storeProductBean);
								}else{
									Intent intent = new Intent(MyLibraryActivity.this,BuyIssueActivity.class);
				                	intent.putExtra("issue", bean);
				        			startActivity(intent);
								}
							}
							
						}
					}
					else{//delete
						ProgressDialog pDialog = new ProgressDialog(context);
						pDialog.setMessage(context.getResources().getString(R.string.loading));
					    pDialog.show();
						String path = currentAppPath + (String)v.getTag();
						File currentFile = new File(path);
						DeleteRecursive(currentFile);
						
						layoutForHorizontalList.removeAllViewsInLayout();
					    setMyLibraryUI();
						
						dbAdapter.open();
						dbAdapter.deleteData(URLDecoder.decode((String)v.getTag()));
						dbAdapter.close();
						
						pDialog.hide();
					}	
				}
			});
			
			return gridView;
	    }

	}
	
	public void DeleteRecursive(File fileOrDirectory) {
	    if (fileOrDirectory.isDirectory())
	        for (File child : fileOrDirectory.listFiles())
	            DeleteRecursive(child);

	    if(fileOrDirectory.getName().startsWith("cover"))
	    	return;
	    fileOrDirectory.delete();
	    
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_store:
			btnStore.setSelected(true);
			btnStore.setBackgroundResource(R.drawable.border_button_selected);
			btnStore.setTextColor(Color.BLACK);
			btnMyLibrary.setSelected(false);
			btnMyLibrary.setBackgroundResource(R.drawable.border_button);
			btnMyLibrary.setTextColor(Color.WHITE);
			layoutForHorizontalList.removeAllViewsInLayout();
			setMyLibraryUI();
			break;

		case R.id.btn_librarys:
			btnMyLibrary.setSelected(true);
			btnMyLibrary.setBackgroundResource(R.drawable.border_button_selected);
			btnMyLibrary.setTextColor(Color.BLACK);
			btnStore.setSelected(false);
			btnStore.setBackgroundResource(R.drawable.border_button);
			btnStore.setTextColor(Color.WHITE);
			layoutForHorizontalList.removeAllViewsInLayout();
			setMyLibraryUI();
			break;
		default:
			break;
		}
	}
	
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event)
//	{
//	    if ((keyCode == KeyEvent.KEYCODE_BACK))
//	    {
//	        finish();
//	    }
//	    return super.onKeyDown(keyCode, event);
//	}

}