package com.muevaelvolante.qiumagazine.activity;

import android.app.Activity;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.muevaelvolante.qiumagazine.R;


public class VideoActivity extends Activity {

	private static final String path = "http://api.qiusystem.com/media/videos/m1_GSM.mp4";
	private VideoView video;
	private MediaController ctlr;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		setContentView(R.layout.videoview);

		VideoView videoView = (VideoView) findViewById(R.id.video);
		MediaController mc = new MediaController(this);
		mc.setAnchorView(videoView);
		mc.setMediaPlayer(videoView);
		Uri video = Uri.parse(path);
		videoView.setMediaController(mc);
		videoView.setVideoURI(video);
		videoView.start();

	}
}
