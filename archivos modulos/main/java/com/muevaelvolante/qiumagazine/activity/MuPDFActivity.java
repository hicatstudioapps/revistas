package com.muevaelvolante.qiumagazine.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.android.vending.util.IabHelper;
import com.android.vending.util.IabResult;
import com.android.vending.util.Inventory;
import com.android.vending.util.Purchase;
import com.artifex.mupdf.MediaHolder;
import com.artifex.mupdf.MuPDFCore;
import com.artifex.mupdf.MuPDFPageAdapter;
import com.artifex.mupdf.MuPDFPageView;
import com.artifex.mupdf.OutlineActivityData;
import com.artifex.mupdf.PDFPreviewPagerAdapter;
import com.artifex.mupdf.SearchTaskResult;
import com.artifex.mupdf.view.DocumentReaderView;
import com.artifex.mupdf.LinkInfo;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.librelio.base.BaseActivity;
import com.librelio.lib.utils.PDFParser;
import com.librelio.task.TinySafeAsyncTask;
import com.librelio.view.HorizontalListView;
import com.librelio.view.ProgressDialogX;
import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.adapter.DBAdapter;
import com.muevaelvolante.qiumagazine.adapter.DBAdapterIssues;
import com.muevaelvolante.qiumagazine.utils.Constant;
import com.muevaelvolante.qiumagazine.utils.DownloadFileAsync;
import com.muevaelvolante.qiumagazine.utils.DownloadTaskCompleteListener;
import com.muevaelvolante.qiumagazine.utils.ManagerGoogleAnalytics;

import java.io.File;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

//TODO: remove preffix mXXXX from all properties this class
@SuppressLint("NewApi")
public class MuPDFActivity extends BaseActivity implements DownloadTaskCompleteListener {
	private static final String TAG = "MuPDFActivity";

	private static final int SEARCH_PROGRESS_DELAY = 200;
	private static final int WAIT_DIALOG = 0;
	private static final String FILE_NAME = "FileName";

	private MuPDFCore core;
	private String fileName;
	private int mOrientation;
	private String mEdition;
	private String mEditionID;
	
	private Uri[] mthumb;

	private int          mPageSliderRes;
	private boolean      buttonsVisible;
	private boolean      mTopBarIsSearch;

	private WeakReference<SearchTask> searchTask;
	private ProgressDialog dialog;

	private AlertDialog.Builder alertBuilder;
	private DocumentReaderView docView;

	private View         buttonsView;
	private EditText     mPasswordView;
	private TextView     mFilenameView;
//	private SeekBar      mPageSlider;
//	private TextView     mPageNumberView;
//	private ImageButton  mSearchButton;
//	private ImageButton  mCancelButton;
//	private ImageButton  mOutlineButton;
	
	public SeekBar audioBar;
	private MediaPlayer media;
	private LinearLayout audioView;
	private ImageButton playPauseAudio;
	private ImageButton closeAudio;
	private TextView currentTime;
	private TextView totalTime;
	private String audioFileString;
	private Timer audioTimer;
	
	
	private Button mHomeButton;
	private Button mThumbsButton;
	private Button mFavButton;
	private Button mShareButton;
	private Button mInfoButton;
	
	private ViewSwitcher mTopBarSwitcher;
// XXX	private ImageButton  mLinkButton;
	private ImageButton  mSearchBack;
	private ImageButton  mSearchFwd;
	private EditText     mSearchText;
	//private SearchTaskResult mSearchTaskResult;
	private final Handler mHandler = new Handler();
	private FrameLayout mPreviewBarHolder;
	private HorizontalListView mPreview;
	private MuPDFPageAdapter mDocViewAdapter;
	private SparseArray<LinkInfo[]> linkOfDocument;
	
	private Dialog fav;
	private Dialog thumbsDialog;
	private GridView layoutForHorizontalList;
	
	RelativeLayout mainLayout;
	
	private DBAdapterIssues dbAdapterIssues = null;
	
	IabHelper mHelper;
	
	public Uri[] getMthumb() {
		return mthumb;
	}

	public void setMthumb(Uri[] mthumb) {
		this.mthumb = mthumb;
	}
	
	public Uri thumbAtIndex(int index){
		return mthumb[index];
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		alertBuilder = new AlertDialog.Builder(this);
		fav = new Dialog(this, R.style.MyDialog);
		thumbsDialog = new Dialog(this, R.style.MyDialog);
		dbAdapterIssues = new DBAdapterIssues(this);
		dbAdapterIssues.open();
		Cursor c = dbAdapterIssues.getEditionById(getIntent().getStringExtra("edition_id"));
		if(c.getCount()>0)
			while(c.moveToNext()){
				mEdition=c.getString(0);
				mEditionID=c.getString(1);
			}
		c.close();
		dbAdapterIssues.close();
	
		System.out.println("El id de edition es: " + mEdition);
		
		core = getMuPdfCore(savedInstanceState);
	
		if (core == null) {
			return;
		}
	
		mOrientation = getResources().getConfiguration().orientation;

		if(mOrientation == Configuration.ORIENTATION_LANDSCAPE) {
			core.setDisplayPages(2);
		} else {
			core.setDisplayPages(1);
		}

		createUI(savedInstanceState);

		File folder = new File(Constant.getAppFilepath(MuPDFActivity.this) + "/" + URLEncoder.encode(getIntent().getStringExtra("edition_id")) + "/Thumb");
		File[] thumbList = folder.listFiles();

		HashMap<Integer, String> allFiles = new HashMap<Integer, String>();
		for (int i = 0; i < thumbList.length; i++) {
			String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_")+1, thumbList[i].getName().lastIndexOf("."));
			allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
		}

		/*mthumb = new Uri[allFiles.size()];
		for (int i = 0; i < allFiles.size(); i++) {
			mthumb[i] = Uri.parse(allFiles.get(i));
		}*/
		
		
		
	}

	
	@Override
	  public void onStart() {
	    super.onStart();

		GoogleAnalytics.getInstance(this).reportActivityStart(this);
		ManagerGoogleAnalytics.getInstance().sendVisitScreen(this, getString(R.string.readingTrackedName)+": "+getIntent().getStringExtra("name"));

	   }
	
	@Override
	  public void onStop() {
	    super.onStop();
	    // The rest of your onStop() code.
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	  }
	
	public DocumentReaderView getDocView() {
		return docView;
	}

	public void setDocView(DocumentReaderView docView) {
		this.docView = docView;
	}
	
	public String getmEdition() {
		return mEdition;
	}

	public void setmEdition(String mEdition) {
		this.mEdition = mEdition;
	}
	
	private void requestPassword(final Bundle savedInstanceState) {
		
		//Configure the password
		//Log.i("password",""+Constant.passA+mEditionID+Constant.passB);
		if (core.authenticatePassword(""+getString(R.string.passA)+mEditionID+getString(R.string.passB))) {
			createUI(savedInstanceState);
		} else {
			requestPassword(savedInstanceState);
		}
		
		/*mPasswordView = new EditText(this);
		mPasswordView.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
		mPasswordView.setTransformationMethod(new PasswordTransformationMethod());

		AlertDialog alert = alertBuilder.create();
		alert.setTitle(R.string.enter_password);
		alert.setView(mPasswordView);
		alert.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ok),
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (core.authenticatePassword(mPasswordView.getText().toString())) {
					createUI(savedInstanceState);
				} else {
					requestPassword(savedInstanceState);
				}
			}
		});
		alert.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		alert.show();*/
	}

	private MuPDFCore getMuPdfCore(Bundle savedInstanceState) {
		MuPDFCore core = null;
		if (core == null) {
			core = (MuPDFCore)getLastNonConfigurationInstance();

			if (savedInstanceState != null && savedInstanceState.containsKey(FILE_NAME)) {
				fileName = savedInstanceState.getString(FILE_NAME);
			}
		}
		if (core == null) {
			Intent intent = getIntent();
			if (Intent.ACTION_VIEW.equals(intent.getAction())) {
				Uri uri = intent.getData();
				if (uri.toString().startsWith("content://media/external/file")) {
					// Handle view requests from the Transformer Prime's file manager
					// Hopefully other file managers will use this same scheme, if not
					// using explicit paths.
					Cursor cursor = getContentResolver().query(uri, new String[]{"_data"}, null, null, null);
					if (cursor.moveToFirst()) {
						uri = Uri.parse(cursor.getString(0));
					}
				}

				core = openFile(Uri.decode(uri.getEncodedPath()));
				SearchTaskResult.recycle();
			}
			if (core != null && core.needsPassword()) {
				requestPassword(savedInstanceState);
				//return null;
			}
		}
		if (core == null) {
			AlertDialog alert = alertBuilder.create();
			
			alert.setTitle(R.string.open_failed);
			alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dismiss),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
			alert.show();
			return null;
		}
		return core;
	}

	private void createUI(Bundle savedInstanceState) {
		if (core == null)
			return;
		// Now create the UI.
		// First create the document view making use of the ReaderView's internal
		// gesture recognition
		docView = new DocumentReaderView(this, linkOfDocument) {

			@Override
			protected void onMoveToChild(View view, int i) {
				Log.d(TAG,"onMoveToChild id = "+i);
				

				if(mOrientation == Configuration.ORIENTATION_LANDSCAPE)
					ManagerGoogleAnalytics.getInstance().sendEvent(MuPDFActivity.this,getString(R.string.readingTrackedName)+": "+getIntent().getStringExtra("name"), getString(R.string.pageTrackedEventName), ""+(i*2)+"-"+((i*2)+1), null);
				else
					ManagerGoogleAnalytics.getInstance().sendEvent(MuPDFActivity.this,getString(R.string.readingTrackedName)+": "+getIntent().getStringExtra("name"), getString(R.string.pageTrackedEventName), ""+(i+1), null);
				
				if(media!=null){
					if(media.isPlaying())
						audioView.setVisibility(View.VISIBLE);
				}

				if(i-1>=0){
					MuPDFPageView pageViewPrevious = ((MuPDFPageView) docView.getViewAtIndex(i-1));
					if(pageViewPrevious!=null)
						pageViewPrevious.deleteMedia();
				}
				if(i+1<core.countPages()){
					MuPDFPageView pageViewNext = ((MuPDFPageView) docView.getViewAtIndex(i+1));
					if(pageViewNext!=null)
						pageViewNext.deleteMedia();
				}	
				
				if (core == null){
					return;
				} 
				MuPDFPageView pageView = (MuPDFPageView) docView.getDisplayedView();
				if(pageView!=null){
					pageView.cleanRunningLinkList();
					pageView.deleteMedia();//delete media
					pageView.loadMedia();
				}
				new ActivateAutoLinks().safeExecute(i);
				super.onMoveToChild(view, i);
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
				if (!isShowButtonsDisabled()) {
					//hideButtons();
				}
				return super.onScroll(e1, e2, distanceX, distanceY);
			}

			@Override
			protected void onContextMenuClick() {
				if (!buttonsVisible) {
					showButtons();
				} else {
					hideButtons();
				}
			}

			@Override
			protected void onBuy(String path) {
				MuPDFActivity.this.onBuy(path);
			}


		};
		mDocViewAdapter = new MuPDFPageAdapter(this, core, Constant.getAppFilepath(MuPDFActivity.this) + "/" + URLEncoder.encode(getIntent().getStringExtra("edition_id")) + "/Thumb");
		docView.setAdapter(mDocViewAdapter);

		// Make the buttons overlay, and store all its
		// controls in variables
		makeButtonsView();

		// Set up the page slider
		int smax = Math.max(core.countPages()-1,1);
		mPageSliderRes = ((10 + smax - 1)/smax) * 2;

		// Set the file-name text
		if((this.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE)
			mFilenameView.setText(getIntent().getStringExtra("name"));

		// Activate the seekbar
//		mPageSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//			public void onStopTrackingTouch(SeekBar seekBar) {
//				mDocView.setDisplayedViewIndex((seekBar.getProgress()+mPageSliderRes/2)/mPageSliderRes);
//			}
//
//			public void onStartTrackingTouch(SeekBar seekBar) {}
//
//			public void onProgressChanged(SeekBar seekBar, int progress,
//					boolean fromUser) {
//				updatePageNumView((progress+mPageSliderRes/2)/mPageSliderRes);
//			}
//		});

		// Activate the search-preparing button
		/*mSearchButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				searchModeOn();
			}
		});

		mCancelButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				searchModeOff();
			}
		});*/
		
		
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onBackPressed();
				
			}
		});
		
		mThumbsButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.i("thumbnail","thumbnail clicked");

				ManagerGoogleAnalytics.getInstance().sendEvent(MuPDFActivity.this,getString(R.string.readingTrackedName)+": "+getIntent().getStringExtra("name"), getString(R.string.thumbnailsTrackedName), null, null);
				
				String path = core.getFileDirectory().replace("/PDF", "/Thumb");
				Log.i("path","path is: "+path);
				File[] files = (new File(path)).listFiles();
				File[] thumbList = (new File(path)).listFiles();
				
				HashMap<Integer, String>allFiles = new HashMap<Integer, String>();
				for (int i = 0; i < thumbList.length; i++) {
					String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_")+1, thumbList[i].getName().lastIndexOf("."));
					allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
				}

				for (int i = 0; i < allFiles.size(); i++) {
					files[i] = new File(allFiles.get(i));
				}
				
				Display disp =((WindowManager)getSystemService(MuPDFActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
				
				thumbsDialog.setContentView(R.layout.thumbnail_view);
				//LinearLayout linear = (LinearLayout)findViewById(R.layout.thumbnail_view);
				//thumbsDialog.setContentView(linear);
				thumbsDialog.getWindow().setLayout(disp.getWidth(), disp.getHeight()-50);
				WindowManager.LayoutParams paramsInfo = thumbsDialog.getWindow().getAttributes();
				paramsInfo.y=50;
				thumbsDialog.getWindow().setAttributes(paramsInfo);
				
				layoutForHorizontalList = (GridView)thumbsDialog.findViewById(R.id.thumbsGrid);
				/*if(mOrientation == Configuration.ORIENTATION_LANDSCAPE)
					layoutForHorizontalList.setNumColumns(6);
				else
					layoutForHorizontalList.setNumColumns(4);*/
				
				//layoutForHorizontalList.setNumColumns((disp.getWidth()-60)/170);
				layoutForHorizontalList.setAdapter(new ImageAdapter(thumbsDialog.getContext(), files));
				thumbsDialog.show();
			}
		});
		
		mFavButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Display disp2 =((WindowManager)getSystemService(MuPDFActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
				int width = disp2.getWidth()/3;
			    int height = disp2.getHeight()/2;
				
			    if(disp2.getWidth()<600)
			    	width=disp2.getWidth()/2;
			    if(!((getContext().getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE))
			    	width=disp2.getWidth()/2;
			    	
				
				fav.setContentView(R.layout.favs_view);
				
				if(mOrientation==Configuration.ORIENTATION_LANDSCAPE)
					fav.getWindow().setLayout(height,width);
				else
					fav.getWindow().setLayout(width,height);
				fav.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
				//fav.getWindow().setAttributes(a)
				WindowManager.LayoutParams params = fav.getWindow().getAttributes();
				params.y=60;
				fav.getWindow().setAttributes(params);
				fav.show();
				
				final SharedPreferences preferences = getSharedPreferences("MyPreferences", MODE_PRIVATE);
				
				fav.findViewById(R.id.btn_addfav).setOnClickListener(new OnClickListener(){
					public void onClick(View v) {
						//IssueActivity.favClicked(preferences);
						
						//IssueActivity issue = new IssueActivity();
						//issue.favClicked(preferences, docView.getDisplayedViewIndex(), URLEncoder.encode(getIntent().getStringExtra("name")));
						
						if(Configuration.ORIENTATION_PORTRAIT==mOrientation)
							favClicked(preferences, docView.getDisplayedViewIndex(), URLEncoder.encode(getIntent().getStringExtra("name")));
						else
							favClicked(preferences, (docView.getDisplayedViewIndex()*2)-1, URLEncoder.encode(getIntent().getStringExtra("name")));
					}
				});
				
				
				File folder = new File(Constant.getAppFilepath(MuPDFActivity.this) + "/" + URLEncoder.encode(getIntent().getStringExtra("edition_id")) + "/Thumb");
				File[] thumbList = folder.listFiles();

				HashMap<Integer, String> allFiles = new HashMap<Integer, String>();
				for (int i = 0; i < thumbList.length; i++) {
					String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_")+1, thumbList[i].getName().lastIndexOf("."));
					allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
				}

				Uri[] thumbUriFiles = new Uri[allFiles.size()];
				for (int i = 0; i < allFiles.size(); i++) {
					thumbUriFiles[i] = Uri.parse(allFiles.get(i));
				}
				
				LinearLayout layout = (LinearLayout) fav.findViewById(R.id.scroll);
				LayoutInflater inflat = (LayoutInflater) getSystemService(fav.getContext().LAYOUT_INFLATER_SERVICE);
				
				for(int i=1;i<=thumbList.length;i++){
					
					int index = preferences.getInt(URLEncoder.encode(getIntent().getStringExtra("name"))+"_"+i, 0);
					if(index==1){
						View placeHolder = inflat.inflate(R.layout.favs_placeholder, null);
						placeHolder.setTag(i-1);
						ImageView img = (ImageView) placeHolder.findViewById(R.id.img_fav);
						img.setImageURI(thumbUriFiles[i-1]);
						TextView txt = (TextView) placeHolder.findViewById(R.id.txt_fav);
						txt.setText("" + (i));
						Log.i("info","Hago esto, index: "+URLEncoder.encode(getIntent().getStringExtra("name"))+"_"+(i-1));
						
						layout.addView(placeHolder);
						
						placeHolder.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								Integer id = (Integer) v.getTag();
								docView.setDisplayedViewIndex(id);
								//thumb.dismiss();
								
								/*if(((MuPDFPageView)(docView.getViewAtIndex(id-1)))!=null)
									((MuPDFPageView)(docView.getViewAtIndex(id-1))).deleteMedia();
								if(((MuPDFPageView)(docView.getViewAtIndex(id+1)))!=null)
									((MuPDFPageView)(docView.getViewAtIndex(id+1))).deleteMedia();
								
								if(((MuPDFPageView)(docView.getViewAtIndex(id)))!=null){
									((MuPDFPageView)(docView.getViewAtIndex(id))).deleteMedia();
									((MuPDFPageView)(docView.getViewAtIndex(id))).loadMedia();
								}*/
							}
						});
						
						Button b = (Button) placeHolder.findViewById(R.id.delete_fav);
						b.setTag(i);
						
						b.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Integer id = (Integer) v.getTag();
								Log.i("delete", "a borrar "+URLEncoder.encode(getIntent().getStringExtra("name"))+"_"+id);
								SharedPreferences.Editor editor = preferences.edit();
								editor.putInt(URLEncoder.encode(getIntent().getStringExtra("name"))+"_"+id, 0);
								editor.commit();								
								fav.hide();
								
							}
						});
					}
				}
				
				//IssueActivity issue = new IssueActivity();
				//issue.updateFavorites();
				
			}
		});
		
		mShareButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Dialog share = new Dialog(getContext());
				share.setContentView(R.layout.share_view);
				share.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
				//share.getWindow().set
				//fav.getWindow().setAttributes(a)
				WindowManager.LayoutParams params2 = share.getWindow().getAttributes();
				params2.y=60;
				share.getWindow().setAttributes(params2);
				share.show();
				
				
				 File folder = new File(Constant.getAppFilepath(MuPDFActivity.this) + "/" + URLEncoder.encode(getIntent().getStringExtra("edition_id")) + "/Thumb");
				 File[] thumbList = folder.listFiles();

				 HashMap<Integer, String> allFiles = new HashMap<Integer, String>();
				for (int i = 0; i < thumbList.length; i++) {
					String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_")+1, thumbList[i].getName().lastIndexOf("."));
					allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
				}

				Uri[] thumbUriFiles = new Uri[allFiles.size()];
				for (int i = 0; i < allFiles.size(); i++) {
					thumbUriFiles[i] = Uri.parse(allFiles.get(i));
				}
			 	final Uri image = thumbUriFiles[docView.getDisplayedViewIndex()];
				Log.i("Imagen a compartir"," " + image.getPath());
				
				share.findViewById(R.id.btn_twitter).setOnClickListener(new OnClickListener(){
					public void onClick(View v) {
						Log.i("share","Share page on twitter clicked");
						
						Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
					    shareIntent.setType("image/*");
					    shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.socialText) + " " + getString(R.string.socialLink));
						//shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(new File("/storage/emulated/0/Android/data/com.jetskinworld/N%C3%BAmero+12+-+Dic%2Fene+2012/Thumb/gand_20121219175417_n12-app_0.jpg")));
					    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(new File(image.getEncodedPath())));
						
					    final PackageManager pm = v.getContext().getPackageManager();
					    final List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
					    for (final ResolveInfo app : activityList) {
					      if ("com.twitter.android.PostActivity".equals(app.activityInfo.name)) {
					        final ActivityInfo activity = app.activityInfo;
					        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
					        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
					        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
					        shareIntent.setComponent(name);
					        v.getContext().startActivity(shareIntent);
					        break;
					      }
					      /*else{
					    	  AlertDialog alert = new AlertDialog.Builder(getContext()).create();
					    	  alert.setTitle(getContext().getResources().getString(R.string.alert));
					    	  alert.setMessage(getContext().getResources().getString(R.string.no_twitter_app));
					    	  alert.show();
					    	  share.hide();
					      }*/
					    }

					}
				});
				share.findViewById(R.id.btn_facebook).setOnClickListener(new OnClickListener(){
					public void onClick(View v) {
						Log.i("share","Share page on facebook clicked");
						
						Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
						shareIntent.setType("text/plain");
						shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.socialText) + " " + getString(R.string.socialLink));
						//shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "sdksdk");
						PackageManager pm = v.getContext().getPackageManager();
						List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
						for (final ResolveInfo app : activityList) {
						    if ((app.activityInfo.name).contains("facebook")) {
						        final ActivityInfo activity = app.activityInfo;
						        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
						        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
						        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |             Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
						        shareIntent.setComponent(name);
						        v.getContext().startActivity(shareIntent);
						        break;
						   }
						}
					}
				});
				share.findViewById(R.id.btn_mail).setOnClickListener(new OnClickListener(){
					public void onClick(View v) {
						Log.i("share","Share page by mail clicked");

						final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
	               
	                    //emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, Constant.mailText);
						
						emailIntent.setType("image/*");
	             
	                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.mailSubject));
	             
	                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.mailText) + " " + getString(R.string.socialLink));
	                    
	                    emailIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(new File(image.getEncodedPath())));
	                    
	                    //emailIntent.putExtra(android.content.Intent.EXTRA_STREAM, image);
	     
	                    v.getContext().startActivity(Intent.createChooser(emailIntent, "Send mail..."));
					}
				});
				share.findViewById(R.id.btn_print).setOnClickListener(new OnClickListener(){
					public void onClick(View v) {
						
						Log.i("share","Print page clicked");
						
						/*String intentToCheck = "org.androidprinting.intent.action.PRINT"; //can be any other intent
						final PackageManager packageManager = getPackageManager();
						final Intent intent = new Intent(intentToCheck);
						List list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
						final boolean isAvailable = list.size() > 0;*/
						
						//if(isAvailable){
							Intent i = new Intent("org.androidprinting.intent.action.PRINT");
							i.addCategory(i.CATEGORY_DEFAULT);
						    i.setDataAndType(Uri.fromFile(new File(image.getEncodedPath())), "image/*");
						    v.getContext().startActivity(i);
						//}
					}
				});
				
			}
		});
		
		mInfoButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showInfo();
				ManagerGoogleAnalytics.getInstance().sendEvent(MuPDFActivity.this,getString(R.string.readingTrackedName)+": "+getIntent().getStringExtra("name"), getString(R.string.infoTrackedName), null, null);
			}
		});

		// Search invoking buttons are disabled while there is no text specified
		mSearchBack.setEnabled(false);
		mSearchFwd.setEnabled(false);

		// React to interaction with the text widget
		mSearchText.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
				boolean haveText = s.toString().length() > 0;
				mSearchBack.setEnabled(haveText);
				mSearchFwd.setEnabled(haveText);

				// Remove any previous search results
				if (SearchTaskResult.get() != null && !mSearchText.getText().toString().equals(SearchTaskResult.get().txt)) {
					SearchTaskResult.recycle();
					docView.resetupChildren();
				}
			}
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {}
		});

		//React to Done button on keyboard
		mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE)
					search(1);
				return false;
			}
		});

		mSearchText.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER)
					search(1);
				return false;
			}
		});

		// Activate search invoking buttons
		mSearchBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				search(-1);
			}
		});
		mSearchFwd.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				search(1);
			}
		});

		/*if (core.hasOutline()) {
			mOutlineButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					OutlineItem outline[] = core.getOutline();
					if (outline != null) {
						OutlineActivityData.get().items = outline;
						Intent intent = new Intent(MuPDFActivity.this, OutlineActivity.class);
						startActivityForResult(intent, 0);
					}
				}
			});
		} else {
			mOutlineButton.setVisibility(View.GONE);
		}*/

		
		// Reenstate last state if it was recorded
		SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
		int orientation = prefs.getInt("orientation", mOrientation);
		
		//Load from first page or last page viewed
		//int pageNum=0;
		int pageNum = prefs.getInt("page"+fileName, 0);
		
		
		if(orientation == mOrientation)
			docView.setDisplayedViewIndex(0);
		else {
			if(orientation == Configuration.ORIENTATION_PORTRAIT) {
				docView.setDisplayedViewIndex((pageNum + 1) / 2);
			} else {
				docView.setDisplayedViewIndex((pageNum == 0) ? 0 : pageNum * 2 - 1);
			}
		}

		if (savedInstanceState == null || !savedInstanceState.getBoolean("ButtonsHidden", false)) {
			showButtons();
		}

		if(savedInstanceState != null && savedInstanceState.getBoolean("SearchMode", false)) {
			searchModeOn();
		}
				

		// Stick the document view and the buttons overlay into a parent view
		RelativeLayout layout = new RelativeLayout(this);
		layout.addView(docView);
		layout.addView(buttonsView);
//		layout.setBackgroundResource(R.drawable.tiled_background);
		//layout.setBackgroundResource(R.color.canvas);
		layout.setBackgroundColor(Color.BLACK);
		mainLayout=layout;
		setContentView(layout);
		
		if(savedInstanceState != null && savedInstanceState.getBoolean("AudioPlaying", false)){
			
			/*media = MediaPlayer.create(getContext(), Uri.fromFile(new File(savedInstanceState.getString("audioFile"))));
			audioBar.setMax(media.getDuration()/1000);
			media.start();
			playPauseAudio.setBackgroundResource(R.drawable.pause_audio_button);*/
			
			showAudioControls(new File(savedInstanceState.getString("audioFile")));
			media.seekTo(savedInstanceState.getInt("audioPosition"));
			if(!savedInstanceState.getBoolean("IsPlaying", false)){
				media.pause();
				playPauseAudio.setBackgroundResource(R.drawable.play_audio_button);
			}
		}
	}
	
	public void onBackPressed(){
		if(media!=null){
			media.stop();
			media=null;
		}
		super.onBackPressed();
	}

	public class ImageAdapter extends BaseAdapter {
		private Context mContext;
		private File[] mImageIds;
		
	    public ImageAdapter(Context context, File[] objects) {
			//super(context, textViewResourceId, objects);
			
			mContext = context;
			mImageIds = objects;
			Log.i("info","adapter"+objects[0].getAbsolutePath());
		}

	    // create a new txtGrid for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
			View gridView = convertView;

			Log.i("load","loading: "+position+" of "+mImageIds.length);
			final Context context = this.mContext;
			
			if (gridView == null) {
				LayoutInflater vi = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				gridView = vi.inflate(R.layout.thumbnail_placeholder, null);
			}

			TextView txtGrid = (TextView) gridView.findViewById(R.id.textThumbnail);
			txtGrid.setText(""+(position+1));

			ImageView imgGrid = (ImageView) gridView.findViewById(R.id.imageThumbnail);
			imgGrid.setImageURI(Uri.fromFile(mImageIds[position]));
			
			gridView.setTag(position);
			
			gridView.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Integer id = Integer.parseInt(v.getTag().toString());
					if(mOrientation == Configuration.ORIENTATION_LANDSCAPE)
						docView.setDisplayedViewIndex((id+1)/2);
					else
						docView.setDisplayedViewIndex(id);
					thumbsDialog.hide();
				}
			});
			
			return gridView;
	    }

		@Override
		public int getCount() {

			return mImageIds.length;
		}

		@Override
		public Object getItem(int position) {

			return position % mImageIds.length;
		}

		@Override
		public long getItemId(int position) {

			return position;
		}
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode >= 0)
			docView.setDisplayedViewIndex(resultCode);
		super.onActivityResult(requestCode, resultCode, data);
		
		Log.d("onResult", "result received");
		
		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d("onResult", "onActivityResult handled by IABUtil.");
        }
	}

	public Object onRetainNonConfigurationInstance() {
		MuPDFCore mycore = core;
		core = null;
		return mycore;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (fileName != null && docView != null) {
			outState.putString("FileName", fileName);

			// Store current page in the prefs against the file name,
			// so that we can pick it up each time the file is loaded
			// Other info is needed only for screen-orientation change,
			// so it can go in the bundle
			SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
			SharedPreferences.Editor edit = prefs.edit();
			edit.putInt("page"+fileName, docView.getDisplayedViewIndex());
			edit.putInt("orientation", mOrientation);
			edit.commit();
		}

		if (!buttonsVisible)
			outState.putBoolean("ButtonsHidden", true);

		if (mTopBarIsSearch)
			outState.putBoolean("SearchMode", true);
		
		if(media!=null){
			//if(media.isPlaying()){
				outState.putBoolean("AudioPlaying", true);
				outState.putInt("audioPosition", media.getCurrentPosition());
				outState.putString("audioFile", audioFileString);
				if(media.isPlaying())
					outState.putBoolean("IsPlaying", true);
				
				PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
				if(powerManager.isScreenOn()){
					media.stop();
					media=null;
				}
				
				//media.stop();
				//media=null;
			//}
		}
		
		/*if(mHelper!=null){
			mHelper.dispose();
			mHelper=null;
		}*/
		
	}

	@Override
	protected void onPause() {
		super.onPause();

		killSearch();

		if (fileName != null && docView != null) {
			SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
			SharedPreferences.Editor edit = prefs.edit();
			edit.putInt("page"+fileName, docView.getDisplayedViewIndex());
			edit.putInt("orientation", mOrientation);
			edit.commit();
		}
	}
	
	@Override
	public void onDestroy() {
		if (core != null) {
			core.onDestroy();
		}
		core = null;

		super.onDestroy();
		mDocViewAdapter=null;
		dbAdapterIssues=null;
		fav=null;
		alertBuilder=null;
		thumbsDialog=null;
		core=null;
	}

	void showButtons() {
		if (core == null) {
			return;
		}
		if (!buttonsVisible) {
			buttonsVisible = true;
			// Update page number text and slider
			int index = docView.getDisplayedViewIndex();
			updatePageNumView(index);
//			mPageSlider.setMax((core.countPages()-1)*mPageSliderRes);
//			mPageSlider.setProgress(index*mPageSliderRes);
			if (mTopBarIsSearch) {
				mSearchText.requestFocus();
				showKeyboard();
			}

			Animation anim = new TranslateAnimation(0, 0, -mTopBarSwitcher.getHeight(), 0);
			anim.setDuration(200);
			anim.setAnimationListener(new Animation.AnimationListener() {
				public void onAnimationStart(Animation animation) {
					mTopBarSwitcher.setVisibility(View.VISIBLE);
					/*if(audioBar.getVisibility()==View.VISIBLE){
						Animation animAudio = new TranslateAnimation(0, 0, -mTopBarSwitcher.getHeight(), 0);
						animAudio.setDuration(200)
					}*/
				}
				public void onAnimationRepeat(Animation animation) {}
				public void onAnimationEnd(Animation animation) {}
			});
			mTopBarSwitcher.startAnimation(anim);
			
			if(audioView.getVisibility()==View.VISIBLE){
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)audioView.getLayoutParams();
				params.setMargins(0, mTopBarSwitcher.getHeight(), 0, 0);
				audioView.setLayoutParams(params);
				
				/*
				anim = new TranslateAnimation(0, 0, 0, mTopBarSwitcher.getHeight());
				anim.setDuration(200);
				//anim.setFillEnabled(true);
			    anim.setFillAfter(true);
				anim.setAnimationListener(new Animation.AnimationListener() {
					
					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
						//audioBar.setVisibility(View.VISIBLE);
						
					}
					
					public void onAnimationRepeat(Animation animation) {}
					public void onAnimationEnd(Animation animation) {
						//audioBar.setVisibility(View.VISIBLE);
						RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)audioView.getLayoutParams();
						params.setMargins(0, 20, 0, 0);
						audioView.setLayoutParams(params);
					}
				});
				
				audioView.startAnimation(anim);*/
			}
			
			// Update listView position
			mPreview.setSelection(docView.getDisplayedViewIndex());
			anim = new TranslateAnimation(0, 0, mPreviewBarHolder.getHeight(), 0);
			anim.setDuration(200);
			anim.setAnimationListener(new Animation.AnimationListener() {
				public void onAnimationStart(Animation animation) {
					mPreviewBarHolder.setVisibility(View.VISIBLE);
				}
				public void onAnimationRepeat(Animation animation) {}
				public void onAnimationEnd(Animation animation) {
				}
			});
			mPreviewBarHolder.startAnimation(anim);
		}
	}

	void hideButtons() {
		if (buttonsVisible) {
			buttonsVisible = false;
			hideKeyboard();

			Animation anim = new TranslateAnimation(0, 0, 0, -mTopBarSwitcher.getHeight());
			anim.setDuration(200);
			anim.setAnimationListener(new Animation.AnimationListener() {
				public void onAnimationStart(Animation animation) {}
				public void onAnimationRepeat(Animation animation) {}
				public void onAnimationEnd(Animation animation) {
					mTopBarSwitcher.setVisibility(View.INVISIBLE);
				}
			});
			mTopBarSwitcher.startAnimation(anim);
			
			if(audioView.getVisibility()==View.VISIBLE){
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)audioView.getLayoutParams();
				params.setMargins(0, 0, 0, 0);
				audioView.setLayoutParams(params);
				
				/*
				anim = new TranslateAnimation(0, 0, mTopBarSwitcher.getHeight(), 0);
				anim.setDuration(200);
				anim.setFillAfter(true);
				anim.setAnimationListener(new Animation.AnimationListener() {
					
					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
						//audioBar.setVisibility(View.VISIBLE);
						
					}
					
					public void onAnimationRepeat(Animation animation) {}
					public void onAnimationEnd(Animation animation) {
						RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)audioView.getLayoutParams();
						params.setMargins(0, 0, 0, 0);
						audioView.setLayoutParams(params);
					}
				});
				
				audioView.startAnimation(anim);*/
			}
			
			
			anim = new TranslateAnimation(0, 0, 0, this.mPreviewBarHolder.getHeight());
			anim.setDuration(200);
			anim.setAnimationListener(new Animation.AnimationListener() {
				public void onAnimationStart(Animation animation) {
					mPreviewBarHolder.setVisibility(View.INVISIBLE);
				}
				public void onAnimationRepeat(Animation animation) {}
				public void onAnimationEnd(Animation animation) {
				}
			});
			mPreviewBarHolder.startAnimation(anim);
		}
	}
	
	void favClicked(SharedPreferences preferences, int index, String issueName){
		Log.i("btn","button add pressed y index: "+index);
		//preferences = this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(issueName+"_"+(index+1), 1);
		
		Log.i("Guardaro","nombre: "+issueName+"_"+(index+1));
		/*int size = preferences.getInt(issueName+"_fav_size", 0);
		size++;
		editor.putInt(issueName+"_fav_size", size);*/
		editor.commit();
		

		ManagerGoogleAnalytics.getInstance().sendEvent(MuPDFActivity.this,getString(R.string.readingTrackedName)+": "+getIntent().getStringExtra("name"), getString(R.string.favoritesTrackedName), ""+(index+1), null);
		
		/*View placeHolder = layoutForHorizontalList.findViewWithTag(index);
		ImageView star = (ImageView) placeHolder.findViewById(R.id.favStar);
		star.setImageResource(R.drawable.fav_star);*/
		
		fav.hide();
		
	}
	
	void showInfo(){
		Log.i("cosa","click detectado");
		// TODO Auto-generated method stub
		Display display =((WindowManager)getSystemService(MuPDFActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
	    int width = display.getWidth();
	    int height = display.getHeight();
		
		Dialog ab = new Dialog(MuPDFActivity.this, R.style.InfoDialog);
		ab.setContentView(R.layout.info_layout);
		ab.getWindow().setLayout(width, height);
		//ab.getWindow().setGravity(Gravity.TOP|Gravity.LEFT);
		
		/*WindowManager.LayoutParams paramsInfo = ab.getWindow().getAttributes();
		paramsInfo.y=50;
		ab.getWindow().setAttributes(paramsInfo);*/
		
		
		/*RelativeLayout relativeLayout = new RelativeLayout(this);
		ImageView info = new ImageView(this);
		info.setImageResource(R.drawable.help_portrait_es);
		//TextView tv = new TextView(this);
        //tv.setText("Test");
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        //lp.addRule(RelativeLayout.ALIGN_RIGHT);
        //lp.addRule(RelativeLayout.ALIGN_TOP+50);
        lp.leftMargin=0;
        lp.topMargin=50;
        
        info.setLayoutParams(lp);
        relativeLayout.addView(info);
        
        mainLayout.addView(relativeLayout);*/
		
		if(mOrientation == Configuration.ORIENTATION_LANDSCAPE){
			//ab.getWindow().setLayout(height-60, width);
			ImageView img = (ImageView) ab.getWindow().findViewById(R.id.imgHelp);
			img.setImageResource(R.drawable.help_landscape_es);
		}
		
		ab.show();
	}

	void searchModeOn() {
		if (!mTopBarIsSearch) {
			mTopBarIsSearch = true;
			//Focus on EditTextWidget
			mSearchText.requestFocus();
			showKeyboard();
			mTopBarSwitcher.showNext();
		}
	}

	void searchModeOff() {
		if (mTopBarIsSearch) {
			mTopBarIsSearch = false;
			hideKeyboard();
			mTopBarSwitcher.showPrevious();
			SearchTaskResult.recycle();
			// Make the ReaderView act on the change to mSearchTaskResult
			// via overridden onChildSetup method.
			docView.resetupChildren();
		}
	}

	void updatePageNumView(int index) {
		if (core == null)
			return;
//		mPageNumberView.setText(String.format("%d/%d", index+1, core.countPages()));
	}

	void makeButtonsView() {
		buttonsView = getLayoutInflater().inflate(R.layout.buttons,null);
		mFilenameView = (TextView)buttonsView.findViewById(R.id.docNameText);
		mHomeButton = (Button)buttonsView.findViewById(R.id.btn_home);
		mThumbsButton = (Button)buttonsView.findViewById(R.id.btn_thumbnail);
		mFavButton = (Button)buttonsView.findViewById(R.id.btn_favorites);
		mShareButton = (Button)buttonsView.findViewById(R.id.btn_share);
		mInfoButton = (Button)buttonsView.findViewById(R.id.btn_info);
		
		audioBar = (SeekBar)buttonsView.findViewById(R.id.seekBar1);
		playPauseAudio = (ImageButton)buttonsView.findViewById(R.id.playButton);
		closeAudio = (ImageButton)buttonsView.findViewById(R.id.closeButton);
		audioView = (LinearLayout)buttonsView.findViewById(R.id.audioView);
		currentTime = (TextView)buttonsView.findViewById(R.id.audioCurrent);
		totalTime = (TextView)buttonsView.findViewById(R.id.audioDuration);
		
		
//		mPageSlider = (SeekBar)mButtonsView.findViewById(R.id.pageSlider);
		mPreviewBarHolder = (FrameLayout) buttonsView.findViewById(R.id.PreviewBarHolder);
		mPreview = new HorizontalListView(this);
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-1, -1);
		mPreview.setLayoutParams(lp);
		mPreview.setAdapter(new PDFPreviewPagerAdapter(this, core));
		mPreview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> pArg0, View pArg1,
					int position, long id) {

				hideButtons();
				docView.setDisplayedViewIndex((int)id);
			}

		});
		mPreviewBarHolder.addView(mPreview);
//		Gallery mGallery = (Gallery) mButtonsView.findViewById(R.id.PreviewGallery);
//		mGallery.setAdapter(new PDFPreviewPagerAdapter(this, core));

//		mPageNumberView = (TextView)mButtonsView.findViewById(R.id.pageNumber);
//		mSearchButton = (ImageButton)buttonsView.findViewById(R.id.searchButton);
//		mCancelButton = (ImageButton)buttonsView.findViewById(R.id.cancel);
		//mOutlineButton = (ImageButton)buttonsView.findViewById(R.id.outlineButton);
		mTopBarSwitcher = (ViewSwitcher)buttonsView.findViewById(R.id.switcher);
		mSearchBack = (ImageButton)buttonsView.findViewById(R.id.searchBack);
		mSearchFwd = (ImageButton)buttonsView.findViewById(R.id.searchForward);
		mSearchText = (EditText)buttonsView.findViewById(R.id.searchText);
// XXX		mLinkButton = (ImageButton)mButtonsView.findViewById(R.id.linkButton);
		mTopBarSwitcher.setVisibility(View.VISIBLE);
//		mPageNumberView.setVisibility(View.INVISIBLE);
//		mPageSlider.setVisibility(View.INVISIBLE);
		mPreviewBarHolder.setVisibility(View.VISIBLE);
	}

	void showKeyboard() {
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null)
			imm.showSoftInput(mSearchText, 0);
	}

	void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null)
			imm.hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);
	}

	void killSearch() {
		if (searchTask != null && null != searchTask.get()) {
			searchTask.get().cancel(true);
			searchTask = null;
		}
	}
	
	public void showBuyDialog(){
		AlertDialog.Builder buyDialog = new AlertDialog.Builder(getContext());
		buyDialog.setMessage(R.string.premiun_message);
		
		if(mHelper==null){
			String base64EncodedPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh5R+JjvQT8jcMCRVGY/jq11oQzjQXOmxfYq3hFGIo827u1o6vXVQ2UhWhYyN6gTfa4gcIFmAhiAzXCa3LpcpQGfnglSacZ90H1EskC1xfaNZ7uUas6oKJ7lEhJWJrhzt1GdUC3Fco1L2kdo9Q2w2ZyHznRZsZDnHpggODyM86Jaw8Jl+877+FK6NoAAwBI5Q7WZVx/AALgqOcCPVq1eDgX6tX0VzFDqfhEJGzqTtnC8MEnkkhkKPsCuco8x5wvjDy16PUV9S4aTOMOHmf7VVEzwY8KCGjrfMOBb6tEPziXjCRDT6QRLaImTnWEUwxoGaR7GGaU92/5m+KF1kCeTMKQIDAQAB";
			mHelper = new IabHelper(MuPDFActivity.this, base64EncodedPublicKey);
			mHelper.enableDebugLogging(true);
			mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
				
				@Override
				public void onIabSetupFinished(IabResult result) {

					Log.d("onCreate", "Problem setting up In-app Billing: " + result);
					
				}
			});
		}
		
		buyDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {

				//mHelper.queryInventoryAsync(mQueryFinishedListener);
			}
		});
		buyDialog.setPositiveButton(R.string.check_price, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				/*mHelper.launchPurchaseFlow(MuPDFActivity.this, "android.test.purchased", 10001, 
		                mPurchaseFinishedListener, "Premium");*/
				mHelper.queryInventoryAsync(mQueryFinishedListener);
				
			}
		});
		
		buyDialog.show();
	}
	
	@Override
	public void onTaskComplete(Boolean result, String method) {

		DBAdapter adapter = new DBAdapter(getContext());
		adapter.open();
		adapter.makePremiumAvailable(getIntent().getStringExtra("edition_id"));
		adapter.close();
	}
	
	public void downloadPremium(){
		System.out.println("Purchased");
		  
		  String[] downloadURLList=null;
			DBAdapter adapter = new DBAdapter(getContext());
			adapter.open();
			Cursor c = adapter.getPremiumData(getIntent().getStringExtra("edition_id"));
			if(c.getCount() > 0){
				downloadURLList = new String[c.getCount()];
				while(c.moveToNext()){
					String[] urlWithImage = c.getString(3).split("/");
					String filename = urlWithImage[urlWithImage.length-1];
					downloadURLList[c.getPosition()]=c.getString(3)+"'"+filename+"!"+c.getString(4);
						
				}
			}
			c.close();
			adapter.close();
			
			System.out.println(""+downloadURLList[0]);
			new DownloadFileAsync(MuPDFActivity.this, MuPDFActivity.this, null, getIntent().getStringExtra("edition_id")).execute(downloadURLList);
	}
	
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener 
	   = new IabHelper.OnIabPurchaseFinishedListener() {
		
		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase info) {

			System.out.println("Listener");
			
			  if (result.isFailure()) {
		         Log.d("onPurchase", "Error purchasing: " + result);
		         return;
		      }
		      else if(info.getSku().equals("android.test.purchased")){
		    	  System.out.println("You have purchased test item");
		    	  	/*ProgressDialog pDialog = new ProgressDialog(context);
					pDialog.setMessage(context.getResources().getString(R.string.startDownload));
				    pDialog.show();*/
		    	  downloadPremium();
		      }
		      else if(info.getSku().equals("com.qiumagazine.cuentosparaelanden.premium"+mEdition)){
		    	  downloadPremium();
		      }
			  
			  if(result.isSuccess()){
				  
			  }
		}
	};

	
	IabHelper.QueryInventoryFinishedListener 
	   mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
		
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {

			if (result.isFailure()) {
		    	  System.out.println("error");
		         return;
		       }
		      
		      /*if (inv.hasPurchase("android.test.purchased")) {

		    	  System.out.println("Lo consumo");
		            //mHelper.consumeAsync(inv.getPurchase("android.test.purchased"), null);
		    	  downloadPremium();
		    	  return;
		      }*/
		      if(inv.hasPurchase("com.qiumagazine.cuentosparaelanden.premium"+mEdition)){
		    	  downloadPremium();
		    	  return;
		      }

		       //String issuePrice = inventory.getSkuDetails("a08fe0404dc58153c5cb543543jhk34jh").getPrice();
		       
		       //Log.i("info","the price is: "+issuePrice);
		       

		       // update the UI 
		      
		      mHelper.launchPurchaseFlow(MuPDFActivity.this, "com.qiumagazine.cuentosparaelanden.premium"+mEdition, 10001, 
		                mPurchaseFinishedListener, "Premium");
			
		}
	};
	
	public void showAudioControls(File audioFile){
		audioView.setVisibility(View.VISIBLE);
		if(mTopBarSwitcher.getVisibility()==View.VISIBLE){
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)audioView.getLayoutParams();
			params.setMargins(0, mTopBarSwitcher.getHeight(), 0, 0);
			audioView.setLayoutParams(params);
		}
		else{
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)audioView.getLayoutParams();
			params.setMargins(0, 0, 0, 0);
			audioView.setLayoutParams(params);
		}
		audioFileString=audioFile.toString();
		
		System.out.println("Llego aqui");
		
		if(media==null){
			media = MediaPlayer.create(getContext(), Uri.fromFile(audioFile));
			audioBar.setMax(media.getDuration()/1000);
			media.start();
			playPauseAudio.setBackgroundResource(R.drawable.pause_audio_button);
			SimpleDateFormat df = new SimpleDateFormat("mm:ss");
			totalTime.setText(df.format(media.getDuration()));
		}
		
		System.out.println("Llego aqui 2");
		
		media.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {

				media.seekTo(0);
				media.pause();
				playPauseAudio.setBackgroundResource(R.drawable.play_audio_button);
			}
		});
		
		System.out.println("Llego aqui 3");
		
		playPauseAudio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				if(media.isPlaying()){
					media.pause();
					playPauseAudio.setBackgroundResource(R.drawable.play_audio_button);
				}
				else{
					media.start();
					playPauseAudio.setBackgroundResource(R.drawable.pause_audio_button);
				}
				
			}
		});
		
		closeAudio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				media.stop();
				media=null;
				audioView.setVisibility(View.INVISIBLE);
				
			}
		});
		
		audioBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if(fromUser)
					media.seekTo(progress*1000);
				
			}
		});
		
		/*audioTimer = new Timer();
		audioTimer.schedule(new TimerTask() {
			
			@Override
			public void run() {

				updateAudioBar();
			}
		}, 1000, 1000);*/
		
		updateAudioBar();
		
	}
	
	public void updateAudioBar(){
		
		
		if(media!=null){
			
			audioBar.setProgress(media.getCurrentPosition()/1000);
			SimpleDateFormat df = new SimpleDateFormat("mm:ss");
			currentTime.setText(df.format(media.getCurrentPosition()));
				
			mHandler.postDelayed(new Runnable() {
				  @Override
				  public void run() {
				    //Do something after 100ms
					  updateAudioBar();
				  }
				}, 1000);
		}
		
		
		
	}

	void search(int direction) {
		hideKeyboard();
		if (core == null)
			return;
		killSearch();

		final int increment = direction;
		final int startIndex = SearchTaskResult.get() == null ? docView.getDisplayedViewIndex() : SearchTaskResult.get().pageNumber + increment;

		SearchTask st = new SearchTask(this, increment, startIndex);
		st.safeExecute();
		searchTask = new WeakReference<MuPDFActivity.SearchTask>(st);
	}

	@Override
	public boolean onSearchRequested() {
		if (buttonsVisible && mTopBarIsSearch) {
			hideButtons();
		} else {
			showButtons();
			searchModeOn();
		}
		return super.onSearchRequested();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (buttonsVisible && !mTopBarIsSearch) {
			hideButtons();
		} else {
			showButtons();
			searchModeOff();
		}
		return super.onPrepareOptionsMenu(menu);
	}

	private MuPDFCore openFile(String path) {
		int lastSlashPos = path.lastIndexOf('/');
		fileName = new String(lastSlashPos == -1
					? path
					: path.substring(lastSlashPos+1));
		Log.d(TAG, "Trying to open " + path);
		PDFParser linkGetter = new PDFParser(MuPDFActivity.this,path);
		linkOfDocument = linkGetter.getLinkInfo();
		Log.d(TAG,"link size = "+linkOfDocument.size());
		for(int i=0;i<linkOfDocument.size();i++){
			Log.d(TAG,"page #" + (i + 1) + ": ");
			if(linkOfDocument.get(i)!=null){
				for(int j=0;j<linkOfDocument.get(i).length;j++){
					String link = linkOfDocument.get(i)[j].uri;
					Log.d(TAG,"link[" + j + "] = "+link);
					String local = "http://localhost";
					if(link.startsWith(local)){
						Log.d(TAG,"   link: "+link);
					}
				}
			}
		}
		try {
			core = new MuPDFCore(path);
			// New file: drop the old outline data
			OutlineActivityData.set(null);
		} catch (Exception e) {
			Log.e(TAG, "get core failed", e);
			return null;
		}
		return core;
	}

	private void onBuy(String path) {
		Log.d(TAG, "onBuy event path = " + path);
		/*MagazineManager magazineManager = new MagazineManager(getContext());
		Magazine magazine = magazineManager.findByFileName(path);
		if (null != magazine) {
			Intent intent = new Intent(getContext(), BillingActivity.class);
			intent
				.putExtra(DownloadActivity.FILE_NAME_KEY, magazine.getFileName())
				.putExtra(DownloadActivity.TITLE_KEY, magazine.getTitle())
				.putExtra(DownloadActivity.SUBTITLE_KEY, magazine.getSubtitle());
			getContext().startActivity(intent);
		}*/
	}

	private Context getContext() {
		return this;
	}

	private class ActivateAutoLinks extends TinySafeAsyncTask<Integer, Void, ArrayList<LinkInfo>> {

		@Override
		protected ArrayList<LinkInfo> doInBackground(Integer... params) {
			int page = params[0].intValue();
			Log.d(TAG, "Page = " + page);
			if (null != core) {
				LinkInfo[] links = core.getPageLinks(page);
				if(null == links){
					return null;
				}
				ArrayList<LinkInfo> autoLinks = new ArrayList<LinkInfo>();
				for (LinkInfo link : links) {
					Log.d(TAG, "activateAutoLinks link: " + link.uri);
					if (null == link.uri) {
						continue;
					}
					if (link.isMediaURI()) {
						if (link.isAutoPlay()) {
							autoLinks.add(link);
						}
					}
				}
				return autoLinks;
			}
			return null;
		}

		@Override
		protected void onPostExecute(final ArrayList<LinkInfo> autoLinks) {
			if (isCancelled() || autoLinks == null) {
				return;
			}
			docView.post(new Runnable() {
				public void run() {
					for(LinkInfo link : autoLinks){
						MuPDFPageView pageView = (MuPDFPageView) docView.getDisplayedView();
						if (pageView != null && null != core) {
							String basePath = core.getFileDirectory();
							MediaHolder mediaHolder = new MediaHolder(getContext(), link, basePath);
							pageView.addMediaHolder(mediaHolder, link.uri);
							pageView.addView(mediaHolder);
							mediaHolder.setVisibility(View.VISIBLE);
							mediaHolder.requestLayout();
						}
					}
				}
			});
		}
	}

	private class SearchTask extends TinySafeAsyncTask<Void, Integer, SearchTaskResult> {
		private final int increment; 
		private final int startIndex;
		private final ProgressDialogX progressDialog;
		
		public SearchTask(Context context, int increment, int startIndex) {
			this.increment = increment;
			this.startIndex = startIndex;
			progressDialog = new ProgressDialogX(context);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressDialog.setTitle(getString(R.string.searching_));
			progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				public void onCancel(DialogInterface dialog) {
					killSearch();
				}
			});
			progressDialog.setMax(core.countPages());

		}

		@Override
		protected SearchTaskResult doInBackground(Void... params) {
			int index = startIndex;

			while (0 <= index && index < core.countPages() && !isCancelled()) {
				publishProgress(index);
				RectF searchHits[] = core.searchPage(index, mSearchText.getText().toString());

				if (searchHits != null && searchHits.length > 0) {
					return SearchTaskResult.init(mSearchText.getText().toString(), index, searchHits);
				}

				index += increment;
			}
			return null;
		}

		@Override
		protected void onPostExecute(SearchTaskResult result) {
			if (isCancelled()) {
				return;
			}
			progressDialog.cancel();
			if (result != null) {
				// Ask the ReaderView to move to the resulting page
				docView.setDisplayedViewIndex(result.pageNumber);
			    SearchTaskResult.recycle();
				// Make the ReaderView act on the change to mSearchTaskResult
				// via overridden onChildSetup method.
			    docView.resetupChildren();
			} else {
				alertBuilder.setTitle(SearchTaskResult.get() == null ? R.string.text_not_found : R.string.no_further_occurences_found);
				AlertDialog alert = alertBuilder.create();
				alert.setButton(AlertDialog.BUTTON_POSITIVE, "Dismiss",
						(DialogInterface.OnClickListener)null);
				alert.show();
			}
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			progressDialog.cancel();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			progressDialog.setProgress(values[0].intValue());
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mHandler.postDelayed(new Runnable() {
				public void run() {
					if (!progressDialog.isCancelled())
					{
						progressDialog.show();
						progressDialog.setProgress(startIndex);
					}
				}
			}, SEARCH_PROGRESS_DELAY);
		}
	}

}
