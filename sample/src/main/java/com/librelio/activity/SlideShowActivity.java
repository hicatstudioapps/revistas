/**
 * 
 */
package com.librelio.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;

import com.librelio.base.BaseActivity;
import com.librelio.view.ImagePager;
import com.muevaelvolante.qiumagazine.R;


/**
 * @author Mike Osipov
 */
public class SlideShowActivity extends BaseActivity {
	private static final String TAG = "SlideShowActivity";
	
	private ImagePager imagePager;
	private Handler autoPlayHandler;
	
	private int autoPlayDelay;
	private boolean transition = true;
	private boolean autoPlay;
	private int bgColor;
	private String fullPath;
	private String []photos;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.sideshow_activity_layout);
		LinearLayout frame = (LinearLayout)findViewById(R.id.slide_show_full);
		
//		autoPlayDelay = 1000;
//		transition = true;
//		autoPlay = true;
//		bgColor = Color.BLUE;
		fullPath = getIntent().getExtras().getString("key");
		photos = getIntent().getExtras().getStringArray("photos");

		System.out.println("Path de fotos: "+fullPath);
		
		for(int i=0;i<photos.length;i++){
			System.out.println("foto recogida: "+photos[i]);
		}
		
		imagePager = new ImagePager(this, photos, true, 100);
		imagePager.post(new Runnable() {
			@Override
			public void run() {
				imagePager.setViewWidth(imagePager.getWidth());
			}
		});
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		lp.gravity = Gravity.CENTER;
		imagePager.setLayoutParams(lp);
		
		if(autoPlay) {
			autoPlayHandler = new Handler();
			autoPlayHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					Log.d(TAG, "autoPlayHandler start");
					imagePager.setCurrentPosition(imagePager.getCurrentPosition() + 1, transition);
					autoPlayHandler.postDelayed(this, autoPlayDelay);
				}}, autoPlayDelay);
		}
		
		imagePager.setBackgroundColor(bgColor);
		frame.addView(imagePager);
	}
}
