package com.muevaelvolante.qiumagazine.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;



import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.bean.CurrentIssueBean;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


public class IssueSyncTask  extends AsyncTask<URL, Void, ArrayList<CurrentIssueBean>>{

	
	private Context context;
	private ProgressDialog pDialog;
	String callTo;
	
	
	public IssueSyncTask(Context context, String callTo){
		this.context = context;
		this.callTo = callTo;
		pDialog = new ProgressDialog(context);
	}
	
	@Override
	protected ArrayList<CurrentIssueBean> doInBackground(URL... url) {
		ArrayList<CurrentIssueBean> currentIssueList = null;
		String response = "";
		try {
			
			DefaultHttpClient client = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url[0].toString());
			try {
				HttpResponse execute = client.execute(httpGet);
				InputStream content = execute.getEntity().getContent();

				BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
				String s = "";
				while ((s = buffer.readLine()) != null) {
					response += s;
				}
				if(response != null){
					Log.i("response","a�adiendo: "+response);
					currentIssueList = StoreProductUtils.getCurrentIssueList(response);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return currentIssueList;
	}

	protected void onPostExecute(ArrayList<CurrentIssueBean> result) {
		
		if(pDialog.isShowing())
			pDialog.dismiss();
	}

	@Override
	protected void onPreExecute() {
		 this.pDialog.setMessage(context.getResources().getString(R.string.loading));
	     this.pDialog.show();
	}
}
