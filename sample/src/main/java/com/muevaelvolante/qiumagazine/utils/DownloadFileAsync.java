package com.muevaelvolante.qiumagazine.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.bean.CurrentIssueBean;
import com.muevaelvolante.qiumagazine.calls.Icall;
import com.muevaelvolante.qiumagazine.service.ServiceGenerator;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DownloadFileAsync extends AsyncTask<String, String, Boolean> {

	private Context context;
	private ArrayList<CurrentIssueBean> currentIssueBeanList = new ArrayList<CurrentIssueBean>();
	private String issueName;
	private String pathDownload;
	private ProgressDialog progressDialog;
	private DownloadTaskCompleteListener cb;
	private int filesize;
	
	public DownloadFileAsync(Context context, DownloadTaskCompleteListener callback,ArrayList<CurrentIssueBean> currentIssueBeanList, String issueName){
		this.context = context;
		this.currentIssueBeanList = currentIssueBeanList;
		this.issueName = issueName;
		this.cb = callback;
		progressDialog = new ProgressDialog(context);
	}
    
    protected Boolean doInBackground(String... aurl) {
		final int[] count = new int[1];
		final boolean[] flag = {false};
		pathDownload = Constant.getAppFilepath(context) + "/" + URLEncoder.encode(issueName);
		final String[] selectionPath = {""};
		Log.i("path", "path es: " + pathDownload);
		for (int i = 0; i < aurl.length; i++) {
			try {
				String[] urlWithImage = aurl[i].split("'");
				Log.i("string", "aurl: " + aurl[i]);
				final String dwnUrl = urlWithImage[0];
				final String[] imageName = {urlWithImage[1]};
				Icall call = ServiceGenerator.createService(Icall.class);
				Call<ResponseBody> fileStream = call.downloadFileWithDynamicUrlAsync(dwnUrl);
				ResponseBody body= fileStream.execute().body();
				if (dwnUrl.contains("pdf")) {
					selectionPath[0] = "/PDF";
					Log.i("pdf", "el path de pdf: " + dwnUrl);
				} else if (dwnUrl.contains("page_edit")) {
					selectionPath[0] = "/PDF";
				} else if (dwnUrl.contains("small_thumbs")) {
					selectionPath[0] = "/Thumb";
				} else if (dwnUrl.contains("big_thumbs")) {
					selectionPath[0] = "/Thumb";
				} else if (dwnUrl.contains("media")) {
					String[] fileNameWithSort = imageName[0].split("!");
					String imagename = fileNameWithSort[0];
					selectionPath[0] = "/Extra";
					imageName[0] = imagename;
				}
				File path = new File(pathDownload + selectionPath[0]);
				if (!path.isDirectory()) {
					if (path.mkdirs()) ;
				}

				File outFile = new File(path, imageName[0]);
				Log.d("file d", outFile.getAbsolutePath());
				if (outFile.exists())
					outFile.delete();
				int fileLength = (int) body.contentLength();
				filesize = fileLength;

				InputStream input = new BufferedInputStream(body.byteStream());
				OutputStream output = null;

				output = new FileOutputStream(outFile);
				byte data[] = new byte[1024];
				long total = 0;
				while ((count[0] = input.read(data)) != -1) {
					total += count[0];
					output.write(data, 0, count[0]);
					publishProgress("" + (int) (total * 100 / fileLength));
				}

				output.flush();
				output.close();
				input.close();

				//progressDialog.setMax(aurl.length);
				publishProgress("" + i);
				flag[0] = true;

			} catch (Exception e) {
				flag[0] = false;
				return false;
			}
		}
		return true;

	}
  

	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@Override
	protected void onPreExecute() {
		progressDialog.setMessage(context.getResources().getString(R.string.downloading));
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
	 */
	@Override
	protected void onProgressUpdate(String... values) {
		// TODO Auto-generated method stub
		 progressDialog.setProgress(Integer.parseInt(values[0]));
	}

	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(Boolean result) {
		if(progressDialog.isShowing())
			progressDialog.dismiss();
		if(result) {
			cb.onTaskComplete(result, "" + filesize,progressDialog);
		}
	}

}
