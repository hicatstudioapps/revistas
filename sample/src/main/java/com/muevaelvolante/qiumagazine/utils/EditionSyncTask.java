package com.muevaelvolante.qiumagazine.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.bean.StoreProductBean;
import com.muevaelvolante.qiumagazine.calls.Icall;
import com.muevaelvolante.qiumagazine.service.ServiceGenerator;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;


public class EditionSyncTask extends AsyncTask<URL, Void, ArrayList<StoreProductBean>> {

	private Context context;
	private ProgressDialog pDialog;
	private AysnTaskCompleteListener<ArrayList<StoreProductBean>> cb = null;
	String callTo;
	String infoResponse;
	
	
	public EditionSyncTask(Context context, AysnTaskCompleteListener<ArrayList<StoreProductBean>> callback, String callTo){
		this.context = context;
		this.callTo = callTo;
		this.cb = callback;
		pDialog = new ProgressDialog(context);
	}
	
	@Override
	protected ArrayList<StoreProductBean> doInBackground(URL... url) {

		ArrayList<StoreProductBean> stroeProductBeanList = null;
		String response = "";

		try {

            DefaultHttpClient client = new DefaultHttpClient();
            HttpRequestBase httpRequest;

            if (Utils.isUserAdmin(context) && callTo.equalsIgnoreCase("EditionList")){

                /*
                 * NSString *post = [NSString stringWithFormat:@"&key=%@&action=issues&module=magazine",API_KEY];
                 */

                httpRequest = new HttpPost(context.getString(R.string.API_URL));
                httpRequest.addHeader("Accept-Encoding", "gzip");

                List<NameValuePair> params = new LinkedList<NameValuePair>();
                params.add(new BasicNameValuePair("key", context.getString(R.string.API_KEY)));
                params.add(new BasicNameValuePair("action", "issues"));
                params.add(new BasicNameValuePair("module", "magazine"));
                params.add(new BasicNameValuePair("preview", "true"));

                try {

                    ((HttpEntityEnclosingRequestBase) httpRequest).setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
					HttpResponse execute = client.execute(httpRequest);
					InputStream content = execute.getEntity().getContent();

					Header contentEncoding = execute.getFirstHeader("Content-Encoding");
					if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
						content = new GZIPInputStream(content);
					}

					BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
					String s = "";
					while ((s = buffer.readLine()) != null) {
						response += s;
					}
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            } else {
				Icall dataCall= ServiceGenerator.createService(Icall.class);
				String data= dataCall.getStringData(url[0].toString()).execute().body().string();
				if(data!= null)
					response=data;
				else
				response=null;
                httpRequest = new HttpGet(url[0].toString());
                httpRequest.addHeader("Accept-Encoding", "gzip");
            }

			try {
				if(response != null){
					if(callTo.equalsIgnoreCase("EditionList"))
						stroeProductBeanList = StoreProductUtils.getAllStoreProduct(context,response);
					else if(callTo.equalsIgnoreCase("InfoEditionList") || callTo.equalsIgnoreCase("menuList"))
						infoResponse=response;
						
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stroeProductBeanList;
	}

	protected void onPostExecute(ArrayList<StoreProductBean> result) {
		if(callTo.equalsIgnoreCase("EditionList"))
			cb.onTaskComplete(result, callTo);
		else if(callTo.equalsIgnoreCase("InfoEditionList") || callTo.equalsIgnoreCase("menuList"))
			cb.onTaskComplete(infoResponse, callTo);
		if(pDialog.isShowing())
			pDialog.dismiss();
		
	}

	@Override
	protected void onPreExecute() {
//		 this.pDialog.setMessage(context.getResources().getString(R.string.loading));
//	     this.pDialog.show();
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
	}

}
