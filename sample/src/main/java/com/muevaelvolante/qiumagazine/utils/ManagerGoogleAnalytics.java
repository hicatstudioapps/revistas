package com.muevaelvolante.qiumagazine.utils;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.muevaelvolante.qiumagazine.R;

import java.util.Map;

/**
 * Created by joseluis on 21/01/2016.
 */
public class ManagerGoogleAnalytics {

    private static ManagerGoogleAnalytics mInstance;
    private Tracker mGaTracker;
    private GoogleAnalytics mGaInstance;

    private  ManagerGoogleAnalytics()
    {

    }

    public static ManagerGoogleAnalytics getInstance()
    {
        if(mInstance == null)
            mInstance = new ManagerGoogleAnalytics();

        return mInstance;
    }

    /**
     * Obtiene el Tracker general
     */
    public synchronized Tracker getAppTracker(Context c) {

        if ( mGaTracker == null) {

            mGaInstance = GoogleAnalytics.getInstance(c);
            mGaTracker = mGaInstance.newTracker(R.xml.analytics);

        }

        return mGaTracker;
    }


    /**
     * Envía la visita a una pantalla general.
     *
     * @param context
     * @param screenName
     */
    public synchronized void sendVisitScreen(Context context, String screenName) {

        Map<String, String> hit = new HitBuilders.ScreenViewBuilder().build();

        Tracker trackerGen = getAppTracker(context);

        if (trackerGen != null) {
            trackerGen.setScreenName(screenName);
            trackerGen.send(hit);
        }
    }

    public synchronized void sendEvent(Context context, String ui_action, String button_press, String play_button)
    {
        sendEvent(context,ui_action,button_press,play_button,null);
    }

    public synchronized void sendEvent(Context context, String ui_action, String button_press, String play_button, String option) {

        Map<String, String> hit = new HitBuilders.EventBuilder()
                .setAction(ui_action)
                .setCategory(button_press)
                .setLabel(play_button)
                .build();


        Tracker trackerGen = getAppTracker(context);

        if (trackerGen != null) {

          //  trackerGen.setScreenName(play_button);
            trackerGen.send(hit);
           /* trackerGen.send(MapBuilder
                            .createEvent(ui_action, button_press, play_button, null)
                            .build()
            );*/
        }
    }
}
