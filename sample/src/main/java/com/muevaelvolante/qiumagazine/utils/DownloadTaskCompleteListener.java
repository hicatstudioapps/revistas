package com.muevaelvolante.qiumagazine.utils;

import android.app.ProgressDialog;

public interface DownloadTaskCompleteListener {
	public void onTaskComplete(Boolean result, String method, ProgressDialog dialig);
}
