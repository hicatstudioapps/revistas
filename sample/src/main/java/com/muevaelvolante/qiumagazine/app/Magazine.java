package com.muevaelvolante.qiumagazine.app;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.multidex.MultiDexApplication;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.bean.StoreProductBean;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseInstallation;

import java.util.ArrayList;
import java.util.Locale;


public class Magazine extends MultiDexApplication {

    public static ArrayList<StoreProductBean> issues;
	
	@Override
    public void onCreate() {
        super.onCreate();
        ExceptionHandler.register(this,"Error revista","leoperezortiz@gmail.com");
        System.out.println("Initializing parse...");
        ParseCrashReporting.enable(this);
        Parse.initialize(this, getString(R.string.parseAppId), getString(R.string.parseClientKey));
        
        String code = Locale.getDefault().getLanguage();
		// Search for "sublocale".
		int index = code.indexOf("_");
	
		if (index != -1) {
			// If sublocale. 
			// cut sublocale
			code =  code.substring(0, index);
		}
		
		ParseInstallation.getCurrentInstallation().put("deviceLanguage", code);
        ParseInstallation.getCurrentInstallation().saveEventually();
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
//                .showImageOnLoading(R.drawable.empty)
//                .showImageOnFail(R.drawable.empty)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                //.memoryCacheExtraOptions(480,480)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
	}

    public static int[] getScreenDimensions (Context context) {

        // Window Manager.
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        // Dimensiones de la pantalla
        DisplayMetrics displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        // Creamos el array.
        int[] height_width = {height, width};

        // Lo devolvemos.
        return height_width;

    }
}
