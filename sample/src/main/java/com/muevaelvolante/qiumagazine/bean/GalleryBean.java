package com.muevaelvolante.qiumagazine.bean;

public class GalleryBean {
	
	String idPage;
	String sort;
	String url;
	Boolean auto;
	Boolean fullscreen;
	int controls;
	int type;
	float startX;
	float startY;
	float endX;
	float endY;
	
	
	public Boolean getAuto() {
		return auto;
	}
	public void setAuto(Boolean auto) {
		this.auto = auto;
	}
	public Boolean getFullscreen() {
		return fullscreen;
	}
	public void setFullscreen(Boolean fullscreen) {
		this.fullscreen = fullscreen;
	}
	public int getControls() {
		return controls;
	}
	public void setControls(int controls) {
		this.controls = controls;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public float getStartX() {
		return startX;
	}
	public void setStartX(float startX) {
		this.startX = startX;
	}
	public float getStartY() {
		return startY;
	}
	public void setStartY(float startY) {
		this.startY = startY;
	}
	public float getEndX() {
		return endX;
	}
	public void setEndX(float endX) {
		this.endX = endX;
	}
	public float getEndY() {
		return endY;
	}
	public void setEndY(float endY) {
		this.endY = endY;
	}
	/**
	 * @return the idPage
	 */
	public String getIdPage() {
		return idPage;
	}
	/**
	 * @param idPage the idPage to set
	 */
	public void setIdPage(String idPage) {
		this.idPage = idPage;
	}
	/**
	 * @return the sort
	 */
	public String getSort() {
		return sort;
	}
	/**
	 * @param sort the sort to set
	 */
	public void setSort(String sort) {
		this.sort = sort;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
