package com.muevaelvolante.qiumagazine.bean;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.muevaelvolante.qiumagazine.utils.Constant;

import java.io.File;
import java.io.Serializable;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.SimpleDateFormat;


@SuppressWarnings("serial")
public class StoreProductBean implements Serializable{

	private String edition;
	private String editionId;
	private String editionName;
	private String issueDate;
	private String screenOrientation;
	private String lang;
	private String price;
	private String subscriptions;
	private String description;
	private String coverName;
	private String coverUrl;
	private String totalPage;
	private String downloadPDF;
	private String pdfName;
	private String text;
	private String publisher_price;
	private String reference_number;
    private String mode;


    public static final String MODE_PUBLISHED = "published";
    public static final String MODE_PREVIEW = "preview";



	public boolean isFree()
	{
		if(getPrice().equalsIgnoreCase("null") || getPrice().equals("0"))
			return true;
		else
			return false;
	}
	
	public String getPdfName() {
		return pdfName;
	}
	public void setPdfName(String pdfName) {
		this.pdfName = pdfName;
	}
	/**
	 * @return the edition
	 */
	public String getEdition() {
		return edition;
	}
	/**
	 * @param edition the edition to set
	 */
	public void setEdition(String edition) {
		this.edition = edition;
	}
	/**
	 * @return the editionId
	 */
	public String getEditionId() {
		return editionId;
	}
	/**
	 * @param editionId the editionId to set
	 */
	public void setEditionId(String editionId) {
		this.editionId = editionId;
	}
	/**
	 * @return the edtionName
	 */
	public String getEditionName() {
		return editionName;
	}
	/**

	 */
	public void setEditionName(String editionName) {
		this.editionName = editionName;
	}
	/**
	 * @return the issueDate
	 */
	public String getIssueDate() {
		return issueDate;
	}
	/**
	 * @param issueDate the issueDate to set
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	/**
	 * @return the screenOrientation
	 */
	public String getScreenOrientation() {
		return screenOrientation;
	}
	/**
	 * @param screenOrientation the screenOrientation to set
	 */
	public void setScreenOrientation(String screenOrientation) {
		this.screenOrientation = screenOrientation;
	}
	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}
	/**
	 * @param lang the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * @return the subscription
	 */
	public String getSubscriptions() {
		return subscriptions;
	}
	/**
	 * @param subscriptions to set
	 */
	public void setSubscriptions(String subscriptions) {
		this.subscriptions = subscriptions;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the coverName
	 */
	public String getCoverName() {
		return coverName;
	}
	/**
	 * @param coverName the coverName to set
	 */
	public void setCoverName(String coverName) {
		this.coverName = coverName;
	}
	/**
	 * @return the coverUrl
	 */
	public String getCoverUrl() {
		return coverUrl;
	}
	/**
	 * @param coverUrl the coverUrl to set
	 */
	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}
	/**
	 * @return the totalPage
	 */
	public String getTotalPage() {
		return totalPage;
	}
	/**
	 * @param totalPage the totalPage to set
	 */
	public void setTotalPage(String totalPage) {
		this.totalPage = totalPage;
	}
	/**
	 * @return the donwloadPDF
	 */
	public String getdownloadPDF() {
		return downloadPDF;
	}
	/**

	 */
	public void setDownloadPDF(String downloadPDF) {
		this.downloadPDF = downloadPDF;
	}
	/**

	 */
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	/**

	 */
	public String getPublisher_price() {
		return publisher_price;
	}
	public void setPublisher_price(String publisher_price) {
		this.publisher_price = publisher_price;
	}
	/**

	 */
	public String getReference_number() {
		return reference_number;
	}
	public void setReference_number(String reference_number) {
		this.reference_number = reference_number;
	}

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
	
	
	public Boolean isDownloaded(Context ctx){
		
		String currentAppPath = Constant.getAppFilepath(ctx) + "/";
		File filePath = new File(currentAppPath);
		
		if(filePath.exists()){
			File currentFile = new File(currentAppPath + URLEncoder.encode(this.getEditionId()));
			Log.i("Path is",currentFile.toString());
			
			int total=0;
			if(currentFile.exists()) {
				for (int j = 0; j < currentFile.list().length; j++) {
					if (currentFile.listFiles()[j].isDirectory()) {
						File f = new File(currentFile.listFiles()[j].getPath());
						total += f.list().length;
					} else {
						total++;
					}
				}
			}else
			return false;
			Log.i("total files",""+total);
			
			if(total>Integer.parseInt(this.getTotalPage()))
				return true;
		}
		
		return false;
	}
	
	public Boolean isSubscriptionActive(Context context){
		
		SharedPreferences preferences = context.getSharedPreferences("MyPreferences", context.MODE_PRIVATE);
		long millisFrom = preferences.getLong("dateFrom", 0L);
		long millisTo = preferences.getLong("dateTo", 0L);
		Date dateFrom = new Date(millisFrom);
		Date dateTo = new Date(millisTo);
		
		SimpleDateFormat  formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date auxDate = (java.util.Date) formatter.parse(getIssueDate());
			java.sql.Date issueDate = new java.sql.Date(auxDate.getTime());
			
			Log.i("date", "dates: "+dateFrom.toString()+" ### "+issueDate.toString()+" ### "+dateTo.toString());
			if(issueDate.after(dateFrom) && issueDate.before(dateTo))
				return true;
		} catch (java.text.ParseException e) {

			e.printStackTrace();
		} 
		
		return false;
	}
}
