package com.muevaelvolante.qiumagazine.calls;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by CQ on 16/05/2016.
 */
public interface Icall {

    @GET()
    public Call<ResponseBody > getStringData(@Url String url);

    @GET
    @Streaming
    Call<ResponseBody> downloadFileWithDynamicUrlAsync(@Url String fileUrl);
}
