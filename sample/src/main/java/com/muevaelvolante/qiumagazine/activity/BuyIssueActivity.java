package com.muevaelvolante.qiumagazine.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.vending.util.IabHelper;
import com.android.vending.util.IabResult;
import com.android.vending.util.Inventory;
import com.android.vending.util.Purchase;
import com.artifex.mupdflib.MuPDFActivity;
import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.bean.StoreProductBean;
import com.muevaelvolante.qiumagazine.utils.Constant;
import com.muevaelvolante.qiumagazine.utils.ImageDownLoader;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.net.URLEncoder;


public class BuyIssueActivity extends Activity {
	
	private File filePath;
	private String currentAppPath;
	
	private StoreProductBean issue;
	IabHelper mHelper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.buy_issue_layout);
		
		currentAppPath = Constant.getAppFilepath(this) + "/";
		filePath = new File(currentAppPath);
		
		String base64EncodedPublicKey= Constant.RSAKey;
		mHelper = new IabHelper(BuyIssueActivity.this, base64EncodedPublicKey);
		mHelper.enableDebugLogging(true);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			   public void onIabSetupFinished(IabResult result) {
			      if (!result.isSuccess()) {
			         // Oh noes, there was a problem.
			         Log.d("onCreate", "Problem setting up In-app Billing: " + result);
			      }            
			         // Hooray, IAB is fully set up!  
			   }
			});
		
		issue = (StoreProductBean)getIntent().getSerializableExtra("issue");
		
		TextView name = (TextView)findViewById(R.id.issueName);
		name.setText(issue.getEditionName());
		
		ImageView image = (ImageView)findViewById(R.id.issueImage);
		ImageLoader.getInstance().displayImage(issue.getCoverUrl(),image);
//		ImageDownLoader.download(issue.getCoverUrl(), image, this, URLEncoder.encode(issue.getEditionId()));
		
		Button buyButton = (Button)findViewById(R.id.buyIssueButton);
		if(filePath.exists()){
			if(issue.isDownloaded(BuyIssueActivity.this)){
				buyButton.setText(R.string.read);
				buyButton.setBackgroundResource(R.drawable.btn_read);	
			}else{
				if (issue.getPrice().equalsIgnoreCase("null")) {
					buyButton.setText(R.string.download);
				}else{
					if(issue.isSubscriptionActive(this))
						buyButton.setText("Subscribe");
					else
						buyButton.setText(issue.getPrice()+" "+getString(R.string.currency));
				}
				buyButton.setBackgroundResource(R.drawable.btn_buy);
			}
		}else{
			if (issue.getPrice().equalsIgnoreCase("null")) {
				buyButton.setText(R.string.download);
			}else{
				if(issue.isSubscriptionActive(this))
					buyButton.setText("Subscribe");
				else
					buyButton.setText(issue.getPrice()+" "+getString(R.string.currency));
			}
			buyButton.setBackgroundResource(R.drawable.btn_buy);
		}
		
		buyButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if(issue.isDownloaded(BuyIssueActivity.this)){
					String totalFilePath = filePath.getAbsolutePath()+"/"+URLEncoder.encode(URLEncoder.encode(issue.getEditionId()))+"/PDF/"+issue.getPdfName();
					Log.i("main","el totalpath es "+totalFilePath);
					Uri uri = Uri.parse(totalFilePath);
					Intent intent = new Intent(BuyIssueActivity.this,MuPDFActivity.class);
					intent.putExtra("pass",getString(R.string.passA)+issue.getEditionId()+getString(R.string.passB));
					intent.putExtra("pkn",getPackageName());
					intent.putExtra("edition_id",issue.getEditionId());
					intent.putExtra("social_text",getString(R.string.socialText));
					intent.putExtra("social_link",getString(R.string.socialText));
					intent.putExtra("mailsub",getString(R.string.mailSubject));
					intent.putExtra("mailtxt",getString(R.string.mailText));
					intent.putExtra("name", issue.getEditionName());
					intent.putExtra("edition_id", issue.getEditionId());
					intent.setAction(Intent.ACTION_VIEW);
					Log.i("afasd","aqui ando");
					intent.setData(uri);
					startActivity(intent);
				}
				else if(issue.getPrice().equalsIgnoreCase("null") || issue.isSubscriptionActive(BuyIssueActivity.this)){
					ProgressDialog pDialog = new ProgressDialog(BuyIssueActivity.this);
					pDialog.setMessage(BuyIssueActivity.this.getResources().getString(R.string.startDownload));
				    pDialog.show();
					Intent intent = new Intent(BuyIssueActivity.this,MyLibraryActivity.class);
					Bundle b = new Bundle();
					b.putSerializable("storeProductBean",issue);
					intent.putExtras(b);
					intent.putExtra("isDownload", true);
					startActivity(intent);
					finish();
				}
				else{
					Log.i("buyButton","this issue is not free");
					mHelper.queryInventoryAsync(mQueryFinishedListener);
				}
			}
		});

	}
	
	
	
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener 
	   = new IabHelper.OnIabPurchaseFinishedListener() {
	   public void onIabPurchaseFinished(IabResult result, Purchase purchase) 
	   {
	      if (result.isFailure()) {
	         Log.d("onPurchase", "Error purchasing: " + result);
	         return;
	      }      
	      else if (purchase.getSku().equals("a08fe0404dc58153c5cb543543jhk34jh")) {
	         // give user access to premium content and update the UI
	      }
	      else if(purchase.getSku().equals("android.test.purchased")){
	    	  System.out.println("You have purchased test item");
	    	  	/*ProgressDialog pDialog = new ProgressDialog(context);
				pDialog.setMessage(context.getResources().getString(R.string.startDownload));
			    pDialog.show();*/
				/*Intent intent = new Intent(MainActivity.this,MyLibraryActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("storeProductBean",buyingStoreproductBean);
				intent.putExtras(b);
				intent.putExtra("isDownload", true);
				startActivity(intent);
				finish();*/
	      }
	      else if(purchase.getSku().equals(getString(R.string.productId)+issue.getReference_number())){
	    	  	Intent intent = new Intent(BuyIssueActivity.this,MyLibraryActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("storeProductBean",issue);
				intent.putExtras(b);
				intent.putExtra("isDownload", true);
				startActivity(intent);
				finish();
	      }
	   }
	};
	
	IabHelper.QueryInventoryFinishedListener 
	   mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
	   public void onQueryInventoryFinished(IabResult result, Inventory inventory)   
	   {
	      if (result.isFailure()) {
	         // handle error
	    	  System.out.println("error");
	         return;
	       }
	      
	      if (inventory.hasPurchase("android.test.purchased")) {

	    	  System.out.println("Lo consumo");
	            mHelper.consumeAsync(inventory.getPurchase("android.test.purchased"), null);
	            return;
	        }
	      
	      if(inventory.hasPurchase(getString(R.string.productId)+issue.getReference_number())){
	    	  Intent intent = new Intent(BuyIssueActivity.this,MyLibraryActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("storeProductBean",issue);
				intent.putExtras(b);
				intent.putExtra("isDownload", true);
				startActivity(intent);
				finish();
	    	  return;
	      }

	       //String issuePrice = inventory.getSkuDetails("a08fe0404dc58153c5cb543543jhk34jh").getPrice();
	       
	       //Log.i("info","the price is: "+issuePrice);
	       
	      
	      mHelper.launchPurchaseFlow(BuyIssueActivity.this, getString(R.string.productId)+issue.getReference_number(), 10001,
	                mPurchaseFinishedListener, "Edition");
	      
	       // update the UI 
	   }
	};
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onResult", "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        
    }
}
