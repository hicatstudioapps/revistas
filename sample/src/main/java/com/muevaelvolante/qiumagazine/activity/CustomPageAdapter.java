package com.muevaelvolante.qiumagazine.activity;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.artifex.mupdflib.MuPDFCore;

import java.io.File;


public class CustomPageAdapter extends PagerAdapter{
	
	private final Uri[] urls;
	private final Context context;
	private final int screenWidth, screenHeight;
	private final String FILE;
	private MuPDFCore core;
	
	
	public CustomPageAdapter(Context context, Uri[] thumbUriFiles, int width, int height, String pdfName) {
		super();
		this.urls = thumbUriFiles;
		this.context = context;
		this.screenWidth = width;
		this.screenHeight = height;
		this.FILE=pdfName;
		
		if (core == null)
			core = openFile();
		if (core == null)
		{
			/* FIXME: Error handling here! */
			return;
		}
	}

	private MuPDFCore openFile()
	{
		//String storageState = Environment.getExternalStorageState();
		File path, file;
		MuPDFCore core;

		/*if (Environment.MEDIA_MOUNTED.equals(storageState))
		{
			System.out.println("Media mounted read/write");
		}
		else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(storageState))
		{
			System.out.println("Media mounted read only");
		}
		else
		{
			System.out.println("No media at all! Bale!\n");
			return null;
		}
		path = Environment.getExternalStorageDirectory();*/
		file = new File(FILE);
		System.out.println("Trying to open "+file.toString());
		try
		{
			System.out.println("Esto si");
			core = new MuPDFCore(context,file.toString());
			System.out.println("Esto no");
		}
		catch (Exception e)
		{
			System.out.println(e);
			return null;
		}
		return core;
	}
	
	@Override
	public void destroyItem(View collection, int position, Object o) {
		View view = (View)o;
        ((ViewPager) collection).removeView(view);
        view = null;
        
        Log.i("release","libero pagina "+position);
	}

	@Override
	public void finishUpdate(View arg0) {
	}

	@Override
	public int getCount() {
		return urls.length;
	}

	@Override
	public Object instantiateItem(View collection, int position) {	
		
		LinearLayout linearLayout = new LinearLayout(context);
		linearLayout.setOrientation(LinearLayout.VERTICAL);

		//linearLayout.getLayoutParams().width=screenWidth;
		//linearLayout.getLayoutParams().height=screenHeight;
		//final TextView textView = new TextView(context);
		//textView.setText("Position: " + position);
		
		/*ImageView image = new ImageView(context);
		//image.setImageBitmap(FetchImage.fetchImage(urls.get(position)));
		image.setImageURI(urls[position]);
		image.setLayoutParams(new LayoutParams(screenWidth, screenHeight));*/
		
		Log.i("load","cargando pagina "+position);
		
		
		//final PixmapView pixmapView = new PixmapView(context, core);
		//...   
		//RelativeLayout layout = new RelativeLayout(context);
		//...
		    RelativeLayout.LayoutParams pixmapParams =
		                      new RelativeLayout.LayoutParams(
		                                RelativeLayout.LayoutParams.FILL_PARENT,
		                                RelativeLayout.LayoutParams.FILL_PARENT);
		    pixmapParams.addRule(RelativeLayout.ABOVE,100);
		    //layout.addView(pixmapView, pixmapParams);
		    //linearLayout.addView(pixmapView, new LayoutParams(screenWidth, screenHeight-30));
		    //setContentView(layout);
		//pixmapView.changePage(position);
		
		    linearLayout.setBackgroundColor(Color.BLACK);
		//linearLayout.addView(textView);
		//linearLayout.addView(image);
		
		/*final int pos = position;
		Button next = new Button(context);
		next.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {

				pixmapView.changePage(pos);
			}
		});*/
		
		//linearLayout.addView(next, new LayoutParams(100, 20));
		
		((ViewPager) collection).addView(linearLayout,0);
		core.gotoPage(position);
		return linearLayout;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		 return view==((LinearLayout)object);
	}

	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1) {
	}

	@Override
	public Parcelable saveState() {

		return null;
	}

	@Override
	public void startUpdate(View arg0) {

		
	}

}
