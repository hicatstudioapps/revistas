package com.muevaelvolante.qiumagazine.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.muevaelvolante.qiumagazine.R;


public class WebContainerActivity extends Activity {

	private String urlToLoad;
	private boolean mLoaded = false;
	private WebView container;
	private ImageView refesh;
	
	private ProgressDialog pDialog;

	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		setContentView(R.layout.web_container);
		
		pDialog = new ProgressDialog(this);
	    pDialog.setMessage(getResources().getString(R.string.loading));
	    
	    refesh = (ImageView) findViewById(R.id.refreshButton);
	    refesh.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				pDialog.show();
				container.clearCache(true);
				container.loadUrl(urlToLoad);
			}
		});
		
		urlToLoad = getIntent().getStringExtra("url");

		container = (WebView)findViewById(R.id.webContainer);
		container.getSettings().setJavaScriptEnabled(true);
		
		container.setWebViewClient(new WebViewClient()
        {
			
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
            	
            	if(url.equals(urlToLoad)){
                	return false;
            	}
            	else if(mLoaded){
            		// do something

                	Intent intent = new Intent(WebContainerActivity.this,WebContainerActivity.class);
                	intent.putExtra("url", url);
        			startActivity(intent);
                	return true;
            	}
            	else{
            		return false;
            	}
            	
            }
            
            @Override
            public void onPageFinished(WebView webView, String url) {
                    mLoaded = true;
                    pDialog.dismiss();
            }
            
        });
		
		if(urlToLoad.endsWith(".pdf"))
			urlToLoad = "http://docs.google.com/gview?embedded=true&url="+urlToLoad;
		
		pDialog.show();
		container.loadUrl(urlToLoad);
	}
}
