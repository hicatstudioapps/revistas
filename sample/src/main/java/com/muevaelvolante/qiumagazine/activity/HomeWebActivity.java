package com.muevaelvolante.qiumagazine.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.util.IabHelper;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.adapter.DBAdapterIssues;
import com.muevaelvolante.qiumagazine.bean.ApiManager;
import com.muevaelvolante.qiumagazine.bean.StoreProductBean;
import com.muevaelvolante.qiumagazine.utils.AysnTaskCompleteListener;
import com.muevaelvolante.qiumagazine.utils.Constant;
import com.muevaelvolante.qiumagazine.utils.EditionSyncTask;
import com.muevaelvolante.qiumagazine.utils.Internet;
import com.muevaelvolante.qiumagazine.utils.ManagerGoogleAnalytics;
import com.muevaelvolante.qiumagazine.utils.StoreProductUtils;
import com.muevaelvolante.qiumagazine.utils.Utils;
import com.parse.LogInCallback;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * New web based main screen
 * 
 * @author Oscar Arteaga
 * 
 */
public class HomeWebActivity extends Activity implements AysnTaskCompleteListener<ArrayList<StoreProductBean>>,OnClickListener{
	private static final String TAG = "HomeWeb";
	
	public static String PACKAGE_NAME;
	
	private ImageView menuButton;
	private ImageView refreshButton;
	private ImageView shareButton;
	private ImageView userButton;
	private WebView mainWeb;
	//private String url;
	
	private Dialog accountDialog;

	IabHelper mHelper;
	
	private File filePath;
	private String currentAppPath;
	EditionSyncTask editionSyncTask;
	URL urlEdition;
	
	private DBAdapterIssues dbAdapterIssues = null;
	public ArrayList<StoreProductBean> issues;
	
	private LinearLayout error_layout;
	private Button error_library_button;
	
	private ProgressDialog pDialog;

	private ListView mMenuList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homeweb);
		
		error_layout = (LinearLayout) findViewById(R.id.error_layout);
		error_library_button = (Button) findViewById(R.id.error_library_button);
		error_library_button.setOnClickListener(this);
		

	    
	    PACKAGE_NAME=getApplicationContext().getPackageName();
		File f = new File(Constant.getAppFilepath(this));
		if(!f.exists())
			f.mkdirs();
	    
	    currentAppPath = Constant.getAppFilepath(this) + "/";
		filePath = new File(currentAppPath);
	    dbAdapterIssues = new DBAdapterIssues(this);
		
	    pDialog = new ProgressDialog(this);
	    pDialog.setMessage(getResources().getString(R.string.loading));

		
		/*
		Ya no es necesario cargarlo aquí porque se carga en ActivityInitLoading
		try {
			if (Internet.checkConnection(this)) {
				urlEdition = new URL(Constant.S3_URL+"/apps/"+Constant.API_KEY+".info.json");
				editionSyncTask = new EditionSyncTask(this, this, "InfoEditionList");
				editionSyncTask.execute(urlEdition);
			} else {
				loadFromInternalDB();
				error_layout.setVisibility(View.VISIBLE);
			}
		} catch (Exception e) {

		}*/




		
		menuButton = (ImageView)findViewById(R.id.menuButton);
		menuButton.setOnClickListener(this);
		
		refreshButton = (ImageView)findViewById(R.id.refreshButton);
		refreshButton.setOnClickListener(this);
		
		shareButton = (ImageView)findViewById(R.id.shareButton);
		shareButton.setOnClickListener(this);
		
		userButton = (ImageView)findViewById(R.id.userButton);
		userButton.setOnClickListener(this);
		
		mainWeb = (WebView)findViewById(R.id.mainWeb);
		mainWeb.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
		mainWeb.getSettings().setJavaScriptEnabled(true);
		
		
		mainWeb.setWebViewClient(new WebViewClient()
        {
			
			public void onPageFinished(WebView view, String url) {
		        // do your stuff here
				pDialog.dismiss();
		    }
			
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
            	if( url.startsWith("magazine://library") ){
                    // do something
                	Intent intent = new Intent(HomeWebActivity.this,MyLibraryActivity.class);
        			intent.putExtra("isDownload", false);
        			intent.putExtra("issues", issues);
        			startActivity(intent);
        			//finish();
                	return true;
                }
                else if(url.startsWith("magazine://issues/")){
                	// do something
                	String idIssue = url.substring(url.lastIndexOf("/")+1);
                	StoreProductBean issue = null;
                	
                	int index = 0;
                	for(int i=0;i<issues.size();i++){
                		StoreProductBean bean = issues.get(i);
                		if(bean.getEditionId().equals(idIssue)){
                			index=i;
                			break;
                		}
                	}
                	
                	issue = issues.get(index);

                	Intent intent = new Intent(HomeWebActivity.this,BuyIssueActivity.class);
                	intent.putExtra("issue", issue);
        			startActivity(intent);
                	return true;
                }
                else{

                	Intent intent = new Intent(HomeWebActivity.this,WebContainerActivity.class);
                	intent.putExtra("url", url);
        			startActivity(intent);
                	return true;
                }
                //return true;
            }
            
        });




		if (Internet.checkConnection(this))
		{
			if( getIntent().getBooleanExtra("needRefreshFromInternet", true))
				loadFromWeb();
			else
				loadFromInternalDB();

			loadWeb();
		}
		else
		{
			loadFromInternalDB();
			error_layout.setVisibility(View.VISIBLE);
		}



		try {
			urlEdition = new URL(ApiManager.getInstance(getApplicationContext()).getMenuURL(getApplicationContext()));
			editionSyncTask = new EditionSyncTask(this, this, "menuList");
			editionSyncTask.execute(urlEdition);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	  public void onStart() {
	    super.onStart();

		GoogleAnalytics.getInstance(this).reportActivityStart(this);
		ManagerGoogleAnalytics.getInstance().sendVisitScreen(this, getString(R.string.storeTrackedName));

    }




    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPref = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);

        long lastUpdate = sharedPref.getLong("dateLongLastUpdate", 0);
        long currentDateMillis = Utils.getCurrentTimeInMillis();

        if ( currentDateMillis - lastUpdate > Utils.HOURS_24_MILLIS ) {

            java.util.Date currentDate = new java.util.Date( currentDateMillis );

            ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
            parseInstallation.put("last_visit", currentDate);
            parseInstallation.saveEventually();

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putLong("dateLongLastUpdate", currentDateMillis);
            editor.apply();

        }

    }



	@Override
	  public void onStop() {
	    super.onStop();

		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	  }
	
	
	public void loadFromWeb(){
		
		//urlEdition = new URL(Constant.API_URL+"&key="+Constant.API_KEY+"&action=issues&module=magazine");
		try {
			editionSyncTask = new EditionSyncTask(this, this, "EditionList");
			urlEdition = new URL(Constant.S3_URL+"/apps/"+getString(R.string.API_KEY)+".issues.json");
			editionSyncTask.execute(urlEdition);
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}		
		
	}
	
	
	public void loadFromInternalDB(){
		issues = StoreProductUtils.loadFromInternalDB(dbAdapterIssues);
	}
	
	public void onTaskComplete(ArrayList<StoreProductBean> result, String method) {

		if (method.equalsIgnoreCase("EditionList")) {
			if(result != null){
				issues=result;
				for(int i=0;i<result.size();i++){
					dbAdapterIssues.open();
					Cursor c = dbAdapterIssues.getData(result.get(i).getEditionId());
					
					//save only new issues
					if(c.getCount()==0){
						dbAdapterIssues.open();
						dbAdapterIssues.inserData(result.get(i).getEdition(), result.get(i).getEditionId(), result.get(i).getEditionName(), result.get(i).getIssueDate(), result.get(i).getScreenOrientation(), result.get(i).getLang(), result.get(i).getPrice(), result.get(i).getDescription(), result.get(i).getCoverName(), result.get(i).getCoverUrl(), result.get(i).getTotalPage(), result.get(i).getSubscriptions(), result.get(i).getdownloadPDF(), result.get(i).getPublisher_price(), result.get(i).getReference_number(), result.get(i).getMode());
						dbAdapterIssues.close();
					}
					else{
						dbAdapterIssues.open();
						dbAdapterIssues.updateEdition(result.get(i).getEditionId(), result.get(i).getEditionName(), result.get(i).getIssueDate(), result.get(i).getScreenOrientation(), result.get(i).getLang(), result.get(i).getPrice(), result.get(i).getDescription(), result.get(i).getSubscriptions(), result.get(i).getPublisher_price(), result.get(i).getReference_number(), result.get(i).getMode());
						dbAdapterIssues.close();
					}
					c.close();
					dbAdapterIssues.close();
				}
				
				dbAdapterIssues.open();
				Cursor c = dbAdapterIssues.getAllData();
				while(c.moveToNext()){
					Boolean find=false;

					for(int i=0;i<result.size();i++){
						if(result.get(i).getEditionId().equals(c.getString(1)))
							find=true;
					}
					

					
					if(!find){
						StoreProductBean product = new StoreProductBean();
						product.setEditionName(c.getString(2));
						product.setTotalPage(c.getString(10));
						if(!isDownloaded((StoreProductBean)product)){
								System.out.println("Se borraria: "+c.getString(1));
								dbAdapterIssues.deleteEdition(c.getString(1));
						}
					}
				}
				c.close();
				dbAdapterIssues.close();
			}else{
				Toast.makeText(this, "Record Not Found ..", Toast.LENGTH_LONG).show();
			}
		}
	}
	
	public void onTaskComplete(String result, String method) {

		
		/*
		Ya no se da porque lo llama ActivityInitLoading
		if(method.equalsIgnoreCase("InfoEditionList")){
			try {
				JSONObject jsonObject = new JSONObject(result);
				if(jsonObject != null){

					String lastUpdate = ApiManager.getInstance(getApplicationContext()).last_update;
					ApiManager.getInstance(getApplicationContext()).parseFromJSONandStore(jsonObject, HomeWebActivity.this);

					if(ApiManager.getInstance(getApplicationContext()).subscribers_db_integration==false);
						//accountButton.setVisibility(View.INVISIBLE);
					
					if(ApiManager.getInstance(getApplicationContext()).onoff==1){
						if(lastUpdate.equals(ApiManager.getInstance(getApplicationContext()).last_update)){
							//Nos ahorramos la carga de internet leyendo de disco
							loadFromInternalDB();
						}else{
							//update from network
							loadFromWeb();
						}
					}
					else if(ApiManager.getInstance(getApplicationContext()).onoff==0){
						AlertDialog alertDialog = new AlertDialog.Builder(this).create();
						alertDialog.setTitle(this.getResources().getString(R.string.error));
						alertDialog.setMessage(this.getResources().getString(R.string.error_message));
						alertDialog.setCanceledOnTouchOutside(false);
						alertDialog.setCancelable(false);
						alertDialog.show();
					}

					url = ApiManager.getInstance(getApplicationContext()).getURL(getApplicationContext());
					//url = "http://jetskin.s3.amazonaws.com/html/en/app/tablet.html";
					loadWeb();
					
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}
		else*/ if(method.equalsIgnoreCase("menuList")){
		
			menuButton.setVisibility(View.VISIBLE);
			try {

				if(result != null)
				{
					JSONObject jsonObject = new JSONObject(result);
					initMenu(jsonObject.getJSONArray("items"));
				}


			} catch (JSONException e) {

				e.printStackTrace();
				menuButton.setVisibility(View.GONE);
			}
		}
		
	}
	
	
	public Boolean isDownloaded(StoreProductBean issue){
		
		if(filePath.exists()){
			File currentFile = new File(currentAppPath + URLEncoder.encode(issue.getEditionId()));

			
			int total=0;
			for(int j=0;j<currentFile.list().length;j++){
				if(currentFile.listFiles()[j].isDirectory()){
					File f = new File(currentFile.listFiles()[j].getPath());
					total += f.list().length;
				}
				else{
					total++;
				}
			}
			Log.i("info","total files: "+total);
			
			if(total>Integer.parseInt(issue.getTotalPage()))
				return true;
		}
		
		return false;
	}
	
	
	public void loadWeb(){
		pDialog.show();
		mainWeb.loadUrl(ApiManager.getInstance(getApplicationContext()).getURL(getApplicationContext()));
	}
	
	public void initMenu(JSONArray items){
		
		if(mMenuList==null){
			mMenuList = (ListView) findViewById(R.id.menuList);
		}
		final MenuAdapter adapter = new MenuAdapter(HomeWebActivity.this,items);
        mMenuList.setAdapter(adapter);
        mMenuList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1, int position,
					long arg3) {

				showOrHideMenu();
				JSONObject item = (JSONObject) adapter.getItemAtPosition(position);
				try {
					if(item.getString("url").startsWith("magazine://library")){
						Intent intent = new Intent(HomeWebActivity.this,MyLibraryActivity.class);
						intent.putExtra("isDownload", false);
						intent.putExtra("issues", issues);
						startActivity(intent);
					}
					else{
						// do something
						Log.i("info", "other: " + ApiManager.getInstance(getApplicationContext()).getURL(getApplicationContext()));
						Intent intent = new Intent(HomeWebActivity.this,WebContainerActivity.class);
						intent.putExtra("url", item.getString("url"));
						startActivity(intent);
					}
				} catch (JSONException e) {

					e.printStackTrace();
				}
				
			}
		});
	}
	
	public void showOrHideMenu(){
		
		if(mMenuList.getVisibility()==View.GONE){
			mMenuList.setVisibility(View.VISIBLE);
			mMenuList.setAlpha(0.0f);
			
			mMenuList.animate()
		    .translationY(0)
		    .alpha(1.0f);
		}
		else{
			//mMenuList.setVisibility(View.INVISIBLE);
			
			mMenuList.animate()
		    .translationY(-mMenuList.getHeight())
		    .alpha(0.0f)
		    .setListener(new AnimatorListenerAdapter() {
		        @Override
		        public void onAnimationEnd(Animator animation) {
		            super.onAnimationEnd(animation);
		            if(mMenuList.getAlpha()==0){
		            	mMenuList.setVisibility(View.GONE);
		            }
		        }
		    });
		}
		
	}
	

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.refreshButton:
			if(Internet.checkConnection(HomeWebActivity.this)){
				pDialog.show();
				mainWeb.clearCache(true);
				mainWeb.loadUrl(ApiManager.getInstance(getApplicationContext()).getURL(getApplicationContext()));
				error_layout.setVisibility(View.INVISIBLE);
			}
			else{
				error_layout.setVisibility(View.VISIBLE);
			}
			break;
		case R.id.error_library_button:
			Intent intent = new Intent(HomeWebActivity.this,MyLibraryActivity.class);
			intent.putExtra("isDownload", false);
			intent.putExtra("issues", issues);
			startActivity(intent);
			break;
			
		case R.id.shareButton:
			
			Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
	        sharingIntent.setType("text/plain");
	        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "See right now in your hand all Jetski news. Download news, photos, movies and much more from the main races and championships. http://bit.ly/1KmBGfd");
	        startActivity(sharingIntent);
			
			
//			View rootView = findViewById(android.R.id.content).getRootView();
//			rootView.setDrawingCacheEnabled(true);
//			Bitmap bitmap = rootView.getDrawingCache();
//			 
//			OutputStream output;
//			// Create a name for the saved image
//			File file = new File(currentAppPath, "screenshot.png");
//			try {
//				// Share Intent
//				Intent share = new Intent(Intent.ACTION_SEND);
//				// Type of file to share
//				share.setType("image/png");
// 
//				output = new FileOutputStream(file);
// 
//				// Compress into png format image from 0% - 100%
//				bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
//				output.flush();
//				output.close();
// 
//				// Locate the image to Share
//				Uri uri = Uri.fromFile(file);
// 
//				// Pass the image into an Intnet
//				share.putExtra(Intent.EXTRA_STREAM, uri);
// 
//				// Show the social share chooser list
//				startActivity(Intent.createChooser(share, "Share Image"));
// 
//			} catch (Exception e) {
//
//				e.printStackTrace();
//			}
			
//			Intent share = new Intent(Intent.ACTION_SEND);
//			share.setType("image/*");
//			share.putExtra(Intent.EXTRA_STREAM, bitmap);
//			startActivity(Intent.createChooser(share, "Share Image"));
			break;

		case R.id.userButton:
			
			if(ParseUser.getCurrentUser()!=null){
				AlertDialog.Builder builder = new AlertDialog.Builder(HomeWebActivity.this);
				builder.setTitle(HomeWebActivity.this.getResources().getString(R.string.error));
				builder.setMessage(HomeWebActivity.this.getResources().getString(R.string.user_logued));
				
				builder.setPositiveButton(getResources().getString(R.string.logout), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {

						ParseUser.logOut();
						
						SharedPreferences preferences = HomeWebActivity.this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
						SharedPreferences.Editor editor = preferences.edit();
						editor.remove("dateFrom");
						editor.remove("dateTo");
						editor.commit();
					}
				});
				builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {

						
					}
				});
				
				AlertDialog alertDialog = builder.create();
				alertDialog.setCanceledOnTouchOutside(true);
				alertDialog.setCancelable(true);
				alertDialog.show();
				
				return;
			}
			
			accountDialog = new Dialog(this, R.style.MyDialog);
			accountDialog.setContentView(R.layout.accounts);

			
			accountDialog.findViewById(R.id.accountCancelButton).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {

					accountDialog.dismiss();
				}
			});
			
			accountDialog.findViewById(R.id.accountValidateButton).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {


					//Añadimos un una comprobación especial para discernir si es un usuario admin/preview y tendrá acceso a descargas
					String user_ = ((EditText)accountDialog.findViewById(R.id.accountUserTextfield)).getText().toString();
					String pass_ = ((EditText)accountDialog.findViewById(R.id.accountPassTextfield)).getText().toString();

					if(Utils.checkIfUserPreview(HomeWebActivity.this, user_, pass_)) {
						accountDialog.dismiss();
						return;
					}


					if(((EditText)accountDialog.findViewById(R.id.accountUserTextfield)).getText().toString().length()>0 && ((EditText)accountDialog.findViewById(R.id.accountPassTextfield)).getText().toString().length()>0){
					
						ParseUser.logInInBackground(((EditText)accountDialog.findViewById(R.id.accountUserTextfield)).getText().toString(), ((EditText)accountDialog.findViewById(R.id.accountPassTextfield)).getText().toString(), new LogInCallback() {

							@Override
							public void done(ParseUser arg0,
									com.parse.ParseException arg1) {

								if(arg0!=null){
									System.out.println("Logueado como: "+arg0.getUsername());
									
									if(arg0.getNumber("id_publication").intValue()!=getResources().getInteger(R.integer.id_publication)){
										AlertDialog alertDialog = new AlertDialog.Builder(HomeWebActivity.this).create();
										alertDialog.setTitle(HomeWebActivity.this.getResources().getString(R.string.error));
										alertDialog.setMessage(HomeWebActivity.this.getResources().getString(R.string.user_or_password_incorrect));
										alertDialog.setCanceledOnTouchOutside(true);
										alertDialog.setCancelable(true);
										alertDialog.show();
										
										ParseUser.logOut();
										
										return;
									}
									
									if(arg0.getNumber("quota").intValue()==0){
										AlertDialog alertDialog = new AlertDialog.Builder(HomeWebActivity.this).create();
										alertDialog.setTitle(HomeWebActivity.this.getResources().getString(R.string.error));
										alertDialog.setMessage(HomeWebActivity.this.getResources().getString(R.string.quota_exceed));
										alertDialog.setCanceledOnTouchOutside(true);
										alertDialog.setCancelable(true);
										alertDialog.show();
										
										ParseUser.logOut();
										
										return;
									}
									
									SharedPreferences preferences = HomeWebActivity.this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
									SharedPreferences.Editor editor = preferences.edit();
									editor.putLong("dateFrom", arg0.getDate("dateFrom").getTime());
									editor.putLong("dateTo", arg0.getDate("dateTo").getTime());
									editor.commit();
									
									accountDialog.dismiss();
									
									SimpleDateFormat  formatter = new SimpleDateFormat("dd-MM-yyyy");
									
									AlertDialog alertDialog = new AlertDialog.Builder(HomeWebActivity.this).create();
									alertDialog.setTitle(HomeWebActivity.this.getResources().getString(R.string.error));
									alertDialog.setMessage(HomeWebActivity.this.getResources().getString(R.string.subscription_active) + " " + formatter.format(arg0.getDate("dateFrom")) + " " + HomeWebActivity.this.getResources().getString(R.string.to) + " " + formatter.format(arg0.getDate("dateTo")));
									alertDialog.setCanceledOnTouchOutside(true);
									alertDialog.setCancelable(true);
									alertDialog.show();
									
									arg0.put("quota", ((arg0.getNumber("quota").intValue())-1));

									//Añadir installations id
									ArrayList <String> installationIds = new ArrayList<String>();
									JSONArray installationIdsJson = arg0.getJSONArray("installation_ids");
									if(installationIdsJson != null)
									{
										for(int i=0; i<installationIdsJson.length(); ++i)
										{
											String aux = (String) installationIdsJson.opt(i);
											installationIds.add(aux);
										}
									}

									//Comprobamos si ya existe
									boolean exists = false;
									for(String aux : installationIds)
									{
										if(aux.equalsIgnoreCase(ParseInstallation.getCurrentInstallation().getInstallationId()))
										{
											exists = true;
											break;
										}
									}

									if(!exists)
									{
										installationIds.add(ParseInstallation.getCurrentInstallation().getInstallationId());
										arg0.put("installation_ids",installationIds);
									}



									arg0.saveEventually();
									
									alertDialog.setOnDismissListener(new AlertDialog.OnDismissListener() {
										
										@Override
										public void onDismiss(DialogInterface dialog) {
											// TODO Auto-generated method stub
											
										}
									});
								
								}
								else{
									AlertDialog alertDialog = new AlertDialog.Builder(HomeWebActivity.this).create();
									alertDialog.setTitle(HomeWebActivity.this.getResources().getString(R.string.error));
									alertDialog.setMessage(HomeWebActivity.this.getResources().getString(R.string.user_or_password_incorrect));
									alertDialog.setCanceledOnTouchOutside(true);
									alertDialog.setCancelable(true);
									alertDialog.show();
								}
							}
						});

					}
					else{
						
						AlertDialog alertDialog = new AlertDialog.Builder(HomeWebActivity.this).create();
						alertDialog.setTitle(HomeWebActivity.this.getResources().getString(R.string.error));
						alertDialog.setMessage(HomeWebActivity.this.getResources().getString(R.string.user_password_empty));
						alertDialog.setCanceledOnTouchOutside(true);
						alertDialog.setCancelable(true);
						alertDialog.show();
					}
				}
			});
			
			accountDialog.show();
			
			break;
			
		case R.id.menuButton:
				
			showOrHideMenu();
			
			break;
		default:
			break;
		}
				
	}
	
	
	
	private class MenuAdapter extends BaseAdapter{
		JSONArray items;
    	
    	public MenuAdapter(Context context, JSONArray items){
    		this.items=items;
    	}
    			
		public boolean isEmpty() {

			return false;
		}
		
		public boolean hasStableIds() {

			return false;
		}
		
		public int getViewTypeCount() {

			return items.length();
		}
		
		public View getView(int position, View v, ViewGroup parent) {

			
			if(v==null){
   	        	 v = View.inflate(HomeWebActivity.this, R.layout.menu_cell, null);
	        }
			
			JSONObject item;
			try {
				item = items.getJSONObject(position);
				TextView name = (TextView)v.findViewById(R.id.title);
				name.setText(item.getString("Title"));
			} catch (JSONException e) {

				e.printStackTrace();
			}   

    	     
    	 return v;
    	     
		}
		
		public long getItemId(int arg0) {

			return arg0;
		}
		
		public JSONObject getItem(int arg0) {

			try {
				return items.getJSONObject(arg0);
			} catch (JSONException e) {

				e.printStackTrace();
			}
			return null;
		}
		
		public int getCount() {

			return items.length();
		}

		public int getItemViewType(int position) {

			return position;
		}

		public boolean areAllItemsEnabled() {
			return false;
		}

		public boolean isEnabled(int position) {
			return true;
		}
		
		public void setItems(JSONArray items){
			this.items=items;
		}
		
	}
	
	
}