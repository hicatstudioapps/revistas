package com.muevaelvolante.qiumagazine.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.Gallery.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.adapter.DBAdapter;
import com.muevaelvolante.qiumagazine.utils.Constant;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


@SuppressLint("UseSparseArrays")
public class IssueActivity extends Activity implements OnClickListener {

	private Gallery gallery;
	private LinearLayout layoutForHorizontalList, layoutTop, layoutBottom, layoutPages;
	private ImageView imgCurrentIssue, imgThumb;
	private TextView txtPageNumber, txtCurrentImageTitle;
	private String issueName, issuePDF;
	public File folder;
	public HashMap<Integer, String> allFiles;
	public HashMap<Integer, String> imageFileNameList;
	public Uri[] uriFiles;
	public ArrayList<Integer> intFileList;
	public Button btnHome, btnInfo, btnFavorites, btnShare, btnThumb;
	public DBAdapter dbAdapter = null;
	RelativeLayout reletiveLayout;
	public boolean isLandscap = false;
	public Dialog thumb, fav;
	private PagerAdapter pageAdapter;
	private ViewPager pager;

	@SuppressLint("UseSparseArrays")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.issue_gallary);

		gallery = (Gallery) findViewById(R.id.gallery);
		layoutForHorizontalList = (LinearLayout) findViewById(R.id.layoutForHorizontalList);
		layoutPages = (LinearLayout) findViewById(R.id.realPageScroll);
		layoutBottom = (LinearLayout) findViewById(R.id.layoutBottom);
		layoutTop = (LinearLayout) findViewById(R.id.layout_top);
		txtCurrentImageTitle = (TextView) findViewById(R.id.txt_pagetitle);
		btnHome = (Button) findViewById(R.id.btn_home);
		btnInfo = (Button) findViewById(R.id.btn_info);
		btnFavorites = (Button) findViewById(R.id.btn_favorites);
		btnShare = (Button) findViewById(R.id.btn_share);
		btnThumb = (Button) findViewById(R.id.btn_thumbnail);
		
		btnHome.setOnClickListener(this);
		btnInfo.setOnClickListener(this);
		btnFavorites.setOnClickListener(this);
		btnShare.setOnClickListener(this);
		btnThumb.setOnClickListener(this);
		
		final Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
		final Animation myFadeOutAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_out);
		try {
			dbAdapter = new DBAdapter(this);
			issueName = getIntent().getStringExtra("issue_name");
			issuePDF = getIntent().getStringExtra("issue_pdf");
			txtCurrentImageTitle.setText(URLDecoder.decode(issueName));
			folder = new File(Constant.getAppFilepath(this) + "/" + URLEncoder.encode(issueName)
					+ "/PDF");
			File[] imagelist = folder.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return ((name.endsWith(".jpg")) || (name.endsWith(".png")));
				}
			});

			allFiles = new HashMap<Integer, String>();
			intFileList = new ArrayList<Integer>();
			imageFileNameList = new HashMap<Integer, String>();
			for (int i = 0; i < imagelist.length; i++) {
				String id = imagelist[i].getName().substring(imagelist[i].getName().lastIndexOf("_")+1, imagelist[i].getName().lastIndexOf("."));
				imageFileNameList.put(Integer.parseInt(id), imagelist[i].getName());
				intFileList.add(Integer.parseInt(id));
				allFiles.put(Integer.parseInt(id), imagelist[i].getAbsolutePath());
			}
		
			Collections.sort(intFileList);
			
			uriFiles = new Uri[allFiles.size()];
			for (int i = 0; i < allFiles.size(); i++) {
				uriFiles[i] = Uri.parse(allFiles.get(i));
			}

			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//gallery.setAdapter(new ImageAdapter(this, uriFiles));
			gallery.setCallbackDuringFling(false);
			gallery.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView parent, View v, int position, long id) {
//					txtCurrentImageTitle.setText(imageFileNameList.get(position));

					if (layoutBottom.getVisibility() == View.GONE) {
						layoutBottom.setVisibility(View.VISIBLE);
						layoutBottom.setAnimation(myFadeInAnimation);
					} else {
						layoutBottom.setVisibility(View.GONE);
						layoutBottom.setAnimation(myFadeOutAnimation);
					}

					if (layoutTop.getVisibility() == View.GONE) {
						TranslateAnimation slide1 = new TranslateAnimation(0, 0, -100, 0);
						slide1.setDuration(400);
						slide1.setFillAfter(true);
						layoutTop.startAnimation(slide1);
						layoutTop.setVisibility(View.VISIBLE);
					} else {
						layoutTop.setVisibility(View.GONE);
						TranslateAnimation slide1 = new TranslateAnimation(0, 0, 0, -1000);
						slide1.setDuration(400);
						slide1.setFillAfter(true);
						layoutTop.startAnimation(slide1);
					}
				}
			});
			gallery.setSpacing(15);
			gallery.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
				}
			});
			
			
					
			File folder = new File(Constant.getAppFilepath(this) + "/" + URLEncoder.encode(issueName) + "/Thumb");
			File[] thumbList = folder.listFiles();

			allFiles = new HashMap<Integer, String>();
			for (int i = 0; i < thumbList.length; i++) {
				String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_")+1, thumbList[i].getName().lastIndexOf("."));
				allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
			}

			Uri[] thumbUriFiles = new Uri[allFiles.size()];
			for (int i = 0; i < allFiles.size(); i++) {
				thumbUriFiles[i] = Uri.parse(allFiles.get(i));
			}
			
			dbAdapter.open();
			Cursor c = dbAdapter.getData(issueName);
			HashMap<Integer, String> extraMap = new HashMap<Integer, String>();
			if(c.getCount() > 0){
				while(c.moveToNext()){
					Integer sortNumber = Integer.parseInt(c.getString(4));
					String extraMedia = c.getString(2);
					extraMap.put(sortNumber, extraMedia);
				}
			}
			c.close();
			dbAdapter.close();
			
			for (int i = 0; i < thumbUriFiles.length; i++) {

				View placeHolder = inflater.inflate(R.layout.horizontal_placeholder_issue, null);
				placeHolder.setTag(i);
				imgCurrentIssue = (ImageView) placeHolder.findViewById(R.id.imgCurrentMagazineIssue);
				imgCurrentIssue.setImageURI(thumbUriFiles[i]);
				txtPageNumber = (TextView) placeHolder.findViewById(R.id.txt_page_number);
				txtPageNumber.setText("" + (i+1));

				final SharedPreferences preferences = this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
				if(preferences.getInt(URLEncoder.encode(issueName)+"_"+(i+1), 0)==1){
					ImageView star = (ImageView) placeHolder.findViewById(R.id.favStar);
					star.setImageResource(R.drawable.fav_star);
				}
				if(extraMap.containsKey(i+1)){
					//System.out.println("Extra MAP : "+ extraMap.get(position + 1));
					if (extraMap.get(i+1).equalsIgnoreCase("link")) {
						ImageView star = (ImageView) placeHolder.findViewById(R.id.media);
						star.setImageResource(R.drawable.link_button);
					}
					else if (extraMap.get(i+1).equalsIgnoreCase("audio")) {
						ImageView star = (ImageView) placeHolder.findViewById(R.id.media);
						star.setImageResource(R.drawable.audio_button);
					}
					else if (extraMap.get(i+1).equalsIgnoreCase("video")) {
						ImageView star = (ImageView) placeHolder.findViewById(R.id.media);
						star.setImageResource(R.drawable.videoplay_button);
					}
				}
				
				layoutForHorizontalList.addView(placeHolder);

				placeHolder.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Integer id = (Integer) v.getTag();
						gallery.setSelection(id);
					}
				});
			}
			
			//LinearLayout layout = (LinearLayout) findViewById(R.id.realPageScroll);
			//LayoutInflater inflat = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			Display display =((WindowManager)getSystemService(IssueActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
			/*for (int j = 0; j < thumbUriFiles.length; j++) {
				View placeHolder = inflater.inflate(R.layout.pages_placeholder, null);
				ImageView img = (ImageView) placeHolder.findViewById(R.id.pageImage);
				img.setImageURI(thumbUriFiles[j]);
				img.getLayoutParams().height=display.getHeight();
				img.getLayoutParams().width=display.getWidth();
				
				Log.i("info","Inflando la scroll");
				
				layoutPages.addView(placeHolder);
			}*/
			String str = Constant.getAppFilepath(this) + "/" + URLEncoder.encode(issueName)
					+ "/PDF/" + issuePDF;
			try {
				pageAdapter = new CustomPageAdapter(this, thumbUriFiles, display.getWidth(), display.getHeight(), str);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			pager = (ViewPager) findViewById(R.id.viewPagerId);
			pager.setAdapter(pageAdapter);
			
			
			pager.setOnTouchListener(new View.OnTouchListener() {
				int scroll;
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					
					switch (event.getAction()) {
					case MotionEvent.ACTION_MOVE: 
						pager.requestDisallowInterceptTouchEvent(true);
				        break;
					case MotionEvent.ACTION_DOWN:
						scroll=pager.getScrollX();
						break;
					case MotionEvent.ACTION_UP:
						Log.i("touch","action down");
						if(scroll==pager.getScrollX()){
							if (layoutBottom.getVisibility() == View.GONE) {
								layoutBottom.setVisibility(View.VISIBLE);
								layoutBottom.setAnimation(myFadeInAnimation);
							} else {
								layoutBottom.setVisibility(View.GONE);
								layoutBottom.setAnimation(myFadeOutAnimation);
							}
	
							if (layoutTop.getVisibility() == View.GONE) {
								TranslateAnimation slide1 = new TranslateAnimation(0, 0, -100, 0);
								slide1.setDuration(400);
								slide1.setFillAfter(true);
								layoutTop.startAnimation(slide1);
								layoutTop.setVisibility(View.VISIBLE);
							} else {
								layoutTop.setVisibility(View.GONE);
								TranslateAnimation slide1 = new TranslateAnimation(0, 0, 0, -1000);
								slide1.setDuration(400);
								slide1.setFillAfter(true);
								layoutTop.startAnimation(slide1);
							}
						}
						break;
					case MotionEvent.ACTION_SCROLL:
						Log.i("touch","action scroll");
						break;
					}
					
					return false;
				}
			});

		} catch (Exception e) {
		}
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);

	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	        gallery.setAdapter(new ImageAdapter(this, uriFiles));
	        isLandscap = true;
	        Log.i("giro","Ahora en landscape");
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	        gallery.setAdapter(new ImageAdapter(this, uriFiles));
	        isLandscap = false;
	        Log.i("giro","Ahora en portrait");
	    }
	}

	
	public class ImageAdapter extends BaseAdapter {
		private Context context;
		private int itemBackground;
		Uri[] uriFiles = null;

		public ImageAdapter(Context c, Uri[] uriFiles) {
			context = c;
			this.uriFiles = uriFiles;
		}

		public int getCount() {
			return uriFiles.length;
		}

		public Object getItem(int position) {
			return position % uriFiles.length;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertViesiw, ViewGroup parent) {
			
			reletiveLayout = new RelativeLayout(context);
			reletiveLayout.setId(position);
			ImageView imageView = new ImageView(context);
			imageView.setId(position);
			imageView.setImageURI(uriFiles[position]);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			
			Gallery.LayoutParams innerLP;
			LayoutParams p;
			if(isLandscap) {
				DisplayMetrics metrics = new DisplayMetrics();
				IssueActivity.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
				
				MarginLayoutParams mlp = (MarginLayoutParams) gallery.getLayoutParams();
				mlp.setMargins(-(metrics.widthPixels/2), 
				               mlp.topMargin, 
				               mlp.rightMargin, 
				               mlp.bottomMargin
				);
				Log.i("margenes","width es: "+metrics.widthPixels);
				innerLP = new Gallery.LayoutParams(820, Gallery.LayoutParams.FILL_PARENT);
				p = new LayoutParams(620, LayoutParams.FILL_PARENT);
			} else {
				DisplayMetrics metrics = new DisplayMetrics();
				IssueActivity.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
				
				MarginLayoutParams mlp = (MarginLayoutParams) gallery.getLayoutParams();
				mlp.setMargins(0, 0, 0, 0);
				innerLP = new Gallery.LayoutParams(Gallery.LayoutParams.FILL_PARENT, Gallery.LayoutParams.FILL_PARENT);
				p = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			}
			
			
			imageView.setLayoutParams(innerLP);
			imageView.setScaleType(ScaleType.FIT_CENTER);
			reletiveLayout.setLayoutParams(p);
			reletiveLayout.addView(imageView); 
			
			dbAdapter.open();
			Cursor c = dbAdapter.getData(issueName);
			HashMap<Integer, String> extraMap = new HashMap<Integer, String>();
			HashMap<Integer, String> extraList = new HashMap<Integer, String>();
			HashMap<Integer, Float> extraStartX = new HashMap<Integer, Float>();
			HashMap<Integer, Double> extraStartY = new HashMap<Integer, Double>();
			HashMap<Integer, Float> extraEndX = new HashMap<Integer, Float>();
			HashMap<Integer, Float> extraEndY = new HashMap<Integer, Float>();
			HashMap<Integer, Integer> extraControls = new HashMap<Integer, Integer>();
			HashMap<Integer, Integer> extraType = new HashMap<Integer, Integer>();
			HashMap<Integer, Boolean> extraAuto = new HashMap<Integer, Boolean>();
			HashMap<Integer, Boolean> extraFullscreen = new HashMap<Integer, Boolean>();
			if(c.getCount() > 0){
				while(c.moveToNext()){
					Integer sortNumber = Integer.parseInt(c.getString(4));
					String extraMedia = c.getString(2);
					String extraData = c.getString(3);
					Float startX = Float.parseFloat(c.getString(5));
					extraMap.put(sortNumber, extraMedia);
					extraList.put(sortNumber,extraData);
					extraStartX.put(sortNumber, startX);
					extraStartY.put(sortNumber, Double.parseDouble(c.getString(6)));
					extraEndX.put(sortNumber, Float.parseFloat(c.getString(7)));
					extraEndY.put(sortNumber, Float.parseFloat(c.getString(8)));
					extraAuto.put(sortNumber, Boolean.parseBoolean(c.getString(9)));
					extraFullscreen.put(sortNumber, Boolean.parseBoolean(c.getString(10)));
					extraControls.put(sortNumber, c.getInt(11));
					extraType.put(sortNumber, c.getInt(12));
					Log.i("extra", "extralist es: "+c.getString(1)+"-"+c.getString(2)+"-"+c.getString(3)+"-"+c.getString(4)+"-"+c.getString(5)+"-"+c.getString(6)
							+"-"+c.getString(7)+"-"+c.getString(8)+"-"+c.getString(9)+"-"+c.getString(10)+"-"+c.getString(11)+"-"+c.getString(12));
				}
			}
			c.close();
			dbAdapter.close();
			if(extraMap.containsKey(position+1)){
				System.out.println("Extra MAP : "+ extraMap.get(position + 1));
				if (extraMap.get(position+1).equalsIgnoreCase("audio")) {
					LinearLayout innerLinear = new LinearLayout(context);
					Button btnAudio = new Button(context);
					btnAudio.setTag(extraList.get(position+1));
					btnAudio.setId(position);
					btnAudio.setBackgroundResource(R.drawable.audio_button);
					RelativeLayout.LayoutParams btnParam = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					btnParam.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
					btnParam.addRule(RelativeLayout.CENTER_HORIZONTAL);
					android.widget.LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lp.setMargins(0, 0, 0, 100);
					innerLinear.setLayoutParams(btnParam);
					innerLinear.addView(btnAudio, lp);
					reletiveLayout.addView(innerLinear);
					btnAudio.setOnClickListener(new OnClickListener() {
						@Override 
						public void onClick(View v) {
							File audioFile = new File(Constant.getAppFilepath(IssueActivity.this) + "/" + URLEncoder.encode(issueName)
									+ "/Extra" + File.separator +(String) v.getTag());
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(Uri.fromFile(audioFile),"audio/*");
							startActivity(intent);
						} 
					}); 
				}else if(extraMap.get(position + 1).equalsIgnoreCase("video")){
					final LinearLayout innerLinear = new LinearLayout(context);
					Button btnVideo = new Button(context);
					btnVideo.setTag(extraList.get(position+1));
					btnVideo.setBackgroundResource(R.drawable.videoplay_button);
					RelativeLayout.LayoutParams btnParam = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					btnParam.addRule(RelativeLayout.CENTER_IN_PARENT, -1);
					btnVideo.setLayoutParams(btnParam);
					reletiveLayout.addView(btnVideo);
					
					Display disp =((WindowManager)getSystemService(IssueActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
					IssueActivity issue = new IssueActivity();
					final RelativeLayout.LayoutParams param = issue.getPosition(disp.getWidth(),disp.getHeight(),extraStartX.get(position+1).floatValue(), extraStartY.get(position+1).floatValue(), extraEndX.get(position+1).floatValue(), extraEndY.get(position+1).floatValue());
					
					btnVideo.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) { 
							File videoFile = new File(Constant.getAppFilepath(IssueActivity.this) + "/" + URLEncoder.encode(issueName)
									+ "/Extra" + File.separator +(String) v.getTag());
							/*Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(Uri.fromFile(videoFile),"video/*");
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
							startActivity(intent);*/
							
							/*VideoView video = new VideoView(context);
							video.setVideoURI(Uri.fromFile(videoFile));
							video.requestFocus();
							video.start();*/
							
							VideoView videoHolder = new VideoView(context);
							videoHolder.setLayoutParams(new LayoutParams(param.width, param.height));
							//videoHolder.setMediaController(new MediaController(context)); 
							//setContentView(videoHolder); 

							// It works fine with .wmv, .3gp and .mp4 formats 
							//Here we have to specify the SD Card path 
							videoHolder.setVideoURI(Uri.fromFile(videoFile)); 
							videoHolder.requestFocus(); 
							videoHolder.start();
							
							
							//reletiveLayout.addView(videoHolder);
							
							RelativeLayout.LayoutParams btnParam = new RelativeLayout.LayoutParams(param.width, param.height);
							btnParam.leftMargin=param.leftMargin;
							btnParam.topMargin=param.topMargin;
						    
							videoHolder.setMediaController(new MediaController(videoHolder.getContext()));
							
							android.widget.LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(param.width, param.height);
							lp.setMargins(0, 0, 0, 0);
							innerLinear.setLayoutParams(btnParam);
							innerLinear.setGravity(Gravity.CENTER);
							innerLinear.addView(videoHolder);
							reletiveLayout.addView(innerLinear);
							
						}
					});
					
					if(extraAuto.get(position+1))
						btnVideo.performClick();
						
				}else if(extraMap.get(position + 1).equalsIgnoreCase("link")){
					LinearLayout innerLinear = new LinearLayout(context);
					Button btnLink = new Button(context);
					btnLink.setTag(extraList.get(position+1));
					//btnLink.setBackgroundResource(R.drawable.link_button);
					//btnLink.setLayoutParams(new LayoutParams(100, 100));
					
					//btnParam.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
					//btnParam.addRule(RelativeLayout.CENTER_HORIZONTAL);
					Display disp =((WindowManager)getSystemService(IssueActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
					//double left=disp.getWidth()*Double.parseDouble(c.getString(5));
					//Log.i("left","el valor es: "+c.getString(5));
					//int leftMargin=(int)(extraStartX.get(position+1).floatValue()*disp.getWidth());
					//int topMargin=(int)(extraStartY.get(position+1).floatValue()*disp.getHeight());
					
					Log.i("tama�o de pantalla", disp.getWidth()+"-"+disp.getHeight());
					
					IssueActivity issue = new IssueActivity();
					RelativeLayout.LayoutParams param = issue.getPosition(disp.getWidth(),disp.getHeight(),extraStartX.get(position+1).floatValue(), extraStartY.get(position+1).floatValue(), extraEndX.get(position+1).floatValue(), extraEndY.get(position+1).floatValue());
					Log.i("devuelto", param.leftMargin+"-"+param.topMargin+"-"+param.width+"-"+param.height);
					
					if(extraControls.get(position+1)==2){//invisible
						
					}else if(extraControls.get(position+1)==3){//invisible highlight
						
						//int tamX = (int)(extraEndX.get(position+1).floatValue()*disp.getWidth()-leftMargin);
						//int tamY = (int)(extraEndY.get(position+1).floatValue()*disp.getHeight()-topMargin);
						//btnLink.setBackgroundResource(R.color.background_issue);
						//Log.i("tam","El tam del boton es: "+tamX+" - "+tamY+"  MARGENES: "+leftMargin+" - "+topMargin);
						btnLink.setLayoutParams(new LayoutParams(param.width, param.height));
						btnLink.setBackgroundColor(Color.BLACK);
						btnLink.getBackground().setAlpha(80);
						
						final Animation animation = new AlphaAnimation(0, 1); // Change alpha from fully visible to invisible
					    animation.setDuration(1000); // duration - a second
					    animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
					    animation.setRepeatCount(Constant.blinks); // Repeat animation
					    animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
					    animation.setFillEnabled(true);
					    animation.setFillAfter(true);
					    btnLink.startAnimation(animation);
					    
					    
					    //btnLink.getBackground().setAlpha(100);
					    
					}else{//button
						if(extraType.get(position+1)==3){//link-web button
							btnLink.setBackgroundResource(R.drawable.link_button);
						}
						else if(extraType.get(position+1)==4){
							btnLink.setBackgroundResource(R.drawable.goto_page_button);
						    }
						else if(extraType.get(position+1)==5){
							btnLink.setBackgroundResource(R.drawable.link_webstore_button);
						    }
						else if(extraType.get(position+1)==6){
							btnLink.setBackgroundResource(R.drawable.link_map_button);
						    }
					}
					RelativeLayout.LayoutParams btnParam = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					btnParam.leftMargin=param.leftMargin;
					btnParam.topMargin=param.topMargin;
				    
					android.widget.LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lp.setMargins(0, 0, 0, 100);
					innerLinear.setLayoutParams(btnParam);
					//innerLinear.addView(btnLink, lp);
					innerLinear.addView(btnLink);
					reletiveLayout.addView(innerLinear);
					
					
					if(extraControls.get(position+1)==4){
						btnLink.setOnClickListener(new OnClickListener() {
							@Override 
							public void onClick(View v) { 
								gallery.setSelection(Integer.parseInt(v.getTag().toString())-1);
							} 
						});
					}
					else{
						btnLink.setOnClickListener(new OnClickListener() {
							@Override 
							public void onClick(View v) { 
								Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setData(Uri.parse((String)v.getTag())); 
								startActivity(intent);
							} 
						});
					}
				}
			}
			return reletiveLayout;
		}
	}

	public RelativeLayout.LayoutParams getPosition(int screenX, int screenY, float startX, float startY, float endX, float endY){
		
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		float pdfX=724, pdfY=1024;
		float scaleX=0,scaleY=0;
		float centerH=0, centerV=0;
		float scale=0;
		//Display display =((WindowManager)getSystemService(IssueActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
		
		Log.i("par",screenX+"-"+screenY+"-"+startX+"-"+startY+"-"+endX+"-"+endY);
		
		scaleX=screenX/pdfX;
		scaleY=screenY/pdfY;
		
		Log.i("escalas",scaleX+"-"+scaleY);
		
		if(scaleX<scaleY){
	        scale=scaleX;
	        centerV = Math.abs(screenY-pdfY*scaleX)/2;
	    }
	    else{
	        scale=scaleY;
	        centerH = Math.abs(screenX-pdfX*scaleY)/2;
	    }
		
		param.leftMargin=(int)(centerH+(startX*pdfX)*scale);
		param.topMargin=(int)(centerV+(startY*pdfY)*scale);
		param.width=(int)((endX-startX)*pdfX*scale);
		param.height=(int)((endY-startY)*pdfY*scale);
		
		Log.i("calculado", param.leftMargin+"-"+param.topMargin+"-"+param.width+"-"+param.height);
		
		return param;
	}
	
	public void updateFavorites(){

		Display disp2 =((WindowManager)getSystemService(IssueActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
		int width = disp2.getWidth()/3;
	    int height=disp2.getHeight()/3;
		
		fav = new Dialog(this, R.style.MyDialog);
		fav.setContentView(R.layout.favs_view);
		fav.getWindow().setLayout(width,height);
		fav.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
		//fav.getWindow().setAttributes(a)
		WindowManager.LayoutParams params = fav.getWindow().getAttributes();
		params.y=60;
		fav.getWindow().setAttributes(params);
		fav.show();
		
		
		final SharedPreferences preferences = this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
		
		fav.findViewById(R.id.btn_addfav).setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				//IssueActivity.favClicked(preferences);
				
				IssueActivity issue = new IssueActivity();
				issue.favClicked(preferences, reletiveLayout.getId(), URLEncoder.encode(issueName));
			}
		});
		
		File folder = new File(Constant.getAppFilepath(IssueActivity.this) + "/" + URLEncoder.encode(issueName) + "/Thumb");
		File[] thumbList = folder.listFiles();

		allFiles = new HashMap<Integer, String>();
		for (int i = 0; i < thumbList.length; i++) {
			String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_")+1, thumbList[i].getName().lastIndexOf("."));
			allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
		}

		Uri[] thumbUriFiles = new Uri[allFiles.size()];
		for (int i = 0; i < allFiles.size(); i++) {
			thumbUriFiles[i] = Uri.parse(allFiles.get(i));
		}
		
		LinearLayout layout = (LinearLayout) fav.findViewById(R.id.scroll);
		LayoutInflater inflat = (LayoutInflater) getSystemService(fav.getContext().LAYOUT_INFLATER_SERVICE);
		
		for(int i=1;i<=gallery.getCount();i++){
			
			int index = preferences.getInt(URLEncoder.encode(issueName)+"_"+i, 0);
			if(index==1){
				View placeHolder = inflat.inflate(R.layout.favs_placeholder, null);
				placeHolder.setTag(i-1);
				ImageView img = (ImageView) placeHolder.findViewById(R.id.img_fav);
				img.setImageURI(thumbUriFiles[i-1]);
				TextView txt = (TextView) placeHolder.findViewById(R.id.txt_fav);
				txt.setText("" + (i));
				Log.i("info","Hago esto, index: "+URLEncoder.encode(issueName)+(i-1));
				
				layout.addView(placeHolder);
				
				placeHolder.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Integer id = (Integer) v.getTag();
						gallery.setSelection(id);
						//thumb.dismiss();
					}
				});
			}
		}
	}
	
	public void favClicked(SharedPreferences preferences, int index, String issueName){
		Log.i("btn","button add pressed y index: "+index);
		//preferences = this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(issueName+"_"+(index+1), 1);
		
		/*int size = preferences.getInt(issueName+"_fav_size", 0);
		size++;
		editor.putInt(issueName+"_fav_size", size);*/
		editor.commit();
		
		/*View placeHolder = layoutForHorizontalList.findViewWithTag(index);
		ImageView star = (ImageView) placeHolder.findViewById(R.id.favStar);
		star.setImageResource(R.drawable.fav_star);*/
		
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_home:
			Intent intent = new Intent(IssueActivity.this, MainActivity.class);
			startActivity(intent);
			break;
		case R.id.btn_info:
			
			Display display =((WindowManager)getSystemService(IssueActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
		    int width = display.getWidth();
		    int height=display.getHeight();
			
			Dialog ab = new Dialog(this, R.style.InfoDialog);
			ab.setContentView(R.layout.info_layout);
			ab.getWindow().setLayout(width,height-60);
			
			WindowManager.LayoutParams paramsInfo = ab.getWindow().getAttributes();
			paramsInfo.y=60;
			ab.getWindow().setAttributes(paramsInfo);
			
			if(isLandscap){
				ab.getWindow().setLayout(height-60, width);
				ImageView img = (ImageView)findViewById(R.id.imgHelp);
				img.setImageResource(R.drawable.help_landscape_es);
			}
			
			ab.show();

			break;
		case R.id.btn_favorites:
			
			/*dbAdapter.open();
			Cursor c = dbAdapter.getData(issueName);
			if(c.getCount() > 0){
				while(c.moveToNext()){
					String titleName = c.getString(2);
					if(titleName.equalsIgnoreCase("summery")){
						if(c.getString(3).equalsIgnoreCase("true"))
							gallery.setSelection(Integer.parseInt(c.getString(4)));
					}
				}
			}
			c.close();
			dbAdapter.close();*/
			Display disp2 =((WindowManager)getSystemService(IssueActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
			width = disp2.getWidth()/3;
		    height=disp2.getHeight()/3;
			
			fav = new Dialog(this, R.style.MyDialog);
			fav.setContentView(R.layout.favs_view);
			fav.getWindow().setLayout(width,height);
			fav.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
			//fav.getWindow().setAttributes(a)
			WindowManager.LayoutParams params = fav.getWindow().getAttributes();
			params.y=60;
			fav.getWindow().setAttributes(params);
			fav.show();
			
			final SharedPreferences preferences = this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
			
			fav.findViewById(R.id.btn_addfav).setOnClickListener(new OnClickListener(){
				public void onClick(View v) {
					//IssueActivity.favClicked(preferences);
					
					IssueActivity issue = new IssueActivity();
					issue.favClicked(preferences, reletiveLayout.getId(), URLEncoder.encode(issueName));
				}
			});
			
			
			File folder = new File(Constant.getAppFilepath(this) + "/" + URLEncoder.encode(issueName) + "/Thumb");
			File[] thumbList = folder.listFiles();

			allFiles = new HashMap<Integer, String>();
			for (int i = 0; i < thumbList.length; i++) {
				String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_")+1, thumbList[i].getName().lastIndexOf("."));
				allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
			}

			Uri[] thumbUriFiles = new Uri[allFiles.size()];
			for (int i = 0; i < allFiles.size(); i++) {
				thumbUriFiles[i] = Uri.parse(allFiles.get(i));
			}
			
			LinearLayout layout = (LinearLayout) fav.findViewById(R.id.scroll);
			LayoutInflater inflat = (LayoutInflater) getSystemService(fav.getContext().LAYOUT_INFLATER_SERVICE);
			
			
			for(int i=1;i<=gallery.getCount();i++){
				
				int index = preferences.getInt(URLEncoder.encode(issueName)+"_"+i, 0);
				if(index==1){
					View placeHolder = inflat.inflate(R.layout.favs_placeholder, null);
					placeHolder.setTag(i-1);
					ImageView img = (ImageView) placeHolder.findViewById(R.id.img_fav);
					img.setImageURI(thumbUriFiles[i-1]);
					TextView txt = (TextView) placeHolder.findViewById(R.id.txt_fav);
					txt.setText("" + (i));
					Log.i("info","Hago esto, index: "+URLEncoder.encode(issueName)+(i-1));
					
					layout.addView(placeHolder);
					
					placeHolder.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							Integer id = (Integer) v.getTag();
							gallery.setSelection(id);
							//thumb.dismiss();
						}
					});
				}
			}
			
			//IssueActivity issue = new IssueActivity();
			//issue.updateFavorites();
		
			break;
		case R.id.btn_share:
			Dialog share = new Dialog(this, R.style.MyDialog);
			share.setContentView(R.layout.share_vieww);
			share.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
			//share.getWindow().set
			//fav.getWindow().setAttributes(a)
			WindowManager.LayoutParams params2 = share.getWindow().getAttributes();
			params2.y=60;
			share.getWindow().setAttributes(params2);
			share.show();
			
			
			 folder = new File(Constant.getAppFilepath(IssueActivity.this) + "/" + URLEncoder.encode(issueName) + "/Thumb");
			 thumbList = folder.listFiles();

			allFiles = new HashMap<Integer, String>();
			for (int i = 0; i < thumbList.length; i++) {
				String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_")+1, thumbList[i].getName().lastIndexOf("."));
				allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
			}

			 thumbUriFiles = new Uri[allFiles.size()];
			for (int i = 0; i < allFiles.size(); i++) {
				thumbUriFiles[i] = Uri.parse(allFiles.get(i));
			}
			final Uri image = thumbUriFiles[reletiveLayout.getId()];
			Log.i("Imagen a compartir"," " + image.getPath());
			
			share.findViewById(R.id.btn_twitter).setOnClickListener(new OnClickListener(){
				public void onClick(View v) {
					Log.i("share","Share page on twitter clicked");
					
					Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
				    shareIntent.setType("*/*");
				    shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.socialText) + " " + getString(R.string.socialLink));
				    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, image);
				 
				    final PackageManager pm = v.getContext().getPackageManager();
				    final List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
				    for (final ResolveInfo app : activityList) {
				      if ("com.twitter.android.PostActivity".equals(app.activityInfo.name)) {
				        final ActivityInfo activity = app.activityInfo;
				        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
				        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
				        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
				        shareIntent.setComponent(name);
				        v.getContext().startActivity(shareIntent);
				        break;
				      }
				    }

				}
			});
			share.findViewById(R.id.btn_facebook).setOnClickListener(new OnClickListener(){
				public void onClick(View v) {
					
					Log.i("share","Share page on facebook clicked");
				}
			});
			share.findViewById(R.id.btn_mail).setOnClickListener(new OnClickListener(){
				public void onClick(View v) {
					Log.i("share","Share page by mail clicked");

					final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
               
                    //emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, Constant.mailText);
             
                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.mailSubject));
             
                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.mailText));
                    
                    emailIntent.setType("image/jpg");
                    
                    //emailIntent.putExtra(android.content.Intent.EXTRA_STREAM, image);
     
                    v.getContext().startActivity(Intent.createChooser(emailIntent, "Send mail..."));
				}
			});
			share.findViewById(R.id.btn_print).setOnClickListener(new OnClickListener(){
				public void onClick(View v) {
					
					Log.i("share","Print page clicked");
				}
			});
			
			break;
		case R.id.btn_thumbnail:
			/*Intent intent1 = new Intent(IssueActivity.this,MyLibraryActivity.class);
			intent1.putExtra("isDownload", false);
			startActivity(intent1);
			finish();*/
			Display disp =((WindowManager)getSystemService(IssueActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
			width = disp.getWidth();
		    height=disp.getHeight();
			thumb = new Dialog(IssueActivity.this);
			thumb.setContentView(R.layout.thumbs_view);
			thumb.getWindow().setLayout(width,height);
			
			//LinearLayout linearlayout = (LinearLayout) findViewById(R.id.thumbLayout);
			//TextView text = (TextView) thumb.findViewById(R.id.txtThumb);
			//text.setText("@string/btn_store");
			
			LinearLayout linearlayout = (LinearLayout) thumb.findViewById(R.id.thumbsLayout);
			//GridView grid = (GridView) thumb.findViewById(R.id.gridThumbs);
			LayoutInflater inflater = (LayoutInflater) getSystemService(thumb.getContext().LAYOUT_INFLATER_SERVICE);
			
			File folder2 = new File(Constant.getAppFilepath(IssueActivity.this) + "/" + URLEncoder.encode(issueName) + "/Thumb");
			File[] thumbList2 = folder2.listFiles();

			allFiles = new HashMap<Integer, String>();
			for (int i = 0; i < thumbList2.length; i++) {
				String id = thumbList2[i].getName().substring(thumbList2[i].getName().lastIndexOf("_")+1, thumbList2[i].getName().lastIndexOf("."));
				allFiles.put(Integer.parseInt(id), thumbList2[i].getAbsolutePath());
			}

			Uri[] thumbUriFiles2 = new Uri[allFiles.size()];
			for (int i = 0; i < allFiles.size(); i++) {
				thumbUriFiles2[i] = Uri.parse(allFiles.get(i));
			}
			
			int col=0;
			for (int i = 0; i < thumbUriFiles2.length; i++) {

				View placeHolder = inflater.inflate(R.layout.thumbs_view, null);
				placeHolder.setTag(i);
				imgThumb = (ImageView) placeHolder.findViewById(R.id.imgThumb);
				imgThumb.setImageURI(thumbUriFiles2[i]);
				txtPageNumber = (TextView) placeHolder.findViewById(R.id.txtThumb);
				txtPageNumber.setText("" + (i+1));

				linearlayout.addView(placeHolder);
				
				//grid.addView(placeHolder);
				placeHolder.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Integer id = (Integer) v.getTag();
						gallery.setSelection(id);
						thumb.dismiss();
					}
				});
				Log.i("index","iterador es: "+i);
				col++;
				if(col==3){
					LinearLayout newRow = new LinearLayout(this);
					newRow.setOrientation(LinearLayout.VERTICAL);
					newRow.setLayoutParams(new LinearLayout.LayoutParams(
                            LayoutParams.MATCH_PARENT,
                            LayoutParams.MATCH_PARENT, 1.0f));
					linearlayout.addView(newRow);
					col=0;
					Log.i("row","salto de linea");
				}
			}
			
			//try{
				/*imgThumb = (ImageView)findViewById(R.id.imgThumb);
				imgThumb.setImageURI(thumbUriFiles[0]);
				imgThumb.setTag(0);
				Log.i("img",""+thumbUriFiles[0].getEncodedPath());
				
				imgThumb.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Integer id = (Integer) v.getTag();
						gallery.setSelection(id);
					}
				});*/
			//}
			//catch (Exception e){};
			
			
			thumb.show();
			
			break;
		default:
			break;
		}
	}
}