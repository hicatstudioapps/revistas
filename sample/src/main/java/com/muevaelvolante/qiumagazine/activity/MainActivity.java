package com.muevaelvolante.qiumagazine.activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.util.IabHelper;
import com.android.vending.util.IabResult;
import com.android.vending.util.Inventory;
import com.android.vending.util.Purchase;
import com.artifex.mupdflib.MuPDFActivity;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.adapter.DBAdapterIssues;
import com.muevaelvolante.qiumagazine.app.Magazine;
import com.muevaelvolante.qiumagazine.bean.StoreProductBean;
import com.muevaelvolante.qiumagazine.utils.AysnTaskCompleteListener;
import com.muevaelvolante.qiumagazine.utils.Constant;
import com.muevaelvolante.qiumagazine.utils.EditionSyncTask;
import com.muevaelvolante.qiumagazine.utils.ImageDownLoader;
import com.muevaelvolante.qiumagazine.utils.Internet;
import com.muevaelvolante.qiumagazine.utils.ManagerGoogleAnalytics;
import com.muevaelvolante.qiumagazine.utils.StoreProductUtils;
import com.muevaelvolante.qiumagazine.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import retrofit2.http.Url;

@SuppressWarnings("WrongConstant")
public class MainActivity extends Activity implements
		AysnTaskCompleteListener<ArrayList<StoreProductBean>>,
		OnClickListener{

	public static String PACKAGE_NAME;
	private LinearLayout layoutForHorizontalList, mainLinearlayout;
	private LinearLayout layoutIssuImage;
	private ImageView imgMagazineIssue, imgIssue;
	private TextView txtIssueText, txtCurrentTitle, txtCurrentDesc, txtMode;
	private Button btnIssue, btnStore, btnMyLibrary, btnCurrentIssue /*btnCurrentIssueSubscriptions*/;
	EditionSyncTask editionSyncTask;
	URL urlEdition;
	private File filePath;
	public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
	private String currentAppPath;
	private DBAdapterIssues dbAdapterIssues = null;
	public ArrayList<StoreProductBean> issues;
	IabHelper mHelper;
	
	private Dialog accountDialog;
	private Button accountButton;
	

	
	private StoreProductBean buyingStoreproductBean;
	private TextView subsTv;
	//private Boolean buyingSubscription=false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		PACKAGE_NAME=getApplicationContext().getPackageName();
		File f = new File(Constant.getAppFilepath(this));
		if(!f.exists())
			f.mkdirs();
		
		btnStore = (Button) findViewById(R.id.btn_store);
		btnMyLibrary = (Button) findViewById(R.id.btn_librarys);
		imgIssue = (ImageView) findViewById(R.id.img_issue);
		mainLinearlayout = (LinearLayout)findViewById(R.id.main_linearlayout);
		layoutIssuImage = (LinearLayout) findViewById(R.id.layout_issuimage);
		txtCurrentTitle = (TextView) findViewById(R.id.txt_current_issue_title);
		txtCurrentDesc = (TextView) findViewById(R.id.txt_current_issue_desc);
		btnCurrentIssue = (Button) findViewById(R.id.btn_current_issue);

		//btnCurrentIssueSubscriptions = (Button) findViewById(R.id.btn_current_issue_subscription);
		currentAppPath = Constant.getAppFilepath(this)+ "/";
		filePath = new File(currentAppPath);
		dbAdapterIssues = new DBAdapterIssues(this);
		
		accountButton = (Button) findViewById(R.id.accountButton);
		
		String base64EncodedPublicKey=Constant.RSAKey;
		mHelper = new IabHelper(MainActivity.this, base64EncodedPublicKey);
		mHelper.enableDebugLogging(true);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			   public void onIabSetupFinished(IabResult result) {
			      if (!result.isSuccess()) {
			         // Oh noes, there was a problem.
			         Log.d("onCreate", "Problem setting up In-app Billing: " + result);
			      }
				   else{
					  Log.d("onCreate", "setting up In-app Billing: " + result);
				  }
			         // Hooray, IAB is fully set up!  
			   }
			});
		
		//Format date from web
		
		/*try {
			urlEdition = new URL(Constant.S3_URL+"/apps/"+Constant.API_KEY+".info.json");
			editionSyncTask = new EditionSyncTask(this, this, "InfoEditionList");
			editionSyncTask.execute(urlEdition);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}*/
		
		/*
		String dtStart = "2010-10-15T09:27:37Z";  
		SimpleDateFormat  format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");  
		try {  
		    Date date = format.parse(dtStart);  
		    System.out.println(date);  
		} catch (ParseException e) {
		    e.printStackTrace();  
		}
		*/
		
		//!(getIntent().getStringExtra("source").equalsIgnoreCase("library"))
		Log.i("source","from: "+getIntent().getBooleanExtra("library", false));
		//SharedPreferences prefs = this.getSharedPreferences("MyPreferences", MODE_PRIVATE);  
		
		/*
		Ya no es necesario cargarlo aquí porque se carga en ActivityInitLoading
		try {
			if (Internet.checkConnection(this)/* && prefs.getBoolean("Initialized", false)==false/ && !getIntent().getBooleanExtra("library", false)) {
				urlEdition = new URL(Constant.S3_URL+"/apps/"+Constant.API_KEY+".info.json");
				editionSyncTask = new EditionSyncTask(this, this, "InfoEditionList");
				editionSyncTask.execute(urlEdition);
			} else {
				loadFromInternalDB();
			}
		} catch (Exception e) {

		}*/

		btnMyLibrary.setOnClickListener(this);
		btnStore.setOnClickListener(this);
		accountButton.setOnClickListener(this);


		if (Internet.checkConnection(this))
		{
			if( getIntent().getBooleanExtra("needRefreshFromInternet", true))
				loadFromWeb();
			else
				loadFromInternalDB();
		}
		else
		{
			loadFromInternalDB();
		}
	}
	
	public void loadFromWeb(){
		
		//urlEdition = new URL(Constant.API_URL+"&key="+Constant.API_KEY+"&action=issues&module=magazine");
		try {
			editionSyncTask = new EditionSyncTask(this, this, "EditionList");
			
			urlEdition = new URL(Constant.S3_URL+"/apps/"+getString(R.string.API_KEY)+".issues.json");
//			urlEdition = new URL("http://192.168.101.1:8081/revistas/"+getString(R.string.API_KEY)+".issues.json");
			Log.d("url Isue",urlEdition.toString());
			//if is the first time, register it
			final SharedPreferences preferences = this.getSharedPreferences("MyPreferences", MODE_PRIVATE);  
			if(preferences.getBoolean("Initialized", false)==false){
				Log.i("inicializando","inicializando");
				Thread trd = new Thread(new Runnable(){
					  @Override
					  public void run(){
					    //code to do the HTTP request
							try {
								String stat = "&stats=true&stat_type=1&device="+Build.MODEL+"&software=Android "+Build.VERSION.RELEASE;
								stat=stat.replaceAll(" ", "%20");
								SharedPreferences.Editor editor = preferences.edit();
								editor.putBoolean("Initialized", true);
								editor.commit();
								URL url = new URL(getString(R.string.API_URL)+"&key="+getString(R.string.API_KEY)+"&action=issues&module=magazine"+stat);
								
								DefaultHttpClient client = new DefaultHttpClient();
								HttpGet httpGet = new HttpGet(url.toString());
							//	HttpResponse execute = client.execute(httpGet);
								
							} catch (MalformedURLException e) {
								e.printStackTrace();
							} catch (IOException e) {

								e.printStackTrace();
							}
					  }
					});
				trd.start();
				
			}
			
			editionSyncTask.execute(urlEdition);
			layoutForHorizontalList = (LinearLayout) findViewById(R.id.layoutForHorizontalList);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}		
		
	}
	
	public void loadFromInternalDB(){

		layoutForHorizontalList = (LinearLayout) findViewById(R.id.layoutForHorizontalList);
		ArrayList<StoreProductBean> getAllStoreProductList = StoreProductUtils.loadFromInternalDB(dbAdapterIssues);
		issues=getAllStoreProductList;
		setEditionUI(getAllStoreProductList);
	}


	@Override
	public void onStart() {
		super.onStart();

		GoogleAnalytics.getInstance(this).reportActivityStart(this);
		ManagerGoogleAnalytics.getInstance().sendVisitScreen(this, getString(R.string.storeTrackedName));
	}




    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPref = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);

        long lastUpdate = sharedPref.getLong("dateLongLastUpdate", 0);
        long currentDateMillis = Utils.getCurrentTimeInMillis();

        if ( currentDateMillis - lastUpdate > Utils.HOURS_24_MILLIS ) {

            java.util.Date currentDate = new java.util.Date( currentDateMillis );

            ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
            parseInstallation.put("last_visit", currentDate);
            parseInstallation.saveEventually();

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putLong("dateLongLastUpdate", currentDateMillis);
            editor.apply();

        }

    }

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public void onStop() {
        super.onStop();
		dbAdapterIssues.close();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	public void onTaskComplete(ArrayList<StoreProductBean> result, String method) {

		if (method.equalsIgnoreCase("EditionList")) {
			if(result != null){
				setEditionUI(result);
				MainActivity.this.issues=result;
				for(int i=0;i<result.size();i++){
					dbAdapterIssues.open();
					Cursor c = dbAdapterIssues.getData(result.get(i).getEditionId());
					//save only new issues
					if(c.getCount()==0){
						dbAdapterIssues.open();
						dbAdapterIssues.inserData(result.get(i).getEdition(), result.get(i).getEditionId(), result.get(i).getEditionName(), result.get(i).getIssueDate(), result.get(i).getScreenOrientation(), result.get(i).getLang(), result.get(i).getPrice(), result.get(i).getDescription(), result.get(i).getCoverName(), result.get(i).getCoverUrl(), result.get(i).getTotalPage(), result.get(i).getSubscriptions(), result.get(i).getdownloadPDF(), result.get(i).getPublisher_price(), result.get(i).getReference_number(), result.get(i).getMode());
						dbAdapterIssues.close();
					}
					else{
						dbAdapterIssues.open();
						dbAdapterIssues.updateEdition(result.get(i).getEditionId(), result.get(i).getEditionName(), result.get(i).getIssueDate(), result.get(i).getScreenOrientation(), result.get(i).getLang(), result.get(i).getPrice(), result.get(i).getDescription(), result.get(i).getSubscriptions(), result.get(i).getPublisher_price(), result.get(i).getReference_number(), result.get(i).getMode());
						dbAdapterIssues.close();
					}
					c.close();
					dbAdapterIssues.close();
				}
				
				dbAdapterIssues.open();
				Cursor c = dbAdapterIssues.getAllData();
				while(c.moveToNext()){
					Boolean find=false;
					Log.i("db","comprobando: "+c.getString(1));
					for(int i=0;i<result.size();i++){
						if(result.get(i).getEditionId().equals(c.getString(1)))
							find=true;
					}

					Log.i("find","find vale: "+find);

					if(!find){
						StoreProductBean product = new StoreProductBean();
						product.setEditionName(c.getString(2));
						product.setTotalPage(c.getString(10));
						if(!isDownloaded((StoreProductBean)product)){
								System.out.println("Se borraria: "+c.getString(1));
								dbAdapterIssues.deleteEdition(c.getString(1));
						}
					}
				}
				c.close();
				dbAdapterIssues.close();
			}else{
				loadFromInternalDB();
				//Toast.makeText(this, "Record Not Found ..", Toast.LENGTH_LONG).show();
			}
		}
	}
	
	public void onTaskComplete(String result, String method) {
	}
	int getScreenWidth(Activity activity) {

		Display display = activity.getWindowManager().getDefaultDisplay();
		Point size = new Point();

		int width;
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB_MR2) {
			display.getSize(size);
			width = size.x;
		} else {
			width = display.getWidth();

		}
	    return width;
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_store:
			layoutIssuImage.setVisibility(View.VISIBLE);
			mainLinearlayout.setBackgroundResource(R.drawable.store_background);
			layoutForHorizontalList.removeAllViewsInLayout();
			if (Internet.checkConnection(this))
			{
				if( getIntent().getBooleanExtra("needRefreshFromInternet", true))
					loadFromWeb();
				else
					loadFromInternalDB();
			}
			else
			{
				loadFromInternalDB();
			}
			break;

		case R.id.btn_librarys:
			Bundle bundle= new Bundle();
			bundle.putSerializable("issues",MainActivity.this.issues);
			Intent intent = new Intent(MainActivity.this,MyLibraryActivity.class);
			intent.putExtras(bundle);
			intent.putExtra("isDownload", false);
			startActivity(intent);
			break;
		case R.id.accountButton:
			System.out.println("Account pressed");
			accountDialog = new Dialog(this, R.style.MyDialog);
			accountDialog.setContentView(R.layout.accounts);
			subsTv=(TextView)accountDialog.findViewById(R.id.subs_info);
			if(ParseUser.getCurrentUser()!=null){
				accountDialog.findViewById(R.id.top).setVisibility(View.GONE);
				accountDialog.findViewById(R.id.buttons).setVisibility(View.GONE);
				SharedPreferences preferences = MainActivity.this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
				subsTv.setText(preferences.getString("subs_time",""));
				subsTv.setVisibility(View.VISIBLE);
				accountDialog.findViewById(R.id.logout).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						ParseUser.logOutInBackground();
						subsTv.setText("");
						subsTv.setVisibility(View.GONE);
						accountDialog.dismiss();
					}
				});
				accountDialog.findViewById(R.id.exit).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						finish();
					}
				});
				accountDialog.show();
				return;
			}

			FrameLayout.LayoutParams lp= new FrameLayout.LayoutParams((int) (getScreenWidth(MainActivity.this)*0.8), LinearLayout.LayoutParams.WRAP_CONTENT);
			accountDialog.findViewById(R.id.layoutAccount).setLayoutParams(lp);
			accountDialog.findViewById(R.id.textView).setVisibility(View.GONE);
			accountDialog.findViewById(R.id.out).setVisibility(View.GONE);
			
			/*Display disp2 =((WindowManager)getSystemService(MainActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
			int width = disp2.getWidth()/3;
		    int height=disp2.getHeight()/3;*/
			
			/*accountDialog.setContentView(R.layout.favs_view);
			accountDialog.getWindow().setLayout(width,height);
			accountDialog.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
			//fav.getWindow().setAttributes(a)
			WindowManager.LayoutParams params = accountDialog.getWindow().getAttributes();
			params.y=60;
			accountDialog.getWindow().setAttributes(params);*/
			
			accountDialog.findViewById(R.id.accountCancelButton).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {

					accountDialog.dismiss();
				}
			});
			
			accountDialog.findViewById(R.id.accountValidateButton).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {

					if(((EditText)accountDialog.findViewById(R.id.accountUserTextfield)).getText().toString().length()>0 && ((EditText)accountDialog.findViewById(R.id.accountPassTextfield)).getText().toString().length()>0){

						//Añadimos un una comprobación especial para discernir si es un usuario admin/preview y tendrá acceso a descargas
						String user_ = ((EditText)accountDialog.findViewById(R.id.accountUserTextfield)).getText().toString();
						String pass_ = ((EditText)accountDialog.findViewById(R.id.accountPassTextfield)).getText().toString();

						if(Utils.checkIfUserPreview(MainActivity.this,user_,pass_)) {
							accountDialog.dismiss();
                            layoutForHorizontalList.removeAllViewsInLayout();
                            loadFromWeb();
							return;
						}

						ParseUser.logInInBackground(((EditText)accountDialog.findViewById(R.id.accountUserTextfield)).getText().toString(), ((EditText)accountDialog.findViewById(R.id.accountPassTextfield)).getText().toString(), new LogInCallback() {

							@Override
							public void done(ParseUser arg0,
									com.parse.ParseException arg1) {

								if(arg0!=null){
									System.out.println("Logueado como: "+arg0.getUsername());
									
									if(arg0.getNumber("id_publication").intValue()!=getResources().getInteger(R.integer.id_publication)){
										AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
										alertDialog.setTitle(MainActivity.this.getResources().getString(R.string.error));
										alertDialog.setMessage(MainActivity.this.getResources().getString(R.string.user_or_password_incorrect));
										alertDialog.setCanceledOnTouchOutside(true);
										alertDialog.setCancelable(true);
										alertDialog.show();
										
										ParseUser.logOut();
										
										return;
									}
									
									if(arg0.getNumber("quota").intValue()==0){
										AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
										alertDialog.setTitle(MainActivity.this.getResources().getString(R.string.error));
										alertDialog.setMessage(MainActivity.this.getResources().getString(R.string.quota_exceed));
										alertDialog.setCanceledOnTouchOutside(true);
										alertDialog.setCancelable(true);
										alertDialog.show();
										
										ParseUser.logOut();
										
										return;
									}
									
									SharedPreferences preferences = MainActivity.this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
									SharedPreferences.Editor editor = preferences.edit();
									editor.putLong("dateFrom", arg0.getDate("dateFrom").getTime());
									editor.putLong("dateTo", arg0.getDate("dateTo").getTime());
									editor.commit();
									SimpleDateFormat  formatter = new SimpleDateFormat("dd-MM-yyyy");
									AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
									alertDialog.setTitle(MainActivity.this.getResources().getString(R.string.error));
									editor.putString("subs_time",MainActivity.this.getResources().getString(R.string.subscription_active)+" "+formatter.format(arg0.getDate("dateFrom"))+" "+MainActivity.this.getResources().getString(R.string.to)+" "+formatter.format(arg0.getDate("dateTo")));
									editor.commit();
									subsTv.setText(MainActivity.this.getResources().getString(R.string.subscription_active)+" "+formatter.format(arg0.getDate("dateFrom"))+" "+MainActivity.this.getResources().getString(R.string.to)+" "+formatter.format(arg0.getDate("dateTo")));
									subsTv.setVisibility(View.VISIBLE);
									alertDialog.setMessage(MainActivity.this.getResources().getString(R.string.subscription_active)+" "+formatter.format(arg0.getDate("dateFrom"))+" "+MainActivity.this.getResources().getString(R.string.to)+" "+formatter.format(arg0.getDate("dateTo")));
									alertDialog.setCanceledOnTouchOutside(true);
									alertDialog.setCancelable(true);
									alertDialog.show();
																		arg0.put("quota", ((arg0.getNumber("quota").intValue())-1));

									//Añadir installations ids
									ArrayList <String> installationIds = new ArrayList<String>();
									JSONArray installationIdsJson = arg0.getJSONArray("installation_ids");
									if(installationIdsJson != null)
									{
										for(int i=0; i<installationIdsJson.length(); ++i)
										{
											String aux = (String) installationIdsJson.opt(i);
											installationIds.add(aux);
										}
									}

									//Comprobamos si ya existe
									boolean exists = false;
									for(String aux : installationIds)
									{
										if(aux.equalsIgnoreCase(ParseInstallation.getCurrentInstallation().getInstallationId()))
										{
											exists = true;
											break;
										}
									}

									if(!exists)
									{
										installationIds.add(ParseInstallation.getCurrentInstallation().getInstallationId());
										arg0.put("installation_ids",installationIds);
									}

									arg0.saveEventually();
									
									alertDialog.setOnDismissListener(new AlertDialog.OnDismissListener() {
										
										@Override
										public void onDismiss(DialogInterface dialog) {

										}
									});
									layoutForHorizontalList.removeAllViewsInLayout();
									loadFromInternalDB();
									accountDialog.dismiss();
								}
								else{
									AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
									alertDialog.setTitle(MainActivity.this.getResources().getString(R.string.error));
									alertDialog.setMessage(MainActivity.this.getResources().getString(R.string.user_or_password_incorrect));
									alertDialog.setCanceledOnTouchOutside(true);
									alertDialog.setCancelable(true);
									alertDialog.show();
								}
							}
						});

					}
					else{
						
						AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
						alertDialog.setTitle(MainActivity.this.getResources().getString(R.string.error));
						alertDialog.setMessage(MainActivity.this.getResources().getString(R.string.user_password_empty));
						alertDialog.setCanceledOnTouchOutside(true);
						alertDialog.setCancelable(true);
						alertDialog.show();
					}
				}
			});
			accountDialog.show();
			break;
		default:
			break;
		}
	}

	public Boolean isSubscriptionActive(StoreProductBean issue){
		
		SharedPreferences preferences = MainActivity.this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
		long millisFrom = preferences.getLong("dateFrom", 0L);
		long millisTo = preferences.getLong("dateTo", 0L);
		Date dateFrom = new Date(millisFrom);
		Date dateTo = new Date(millisTo);
		
		SimpleDateFormat  formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date auxDate = (java.util.Date) formatter.parse(issue.getIssueDate());
			java.sql.Date issueDate = new java.sql.Date(auxDate.getTime());
			
			Log.i("date", "dates: "+dateFrom.toString()+" ### "+issueDate.toString()+" ### "+dateTo.toString());
			if(issueDate.after(dateFrom) && issueDate.before(dateTo))
				return true;
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		} 
		
		return false;
	}
	
	public Boolean isDownloaded(StoreProductBean issue){
		File currentFile = new File(currentAppPath + URLEncoder.encode(issue.getEditionId()));
		if(currentFile.exists()){

			Log.i("Path is",currentFile.toString());
			int total=0;
			String[] currentList=currentFile.list();
			for(int j=0;j<currentFile.list().length;j++){
				if(currentFile.listFiles()[j].isDirectory()){
					File f = new File(currentFile.listFiles()[j].getPath());
					total += f.list().length;
				}
				else{
					total++;
				}
			}
			Log.i("total files",""+total);
			
			if(total>Integer.parseInt(issue.getTotalPage()))
				return true;
		}
		
		return false;
	}
	
	/* Store Product UI */
	public void setEditionUI(ArrayList<StoreProductBean> result) {
		
		Log.i("count","hay: "+result.size());

		final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final Context context = this;
		View container= findViewById(R.id.bottom_issuelist_layout);
		container.measure(0,0);
		int height= container.getMeasuredHeight();
		for (int i = 0; i < result.size(); i++) {
			View placeHolder = inflater.inflate(R.layout.horizontal_placeholder, null);

			placeHolder.setTag(result.get(i));
			imgMagazineIssue = (ImageView) placeHolder.findViewById(R.id.imgMagazineIssue);
			txtIssueText = (TextView) placeHolder.findViewById(R.id.txtIssue);
			btnIssue = (Button) placeHolder.findViewById(R.id.btnIssue);
            txtMode = (TextView) placeHolder.findViewById(R.id.txtStatus);



			btnIssue.setTag(result.get(i));
			btnIssue.setId(Integer.parseInt(((StoreProductBean) result.get(i)).getEdition()));
			txtIssueText.setText(((StoreProductBean) result.get(i)).getEditionName());
			if(result.get(i).getEditionName().length()>25)
				txtIssueText.setText(result.get(i).getEditionName().subSequence(0, 22)+"...");
		//	else
				ImageLoader.getInstance().displayImage(result.get(i).getCoverUrl(), imgMagazineIssue, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String s, View view) {

					}

					@Override
					public void onLoadingFailed(String s, View view, FailReason failReason) {

					}

					@Override
					public void onLoadingComplete(String s, View view, Bitmap bitmap) {
						if(getResources().getBoolean(R.bool.isTablet)){
							float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
							int alto= (int) ((bitmap.getWidth()*0.8)/proportion);
							LinearLayout.LayoutParams lp= (LinearLayout.LayoutParams)view.getLayoutParams();
							lp.height=alto;
							lp.width= (int) (bitmap.getWidth()*0.8);
							view.setLayoutParams(lp);
						}
					}

					@Override
					public void onLoadingCancelled(String s, View view) {

					}
				});
			Log.d("name",result.get(i).getEditionName());
			Log.d("name",result.get(i).getCoverUrl());

            if (Utils.isUserAdmin(this)) {
                txtMode.setVisibility(View.VISIBLE);
                txtMode.setText(result.get(i).getMode());
                txtMode.setCompoundDrawablesWithIntrinsicBounds(StoreProductUtils.getModeDrawable( result.get(i).getMode() ), 0, 0 ,0);

            } else {
                txtMode.setVisibility(View.GONE);
            }

			if(filePath.exists()){

				
				if(isDownloaded((StoreProductBean)result.get(i))){
					btnIssue.setText(R.string.read);
					btnIssue.setBackgroundResource(R.drawable.btn_read);	
				}else{
					if(Utils.isUserAdmin(getApplicationContext()))
						btnIssue.setText(R.string.download);
					else if (((StoreProductBean) result.get(i)).getPrice().equalsIgnoreCase("null")) {
						btnIssue.setText(R.string.download);
					}else{
						if(isSubscriptionActive((StoreProductBean)result.get(i)))
							btnIssue.setText(R.string.subscribe);
						else
							btnIssue.setText(((StoreProductBean) result.get(i)).getPrice()+" "+getString(R.string.currency));
					}
					btnIssue.setBackgroundResource(R.drawable.btn_buy);
				}
			}else{

				if(Utils.isUserAdmin(getApplicationContext()))
					btnIssue.setText(R.string.download);

				else if (((StoreProductBean) result.get(i)).getPrice().equalsIgnoreCase("null")) {
					btnIssue.setText(R.string.download);
				}else{
					if(isSubscriptionActive((StoreProductBean)result.get(i)))
						btnIssue.setText(R.string.subscribe);
					else
						btnIssue.setText(((StoreProductBean) result.get(i)).getPrice()+" "+getString(R.string.currency));
				}
				btnIssue.setBackgroundResource(R.drawable.btn_buy);
			}

			layoutForHorizontalList.addView(placeHolder);
			
			if(i == 0){
				ImageLoader.getInstance().displayImage(result.get(0).getCoverUrl(),imgIssue);
				//ImageDownLoader.download(((StoreProductBean) ,imgIssue, this, URLEncoder.encode(((StoreProductBean) result.get(0)).getEditionId()));
				//imgIssue.setImageResource(R.drawable.ic_launcher);
				txtCurrentTitle.setText(((StoreProductBean) result.get(0)).getEditionName());
				if(result.get(i).getEditionName().length()>20)
					txtCurrentTitle.setText(((StoreProductBean) result.get(0)).getEditionName().subSequence(0, 17)+"...");
				else
					txtCurrentTitle.setText(((StoreProductBean) result.get(0)).getEditionName());
				String description = ((StoreProductBean) result.get(0)).getDescription();
				//if(description.toCharArray().length>200){
					//description = description.substring(0, 200);
					txtCurrentDesc.setScroller(new Scroller(context)); 
					//txtCurrentDesc.setMaxLines(10); 
					txtCurrentDesc.setVerticalScrollBarEnabled(true); 
					txtCurrentDesc.setMovementMethod(new ScrollingMovementMethod()); 
				//}
				txtCurrentDesc.setText(description);
				btnCurrentIssue.setVisibility(View.VISIBLE);
				//btnCurrentIssueSubscriptions.setVisibility(View.VISIBLE);
				btnCurrentIssue.setTag(((StoreProductBean) result.get(0)));
				
				if(filePath.exists()){

					if(isDownloaded((StoreProductBean)result.get(0))){
						btnCurrentIssue.setText(R.string.read);
						btnCurrentIssue.setBackgroundResource(R.drawable.btn_read);	
					}else{
						if(Utils.isUserAdmin(getApplicationContext()))
							btnCurrentIssue.setText(R.string.download);
						else if (((StoreProductBean) result.get(0)).getPrice().equalsIgnoreCase("null")) {
							btnCurrentIssue.setText(R.string.download);
						}else{
							if(isSubscriptionActive((StoreProductBean)result.get(0)))
								btnCurrentIssue.setText(R.string.subscribe);
							else
								btnCurrentIssue.setText(((StoreProductBean) result.get(0)).getPrice()+" "+getString(R.string.currency));
						}
						btnCurrentIssue.setBackgroundResource(R.drawable.btn_buy);
					}
				}else{
					if(Utils.isUserAdmin(getApplicationContext()))
						btnCurrentIssue.setText(R.string.download);
					else if (((StoreProductBean) result.get(0)).getPrice().equalsIgnoreCase("null")) {
						btnCurrentIssue.setText(R.string.download);
					}else{
						btnCurrentIssue.setText(((StoreProductBean) result.get(0)).getPrice()+" "+getString(R.string.currency));
					}
					btnCurrentIssue.setBackgroundResource(R.drawable.btn_buy);
				}
			}
			
			placeHolder.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					StoreProductBean storeProductBean = (StoreProductBean) v.getTag();
					txtCurrentTitle.setText(storeProductBean.getEditionName());
					String description = storeProductBean.getDescription();
					//if(description.toCharArray().length>200){
						//description = description.substring(0, 200);
						txtCurrentDesc.setScroller(new Scroller(context)); 
						//txtCurrentDesc.setMaxLines(10); 
						txtCurrentDesc.setVerticalScrollBarEnabled(true); 
						txtCurrentDesc.setMovementMethod(new ScrollingMovementMethod()); 
					//}
					txtCurrentDesc.setText(description);
					btnCurrentIssue.setTag(storeProductBean);
					if(filePath.exists()){
						/*File currentFile = new File(currentAppPath + URLEncoder.encode(storeProductBean.getEditionName()));
						int total=0;
						for(int j=0;j<currentFile.list().length;j++){
							File f = new File(currentFile.listFiles()[j].getPath());
							total += f.list().length;
						}
						Log.i("total files",""+total);*/
						if(isDownloaded((StoreProductBean)storeProductBean)){
							btnCurrentIssue.setText(R.string.read);
							btnCurrentIssue.setBackgroundResource(R.drawable.btn_read);	
						}else{
							if (storeProductBean.getPrice().equalsIgnoreCase("null")) {
								btnCurrentIssue.setText(R.string.download);
							}else{
								if(isSubscriptionActive((StoreProductBean)storeProductBean))
									btnCurrentIssue.setText("Subscribe");
								else
									btnCurrentIssue.setText(((StoreProductBean)storeProductBean).getPrice()+" "+getString(R.string.currency));
							}
							btnCurrentIssue.setBackgroundResource(R.drawable.btn_buy);
						}
					}else{
						if (storeProductBean.getPrice().equalsIgnoreCase("null")) {
							btnCurrentIssue.setText(R.string.download);
						}else{
							if(isSubscriptionActive((StoreProductBean)storeProductBean))
								btnCurrentIssue.setText("Subscribe");
							else
								btnCurrentIssue.setText(((StoreProductBean)storeProductBean).getPrice()+" "+getString(R.string.currency));
						}
						btnCurrentIssue.setBackgroundResource(R.drawable.btn_buy);
					}
					ImageLoader.getInstance().displayImage(storeProductBean.getCoverUrl(),imgIssue);
//					ImageDownLoader.download(storeProductBean.getCoverUrl(),imgIssue, MainActivity.this, URLEncoder.encode(storeProductBean.getEditionId()));
					//imgIssue.setImageResource(R.drawable.ic_launcher);
				}
			});
			btnIssue.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Button button = (Button)v;
					StoreProductBean storeProductBean = (StoreProductBean) v.getTag();
					if(button.getText().toString().equals(getString(R.string.read))){
						/*Intent intent = new Intent(MainActivity.this,IssueActivity.class);
						intent.putExtra("issue_name",(String) storeProductBean.getEditionName());
						intent.putExtra("issue_pdf", (String) storeProductBean.getPdfName());
						startActivity(intent);*/
						try{
							String totalFilePath = filePath.getAbsolutePath()+"/"+URLEncoder.encode(URLEncoder.encode(storeProductBean.getEditionId()))+"/PDF/"+storeProductBean.getPdfName();
							Uri uri = Uri.fromFile(new File(totalFilePath));
							Intent intent = new Intent(MainActivity.this,MuPDFActivity.class);
							intent.setAction(Intent.ACTION_VIEW);
							intent.setData(uri);
							intent.putExtra("pass",getString(R.string.passA)+storeProductBean.getEditionId()+getString(R.string.passB));
							intent.putExtra("pkn",getPackageName());
							intent.putExtra("edition_id",storeProductBean.getEditionId());
							intent.putExtra("social_text",getString(R.string.socialText));
							intent.putExtra("social_link",getString(R.string.socialText));
							intent.putExtra("mailsub",getString(R.string.mailSubject));
							intent.putExtra("mailtxt",getString(R.string.mailText));
							intent.putExtra("name", storeProductBean.getEditionName());
//							intent.putExtra("edition_id", storeProductBean.getEditionId());
//							intent.setDataAndType(uri,"application/pdf");
							//intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
							//intent.setData(uri);
							context.startActivity(intent);
							ClipboardManager clipboardmanager = (ClipboardManager) getSystemService("clipboard");
							Log.d("pass",getString(R.string.passA)+storeProductBean.getEditionId()+getString(R.string.passB));
							clipboardmanager.setPrimaryClip(ClipData.newPlainText("",""+getString(R.string.passA)+storeProductBean.getEditionId()+getString(R.string.passB)));
						} catch (Exception e) {
							Log.i("Error","Problem with starting PDF-activity, path: "+filePath,e);
						}
					}else if (Utils.isUserAdmin(getApplicationContext()) || storeProductBean.getPrice().equalsIgnoreCase("null") || button.getText().toString().equals(getString(R.string.subscribe))){

						//Tenemso permiso para descargar

						ProgressDialog pDialog = new ProgressDialog(context);
						pDialog.setMessage(context.getResources().getString(R.string.startDownload));
					    pDialog.show();
						Intent intent = new Intent(MainActivity.this,MyLibraryActivity.class);
						Bundle b = new Bundle();
						b.putSerializable("storeProductBean",storeProductBean);
						b.putSerializable("issues",issues);
						intent.putExtras(b);
						intent.putExtra("isDownload", true);
						startActivity(intent);
						finish();
					} else{
						Log.i("buyButton","this issue is not free");
						buyingStoreproductBean=storeProductBean;
						mHelper.queryInventoryAsync(mQueryFinishedListener);
						//mHelper.launchPurchaseFlow(MainActivity.this, "android.test.purchased", 10001, mPurchaseFinishedListener, storeProductBean.getEditionName());
						
						/*ArrayList<String> additionalSkuList = new ArrayList<String>();
						additionalSkuList.add("a08fe0404dc58153c5cb543543jhk34jh");
						mHelper.queryInventoryAsync(true, additionalSkuList,
						   mQueryFinishedListener);*/
						
					}
				}
			});

			btnCurrentIssue.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					StoreProductBean storeProductBean = (StoreProductBean) v.getTag();
					if(btnCurrentIssue.getText().toString().equals(getString(R.string.read))){
						/*Intent intent = new Intent(MainActivity.this,IssueActivity.class);
						intent.putExtra("issue_name",(String) storeProductBean.getEditionName());
						startActivity(intent);*/
						try{

							String totalFilePath = filePath.getAbsolutePath()+"/"+URLEncoder.encode(URLEncoder.encode(storeProductBean.getEditionId()))+"/PDF/"+storeProductBean.getPdfName();
							Uri uri = Uri.fromFile(new File(totalFilePath));
							Intent intent = new Intent(MainActivity.this,MuPDFActivity.class);
							intent.setAction(Intent.ACTION_VIEW);
							intent.setData(uri);
							intent.putExtra("pass",getString(R.string.passA)+storeProductBean.getEditionId()+getString(R.string.passB));
							intent.putExtra("pkn",getPackageName());
							intent.putExtra("edition_id",storeProductBean.getEditionId());
							intent.putExtra("social_text",getString(R.string.socialText));
							intent.putExtra("social_link",getString(R.string.socialText));
							intent.putExtra("mailsub",getString(R.string.mailSubject));
							intent.putExtra("mailtxt",getString(R.string.mailText));
							intent.putExtra("name", storeProductBean.getEditionName());
//							intent.putExtra("edition_id", storeProductBean.getEditionId());
//							intent.setDataAndType(uri,"application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
							//intent.setData(uri);
							context.startActivity(intent);
						} catch (Exception e) {
							Log.i("Error","Problem with starting PDF-activity, path: "+filePath,e);
						}
					}else if ( Utils.isUserAdmin(getApplicationContext()) ||storeProductBean.getPrice().equalsIgnoreCase("null") || btnCurrentIssue.getText().toString().equals(getString(R.string.subscribe))){

						//Permiso para descargar

						ProgressDialog pDialog = new ProgressDialog(context);
						pDialog.setMessage(context.getResources().getString(R.string.startDownload));
					    pDialog.show();
						Intent intent = new Intent(MainActivity.this,MyLibraryActivity.class);
						Bundle b = new Bundle();
						b.putSerializable("storeProductBean",storeProductBean);
						intent.putExtras(b);
						intent.putExtra("isDownload", true);
						startActivity(intent);
						finish();
					} else{
						Log.i("buyButton","this issue is not free");
						buyingStoreproductBean=storeProductBean;
						mHelper.queryInventoryAsync(mQueryFinishedListener);
					}
				}
			});
		}
	}
	
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener 
	   = new IabHelper.OnIabPurchaseFinishedListener() {
	   public void onIabPurchaseFinished(IabResult result, Purchase purchase) 
	   {
	      if (result.isFailure()) {
	         Log.d("onPurchase", "Error purchasing: " + result);
		//	  Toast.makeText(MainActivity.this, "Error purchasing: " + result,Toast.LENGTH_LONG).show();
	         return;
	      }      
	      else if (purchase.getSku().equals("a08fe0404dc58153c5cb543543jhk34jh")) {
	         // give user access to premium content and update the UI
			//  Toast.makeText(MainActivity.this,"access premium",Toast.LENGTH_LONG).show();
	      }
	      else if(purchase.getSku().equals("android.test.purchased")){
	    	  System.out.println("You have purchased test item");
	    	  	/*ProgressDialog pDialog = new ProgressDialog(context);
				pDialog.setMessage(context.getResources().getString(R.string.startDownload));
			    pDialog.show();*/
				/*Intent intent = new Intent(MainActivity.this,MyLibraryActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("storeProductBean",buyingStoreproductBean);
				intent.putExtras(b);
				intent.putExtra("isDownload", true);
				startActivity(intent);
				finish();*/
			  Intent intent = new Intent(MainActivity.this,MyLibraryActivity.class);
			  Bundle b = new Bundle();
			  b.putSerializable("storeProductBean",buyingStoreproductBean);
			  intent.putExtras(b);
			  intent.putExtra("isDownload", true);
			  startActivity(intent);
			  finish();
	      }
	      else if(purchase.getSku().equals(getString(R.string.productId)+buyingStoreproductBean.getReference_number())){
			  Toast.makeText(MainActivity.this,"lauching download",Toast.LENGTH_LONG).show();
	    	  	Intent intent = new Intent(MainActivity.this,MyLibraryActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("storeProductBean",buyingStoreproductBean);
				intent.putExtras(b);
				intent.putExtra("isDownload", true);
				startActivity(intent);
				finish();
			  return;
	      }

	   }
	};
	
	IabHelper.QueryInventoryFinishedListener 
	   mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
	   public void onQueryInventoryFinished(IabResult result, Inventory inventory)   
	   {
	      if (result.isFailure()) {
	         // handle error
			//  Toast.makeText(MainActivity.this,"inventory error"+result.getMessage(),Toast.LENGTH_LONG).show();
	    	  System.out.println("error");
	         return;
	       }
	      
	      if (inventory.hasPurchase("android.test.purchased")) {

	    	  System.out.println("Lo consumo");
	            mHelper.consumeAsync(inventory.getPurchase("android.test.purchased"), null);
	            return;
	        }

	      if(inventory.hasPurchase(getString(R.string.productId)+buyingStoreproductBean.getReference_number())){
	    	    Intent intent = new Intent(MainActivity.this,MyLibraryActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("storeProductBean",buyingStoreproductBean);
				intent.putExtras(b);
				intent.putExtra("isDownload", true);
				startActivity(intent);
				finish();
	    	    return;
	      }

	       //String issuePrice = inventory.getSkuDetails("a08fe0404dc58153c5cb543543jhk34jh").getPrice();
	       
	       //Log.i("info","the price is: "+issuePrice);

//		   android.test.purchased
//		   mHelper.launchPurchaseFlow(MainActivity.this,"android.test.purchased", 10001,
//				   mPurchaseFinishedListener, "Edition");
	      mHelper.launchPurchaseFlow(MainActivity.this, getString(R.string.productId)+buyingStoreproductBean.getReference_number(), 10001,
	                mPurchaseFinishedListener, "Edition");
	      // update the UI
	   }
	};
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onResult", "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
        	/*if(resultCode==RESULT_OK && buyingSubscription){
        		String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
        		JSONObject jo;
				try {
					Calendar dateFrom = Calendar.getInstance();
					dateFrom.getTime().toString();
					Calendar dateTo = Calendar.getInstance();
					dateTo.add(Calendar.YEAR, 1);
					
					String dateFromString = dateFrom.get(Calendar.DAY_OF_MONTH)+"-"+dateFrom.get(Calendar.MONTH)+"-"+dateFrom.get(Calendar.YEAR);
					String dateToString = dateTo.get(Calendar.DAY_OF_MONTH)+"-"+dateTo.get(Calendar.MONTH)+"-"+dateTo.get(Calendar.YEAR);
					System.out.println("Subscription from "+dateFromString+" to "+dateToString);
					
					SharedPreferences prefs = MainActivity.this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("dateFrom", dateFromString);
					editor.putString("dateTo", dateToString);
					editor.commit();
					jo = new JSONObject(purchaseData);
					int time = jo.getInt("purchaseTime");
					
					Calendar purchaseTime = Calendar.getInstance();
					purchaseTime.setTimeInMillis(time);
					String purchaseTimeString = purchaseTime.get(Calendar.DAY_OF_MONTH)+"-"+purchaseTime.get(Calendar.MONTH)+"-"+purchaseTime.get(Calendar.YEAR);
					System.out.println("Momento de la compra: "+purchaseTimeString);
					editor.putString("dateFrom", purchaseTimeString);
					editor.commit();
					
					buyingSubscription=false;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                
        	}*/
            Log.d("onResult", "onActivityResult handled by IABUtil.");
        }
    }
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	        finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}
}