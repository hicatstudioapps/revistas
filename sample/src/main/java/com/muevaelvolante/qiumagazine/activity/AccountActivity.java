package com.muevaelvolante.qiumagazine.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.utils.Utils;
import com.parse.LogInCallback;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class AccountActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accounts);
        findViewById(R.id.accountCancelButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
finish();
            }
        });

        findViewById(R.id.accountValidateButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(((EditText)findViewById(R.id.accountUserTextfield)).getText().toString().length()>0 && ((EditText)findViewById(R.id.accountPassTextfield)).getText().toString().length()>0){

                    //Añadimos un una comprobación especial para discernir si es un usuario admin/preview y tendrá acceso a descargas
                    String user_ = ((EditText)findViewById(R.id.accountUserTextfield)).getText().toString();
                    String pass_ = ((EditText)findViewById(R.id.accountPassTextfield)).getText().toString();

                    if(Utils.checkIfUserPreview(AccountActivity.this,user_,pass_)) {
                        finish();
//                        layoutForHorizontalList.removeAllViewsInLayout();
//                        loadFromWeb();
                        return;
                    }

                    ParseUser.logInInBackground(((EditText)findViewById(R.id.accountUserTextfield)).getText().toString(), ((EditText)findViewById(R.id.accountPassTextfield)).getText().toString(), new LogInCallback() {

                        @Override
                        public void done(ParseUser arg0,
                                         com.parse.ParseException arg1) {

                            if(arg0!=null){
                                System.out.println("Logueado como: "+arg0.getUsername());

                                if(arg0.getNumber("id_publication").intValue()!=getResources().getInteger(R.integer.id_publication)){
                                    AlertDialog alertDialog = new AlertDialog.Builder(AccountActivity.this).create();
                                    alertDialog.setTitle(AccountActivity.this.getResources().getString(R.string.error));
                                    alertDialog.setMessage(AccountActivity.this.getResources().getString(R.string.user_or_password_incorrect));
                                    alertDialog.setCanceledOnTouchOutside(true);
                                    alertDialog.setCancelable(true);
                                    alertDialog.show();

                                    ParseUser.logOut();

                                    return;
                                }

                                if(arg0.getNumber("quota").intValue()==0){
                                    AlertDialog alertDialog = new AlertDialog.Builder(AccountActivity.this).create();
                                    alertDialog.setTitle(AccountActivity.this.getResources().getString(R.string.error));
                                    alertDialog.setMessage(AccountActivity.this.getResources().getString(R.string.quota_exceed));
                                    alertDialog.setCanceledOnTouchOutside(true);
                                    alertDialog.setCancelable(true);
                                    alertDialog.show();

                                    ParseUser.logOut();

                                    return;
                                }

                                SharedPreferences preferences = AccountActivity.this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putLong("dateFrom", arg0.getDate("dateFrom").getTime());
                                editor.putLong("dateTo", arg0.getDate("dateTo").getTime());
                                editor.commit();
//
//                                accountDialog.dismiss();

                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

                                AlertDialog alertDialog = new AlertDialog.Builder(AccountActivity.this).create();
                                alertDialog.setTitle(AccountActivity.this.getResources().getString(R.string.error));
                                alertDialog.setMessage(AccountActivity.this.getResources().getString(R.string.subscription_active)+" "+formatter.format(arg0.getDate("dateFrom"))+" "+AccountActivity.this.getResources().getString(R.string.to)+" "+formatter.format(arg0.getDate("dateTo")));
                                alertDialog.setCanceledOnTouchOutside(true);
                                alertDialog.setCancelable(true);
                                alertDialog.show();

                                arg0.put("quota", ((arg0.getNumber("quota").intValue())-1));

                                //Añadir installations ids
                                ArrayList<String> installationIds = new ArrayList<String>();
                                JSONArray installationIdsJson = arg0.getJSONArray("installation_ids");
                                if(installationIdsJson != null)
                                {
                                    for(int i=0; i<installationIdsJson.length(); ++i)
                                    {
                                        String aux = (String) installationIdsJson.opt(i);
                                        installationIds.add(aux);
                                    }
                                }

                                //Comprobamos si ya existe
                                boolean exists = false;
                                for(String aux : installationIds)
                                {
                                    if(aux.equalsIgnoreCase(ParseInstallation.getCurrentInstallation().getInstallationId()))
                                    {
                                        exists = true;
                                        break;
                                    }
                                }

                                if(!exists)
                                {
                                    installationIds.add(ParseInstallation.getCurrentInstallation().getInstallationId());
                                    arg0.put("installation_ids",installationIds);
                                }

                                arg0.saveEventually();

                                alertDialog.setOnDismissListener(new AlertDialog.OnDismissListener() {

                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
//                                        layoutForHorizontalList.removeAllViewsInLayout();
//                                        loadFromInternalDB();
                                    }
                                });

                            }
                            else{
                                AlertDialog alertDialog = new AlertDialog.Builder(AccountActivity.this).create();
                                alertDialog.setTitle(AccountActivity.this.getResources().getString(R.string.error));
                                alertDialog.setMessage(AccountActivity.this.getResources().getString(R.string.user_or_password_incorrect));
                                alertDialog.setCanceledOnTouchOutside(true);
                                alertDialog.setCancelable(true);
                                alertDialog.show();
                            }
                        }
                    });

                }
                else{

                    AlertDialog alertDialog = new AlertDialog.Builder(AccountActivity.this).create();
                    alertDialog.setTitle(AccountActivity.this.getResources().getString(R.string.error));
                    alertDialog.setMessage(AccountActivity.this.getResources().getString(R.string.user_password_empty));
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.setCancelable(true);
                    alertDialog.show();
                }
            }
        });
    }
}
