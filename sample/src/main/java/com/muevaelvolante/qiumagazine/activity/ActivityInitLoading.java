package com.muevaelvolante.qiumagazine.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.adapter.DBAdapterIssues;
import com.muevaelvolante.qiumagazine.bean.ApiManager;
import com.muevaelvolante.qiumagazine.bean.StoreProductBean;
import com.muevaelvolante.qiumagazine.utils.AysnTaskCompleteListener;
import com.muevaelvolante.qiumagazine.utils.Constant;
import com.muevaelvolante.qiumagazine.utils.EditionSyncTask;
import com.muevaelvolante.qiumagazine.utils.Internet;
import com.muevaelvolante.qiumagazine.utils.StoreProductUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by idintel on 19/01/2016.
 *
 * Esta clase se encarga de leer el fichero info.json bien de internet o de disco para poder
 * cargar el tipo de vista de inicio: HTML o standar
 */
public class ActivityInitLoading extends Activity implements AysnTaskCompleteListener<ArrayList<StoreProductBean>> {


    EditionSyncTask editionSyncTask;
    View error_layout;
    View loadingView;
    View buttonMyLibrary;

    private DBAdapterIssues dbAdapterIssues = null;
    public ArrayList<StoreProductBean> issues;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_init_loading);
        dbAdapterIssues= new DBAdapterIssues(this);
        error_layout = findViewById(R.id.error_layout);
        loadingView = findViewById(R.id.progressBar);
        buttonMyLibrary  = findViewById(R.id.error_library_button);

//        if(Permission.hasPermission(this, Manifest.permission.GET_ACCOUNTS) && Permission.hasPermission(this,Permission.WRITE_EXTERNAL)) {
           loadData();
//        }else {
//            ArrayList<String> perms= new ArrayList<>();
//            perms.add(Manifest.permission.GET_ACCOUNTS);
//            perms.add(Permission.WRITE_EXTERNAL);
//            askPermision(perms,1);
//        }

        buttonMyLibrary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityInitLoading.this,MyLibraryActivity.class);
                intent.putExtra("isDownload", false);
                intent.putExtra("issues", issues);
                startActivity(intent);
            }
        });
    }

    private void loadData() {
        //Realizamos la carga inicial de la información
//        startActivity(new Intent(ActivityInitLoading.this, MainActivity.class));
        try {
            if (Internet.checkConnection(this)) {
                URL urlEdition = new URL(Constant.S3_URL + "/apps/" + getString(R.string.API_KEY) + ".info.json");
//                 urlEdition = new URL("http://192.168.101.1:8081/revistas/"+getString(R.string.API_KEY)+".info.json");
                Log.d("url ", urlEdition.toString());
                editionSyncTask = new EditionSyncTask(this, this, "InfoEditionList");
                editionSyncTask.execute(urlEdition);
                Log.d("url", urlEdition.toString());
            } else {
                issues = StoreProductUtils.loadFromInternalDB(dbAdapterIssues);
                loadingView.setVisibility(View.INVISIBLE);
                error_layout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(ArrayList<StoreProductBean> result, String method) {
        //No es necesario implementarlo. ESto así por una pesíma implementación del anterior desarrollador
    }

    @Override
    public void onTaskComplete(String result, String method) {

        if(method.equalsIgnoreCase("InfoEditionList")) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject != null) {

                    String lastUpdate = ApiManager.getInstance(getApplicationContext()).last_update;
                    ApiManager.getInstance(getApplicationContext()).parseFromJSONandStore(jsonObject, getApplicationContext());

                    if(ApiManager.getInstance(getApplicationContext()).onoff==0){
                        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                        alertDialog.setTitle(this.getResources().getString(R.string.error));
                        alertDialog.setMessage(this.getResources().getString(R.string.error_message));
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                        loadingView.setVisibility(View.INVISIBLE);
                    }
                    else
                    {
                        if(ApiManager.getInstance(getApplicationContext()).homestyle != null && ApiManager.getInstance(getApplicationContext()).homestyle.equalsIgnoreCase("html5"))
                        {
                            startActivity(new Intent(ActivityInitLoading.this,HomeWebActivity.class)
                            .putExtra("needRefreshFromInternet",!lastUpdate.equals(ApiManager.getInstance(getApplicationContext()).last_update)));

                            ActivityInitLoading.this.finish();
                        }
                        else
                        {
                            startActivity(new Intent(ActivityInitLoading.this, MainActivity.class)
                                    .putExtra("needRefreshFromInternet", !lastUpdate.equals(ApiManager.getInstance(getApplicationContext()).last_update)));

                            ActivityInitLoading.this.finish();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                //startActivity(new Intent(this,MainActivity.class));
                Toast.makeText(this,getString(R.string.error_message),Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if(Permission.hasPermission(this,Manifest.permission.GET_ACCOUNTS) && Permission.hasPermission(this,Permission.WRITE_EXTERNAL)) {
//            loadData();
//        }
//        else
//        {
//            Permission.fail(this);
//            finish();
//        }
//    }
}
