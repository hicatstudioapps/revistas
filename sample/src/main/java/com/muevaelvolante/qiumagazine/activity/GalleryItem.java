package com.muevaelvolante.qiumagazine.activity;


import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GalleryItem#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GalleryItem extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String url;
    private String mParam2;

    @Bind(R.id.image)
    ImageView image;
    @Bind(R.id.next)
    View next;
    @Bind(R.id.prev)
    View prev;
    ViewPager pager;
    private int position;
    private int total;
    private ProgressDialog progressDialog;

    public GalleryItem() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static GalleryItem newInstance(String url, int position, int photos) {
        GalleryItem fragment = new GalleryItem();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, url);
        args.putInt("position", position);
        args.putInt("total", photos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(ARG_PARAM1);
            position = getArguments().getInt("position");
            total = getArguments().getInt("total");
            pager = ((GalleryActivity) getActivity()).getPager();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gallery_item, container, false);
        ButterKnife.bind(this, view);
        //url="http://192.168.101.1/ben/ben.png";
        ImageLoader.getInstance().displayImage(url, image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {
                showProgreesDialog();
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(),"Fallo, compruebe su conexión a internet",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {

                if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
                    float proportion = (float) bitmap.getHeight() / bitmap.getWidth();
                    int alto = (int) ((int) (Utils.getScreenDimensions(getActivity().getApplicationContext())[0] / proportion));
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                    lp.height = (int) Utils.getScreenDimensions(getActivity().getApplicationContext())[0];
                    lp.width = alto;
                    image.setLayoutParams(lp);
                } else {
                    float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                    int alto = (int) (((int) Utils.getScreenDimensions(getActivity().getApplicationContext())[1]) / proportion);
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                    lp.height = alto;
                    lp.width = (int) Utils.getScreenDimensions(getActivity().getApplicationContext())[0];
                    image.setLayoutParams(lp);

                }
                progressDialog.dismiss();
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
        pager = ((GalleryActivity) getActivity()).getPager();
        if (position < total - 1) {
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pager.getCurrentItem() + 1 <= total - 1)
                        pager.setCurrentItem(pager.getCurrentItem() + 1);
                }
            });
        } else
            next.setVisibility(View.GONE);
        if (position > 0) {
            prev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pager.getCurrentItem() - 1 >= 0)
                        pager.setCurrentItem(pager.getCurrentItem() - 1);
                }
            });
        } else
            prev.setVisibility(View.GONE);
        return view;
    }

    void showProgreesDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
    }
}
