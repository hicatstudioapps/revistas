package com.muevaelvolante.qiumagazine.service;

import java.io.FileWriter;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by CQ on 22/03/2016.
 */
public class ServiceGenerator  {

    private static final String BASE_URL = "http://benainslie-racing.qiupreview.com/";
//    private static final String BASE_URL = "http://192.168.137.1/ben/";

             private static Retrofit.Builder builder =
             new Retrofit.Builder()
             .baseUrl(BASE_URL)
                     .addConverterFactory(GsonConverterFactory.create());
             private static OkHttpClient.Builder httpClient =
             new OkHttpClient.Builder();

             public static <S> S createService(Class<S> serviceClass) {
         builder.client(httpClient.build());
         Retrofit retrofit = builder.build();
         return retrofit.create(serviceClass);

         }
}
