package com.artifex.mupdflib;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.VideoView;

import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.activity.GalleryActivity;
import com.muevaelvolante.qiumagazine.adapter.DBAdapter;
import com.muevaelvolante.qiumagazine.utils.Constant;
import com.muevaelvolante.qiumagazine.utils.ManagerGoogleAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

/* This enum should be kept in line with the cooresponding C enum in mupdf.c */
enum SignatureState {
	NoSupport,
	Unsigned,
	Signed
}

abstract class PassClickResultVisitor {
	public abstract void visitText(PassClickResultText result);
	public abstract void visitChoice(PassClickResultChoice result);
	public abstract void visitSignature(PassClickResultSignature result);
}

class PassClickResult {
	public final boolean changed;

	public PassClickResult(boolean _changed) {
		changed = _changed;
	}

	public void acceptVisitor(PassClickResultVisitor visitor) {
	}
}

class PassClickResultText extends PassClickResult {
	public final String text;

	public PassClickResultText(boolean _changed, String _text) {
		super(_changed);
		text = _text;
	}

	public void acceptVisitor(PassClickResultVisitor visitor) {
		visitor.visitText(this);
	}
}

class PassClickResultChoice extends PassClickResult {
	public final String[] options;
	public final String[] selected;

	public PassClickResultChoice(boolean _changed, String[] _options,
			String[] _selected) {
		super(_changed);
		options = _options;
		selected = _selected;
	}

	public void acceptVisitor(PassClickResultVisitor visitor) {
		visitor.visitChoice(this);
	}
}

class PassClickResultSignature extends PassClickResult {
	public final SignatureState state;

	public PassClickResultSignature(boolean _changed, int _state) {
		super(_changed);
		state = SignatureState.values()[_state];
	}

	public void acceptVisitor(PassClickResultVisitor visitor) {
		visitor.visitSignature(this);
	}
}

public class MuPDFPageView extends PageView implements MuPDFView {
	final private FilePicker.FilePickerSupport mFilePickerSupport;
	private final MuPDFCore mCore;
	private AsyncTask<Void, Void, PassClickResult> mPassClick;
	private RectF mWidgetAreas[];
	private Annotation mAnnotations[];
	private int mSelectedAnnotationIndex = -1;
	private AsyncTask<Void, Void, RectF[]> mLoadWidgetAreas;
	private AsyncTask<Void, Void, Annotation[]> mLoadAnnotations;
	private AlertDialog.Builder mTextEntryBuilder;
	private AlertDialog.Builder mChoiceEntryBuilder;
	private AlertDialog.Builder mSigningDialogBuilder;
	private AlertDialog.Builder mSignatureReportBuilder;
	private AlertDialog.Builder mPasswordEntryBuilder;
	private EditText mPasswordText;
	private AlertDialog mTextEntry;
	private AlertDialog mPasswordEntry;
	private EditText mEditText;
	private AsyncTask<String, Void, Boolean> mSetWidgetText;
	private AsyncTask<String, Void, Void> mSetWidgetChoice;
	private AsyncTask<PointF[], Void, Void> mAddStrikeOut;
	private AsyncTask<PointF[][], Void, Void> mAddInk;
	private AsyncTask<Integer, Void, Void> mDeleteAnnotation;
	private AsyncTask<Void,Void,String> mCheckSignature;
	private AsyncTask<Void,Void,Boolean> mSign;
	private Runnable changeReporter;
    private Button[] btnLink;
    private Button[] btnVideo;
    private int numLinksInPage;
    private int numVideosInPage;
    private Button[] btnAudio;
    private int numAudiosInPage;
    private Button[] btnAudioPremium;
    private int numAudiosPremiumInPage;
    private WebView[] webView;
    private int numHtml5InPage;
    private Button[] btnGallery;
    private int numGallerysInPage;

    private Boolean autoAudioPlaying = false;
    private Boolean autoVideoPlaying = false;

    private String pdfName;

    private DBAdapter dbAdapter = null;

    private Bitmap[] bitmaps;

    private VideoView videoHolder;

    private MediaPlayer media;

    private Boolean mediaLoaded;

    private boolean mFirstTime;

    public MuPDFPageView(Context c, FilePicker.FilePickerSupport filePickerSupport, MuPDFCore core, Point parentSize, Bitmap sharedHqBm) {
		super(c, parentSize, sharedHqBm);
		mFilePickerSupport = filePickerSupport;
		mCore = core;
		mTextEntryBuilder = new AlertDialog.Builder(c);
		mTextEntryBuilder.setTitle("MuPDF: "
				+ getContext().getString(R.string.fill_out_text_field));
		LayoutInflater inflater = (LayoutInflater) c
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mEditText = (EditText) inflater.inflate(R.layout.textentry, null);
		mTextEntryBuilder.setView(mEditText);
		mTextEntryBuilder.setNegativeButton(R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		mTextEntryBuilder.setPositiveButton(R.string.okay,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						mSetWidgetText = new AsyncTask<String, Void, Boolean>() {
							@Override
							protected Boolean doInBackground(String... arg0) {
								return mCore.setFocusedWidgetText(mPageNumber,
										arg0[0]);
							}

							@Override
							protected void onPostExecute(Boolean result) {
								changeReporter.run();
								if (!result)
									invokeTextDialog(mEditText.getText()
											.toString());
							}
						};

						mSetWidgetText.execute(mEditText.getText().toString());
					}
		});
		
		mTextEntry = mTextEntryBuilder.create();

		mChoiceEntryBuilder = new AlertDialog.Builder(c);
		mChoiceEntryBuilder.setTitle("MuPDF: " + getContext().getString(R.string.choose_value));

		mSigningDialogBuilder = new AlertDialog.Builder(c);
		mSigningDialogBuilder.setTitle("Select certificate and sign?");
		mSigningDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}

		});
		mSigningDialogBuilder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				FilePicker picker = new FilePicker(mFilePickerSupport) {
					@Override
					void onPick(Uri uri) {
						signWithKeyFile(uri);
					}
				};

				picker.pick();
			}

		});
		
		mSignatureReportBuilder = new AlertDialog.Builder(c);
		mSignatureReportBuilder.setTitle("Signature checked");
		mSignatureReportBuilder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		mPasswordText = new EditText(c);
		mPasswordText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
		mPasswordText.setTransformationMethod(new PasswordTransformationMethod());

		mPasswordEntryBuilder = new AlertDialog.Builder(c);
		mPasswordEntryBuilder.setTitle(R.string.enter_password);
		mPasswordEntryBuilder.setView(mPasswordText);
		mPasswordEntryBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		mPasswordEntry = mPasswordEntryBuilder.create();
        String []split = mCore.getFileName().split("/");
        pdfName= URLDecoder.decode(split[split.length-3]);
        mediaLoaded = false;
        this.mFirstTime = true;
	}

	private void signWithKeyFile(final Uri uri) {
		mPasswordEntry.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		mPasswordEntry.setButton(AlertDialog.BUTTON_POSITIVE, "Sign", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				signWithKeyFileAndPassword(uri, mPasswordText.getText().toString());
			}
		});

		mPasswordEntry.show();
	}

	private void signWithKeyFileAndPassword(final Uri uri, final String password) {
		mSign = new AsyncTask<Void,Void,Boolean>() {
			@Override
			protected Boolean doInBackground(Void... params) {
				return mCore.signFocusedSignature(Uri.decode(uri.getEncodedPath()), password);
			}
			@Override
			protected void onPostExecute(Boolean result) {
				if (result)
				{
					changeReporter.run();
				}
				else
				{
					mPasswordText.setText("");
					signWithKeyFile(uri);
				}
			}

		};

		mSign.execute();

	}

	public LinkInfo hitLink(float x, float y) {
		// Since link highlighting was implemented, the super class
		// PageView has had sufficient information to be able to
		// perform this method directly. Making that change would
		// make MuPDFCore.hitLinkPage superfluous.
		float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
		float docRelX = (x - getLeft()) / scale;
		float docRelY = (y - getTop()) / scale;
		
		if (mLinks!=null) {
			for (LinkInfo l : mLinks)
				if (l.rect.contains(docRelX, docRelY))
					return l;
		}
		
		return null;
	}

	private void invokeTextDialog(String text) {
		mEditText.setText(text);
		mTextEntry.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		mTextEntry.show();
	}

	private void invokeChoiceDialog(final String[] options) {
		mChoiceEntryBuilder.setItems(options,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						mSetWidgetChoice = new AsyncTask<String, Void, Void>() {
							@Override
							protected Void doInBackground(String... params) {
								String[] sel = { params[0] };
								mCore.setFocusedWidgetChoiceSelected(sel);
								return null;
							}

							@Override
							protected void onPostExecute(Void result) {
								changeReporter.run();
							}
						};

						mSetWidgetChoice.execute(options[which]);
					}
				});
		AlertDialog dialog = mChoiceEntryBuilder.create();
		dialog.show();
	}
	private void invokeSignatureCheckingDialog() {
		mCheckSignature = new AsyncTask<Void,Void,String> () {
			@Override
			protected String doInBackground(Void... params) {
				return mCore.checkFocusedSignature();
			}
			@Override
			protected void onPostExecute(String result) {
				AlertDialog report = mSignatureReportBuilder.create();
				report.setMessage(result);
				report.show();
			}
		};

		mCheckSignature.execute();
	}

	private void invokeSigningDialog() {
		AlertDialog dialog = mSigningDialogBuilder.create();
		dialog.show();
	}
	
	private void warnNoSignatureSupport() {
		AlertDialog dialog = mSignatureReportBuilder.create();
		dialog.setTitle("App built with no signature support");
		dialog.show();
	}
	
	public void setChangeReporter(Runnable reporter) {
		changeReporter = reporter;
	}

	public Hit passClickEvent(float x, float y) {
		float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
		final float docRelX = (x - getLeft()) / scale;
		final float docRelY = (y - getTop()) / scale;
		boolean hit = false;
		int i;

		if (mAnnotations != null) {
			for (i = 0; i < mAnnotations.length; i++)
				if (mAnnotations[i].contains(docRelX, docRelY)) {
					hit = true;
					break;
				}

			if (hit) {
				switch (mAnnotations[i].type) {
				case HIGHLIGHT:
				case UNDERLINE:
				case SQUIGGLY:
				case STRIKEOUT:
				case INK:
					mSelectedAnnotationIndex = i;
					setItemSelectBox(mAnnotations[i]);
					return Hit.Annotation;
				}
			}
		}

		mSelectedAnnotationIndex = -1;
		setItemSelectBox(null);

		if (!mCore.javascriptSupported())
			return Hit.Nothing;

		if (mWidgetAreas != null) {
			for (i = 0; i < mWidgetAreas.length && !hit; i++)
				if (mWidgetAreas[i].contains(docRelX, docRelY))
					hit = true;
		}

		if (hit) {
			mPassClick = new AsyncTask<Void, Void, PassClickResult>() {
				@Override
				protected PassClickResult doInBackground(Void... arg0) {
					return mCore.passClickEvent(mPageNumber, docRelX, docRelY);
				}

				@Override
				protected void onPostExecute(PassClickResult result) {
					if (result.changed) {
						changeReporter.run();
					}

					result.acceptVisitor(new PassClickResultVisitor() {
						@Override
						public void visitText(PassClickResultText result) {
							invokeTextDialog(result.text);
						}

						@Override
						public void visitChoice(PassClickResultChoice result) {
							invokeChoiceDialog(result.options);
						}

						@Override
						public void visitSignature(PassClickResultSignature result) {
							//if (result.isSigned)
							//	invokeSignatureCheckingDialog();
							//else
							//	invokeSigningDialog();
							switch (result.state) {
							case NoSupport:
								warnNoSignatureSupport();
								break;
							case Unsigned:
								invokeSigningDialog();
								break;
							case Signed:
								invokeSignatureCheckingDialog();
								break;
							}
						}
					});
				}
			};

			mPassClick.execute();
			return Hit.Widget;
		}

		return Hit.Nothing;
	}

	@SuppressWarnings("deprecation")
	@TargetApi(11)
	public boolean copySelection() {
		final StringBuilder text = new StringBuilder();

		processSelectedText(new TextProcessor() {
			StringBuilder line;

			public void onStartLine() {
				line = new StringBuilder();
			}

			public void onWord(TextWord word) {
				if (line.length() > 0)
					line.append(' ');
				line.append(word.w);
			}

			public void onEndLine() {
				if (text.length() > 0)
					text.append('\n');
				text.append(line);
			}
		});

		if (text.length() == 0)
			return false;

		int currentApiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentApiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			android.content.ClipboardManager cm = (android.content.ClipboardManager) mContext
					.getSystemService(Context.CLIPBOARD_SERVICE);

			cm.setPrimaryClip(ClipData.newPlainText("MuPDF", text));
		} else {
			android.text.ClipboardManager cm = (android.text.ClipboardManager) mContext
					.getSystemService(Context.CLIPBOARD_SERVICE);
			cm.setText(text);
		}

		deselectText();

		return true;
	}

	public boolean markupSelection(final Annotation.Type type) {
		final ArrayList<PointF> quadPoints = new ArrayList<PointF>();
		processSelectedText(new TextProcessor() {
			RectF rect;

			public void onStartLine() {
				rect = new RectF();
			}

			public void onWord(TextWord word) {
				rect.union(word);
			}

			public void onEndLine() {
				if (!rect.isEmpty()) {
					quadPoints.add(new PointF(rect.left, rect.bottom));
					quadPoints.add(new PointF(rect.right, rect.bottom));
					quadPoints.add(new PointF(rect.right, rect.top));
					quadPoints.add(new PointF(rect.left, rect.top));
				}
			}
		});

		if (quadPoints.size() == 0)
			return false;

		mAddStrikeOut = new AsyncTask<PointF[], Void, Void>() {
			@Override
			protected Void doInBackground(PointF[]... params) {
				addMarkup(params[0], type);
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				loadAnnotations();
				update();
			}
		};

		mAddStrikeOut
				.execute(quadPoints.toArray(new PointF[quadPoints.size()]));

		deselectText();

		return true;
	}

	public void deleteSelectedAnnotation() {
		if (mSelectedAnnotationIndex != -1) {
			if (mDeleteAnnotation != null)
				mDeleteAnnotation.cancel(true);

			mDeleteAnnotation = new AsyncTask<Integer, Void, Void>() {
				@Override
				protected Void doInBackground(Integer... params) {
					mCore.deleteAnnotation(mPageNumber, params[0]);
					return null;
				}

				@Override
				protected void onPostExecute(Void result) {
					loadAnnotations();
					update();
				}
			};

			mDeleteAnnotation.execute(mSelectedAnnotationIndex);

			mSelectedAnnotationIndex = -1;
			setItemSelectBox(null);
		}
	}

	public void deselectAnnotation() {
		mSelectedAnnotationIndex = -1;
		setItemSelectBox(null);
	}

	public boolean saveDraw() {
		PointF[][] path = getDraw();

		if (path == null)
			return false;

		if (mAddInk != null) {
			mAddInk.cancel(true);
			mAddInk = null;
		}
		mAddInk = new AsyncTask<PointF[][], Void, Void>() {
			@Override
			protected Void doInBackground(PointF[][]... params) {
				mCore.addInkAnnotation(mPageNumber, params[0]);
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				loadAnnotations();
				update();
			}

		};

		mAddInk.execute(getDraw());
		cancelDraw();

		return true;
	}

	
	/*
	protected void drawPage(Bitmap bm, int sizeX, int sizeY, int patchX, int patchY,
			int patchWidth, int patchHeight) {
		mCore.drawPage(bm, mPageNumber, sizeX, sizeY, patchX, patchY,
				patchWidth, patchHeight);
	}
	*/
	@Override
	protected CancellableTaskDefinition<Void, Void> getDrawPageTask(final Bitmap bm, final int sizeX, final int sizeY,
			final int patchX, final int patchY, final int patchWidth, final int patchHeight) {
		return new MuPDFCancellableTaskDefinition<Void, Void>(mCore) {
			@Override
			public Void doInBackground(MuPDFCore.Cookie cookie, Void ... params) {
				// Workaround bug in Android Honeycomb 3.x, where the bitmap generation count
				// is not incremented when drawing.
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB &&
						Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
					bm.eraseColor(0);
				mCore.drawPage(bm, mPageNumber, sizeX, sizeY, patchX, patchY, patchWidth, patchHeight, cookie);
				return null;
			}
		};

	}

	
	/*
	protected void updatePage(Bitmap bm, int sizeX, int sizeY,
			int patchX, int patchY, int patchWidth, int patchHeight) {
		mCore.updatePage(bm, mPageNumber, sizeX, sizeY, patchX, patchY,
				patchWidth, patchHeight);
	}
	*/
	@Override
	protected CancellableTaskDefinition<Void, Void> getUpdatePageTask(final Bitmap bm, final int sizeX, final int sizeY,
			final int patchX, final int patchY, final int patchWidth, final int patchHeight)
	{
		return new MuPDFCancellableTaskDefinition<Void, Void>(mCore) {

			@Override
			public Void doInBackground(MuPDFCore.Cookie cookie, Void ... params) {
				// Workaround bug in Android Honeycomb 3.x, where the bitmap generation count
				// is not incremented when drawing.
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB &&
						Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
					bm.eraseColor(0);
				mCore.updatePage(bm, mPageNumber, sizeX, sizeY, patchX, patchY, patchWidth, patchHeight, cookie);
				return null;
			}
		};
	}


	@Override
	protected LinkInfo[] getLinkInfo() {
		return mCore.getPageLinks(mPageNumber);
	}

	@Override
	protected TextWord[][] getText() {
		return mCore.textLines(mPageNumber);
	}

	@Override
	protected void addMarkup(PointF[] quadPoints, Annotation.Type type) {
		mCore.addMarkupAnnotation(mPageNumber, quadPoints, type);
	}

	private void loadAnnotations() {
		mAnnotations = null;
		if (mLoadAnnotations != null)
			mLoadAnnotations.cancel(true);
		mLoadAnnotations = new AsyncTask<Void, Void, Annotation[]>() {
			@Override
			protected Annotation[] doInBackground(Void... params) {
				return mCore.getAnnoations(mPageNumber);
			}

			@Override
			protected void onPostExecute(Annotation[] result) {
				mAnnotations = result;
			}
		};

		mLoadAnnotations.execute();
	}

	@Override
	public void setPage(final int page, PointF size) {
		loadAnnotations();

		mLoadWidgetAreas = new AsyncTask<Void, Void, RectF[]>() {
			@Override
			protected RectF[] doInBackground(Void... arg0) {
				return mCore.getWidgetAreas(page);
			}

			@Override
			protected void onPostExecute(RectF[] result) {
				mWidgetAreas = result;
			}
		};

		mLoadWidgetAreas.execute();

		super.setPage(page, size);
	}

	public void setScale(float scale) {
		// This type of view scales automatically to fit the size
		// determined by the parent view groups during layout
	}

    @Override
    public void updateHq(boolean update) {
       Log.d("estoy en hq", "que pinga hace esto?");
        requestLayout();
        Rect viewArea = new Rect(getLeft(),getTop(),getRight(),getBottom());
        if(!(viewArea.width()-5<mSize.x && mSize.x<viewArea.width()+5) || !(viewArea.height()-5<mSize.y && mSize.y<viewArea.height()+5)){
            deleteMedia();
            if(mediaLoaded!=true) {

                loadMedia();
            }
            requestLayout();
        }
        else if(mediaLoaded!=true){
            deleteMedia();
            loadMedia();
            requestLayout();
        }else{
            //como ya lo habia mostrado escondo los links

        }

        if (this.mFirstTime && !mediaLoaded) {
            this.mFirstTime = false;
            loadMedia();
            requestLayout();
        }


    }

	void loadMedia() {
		mediaLoaded = true;
		System.out.println("el tamaño es: " + mSize.x + "-" + mSize.y + " y la pagina es: " + mPageNumber);
		if (dbAdapter == null)
			dbAdapter = new DBAdapter(getContext());
		dbAdapter.open();
		Cursor c;
		if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
			c = dbAdapter.getData(pdfName, "" + (mPageNumber * 2), "" + ((mPageNumber * 2) + 1));
		} else {
			c = dbAdapter.getData(pdfName, "" + (mPageNumber + 1));
		}
		//c = dbAdapter.getData(pdfName, "" + (mPageNumber + 1));
		if (c.getCount() > 0) {
			int index = 0;
			btnLink = new Button[100];//initialize links array
			int indexVideo = 0;
			btnVideo = new Button[100];//initialize video array
			int indexAudio = 0;
			btnAudio = new Button[100];//initialize audio array
			int indexAudioPremium = 0;
			btnAudioPremium = new Button[100];//initialize audio array
			int indexHtml5 = 0;
			webView = new WebView[100];
			int indexGallery = 0;
			btnGallery = new Button[100];//initialize gallery buttons array
			while (c.moveToNext()) {
				if (c.getString(2).equalsIgnoreCase("link")) {
					System.out.println("issueID:" +c.getString(0) + "-\\n" +"issueNAME"+ c.getString(1) + "-\\n" +
							"extra MediaNAME:"+ c.getString(2) + "-\\n" + "extraMediaDATA:"+c.getString(3) + "-\\n"
									+ "pageNumber: "+c.getString(4) + "-\\n" + "startX"+c.getString(5) + "-\\n"
									+ "startY"+c.getString(6) + "-\\n" + "endX"+c.getString(7) + "-\\n"
									+ "endY"+c.getString(8) + "-\\n" + "auto:"+c.getString(9) + "-\\n"
									+ "fullScreen:"+c.getString(10) + "-\\n" + "controls:"+c.getString(11) + "-\\n"
									+ "type:"+c.getString(12) + "-");
					btnLink[index] = new Button(getContext());//initialize elements
					//btnLink[index].setText(c.getString(3));
					if (c.getString(11).equalsIgnoreCase("2")) {//tipo de boton, transp,blink o centrado
						btnLink[index].setBackgroundColor(Color.TRANSPARENT);
					} else if (c.getString(11).equalsIgnoreCase("3")) {
						//Set black color for highlight
						btnLink[index].setBackgroundColor(Color.BLACK);
						//set the animation
						final int ii= index;
						final Animation animation = new AlphaAnimation(0, Float.parseFloat("0.5")); // Change alpha from fully visible to invisible
						animation.setDuration(1000); // duration - a second
						animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
						animation.setRepeatCount(Constant.blinks); // Repeat animation
						animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
						animation.setFillEnabled(true);
						animation.setFillAfter(true);
						animation.setAnimationListener(new Animation.AnimationListener() {
							@Override
							public void onAnimationStart(Animation animation) {

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								btnLink[ii].setBackgroundColor(Color.TRANSPARENT);
							}

							@Override
							public void onAnimationRepeat(Animation animation) {

							}
						});
						btnLink[index].startAnimation(animation);
					} else {
						//center button
						if (c.getString(12).equalsIgnoreCase("3"))
							btnLink[index].setBackgroundResource(R.drawable.link_button);
						else if (c.getString(12).equalsIgnoreCase("4"))
							btnLink[index].setBackgroundResource(R.drawable.goto_page_button);
						else if (c.getString(12).equalsIgnoreCase("5"))
							btnLink[index].setBackgroundResource(R.drawable.link_webstore_button);
					}
					addView(btnLink[index]);
					System.out.println("Controls vale: " + c.getString(11));
					int centerX = 0;
					int centerY = 0;
                    if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
                        centerX = (int) (((c.getFloat(7) * mSize.x / 2 - c.getFloat(5) * mSize.x / 2) / 2) + (c.getFloat(5) * mSize.x / 2) - (mSize.x / 2 / 20));
                        centerY = (int) (((c.getFloat(8) * mSize.y - c.getFloat(6) * mSize.y) / 2) + (c.getFloat(6) * mSize.y) - (mSize.x / 2 / 20));
                        if (Integer.parseInt(c.getString(4)) == mPageNumber * 2) {
                            if (Integer.parseInt(c.getString(11)) < 4) {
                                btnLink[index].layout((int) (Float.parseFloat(c.getString(5)) * mSize.x / 2), (int) (Float.parseFloat(c.getString(6)) * mSize.y), (int) (Float.parseFloat(c.getString(7)) * mSize.x / 2), (int) (Float.parseFloat(c.getString(8)) * mSize.y));
                            } else {
                                if (Integer.parseInt(c.getString(11)) <= 4)//center
                                    btnLink[index].layout(centerX, centerY, centerX + (mSize.x / 20), centerY + (mSize.x / 20));
                                else if (Integer.parseInt(c.getString(11)) == 5)//bottom-left
                                    btnLink[index].layout((int) (c.getFloat(5) * mSize.x / 2), (int) ((c.getFloat(8) * mSize.y) - mSize.x / 20), (int) ((c.getFloat(5) * mSize.x / 2) + (mSize.x / 20)), (int) (c.getFloat(8) * mSize.y));
                                else if (Integer.parseInt(c.getString(11)) == 6)//bottom-center
                                    btnLink[index].layout(centerX, (int) ((c.getFloat(8) * mSize.y) - mSize.x / 20), centerX + (mSize.x / 20), (int) (c.getFloat(8) * mSize.y));
                                else if (Integer.parseInt(c.getString(11)) == 7)//bottom-right
                                    btnLink[index].layout((int) (c.getFloat(7) * mSize.x / 2 - mSize.x / 20), (int) ((c.getFloat(8) * mSize.y) - mSize.x / 20), (int) (c.getFloat(7) * mSize.x / 2), (int) (c.getFloat(8) * mSize.y));
                            }

                        } else {
//                            btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/2/10)+mSize.x/2, centerY+(mSize.x/2/10));
                            if (Integer.parseInt(c.getString(11)) < 4) {
                                btnLink[index].layout((int) (Float.parseFloat(c.getString(5)) * mSize.x / 2 + mSize.x / 2), (int) (Float.parseFloat(c.getString(6)) * mSize.y), (int) (Float.parseFloat(c.getString(7)) * mSize.x / 2 + mSize.x / 2), (int) (Float.parseFloat(c.getString(8)) * mSize.y));
                            } else {
                                if (Integer.parseInt(c.getString(11)) <= 4)//center
                                    btnLink[index].layout(centerX + mSize.x / 2, centerY, centerX + (mSize.x / 20) + mSize.x / 2, centerY + (mSize.x / 20));
                                else if (Integer.parseInt(c.getString(11)) == 5)//bottom-left
                                    btnLink[index].layout((int) (c.getFloat(5) * mSize.x / 2 + mSize.x / 2), (int) ((c.getFloat(8) * mSize.y) - mSize.x / 20), (int) ((c.getFloat(5) * mSize.x / 2 + mSize.x / 2) + (mSize.x / 20)), (int) (c.getFloat(8) * mSize.y));
                                else if (Integer.parseInt(c.getString(11)) == 6)//bottom-center
                                    btnLink[index].layout(centerX + mSize.x / 2, (int) ((c.getFloat(8) * mSize.y) - mSize.x / 20), centerX + (mSize.x / 20 + mSize.x / 2), (int) (c.getFloat(8) * mSize.y));
                                else if (Integer.parseInt(c.getString(11)) == 7)//bottom-right
                                    btnLink[index].layout((int) (c.getFloat(7) * mSize.x / 2 - mSize.x / 20 + mSize.x / 2), (int) ((c.getFloat(8) * mSize.y) - mSize.x / 20), (int) (c.getFloat(7) * mSize.x / 2 + mSize.x / 2), (int) (c.getFloat(8) * mSize.y));
                            }
                        }

                    } else {
					//7 end x, 8 end y, 5 start x,6 start y
					centerX = (int) (((c.getFloat(7) * mSize.x - c.getFloat(5) * mSize.x) / 2) + (c.getFloat(5) * mSize.x) - (mSize.x / 20));
					centerY = (int) (((c.getFloat(8) * mSize.y - c.getFloat(6) * mSize.y) / 2) + (c.getFloat(6) * mSize.y) - (mSize.x / 20));
					if (Integer.parseInt(c.getString(11)) < 4) {
						btnLink[index].layout((int) (Float.parseFloat(c.getString(5)) * mSize.x), (int) (Float.parseFloat(c.getString(6)) * mSize.y), (int) (Float.parseFloat(c.getString(7)) * mSize.x), (int) (Float.parseFloat(c.getString(8)) * mSize.y));
					} else {
						if (Integer.parseInt(c.getString(11)) == 4)//center
							btnLink[index].layout(centerX, centerY, centerX + (mSize.x / 10), centerY + (mSize.x / 10));
						else if (Integer.parseInt(c.getString(11)) == 5)//bottom-left
							btnLink[index].layout((int) c.getFloat(5) * mSize.x, (int) (c.getFloat(8) * mSize.y - mSize.x / 10), (int) (c.getFloat(5) * mSize.x + (mSize.x / 10)), (int) (c.getFloat(8) * mSize.y));
						else if (Integer.parseInt(c.getString(11)) == 6)//bottom-center
							btnLink[index].layout(centerX, (int) ((c.getFloat(8) * mSize.y) - mSize.x / 10), centerX + (mSize.x / 10), (int) (c.getFloat(8) * mSize.y));
						else if (Integer.parseInt(c.getString(11)) == 7)//bottom-right
							btnLink[index].layout((int) (c.getFloat(7) * mSize.x - mSize.x / 10), (int) ((c.getFloat(8) * mSize.y) - mSize.x / 10), (int) (c.getFloat(7) * mSize.x), (int) (c.getFloat(8) * mSize.y));
					}
//					btnLink[index].layout(l, t, r, b);
                    }

					btnLink[index].setTag(c.getString(4));//save page number in tag
					System.out.println("a�ado el " + index);

					final String url = c.getString(3);
					final int type = Integer.parseInt(c.getString(12));

					btnLink[index].setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							MuPDFActivity pdf = (MuPDFActivity) getContext();
							if (type == 3) {
								Intent intent = new Intent(Intent.ACTION_VIEW);
								/*String aux=url;
								if(url.startsWith("http://"))
									aux=url.replace("http://", "");
								if(url.startsWith("https://"))
									aux=url.replace("https://", "");
								String composeURL = Constant.API_URL+"&key="+Constant.API_KEY+"&module=magazine&action=stats&stats=true&device="+Build.MODEL+"&software=Android "+Build.VERSION.RELEASE+"&page="+(String)v.getTag()+"&stat_type=3&id_issue="+pdf.getmEdition()+"&media="+aux;
								*/
								intent.setData(Uri.parse(url));
								getContext().startActivity(intent);

								ManagerGoogleAnalytics.getInstance().sendEvent((MuPDFActivity) getContext(), getResources().getString(R.string.readingTrackedName) + ": " + pdf.getIntent().getStringExtra("name"), getResources().getString(R.string.linkTrackedName), url, null);

							} else if (type == 4) {
								System.out.println("Nos vamos a la pagina: " + url);
								if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
									pdf.getmDocView().setDisplayedViewIndex(Integer.parseInt(url) / 2);
								} else {
									pdf.getmDocView().setDisplayedViewIndex(Integer.parseInt(url) - 1);
								}
							}
						}
					});
					index++;
				}
				else if(c.getString(2).equalsIgnoreCase("video")){
					System.out.println(c.getString(0)+"-"+c.getString(1)+"-"+c.getString(2)+"-"+c.getString(3)+"-"+c.getString(4)+"-"+c.getString(5)+"-"+c.getString(6)+"-"+c.getString(7)+"-"+c.getString(8)+"-"+c.getString(9)+"-"+c.getString(10)+"-"+c.getString(11)+"-"+c.getString(12)+"-");
					btnVideo[indexVideo] = new Button(getContext());//initialize elements
					int leftAux=0;
					int rightAux=0;
					if(c.getInt(12)==2 || c.getInt(12)==1){

						if(c.getString(11).equalsIgnoreCase("2")){
							btnVideo[indexVideo].setBackgroundColor(Color.TRANSPARENT);
						}
						else if(c.getString(11).equalsIgnoreCase("3")){
							//Set black color for highlight
							btnVideo[indexVideo].setBackgroundColor(Color.BLACK);

							//set the animation
							final Animation animation = new AlphaAnimation(0, Float.parseFloat("0.5")); // Change alpha from fully visible to invisible
							animation.setDuration(1000); // duration - a second
							animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
							animation.setRepeatCount(Constant.blinks); // Repeat animation
							animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
							animation.setFillEnabled(true);
							animation.setFillAfter(true);
							btnVideo[indexVideo].startAnimation(animation);
						}
						else{
							//Local video
							if(c.getInt(12)==2)
								btnVideo[indexVideo].setBackgroundResource(R.drawable.videoplay_button);
							else
								btnVideo[indexVideo].setBackgroundResource(R.drawable.videoplay_streaming_button);
						}



						addView(btnVideo[indexVideo]);
						int centerX=0;
						int centerY=0;

                        if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
                            centerX=(int)(((c.getFloat(7)*mSize.x/2 - c.getFloat(5)*mSize.x/2)/2)+(c.getFloat(5)*mSize.x/2)-(mSize.x/2/20));
                            centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/2/20));
                            if(Integer.parseInt(c.getString(4))==mPageNumber*2){

                                if(Integer.parseInt(c.getString(11))<=4)//center
                                    btnVideo[indexVideo].layout(centerX, centerY, centerX+(mSize.x/20), centerY+(mSize.x/20));
                                else if(Integer.parseInt(c.getString(11))==5)//bottom-left
                                    btnVideo[indexVideo].layout((int)(c.getFloat(5)*mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
                                else if(Integer.parseInt(c.getString(11))==6)//bottom-center
                                    btnVideo[indexVideo].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20), (int)(c.getFloat(8)*mSize.y));
                                else if(Integer.parseInt(c.getString(11))==7)//bottom-right
                                    btnVideo[indexVideo].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2), (int)(c.getFloat(8)*mSize.y));

                                leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2);
                                rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2);
                            }else{
                                //btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/2/10)+mSize.x/2, centerY+(mSize.x/2/10));

                                if(Integer.parseInt(c.getString(11))<=4)//center
                                    btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/20)+mSize.x/2, centerY+(mSize.x/20));
                                else if(Integer.parseInt(c.getString(11))==5)//bottom-left
                                    btnVideo[indexVideo].layout((int)(c.getFloat(5)*mSize.x/2+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2+mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
                                else if(Integer.parseInt(c.getString(11))==6)//bottom-center
                                    btnVideo[indexVideo].layout(centerX+mSize.x/2, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20+mSize.x/2), (int)(c.getFloat(8)*mSize.y));
                                else if(Integer.parseInt(c.getString(11))==7)//bottom-right
                                    btnVideo[indexVideo].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2+mSize.x/2), (int)(c.getFloat(8)*mSize.y));
                                leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2)+mSize.x/2;
                                rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2)+mSize.x/2;
                            }

                        } else{
						centerX=(int)(((c.getFloat(7)*mSize.x - c.getFloat(5)*mSize.x)/2)+(c.getFloat(5)*mSize.x)-(mSize.x/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/20));
						if(Integer.parseInt(c.getString(11))<=4)//center
							btnVideo[indexVideo].layout(centerX, centerY, centerX+(mSize.x/10), centerY+(mSize.x/10));
						else if(Integer.parseInt(c.getString(11))==5)//bottom-left
							btnVideo[indexVideo].layout((int)(c.getFloat(5)*mSize.x), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)((c.getFloat(5)*mSize.x)+(mSize.x/10)), (int)(c.getFloat(8)*mSize.y));
						else if(Integer.parseInt(c.getString(11))==6)//bottom-center
							btnVideo[indexVideo].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/10), centerX+(mSize.x/10), (int)(c.getFloat(8)*mSize.y));
						else if(Integer.parseInt(c.getString(11))==7)//bottom-right
							btnVideo[indexVideo].layout((int)(c.getFloat(7)*mSize.x-mSize.x/10), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)(c.getFloat(7)*mSize.x), (int)(c.getFloat(8)*mSize.y));
						leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x);
						rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x);
						//btnVideo[index].layout((int)(Float.parseFloat(c.getString(5))*mSize.x), (int)(Float.parseFloat(c.getString(6))*mSize.y), (int)(Float.parseFloat(c.getString(7))*mSize.x), (int)(Float.parseFloat(c.getString(8))*mSize.y));
//						 }
					}
					btnVideo[indexVideo].setTag(c.getString(3));
					final String folder = c.getString(0);
					final int left=leftAux;
					final int top=(int)(Float.parseFloat(c.getString(6))*mSize.y);
					final int right=rightAux;
					final int bottom=(int)(Float.parseFloat(c.getString(8))*mSize.y);
					final int fullscreen = Integer.parseInt(c.getString(10));
					final int type = c.getInt(12);
					final String owner = c.getString(13);
					final String title = c.getString(14);

					btnVideo[indexVideo].setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							if(type==2){
								System.out.println("El path es: " + Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
										+ "/Extra" + (String) v.getTag());
								File videoFile = new File(Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
										+ "/Extra/" + (String) v.getTag());
								if(!videoFile.exists())
									return;
								if(videoHolder==null)
									videoHolder = new VideoView(getContext());
								videoHolder.setLayoutParams(new LayoutParams(2000, 500));

								videoHolder.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
									@Override
									public void onPrepared(MediaPlayer mp) {
										videoHolder.bringToFront();
									}
								});

								// It works fine with .wmv, .3gp and .mp4 formats
								//Here we have to specify the SD Card path
								videoHolder.setVideoURI(Uri.fromFile(videoFile));
								videoHolder.requestFocus();
								videoHolder.start();


								videoHolder.setMediaController(new MediaController(videoHolder.getContext()));
								MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
								metaRetriever.setDataSource(Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
										+ "/Extra/" + (String) v.getTag());
								int height = Integer.parseInt(metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
								int width = Integer.parseInt(metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));

								Log.d("RES","alto: "+height+" ancho: "+width);

								float ratioX,ratioY;
								int videoFrameX,videoFrameY,videoFrameStartX=0,videoFrameStartY=0;

								if(fullscreen==1){
									//Estamos en fullScreen
//
//                                    videoFrameStartX = 0;
//                                    videoFrameX=mSize.x;
//                                    videoFrameY=videoFrameX*height/width;
//                                    videoFrameStartY=(int)((mSize.y-videoFrameY)/2);
//                                    int [] dimen= Magazine.getScreenDimensions(getContext());
//                                    Log.i("VIDEO FRAME", "alto: " + videoFrameY + " ancho: " + videoFrameX);
//                                    //videoHolder.layout(0, 0, dimen[1],dimen[0]);
//                                    if(videoHolder.getParent() != null && videoHolder.getParent() instanceof ViewGroup)
//                                        ((ViewGroup)videoHolder.getParent()).removeView(videoHolder);
//
//                                    addView(videoHolder);
									((MuPDFActivity)getContext()).playFullScreenVideo(Uri.fromFile(videoFile));
								}
								else{
									if(videoHolder.getParent() != null && videoHolder.getParent() instanceof ViewGroup)
										((ViewGroup)videoHolder.getParent()).removeView(videoHolder);
									videoHolder.layout(left, top, right, bottom);
									addView(videoHolder);

								}
								MuPDFActivity pdf = (MuPDFActivity)getContext();

								ManagerGoogleAnalytics.getInstance().sendEvent((MuPDFActivity) getContext(), getResources().getString(R.string.readingTrackedName) + ": " + pdf.getIntent().getStringExtra("name"), getResources().getString(R.string.videoOfflineTrackedName), (String) v.getTag(), null);
								videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
									@Override
									public void onCompletion(MediaPlayer mp) {
										videoHolder.layout(0, 0, 0, 0);
										videoHolder.setVideoURI(null);
										removeView(videoHolder);
										videoHolder=null;
									}
								});
							}
							else if(type==1){
								Intent intent = new Intent(Intent.ACTION_VIEW);
								String composeURL="";
								if(owner.equals("yt"))
									composeURL="http://www.youtube.com/watch?v="+(String)v.getTag();
								else if(owner.equals("vimeo"))
									composeURL="http://player.vimeo.com/video/"+(String)v.getTag();
								intent.setData(Uri.parse(composeURL));
								getContext().startActivity(intent);
								MuPDFActivity pdf = (MuPDFActivity)getContext();
								ManagerGoogleAnalytics.getInstance().sendEvent((MuPDFActivity)getContext(),getResources().getString(R.string.readingTrackedName)+": "+pdf.getIntent().getStringExtra("name"), getResources().getString(R.string.videoOnlineTrackedName), title, null);
							}
						}
					});
					if(Integer.parseInt(c.getString(9))==1){
						btnVideo[indexVideo].setBackgroundResource(0);
						final int indexVideoAuto = indexVideo;
						Handler handler = new Handler();
						handler.postDelayed(new Runnable() {
							public void run() {
								if (btnVideo != null) {
									btnVideo[indexVideoAuto].performClick();
								}

							}

						}, 400);
						autoVideoPlaying=true;
					}

					indexVideo++;
				}
				}
				else if(c.getString(2).equalsIgnoreCase("gallery")){

					System.out.println("Hay gallery aqui");

					btnGallery[indexGallery] = new Button(getContext());

					if(c.getString(11).equalsIgnoreCase("2")){
						btnGallery[indexGallery].setBackgroundColor(Color.TRANSPARENT);
					}
					else if(c.getString(11).equalsIgnoreCase("3")){
						//Set black color for highlight
						btnGallery[indexGallery].setBackgroundColor(Color.BLACK);

						//set the animation
						final Animation animation = new AlphaAnimation(0, Float.parseFloat("0.5")); // Change alpha from fully visible to invisible
						animation.setDuration(1000); // duration - a second
						animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
						animation.setRepeatCount(Constant.blinks); // Repeat animation
						animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
						animation.setFillEnabled(true);
						animation.setFillAfter(true);
						btnGallery[indexGallery].startAnimation(animation);
					}
					else{
						btnGallery[indexGallery].setBackgroundResource(R.drawable.gallery_button);
					}

					addView(btnGallery[indexGallery]);

					int leftAux=0;
					int rightAux=0;
					int centerX=0;
					 int centerY=0;
					if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
						centerX=(int)(((c.getFloat(7)*mSize.x/2 - c.getFloat(5)*mSize.x/2)/2)+(c.getFloat(5)*mSize.x/2)-(mSize.x/2/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/2/20));
						if(Integer.parseInt(c.getString(4))==mPageNumber*2){

							if(Integer.parseInt(c.getString(11))<=4)//center
								btnGallery[indexGallery].layout(centerX, centerY, centerX+(mSize.x/20), centerY+(mSize.x/20));
							else if(Integer.parseInt(c.getString(11))==5)//bottom-left
								btnGallery[indexGallery].layout((int)(c.getFloat(5)*mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==6)//bottom-center
								btnGallery[indexGallery].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==7)//bottom-right
								btnGallery[indexGallery].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2), (int)(c.getFloat(8)*mSize.y));

							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2);
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2);
						}else{
							//btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/2/10)+mSize.x/2, centerY+(mSize.x/2/10));

							if(Integer.parseInt(c.getString(11))<=4)//center
								btnGallery[indexGallery].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/20)+mSize.x/2, centerY+(mSize.x/20));
							else if(Integer.parseInt(c.getString(11))==5)//bottom-left
								btnGallery[indexGallery].layout((int)(c.getFloat(5)*mSize.x/2+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2+mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==6)//bottom-center
								btnGallery[indexGallery].layout(centerX+mSize.x/2, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20+mSize.x/2), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==7)//bottom-right
								btnGallery[indexGallery].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2+mSize.x/2), (int)(c.getFloat(8)*mSize.y));


							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2)+mSize.x/2;
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2)+mSize.x/2;
						}

					}
					else{
						centerX=(int)(((c.getFloat(7)*mSize.x - c.getFloat(5)*mSize.x)/2)+(c.getFloat(5)*mSize.x)-(mSize.x/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/20));

						if(Integer.parseInt(c.getString(11))<=4)//center
							btnGallery[indexGallery].layout(centerX, centerY, centerX+(mSize.x/10), centerY+(mSize.x/10));
						else if(Integer.parseInt(c.getString(11))==5)//bottom-left
							btnGallery[indexGallery].layout((int)(c.getFloat(5)*mSize.x), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)((c.getFloat(5)*mSize.x)+(mSize.x/10)), (int)(c.getFloat(8)*mSize.y));
						else if(Integer.parseInt(c.getString(11))==6)//bottom-center
							btnGallery[indexGallery].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/10), centerX+(mSize.x/10), (int)(c.getFloat(8)*mSize.y));
						else if(Integer.parseInt(c.getString(11))==7)//bottom-right
							btnGallery[indexGallery].layout((int)(c.getFloat(7)*mSize.x-mSize.x/10), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)(c.getFloat(7)*mSize.x), (int)(c.getFloat(8)*mSize.y));

						leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x);
						rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x);
					}

					final String folder = c.getString(0);
					final String data = c.getString(3);

					btnGallery[indexGallery].setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							try {
								JSONArray jsonArry = new JSONArray(data);

								//New activity only if parse result OK
								if(jsonArry != null || jsonArry.length() > 0){

									String []photos = new String[jsonArry.length()];

									for(int i=0; i<jsonArry.length(); i++){
										JSONObject jsonObject = jsonArry.getJSONObject(i);
										String photoURL = jsonObject.optString("url");
//                                        photos[i] = Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
//                                                + "/images/"+photoURL.substring(photoURL.lastIndexOf("/")+1);
										photos[i]=photoURL;
									}
									Intent intent = new Intent(getContext(),GalleryActivity.class);
									intent.putExtra("key", Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
											+ "/images");
									intent.putExtra("photos", photos);
									getContext().startActivity(intent);

									MuPDFActivity pdf = (MuPDFActivity)getContext();
									ManagerGoogleAnalytics.getInstance().sendEvent((MuPDFActivity)getContext(),getResources().getString(R.string.readingTrackedName)+": "+pdf.getIntent().getStringExtra("name"), getResources().getString(R.string.galleryTrackedName), ""+mPageNumber, null);
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});

					indexGallery++;
				}
				else if(c.getString(2).equalsIgnoreCase("audio")){
					System.out.println("AUDIOOOOOOOOO");
					System.out.println(c.getString(0)+"-"+c.getString(1)+"-"+c.getString(2)+"-"+c.getString(3)+"-"+c.getString(4)+"-"+c.getString(5)+"-"+c.getString(6)+"-"+c.getString(7)+"-"+c.getString(8)+"-"+c.getString(9)+"-"+c.getString(10)+"-"+c.getString(11)+"-"+c.getString(12)+"-"+c.getString(14));

					btnAudio[indexAudio] = new Button(getContext());

					if(c.getString(11).equalsIgnoreCase("2")){
						btnAudio[indexAudio].setBackgroundColor(Color.TRANSPARENT);
					}
					else if(c.getString(11).equalsIgnoreCase("3")){
						//Set black color for highlight
						btnAudio[indexAudio].setBackgroundColor(Color.BLACK);

						//set the animation
						final Animation animation = new AlphaAnimation(0, Float.parseFloat("0.5")); // Change alpha from fully visible to invisible
						animation.setDuration(1000); // duration - a second
						animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
						animation.setRepeatCount(Constant.blinks); // Repeat animation
						animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
						animation.setFillEnabled(true);
						animation.setFillAfter(true);
						btnAudio[indexAudio].startAnimation(animation);
					}
					else{
						btnAudio[indexAudio].setBackgroundResource(R.drawable.audio_button);
					}

					addView(btnAudio[indexAudio]);

					int leftAux=0;
					int rightAux=0;
					int centerX=0;
					int centerY=0;
					if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
						centerX=(int)(((c.getFloat(7)*mSize.x/2 - c.getFloat(5)*mSize.x/2)/2)+(c.getFloat(5)*mSize.x/2)-(mSize.x/2/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/2/20));
						if(Integer.parseInt(c.getString(4))==mPageNumber*2){

							if(Integer.parseInt(c.getString(11))<=4)//center
								btnAudio[indexAudio].layout(centerX, centerY, centerX+(mSize.x/20), centerY+(mSize.x/20));
							else if(Integer.parseInt(c.getString(11))==5)//bottom-left
								btnAudio[indexAudio].layout((int)(c.getFloat(5)*mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==6)//bottom-center
								btnAudio[indexAudio].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==7)//bottom-right
								btnAudio[indexAudio].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2), (int)(c.getFloat(8)*mSize.y));

							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2);
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2);
						}else{
							//btnVideo[indexVideo].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/2/10)+mSize.x/2, centerY+(mSize.x/2/10));

							if(Integer.parseInt(c.getString(11))<=4)//center
								btnAudio[indexAudio].layout(centerX+mSize.x/2, centerY, centerX+(mSize.x/20)+mSize.x/2, centerY+(mSize.x/20));
							else if(Integer.parseInt(c.getString(11))==5)//bottom-left
								btnAudio[indexAudio].layout((int)(c.getFloat(5)*mSize.x/2+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)((c.getFloat(5)*mSize.x/2+mSize.x/2)+(mSize.x/20)), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==6)//bottom-center
								btnAudio[indexAudio].layout(centerX+mSize.x/2, (int)((c.getFloat(8)*mSize.y)-mSize.x/20), centerX+(mSize.x/20+mSize.x/2), (int)(c.getFloat(8)*mSize.y));
							else if(Integer.parseInt(c.getString(11))==7)//bottom-right
								btnAudio[indexAudio].layout((int)(c.getFloat(7)*mSize.x/2-mSize.x/20+mSize.x/2), (int)((c.getFloat(8)*mSize.y)-mSize.x/20), (int)(c.getFloat(7)*mSize.x/2+mSize.x/2), (int)(c.getFloat(8)*mSize.y));


							leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x/2)+mSize.x/2;
							rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x/2)+mSize.x/2;
						}

					}
					else{
						centerX=(int)(((c.getFloat(7)*mSize.x - c.getFloat(5)*mSize.x)/2)+(c.getFloat(5)*mSize.x)-(mSize.x/20));
						centerY=(int)(((c.getFloat(8)*mSize.y - c.getFloat(6)*mSize.y)/2)+(c.getFloat(6)*mSize.y)-(mSize.x/20));

						if(Integer.parseInt(c.getString(11))<=4)//center
							btnAudio[indexAudio].layout(centerX, centerY, centerX+(mSize.x/10), centerY+(mSize.x/10));
						else if(Integer.parseInt(c.getString(11))==5)//bottom-left
							btnAudio[indexAudio].layout((int)(c.getFloat(5)*mSize.x), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)((c.getFloat(5)*mSize.x)+(mSize.x/10)), (int)(c.getFloat(8)*mSize.y));
						else if(Integer.parseInt(c.getString(11))==6)//bottom-center
							btnAudio[indexAudio].layout(centerX, (int)((c.getFloat(8)*mSize.y)-mSize.x/10), centerX+(mSize.x/10), (int)(c.getFloat(8)*mSize.y));
						else if(Integer.parseInt(c.getString(11))==7)//bottom-right
							btnAudio[indexAudio].layout((int)(c.getFloat(7)*mSize.x-mSize.x/10), (int)((c.getFloat(8)*mSize.y)-mSize.x/10), (int)(c.getFloat(7)*mSize.x), (int)(c.getFloat(8)*mSize.y));

						leftAux=(int)(Float.parseFloat(c.getString(5))*mSize.x);
						rightAux=(int)(Float.parseFloat(c.getString(7))*mSize.x);
					}
					//el tag va hacer un array, pk puede k el audio sea online
					String [] audio= new String[]{c.getString(3),c.getString(15)};
					btnAudio[indexAudio].setTag(audio);
					final String folder = c.getString(0);
					final int indexAux=indexAudio;

					if(c.getString(3).startsWith("http://"))
						btnAudio[indexAudio].setBackgroundResource(R.drawable.audio_premium_button_open);

					btnAudio[indexAudio].setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							//if(media==null){
							String [] filename = (String[]) v.getTag();
							if(filename[0].startsWith("http://")){
								String[] splited = filename[0].split("/");
								filename[0] = splited[splited.length-1];
							}
							File audioFile = new File(Constant.getAppFilepath(getContext()) + "/" + URLEncoder.encode(folder)
									+ "/Extra/"+filename[0]);
							System.out.println("Direccion de audio: "+audioFile.toString());
								/*Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setDataAndType(Uri.fromFile(audioFile),"audio/*");
								getContext().startActivity(intent);*/

								/*media = MediaPlayer.create(getContext(), Uri.fromFile(audioFile));
								media.start();

								btnAudio[indexAux].setBackgroundResource(R.drawable.audio_pause_button);

								media.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

									@Override
									public void onCompletion(MediaPlayer mp) {
										// TODO Auto-generated method stub
										media.stop();
										media=null;

									}
								});*/

							MuPDFActivity pdf = (MuPDFActivity)getContext();
							// ManagerGoogleAnalytics.getInstance().sendEvent((MuPDFActivity)getContext(),getResources().getString(R.string.readingTrackedName)+": "+pdf.getIntent().getStringExtra("name"), getResources().getString(R.string.audioTrackedName), (String)v.getTag(), null);
							filename[0]=audioFile.getAbsolutePath();
							pdf.showAudioControls(filename);
							//pdf.audioBar.setMax(media.getDuration()/1000);
							/*}
							else if(media.isPlaying()){
									media.pause();
									btnAudio[indexAux].setBackgroundResource(R.drawable.audio_button);
							}
								else{
									media.start();
									btnAudio[indexAux].setBackgroundResource(R.drawable.audio_pause_button);
							}*/

						}
					});

					if(Integer.parseInt(c.getString(9))==1){
						btnAudio[indexAudio].setBackgroundResource(0);
						btnAudio[indexAudio].performClick();
						autoAudioPlaying=true;
					}

					indexAudio++;
				}
			}

			numLinksInPage=index;
			numVideosInPage=indexVideo;
			numAudiosInPage=indexAudio;
			numAudiosPremiumInPage=indexAudioPremium;
			numHtml5InPage=indexHtml5;
			numGallerysInPage=indexGallery;
			System.out.println("En esta pagina hay: "+numLinksInPage);
		}
		c.close();
		dbAdapter.close();
	}

    public void deleteMedia(){
        System.out.println("Elimino media de la pagina: " + mPageNumber);
        if(numLinksInPage!=0){
            for(int s=0;s<numLinksInPage;s++){
                Log.i("deleteMEdia","elimino button["+s+"]");
                btnLink[s].layout(0, 0, 0, 0);
                removeView(btnLink[s]);
                btnLink[s]=null;
            }
            btnLink=null;
            numLinksInPage=0;
        }

        if(numAudiosInPage!=0){
            for(int s=0;s<numAudiosInPage;s++){
                btnAudio[s].layout(0, 0, 0, 0);
                removeView(btnAudio[s]);
                btnAudio[s]=null;
            }

			/*if(media!=null){
				if(!media.isPlaying()){
					media.stop();
					media=null;
				}
			}

			if(autoAudioPlaying && media!=null){
				media.stop();
				media=null;
			}*/

            autoAudioPlaying=false;
            btnAudio=null;
            numAudiosInPage=0;
        }

        if(numAudiosPremiumInPage!=0){
            for(int s=0;s<numAudiosPremiumInPage;s++){
                btnAudioPremium[s].layout(0, 0, 0, 0);
                removeView(btnAudioPremium[s]);
                btnAudioPremium[s]=null;
            }

			/*if(media!=null){
				if(!media.isPlaying()){
					media.stop();
					media=null;
				}
			}

			if(autoAudioPlaying && media!=null){
				media.stop();
				media=null;
			}*/

            autoAudioPlaying=false;
            btnAudioPremium=null;
            numAudiosPremiumInPage=0;
        }


        if(numVideosInPage!=0){
            for(int s=0;s<numVideosInPage;s++){
                Log.i("deleteMEdia","elimino button["+s+"]");
                btnVideo[s].layout(0, 0, 0, 0);
                removeView(btnVideo[s]);
                btnVideo[s]=null;
            }
            autoVideoPlaying = false;
            btnVideo=null;
            numVideosInPage=0;
        }

        if(videoHolder!=null){
            videoHolder.stopPlayback();
            videoHolder.layout(0, 0, 0, 0);
            videoHolder.setMediaController(null);
            videoHolder.setVideoURI(null);
            videoHolder=null;
        }

        mediaLoaded=false;

        if(numHtml5InPage!=0){
            for(int s=0;s<numHtml5InPage;s++){
                webView[s].layout(0, 0, 0, 0);
                removeView(webView[s]);
                webView[s]=null;
            }
            webView=null;
            numHtml5InPage=0;
        }

        if(numGallerysInPage!=0){
            for(int s=0;s<numGallerysInPage;s++){
                btnGallery[s].layout(0, 0, 0, 0);
                removeView(btnGallery[s]);
                btnGallery[s]=null;
            }
            btnGallery=null;
            numGallerysInPage=0;
        }
    }

	@Override
	public void releaseResources() {
		if (mPassClick != null) {
			mPassClick.cancel(true);
			mPassClick = null;
		}

		if (mLoadWidgetAreas != null) {
			mLoadWidgetAreas.cancel(true);
			mLoadWidgetAreas = null;
		}

		if (mLoadAnnotations != null) {
			mLoadAnnotations.cancel(true);
			mLoadAnnotations = null;
		}

		if (mSetWidgetText != null) {
			mSetWidgetText.cancel(true);
			mSetWidgetText = null;
		}

		if (mSetWidgetChoice != null) {
			mSetWidgetChoice.cancel(true);
			mSetWidgetChoice = null;
		}

		if (mAddStrikeOut != null) {
			mAddStrikeOut.cancel(true);
			mAddStrikeOut = null;
		}

		if (mDeleteAnnotation != null) {
			mDeleteAnnotation.cancel(true);
			mDeleteAnnotation = null;
		}

		super.releaseResources();
	}
}
