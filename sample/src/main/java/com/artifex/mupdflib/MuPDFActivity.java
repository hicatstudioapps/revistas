package com.artifex.mupdflib;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import com.artifex.mupdflib.CallbackApplication.MuPDFCallbackClass;
import com.artifex.mupdflib.TwoWayView.Orientation;
import com.muevaelvolante.qiumagazine.R;
import com.muevaelvolante.qiumagazine.app.Magazine;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.Executor;

//import android.text.method.PasswordTransformationMethod;
//import android.widget.SeekBar;

class ThreadPerTaskExecutor implements Executor {
	public void execute(Runnable r) {
		new Thread(r).start();
	}
}

public class MuPDFActivity extends Activity implements FilePicker.FilePickerSupport {
	private Dialog fav;

	/* The core rendering instance */
	//enum TopBarMode {Main, Search, Annot, Delete, More, Accept};
	enum TopBarMode {Main, Search};
	//enum AcceptMode {Highlight, Underline, StrikeOut, Ink, CopyText};

	//private final int OUTLINE_REQUEST = 0;
	private final int PRINT_REQUEST = 1;
	private final int FILEPICK_REQUEST = 2;
	private final int PAGE_CHOICE_REQUEST = 3;
	private final int PROOF_REQUEST=4;
	private MuPDFCore core;
	private String mFileName;
	private String mDocName;
	private int mOrientation;
	private MuPDFReaderView mDocView;
	private View mButtonsView;
	private boolean mButtonsVisible;
	// private EditText mPasswordView;
	// private TextView mFilenameView;
	///private SeekBar mPageSlider;
	///private int mPageSliderRes;
	private TextView mPageNumberView;
	private TextView mInfoView;
	private ImageButton mSearchButton;
	//private ImageButton mReflowButton;
	//private ImageButton mOutlineButton;
	//private ImageButton	mMoreButton;
	//private TextView     mAnnotTypeText;
	//private ImageButton mAnnotButton;
	private ViewAnimator mTopBarSwitcher;
	//private ImageButton mLinkButton;
	private TopBarMode   mTopBarMode = TopBarMode.Main;
	//private AcceptMode   mAcceptMode;
	private ImageButton mSearchBack;
	private ImageButton mSearchFwd;
	private EditText mSearchText;
	private SearchTask mSearchTask;
	private AlertDialog.Builder mAlertBuilder;
	//private boolean mLinkHighlight = false;
	private final Handler mHandler = new Handler();
	private LinearLayout mPreviewBarHolder;
	private TwoWayView mPreview;
	private ToolbarPreviewAdapter pdfPreviewPagerAdapter;
	private boolean mAlertsActive = false;
	//private boolean mReflow = false;
	private AsyncTask<Void, Void, MuPDFAlert> mAlertTask;
	private AlertDialog mAlertDialog;
	private FilePicker mFilePicker;
	private String mProofFile;
	private boolean mSepEnabled[][];


	static private AlertDialog.Builder gAlertBuilder;
	static public AlertDialog.Builder getAlertBuilder() {return gAlertBuilder;}

	public SeekBar audioBar;
	private MediaPlayer media;
	private LinearLayout audioView;
	private ImageButton playPauseAudio;
	private ImageButton closeAudio;
	private TextView currentTime;
	private TextView totalTime;
	private String audioFileString;
	private Timer audioTimer;
	private VideoView videoHolder;
	private RelativeLayout full_scree_video_container;
	private ImageView imageView;
	private boolean audioFromUrl;
	private ProgressDialog streamingProgres;

	
	public void createAlertWaiter() {
		mAlertsActive = true;
		// All mupdf library calls are performed on asynchronous tasks to avoid stalling
		// the UI. Some calls can lead to javascript-invoked requests to display an
		// alert dialog and collect a reply from the user. The task has to be blocked
		// until the user's reply is received. This method creates an asynchronous task,
		// the purpose of which is to wait of these requests and produce the dialog
		// in response, while leaving the core blocked. When the dialog receives the
		// user's response, it is sent to the core via replyToAlert, unblocking it.
		// Another alert-waiting task is then created to pick up the next alert.
		if (mAlertTask != null) {
			mAlertTask.cancel(true);
			mAlertTask = null;
		}
		if (mAlertDialog != null) {
			mAlertDialog.cancel();
			mAlertDialog = null;
		}
		mAlertTask = new AsyncTask<Void,Void,MuPDFAlert>() {

			@Override
			protected MuPDFAlert doInBackground(Void... arg0) {
				if (!mAlertsActive)
					return null;

				return core.waitForAlert();
			}

			@Override
			protected void onPostExecute(final MuPDFAlert result) {
				// core.waitForAlert may return null when shutting down
				if (result == null)
					return;
				final MuPDFAlert.ButtonPressed pressed[] = new MuPDFAlert.ButtonPressed[3];
				for(int i = 0; i < 3; i++)
					pressed[i] = MuPDFAlert.ButtonPressed.None;
				DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						mAlertDialog = null;
						if (mAlertsActive) {
							int index = 0;
							switch (which) {
							case AlertDialog.BUTTON1: index=0; break;
							case AlertDialog.BUTTON2: index=1; break;
							case AlertDialog.BUTTON3: index=2; break;
							}
							result.buttonPressed = pressed[index];
							// Send the user's response to the core, so that it can
							// continue processing.
							core.replyToAlert(result);
							// Create another alert-waiter to pick up the next alert.
							createAlertWaiter();
						}
					}
				};
				mAlertDialog = mAlertBuilder.create();
				mAlertDialog.setTitle(result.title);
				mAlertDialog.setMessage(result.message);
				switch (result.iconType)
				{
				case Error:
					break;
				case Warning:
					break;
				case Question:
					break;
				case Status:
					break;
				}
				switch (result.buttonGroupType)
				{
				case OkCancel:
					mAlertDialog.setButton(AlertDialog.BUTTON2, getString(R.string.cancel), listener);
					pressed[1] = MuPDFAlert.ButtonPressed.Cancel;
				case Ok:
					mAlertDialog.setButton(AlertDialog.BUTTON1, getString(R.string.okay), listener);
					pressed[0] = MuPDFAlert.ButtonPressed.Ok;
					break;
				case YesNoCancel:
					mAlertDialog.setButton(AlertDialog.BUTTON3, getString(R.string.cancel), listener);
					pressed[2] = MuPDFAlert.ButtonPressed.Cancel;
				case YesNo:
					mAlertDialog.setButton(AlertDialog.BUTTON1, getString(R.string.yes), listener);
					pressed[0] = MuPDFAlert.ButtonPressed.Yes;
					mAlertDialog.setButton(AlertDialog.BUTTON2, getString(R.string.no), listener);
					pressed[1] = MuPDFAlert.ButtonPressed.No;
					break;
				}
				mAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
					public void onCancel(DialogInterface dialog) {
						mAlertDialog = null;
						if (mAlertsActive) {
							result.buttonPressed = MuPDFAlert.ButtonPressed.None;
							core.replyToAlert(result);
							createAlertWaiter();
						}
					}
				});

				mAlertDialog.show();
			}
		};

		mAlertTask.executeOnExecutor(new ThreadPerTaskExecutor());
	}

	public void destroyAlertWaiter() {
		mAlertsActive = false;
		if (mAlertDialog != null) {
			mAlertDialog.cancel();
			mAlertDialog = null;
		}
		if (mAlertTask != null) {
			mAlertTask.cancel(true);
			mAlertTask = null;
		}
	}

	private MuPDFCore openFile(String path) {
		int lastSlashPos = path.lastIndexOf('/');
		mFileName = new String(lastSlashPos == -1 ? path
				: path.substring(lastSlashPos + 1));
		System.out.println("Trying to open " + path);
		try {
			core = new MuPDFCore(this, path);
			// New file: drop the old outline data
			//OutlineActivityData.set(null);
			PDFPreviewGridActivityData.set(null);
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		catch (java.lang.OutOfMemoryError e)
		{
			//  out of memory is not an Exception, so we catch it separately.
			System.out.println(e);
			return null;
		}
		return core;
	}

	//private MuPDFCore openBuffer(byte buffer[]) 
	private MuPDFCore openBuffer(byte buffer[], String magic) {
		System.out.println("Trying to open byte buffer");
		try {
			//core = new MuPDFCore(this, buffer);
			core = new MuPDFCore(this, buffer, magic);

			// New file: drop the old outline data
			//OutlineActivityData.set(null);
			PDFPreviewGridActivityData.set(null);
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		catch (java.lang.OutOfMemoryError e)
		{
			//  out of memory is not an Exception, so we catch it separately.
			System.out.println(e);
			return null;
		}
		return core;
	}

	
	@SuppressWarnings("unused")
	private Context getContext() {
		return this;
	}

	private void setCurrentlyViewedPreview() {
		int i = mDocView.getDisplayedViewIndex();
		if (core.getDisplayPages() == 2) {
			i = (i * 2) - 1;
		}
		pdfPreviewPagerAdapter.setCurrentlyViewing(i);
		centerPreviewAtPosition(i);
	}

	public void centerPreviewAtPosition(int position) {
		if (mPreview.getChildCount() > 0) {
			View child = mPreview.getChildAt(0);
			// assume all children the same width
			int childmeasuredwidth = child.getMeasuredWidth();

			if (childmeasuredwidth > 0) {
				if (core.getDisplayPages() == 2) {
					mPreview.setSelectionFromOffset(position,
							(mPreview.getWidth() / 2) - (childmeasuredwidth));
				} else {
					mPreview.setSelectionFromOffset(position,
							(mPreview.getWidth() / 2)
									- (childmeasuredwidth / 2));
				}
			} else {
				Log.e("centerOnPosition", "childmeasuredwidth = 0");
			}
		} else {
			Log.e("centerOnPosition", "childcount = 0");
		}
	}
	
	//  determine whether the current activity is a proofing activity.
	public boolean isProofing()
	{
		String format = core.fileFormat();
		return (format.equals("GPROOF"));
	}

	
	/** Called when the activity is first created. */
	@SuppressLint({ "StringFormatMatches", "NewApi" })
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//	        getWindow().setStatusBarColor(getResources().getColor(R.color.actionbar_background_dark));
//	        getWindow().setNavigationBarColor(getResources().getColor(R.color.actionbar_background_dark));
//	    }
		
		LibraryUtils.reloadLocale(getApplicationContext());
		
		mAlertBuilder = new AlertDialog.Builder(this);
		gAlertBuilder = mAlertBuilder;  //  keep a static copy of this that other classes can use
		fav=new Dialog(MuPDFActivity.this,R.style.InfoDialog);
		if (core == null) {
			core = (MuPDFCore) getLastNonConfigurationInstance();

			if (savedInstanceState != null && savedInstanceState.containsKey("FileName")) {
				mFileName = savedInstanceState.getString("FileName");
			}
		}
		if (core == null) {
			Intent intent = getIntent();
			byte buffer[] = null;
			if (Intent.ACTION_VIEW.equals(intent.getAction())) {
				Uri uri = intent.getData();
				System.out.println("URI to open is: " + uri);
				if (uri.toString().startsWith("content://")) {
					/*
					// Handle view requests from the Transformer Prime's file
					// manager
					// Hopefully other file managers will use this same scheme,
					// if not
					// using explicit paths.
					Cursor cursor = getContentResolver().query(uri,
							new String[] { "_data" }, null, null, null);
					if (cursor.moveToFirst()) {
						String str = cursor.getString(0);
						String reason = null;
						if (str == null) {
							try {
								InputStream is = getContentResolver()
										.openInputStream(uri);
								int len = is.available();
								buffer = new byte[len];
								is.read(buffer, 0, len);
								is.close();
							} catch (java.lang.OutOfMemoryError e) {
								System.out
										.println("Out of memory during buffer reading");
								reason = e.toString();
							} catch (Exception e) {
								reason = e.toString();
							}
							if (reason != null) {
								buffer = null;
								Resources res = getResources();
								AlertDialog alert = mAlertBuilder.create();
								setTitle(String.format(res.getString(R.string.cannot_open_document_Reason), reason));
								alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dismiss), new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialog,
											int which) {
										finish();
									}
								});
								alert.setOnCancelListener(new OnCancelListener() {
									@Override
									public void onCancel(DialogInterface dialog) {
										finish();
									}
								});

								alert.show();
								return;
						*/

					String reason = null;
					try {
						InputStream is = getContentResolver().openInputStream(uri);
						int len = is.available();
						buffer = new byte[len];
						is.read(buffer, 0, len);
						is.close();
					}
					catch (java.lang.OutOfMemoryError e) {
						System.out.println("Out of memory during buffer reading");
						reason = e.toString();
					}
					catch (Exception e) {
						System.out.println("Exception reading from stream: " + e);

						// Handle view requests from the Transformer Prime's file manager
						// Hopefully other file managers will use this same scheme, if not
						// using explicit paths.
						// I'm hoping that this case below is no longer needed...but it's
						// hard to test as the file manager seems to have changed in 4.x.
						try {
							Cursor cursor = getContentResolver().query(uri, new String[]{"_data"}, null, null, null);
							if (cursor.moveToFirst()) {
								String str = cursor.getString(0);
								if (str == null) {
									reason = "Couldn't parse data in intent";
								}
								else {
									uri = Uri.parse(str);
								}

							}
						//} else {
						//	uri = Uri.parse(str);
						}
						catch (Exception e2) {
							System.out.println("Exception in Transformer Prime file manager code: " + e2);
							reason = e2.toString();
						}
					}
					if (reason != null) {
						buffer = null;
						Resources res = getResources();
						AlertDialog alert = mAlertBuilder.create();
						setTitle(String.format(res.getString(R.string.cannot_open_document_Reason), reason));
						alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dismiss),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										finish();
									}
								});
						alert.show();
						return;

					}
				}
				if (buffer != null) {
					//core = openBuffer(buffer);
					core = openBuffer(buffer, intent.getType());

				} else {
					//core = openFile(Uri.decode(uri.getEncodedPath()));
					String path = Uri.decode(uri.getEncodedPath());
					if (path == null) {
						path = uri.toString();
					}
					core = openFile(path);
				}
				SearchTaskResult.set(null);
				if (core.countPages() == 0)
					core = null;
			}
			if (core != null && core.needsPassword()) {
				// requestPassword(savedInstanceState);
				// required password getteris from data
				String password = intent.getStringExtra("pass");
				core.authenticatePassword(password);
				// return;
			}
			if (core != null && core.countPages() == 0)
			{
				core = null;
			}

		}
		if (core == null) {
			AlertDialog alert = mAlertBuilder.create();
			alert.setTitle(R.string.cannot_open_document);
			alert.setButton(AlertDialog.BUTTON_POSITIVE,
					getString(R.string.dismiss),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
			alert.show();
			return;
		}

		mOrientation = getResources().getConfiguration().orientation;

		if(mOrientation == Configuration.ORIENTATION_LANDSCAPE) {
			core.setDisplayPages(2);
		} else {
			core.setDisplayPages(1);
		}
		
		createUI(savedInstanceState);
		/*
		//  hide the proof button if this file can't be proofed
		if (!core.canProof()) {
			mProofButton.setVisibility(View.INVISIBLE);
		}

		if (isProofing()) {

			//  start the activity with a new array
			mSepEnabled = null;

			//  show the separations button
			mSepsButton.setVisibility(View.VISIBLE);

			//  hide some other buttons
			mLinkButton.setVisibility(View.INVISIBLE);
			mReflowButton.setVisibility(View.INVISIBLE);
			mOutlineButton.setVisibility(View.INVISIBLE);
			mSearchButton.setVisibility(View.INVISIBLE);
			mMoreButton.setVisibility(View.INVISIBLE);
		}
		else {
			//  hide the separations button
			mSepsButton.setVisibility(View.INVISIBLE);
		}
		 */
	}

	/*
	 * public void requestPassword(final Bundle savedInstanceState) {
	 * mPasswordView = new EditText(this);
	 * mPasswordView.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
	 * mPasswordView.setTransformationMethod(new
	 * PasswordTransformationMethod());
	 * 
	 * AlertDialog alert = mAlertBuilder.create();
	 * alert.setTitle(R.string.enter_password); alert.setView(mPasswordView);
	 * alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok), new
	 * DialogInterface.OnClickListener() { public void onClick(DialogInterface
	 * dialog, int which) { if
	 * (core.authenticatePassword(mPasswordView.getText().toString())) {
	 * createUI(savedInstanceState); } else {
	 * requestPassword(savedInstanceState); } } });
	 * alert.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel),
	 * new DialogInterface.OnClickListener() {
	 * 
	 * public void onClick(DialogInterface dialog, int which) { finish(); } });
	 * alert.show(); }
	 */

	public void createUI(Bundle savedInstanceState) {
		if (core == null)
			return;

		// Now create the UI.
		// First create the document view
		mDocView = new MuPDFReaderView(this) {
			@Override
			protected void onMoveToChild(int i) {
				updatePageNumView(i);
				//if (core == null)
				//	return;
				//mPageNumberView.setText(String.format("%d / %d", i + 1,
				//		core.countPages()));
				///mPageSlider.setMax((core.countPages() - 1) * mPageSliderRes);
				///mPageSlider.setProgress(i * mPageSliderRes);
				super.onMoveToChild(i);
				setCurrentlyViewedPreview();
				
				
			}

			@Override
			protected void onTapMainDocArea() {
				if (!mButtonsVisible) {
					showButtons();
				} else {
					if (mTopBarMode == TopBarMode.Main)
						hideButtons();
				}
			}

			@Override
			protected void onDocMotion() {
				hideButtons();
			}

			@Override
			protected void onHit(Hit item) {
				/*
				switch (mTopBarMode) {
				case Annot:
					if (item == Hit.Annotation) {
						showButtons();
						mTopBarMode = TopBarMode.Delete;
						mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
					}
					break;
				case Delete:
					mTopBarMode = TopBarMode.Annot;
					mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
				// fall through
				default:
				*/
					// Not in annotation editing mode, but the pageview will
					// still select and highlight hit annotations, so
					// deselect just in case.
					MuPDFView pageView = (MuPDFView) mDocView.getDisplayedView();
					if (pageView != null)
						pageView.deselectAnnotation();
					//break;
				//}
			}
		};
		mDocView.setAdapter(new MuPDFPageAdapter(this, this, core));
		
		Intent intent = getIntent();
		boolean idleenabled = intent.getBooleanExtra("idleenabled", false);
		boolean highlight = intent.getBooleanExtra("linkhighlight", false);
		boolean horizontalscrolling = intent.getBooleanExtra("horizontalscrolling", true);
		mDocView.setKeepScreenOn(!idleenabled);
		mDocView.setLinksHighlighted(highlight);
		mDocView.setScrollingDirectionHorizontal(horizontalscrolling);
		mDocName = intent.getStringExtra("docname");
		
		mSearchTask = new SearchTask(this, core) {
			@Override
			protected void onTextFound(SearchTaskResult result) {
				SearchTaskResult.set(result);
				// Ask the ReaderView to move to the resulting page
				mDocView.setDisplayedViewIndex(result.pageNumber);
				// Make the ReaderView act on the change to SearchTaskResult
				// via overridden onChildSetup method.
				mDocView.resetupChildren();
			}
		};

		// Make the buttons overlay, and store all its
		// controls in variables
		makeButtonsView();

		// Set up the page slider
		///int smax = Math.max(core.countPages() - 1, 1);
		///mPageSliderRes = ((10 + smax - 1) / smax) * 2;

		// Set the file-name text
		// /////////mFilenameView.setText(mFileName);

		// Activate the seekbar
		/*
		mPageSlider
				.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
					public void onStopTrackingTouch(SeekBar seekBar) {
						mDocView.setDisplayedViewIndex((seekBar.getProgress() + mPageSliderRes / 2)
								/ mPageSliderRes);
					}

					public void onStartTrackingTouch(SeekBar seekBar) {
					}

					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						updatePageNumView((progress + mPageSliderRes / 2)
								/ mPageSliderRes);
					}
				});
		*/
		// Activate the search-preparing button
		mSearchButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				searchModeOn();
			}
		});

		// Activate the reflow button
		/*
		mReflowButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				toggleReflow();
			}
		});
		*/
		/*
		if (core.fileFormat().startsWith("PDF")) {
			mAnnotButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					mTopBarMode = TopBarMode.Annot;
					mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
				}
			});
		} else {
			mAnnotButton.setVisibility(View.GONE);
		}
		*/
		// Search invoking buttons are disabled while there is no text specified
		mSearchBack.setEnabled(false);
		mSearchFwd.setEnabled(false);
		mSearchBack.setColorFilter(Color.argb(255, 128, 128, 128));
		mSearchFwd.setColorFilter(Color.argb(255, 128, 128, 128));

		// React to interaction with the text widget
		mSearchText.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
				boolean haveText = s.toString().length() > 0;
				setButtonEnabled(mSearchBack, haveText);
				setButtonEnabled(mSearchFwd, haveText);

				// Remove any previous search results
				if (SearchTaskResult.get() != null 
						&& !mSearchText.getText().toString().equals(SearchTaskResult.get().txt)) {
					SearchTaskResult.set(null);
					mDocView.resetupChildren();
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});

		// React to Done button on keyboard
		mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE)
					search(1);
				return false;
			}
		});

		mSearchText.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN
						&& keyCode == KeyEvent.KEYCODE_ENTER)
					search(1);
				return false;
			}
		});

		// Activate search invoking buttons
		mSearchBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				search(-1);
			}
		});
		mSearchFwd.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				search(1);
			}
		});
		/*
		mLinkButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				setLinkHighlight(!mLinkHighlight);
			}
		});
		 */
		/*
		if (core.hasOutline()) {
			mOutlineButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					OutlineItem outline[] = core.getOutline();
					if (outline != null) {
						OutlineActivityData.get().items = outline;
						Intent intent = new Intent(MuPDFActivity.this,OutlineActivity.class);
						startActivityForResult(intent, OUTLINE_REQUEST);
					}
				}
			});
		} else {
			mOutlineButton.setVisibility(View.GONE);
		}
		*/
		// Reenstate last state if it was recorded
		///SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
		///mDocView.setDisplayedViewIndex(prefs.getInt("page" + mFileName, 0));
		SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
		int orientation = prefs.getInt("orientation", mOrientation);
		int pageNum = prefs.getInt("page"+mFileName, 0);
		if(orientation == mOrientation)
			mDocView.setDisplayedViewIndex(pageNum);
		else {
			if(orientation == Configuration.ORIENTATION_PORTRAIT) {
				mDocView.setDisplayedViewIndex((pageNum + 1) / 2);
			} else {
				mDocView.setDisplayedViewIndex((pageNum == 0) ? 0 : pageNum * 2 - 1);
			}
		}
		
		//if (savedInstanceState == null
		//		|| !savedInstanceState.getBoolean("ButtonsHidden", false))
		//	showButtons();

		if (savedInstanceState != null && savedInstanceState.getBoolean("SearchMode", false))
			searchModeOn();

		//if (savedInstanceState != null && savedInstanceState.getBoolean("ReflowMode", false))
			//reflowModeSet(true);

		// Give preview thumbnails time to appear before showing bottom bar
		if (savedInstanceState == null
				|| !savedInstanceState.getBoolean("ButtonsHidden", false)) {
			mPreview.postDelayed(new Runnable() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							showButtons();
						}
					});
				}
			}, 250);
		}
				
		// Stick the document view and the buttons overlay into a parent view
		RelativeLayout layout = new RelativeLayout(this);
		layout.addView(mDocView);
		layout.addView(mButtonsView);
		//layout.setBackgroundResource(R.drawable.tiled_background);
		//layout.setBackgroundResource(R.color.canvas);
		//creo el video para full screen
		full_scree_video_container = new RelativeLayout(this);
		full_scree_video_container.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		full_scree_video_container.setVisibility(View.GONE);
		videoHolder = new VideoView(this);
		int [] dimen= Magazine.getScreenDimensions(this);
		RelativeLayout.LayoutParams center= new RelativeLayout.LayoutParams(dimen[1], dimen[0]);
		center.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.CENTER_VERTICAL);
		videoHolder.setLayoutParams(center);
		videoHolder.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				return;
			}
		});
		videoHolder.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				videoHolder.bringToFront();
				full_scree_video_container.removeView(imageView);
				full_scree_video_container.addView(imageView);
			}
		});
		videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				videoHolder.setVideoURI(null);
				full_scree_video_container.removeAllViews();
				full_scree_video_container.setVisibility(View.GONE);
			}
		});
		//imagen de cerrar
		imageView= new ImageView(this);
		RelativeLayout.LayoutParams imageParam= new RelativeLayout.LayoutParams((int)getResources().getDimension(R.dimen.close_button),(int)getResources().getDimension(R.dimen.close_button));
		imageParam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		imageParam.setMargins(10,10,10,10);
		imageView.setLayoutParams(imageParam);
		imageView.setImageResource(R.drawable.close_audio_button);
		imageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				videoHolder.stopPlayback();
				full_scree_video_container.removeAllViews();
				full_scree_video_container.setVisibility(View.GONE);
			}
		});
		layout.addView(full_scree_video_container);
//        progress dialog para la carga del streaming de internet
		streamingProgres= new ProgressDialog(this);
		streamingProgres. setMessage("Cargando...");

		setContentView(layout);

		if (isProofing()) {
			//  go to the current page
			int currentPage = getIntent().getIntExtra("startingPage", 0);
			mDocView.setDisplayedViewIndex(currentPage);
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			//case OUTLINE_REQUEST:
			//	if (resultCode >= 0)
			//		mDocView.setDisplayedViewIndex(resultCode);
			//	break;
			case PRINT_REQUEST:
				if (resultCode == RESULT_CANCELED)
					showInfo(getString(R.string.print_failed));
				break;
			case FILEPICK_REQUEST:
				if (mFilePicker != null && resultCode == RESULT_OK)
					mFilePicker.onPick(data.getData());
				break;
			case PAGE_CHOICE_REQUEST:
				if (resultCode >= 0) {
					int page = resultCode;
					if (core.getDisplayPages() == 2) {
						page = (page + 1) / 2;
					}
					mDocView.setDisplayedViewIndex(page);
					setCurrentlyViewedPreview();
				}
				break;
			case PROOF_REQUEST:
				//  we're returning from a proofing activity

				if (mProofFile != null)
				{
					core.endProof(mProofFile);
					mProofFile = null;
				}

				//  return the top bar to default
				mTopBarMode = TopBarMode.Main;
				mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public Object onRetainNonConfigurationInstance() {
		MuPDFCore mycore = core;
		core = null;
		return mycore;
	}
/*
	private void reflowModeSet(boolean reflow) {
		mReflow = reflow;
		mDocView.setAdapter(mReflow ? new MuPDFReflowAdapter(this, core) : new MuPDFPageAdapter(this, this, core));
		mReflowButton.setColorFilter(mReflow ? Color.argb(0xFF, 172, 114, 37) : Color.argb(0xFF, 255, 255, 255));
		setButtonEnabled(mAnnotButton, !reflow);
		setButtonEnabled(mSearchButton, !reflow);
		if (reflow)
			setLinkHighlight(false);
		setButtonEnabled(mLinkButton, !reflow);
		setButtonEnabled(mMoreButton, !reflow);
		mDocView.refresh(mReflow);
	}
*/
/*
	private void toggleReflow() {
		reflowModeSet(!mReflow);
		showInfo(mReflow ? getString(R.string.entering_reflow_mode)
				: getString(R.string.leaving_reflow_mode));
	}
*/
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (mFileName != null && mDocView != null) {
			outState.putString("FileName", mFileName);

			// Store current page in the prefs against the file name,
			// so that we can pick it up each time the file is loaded
			// Other info is needed only for screen-orientation change,
			// so it can go in the bundle
			SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
			SharedPreferences.Editor edit = prefs.edit();
			edit.putInt("page" + mFileName, mDocView.getDisplayedViewIndex());
			edit.putInt("orientation", mOrientation);
			edit.commit();
		}

		if (!mButtonsVisible)
			outState.putBoolean("ButtonsHidden", true);

		if (mTopBarMode == TopBarMode.Search)
			outState.putBoolean("SearchMode", true);

		//if (mReflow)
		//	outState.putBoolean("ReflowMode", true);
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (mSearchTask != null)
			mSearchTask.stop();

		if (mFileName != null && mDocView != null) {
			SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
			SharedPreferences.Editor edit = prefs.edit();
			edit.putInt("page" + mFileName, mDocView.getDisplayedViewIndex());
			edit.putInt("orientation", mOrientation);
			edit.commit();
		}
	}

	public void onDestroy() {
		if (mDocView != null) {
			mDocView.applyToChildren(new ReaderView.ViewMapper() {
				void applyToView(View view) {
					((MuPDFView)view).releaseBitmaps();
				}
			});
		}
		if (core != null)
			core.onDestroy();
		if (mAlertTask != null) {
			mAlertTask.cancel(true);
			mAlertTask = null;
		}
		core = null;
		super.onDestroy();
	}

	private void setButtonEnabled(ImageButton button, boolean enabled) {
		button.setEnabled(enabled);
		button.setColorFilter(enabled ? Color.argb(0, 255, 255, 255) : Color
				.argb(255, 128, 128, 128));
	}
	/*
	private void setLinkHighlight(boolean highlight) {
		mLinkHighlight = highlight;
		// LINK_COLOR tint
		mLinkButton.setColorFilter(highlight ? Color.argb(0xFF, 172, 114, 37)
				: Color.argb(0xFF, 255, 255, 255));
		// Inform pages of the change.
		mDocView.setLinksEnabled(highlight);
	}
	*/
	private void showButtons() {
		if (core == null)
			return;
		if (!mButtonsVisible) {
			mButtonsVisible = true;
			// Update page number text and slider
			int index = mDocView.getDisplayedViewIndex();
			updatePageNumView(index);
			///mPageSlider.setMax((core.countPages() - 1) * mPageSliderRes);
			///mPageSlider.setProgress(index * mPageSliderRes);
			if (mTopBarMode == TopBarMode.Search) {
				mSearchText.requestFocus();
				showKeyboard();
			}
			mTopBarSwitcher.setVisibility(View.VISIBLE);
			Animation anim = new TranslateAnimation(0, 0,
					-mTopBarSwitcher.getHeight(), 0);
			anim.setDuration(200);
			anim.setAnimationListener(new Animation.AnimationListener() {
				public void onAnimationStart(Animation animation) {
					mTopBarSwitcher.setVisibility(View.VISIBLE);
				}

				public void onAnimationRepeat(Animation animation) {
				}

				public void onAnimationEnd(Animation animation) {
				}
			});
//			mTopBarSwitcher.startAnimation(anim);


			// Update listView position
			setCurrentlyViewedPreview();
			///anim = new TranslateAnimation(0, 0, mPageSlider.getHeight(), 0);
			anim = new TranslateAnimation(0, 0, mPreviewBarHolder.getHeight(), 0);
			anim.setDuration(200);
			anim.setAnimationListener(new Animation.AnimationListener() {
				public void onAnimationStart(Animation animation) {
					///mPageSlider.setVisibility(View.VISIBLE);
					mPreviewBarHolder.setVisibility(View.VISIBLE);
				}

				public void onAnimationRepeat(Animation animation) {
				}

				public void onAnimationEnd(Animation animation) {
					mPageNumberView.setVisibility(View.VISIBLE);
				}
			});
			///mPageSlider.startAnimation(anim);
			mPreviewBarHolder.startAnimation(anim);
		}
	}

	private void hideButtons() {
		if (mButtonsVisible) {
			mButtonsVisible = false;
			hideKeyboard();

			Animation anim = new TranslateAnimation(0, 0, 0,
					-mTopBarSwitcher.getHeight());
			anim.setDuration(200);
			mTopBarSwitcher.setVisibility(View.INVISIBLE);
			anim.setAnimationListener(new Animation.AnimationListener() {
				public void onAnimationStart(Animation animation) {
				}

				public void onAnimationRepeat(Animation animation) {
				}

				public void onAnimationEnd(Animation animation) {
					mTopBarSwitcher.setVisibility(View.INVISIBLE);
				}
			});
			//mTopBarSwitcher.startAnimation(anim);

			///anim = new TranslateAnimation(0, 0, 0, mPageSlider.getHeight());
			anim = new TranslateAnimation(0, 0, 0, mPreviewBarHolder.getHeight());
			anim.setDuration(200);
			anim.setAnimationListener(new Animation.AnimationListener() {
				public void onAnimationStart(Animation animation) {
					mPageNumberView.setVisibility(View.INVISIBLE);
				}

				public void onAnimationRepeat(Animation animation) {
				}

				public void onAnimationEnd(Animation animation) {
					///mPageSlider.setVisibility(View.INVISIBLE);
					mPreviewBarHolder.setVisibility(View.INVISIBLE);
				}
			});
			///mPageSlider.startAnimation(anim);
			mPreviewBarHolder.startAnimation(anim);
		}
	}

	private void searchModeOn() {
		if (mTopBarMode != TopBarMode.Search) {
			mTopBarMode = TopBarMode.Search;
			// Focus on EditTextWidget
			mSearchText.requestFocus();
			showKeyboard();
			mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
		}
	}

	private void searchModeOff() {
		if (mTopBarMode == TopBarMode.Search) {
			mTopBarMode = TopBarMode.Main;
			hideKeyboard();
			mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
			SearchTaskResult.set(null);
			// Make the ReaderView act on the change to mSearchTaskResult
			// via overridden onChildSetup method.
			mDocView.resetupChildren();
		}
	}

	@SuppressLint("DefaultLocale")
	private void updatePageNumView(int index) {
		if (core == null)
			return;
		String pageStr = "";
		if (core.getDisplayPages() == 2 && index!=0 && index!=core.countPages()-1) {
			pageStr = String.format("%1$d-%2$d", (index*2), (index*2)+1);
			mPageNumberView.setText(String.format(getString(R.string.two_pages_of_count), (index*2), (index*2)+1, core.countSinglePages()));
		}
		else if (core.getDisplayPages() == 2 && index==0) {
			pageStr = String.format("%1$d", index+1);
			mPageNumberView.setText(String.format(getString(R.string.one_page_of_count), index+1, core.countSinglePages()));
		}
		else if (core.getDisplayPages() == 2 && index==core.countPages()-1) {
			pageStr = String.format("%1$d", (index*2));
			mPageNumberView.setText(String.format(getString(R.string.one_page_of_count), (index*2), core.countSinglePages()));
		}
		else {
			pageStr = String.format("%1$d", index+1);
			mPageNumberView.setText(String.format(getString(R.string.one_page_of_count), index+1, core.countPages()));
		}

		MuPDFCallbackClass.sendGaiView(String.format("documentView (%1$s), page (%2$s)", mDocName, pageStr));
	}
	private void printDoc() {
		if (!core.fileFormat().startsWith("PDF")) {
			showInfo(getString(R.string.format_currently_not_supported));
			return;
		}

		Intent myIntent = getIntent();
		Uri docUri = myIntent != null ? myIntent.getData() : null;

		if (docUri == null) {
			showInfo(getString(R.string.print_failed));
		}

		if (docUri.getScheme() == null)
			docUri = Uri.parse("file://" + docUri.toString());

		Intent printIntent = new Intent(this, PrintDialogActivity.class);
		printIntent.setDataAndType(docUri, "aplication/pdf");
		printIntent.putExtra("title", mFileName);
		startActivityForResult(printIntent, PRINT_REQUEST);
	}

	private void showInfo(String message) {
		mInfoView.setText(message);
		int currentApiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentApiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			@SuppressWarnings("unused")
			SafeAnimatorInflater safe = new SafeAnimatorInflater((Activity)this, R.anim.info, (View)mInfoView);
		} else {
			mInfoView.setVisibility(View.VISIBLE);
			mHandler.postDelayed(new Runnable() {
				public void run() {
					mInfoView.setVisibility(View.INVISIBLE);
				}
			}, 500);
		}
	}

	void showInfo(){
		Log.i("cosa","click detectado");
		// TODO Auto-generated method stub
		Display display =((WindowManager)getSystemService(MuPDFActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();

		Dialog ab = new Dialog(MuPDFActivity.this, R.style.InfoDialog);
		ab.setContentView(R.layout.info_layout);
		ab.getWindow().setLayout(width, height);
		ab.show();
	}

	private void makeButtonsView() {
		mButtonsView = getLayoutInflater().inflate(R.layout.buttons, null);
		// ///////mFilenameView =
		// (TextView)mButtonsView.findViewById(R.id.docNameText);
		///mPageSlider = (SeekBar) mButtonsView.findViewById(R.id.pageSlider);
		mPreviewBarHolder = (LinearLayout) mButtonsView.findViewById(R.id.PreviewBarHolder);
		TextView txt= (TextView)mButtonsView.findViewById(R.id.PreviewPageNumber);
		txt.measure(0,0);
		int plus_height= txt.getMeasuredHeight();
		int plus_padding= getResources().getDimensionPixelOffset(R.dimen.toolbar_height);
		int plus_margin= getResources().getDimensionPixelOffset(R.dimen.preview_margin);
		RelativeLayout.LayoutParams lpp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,plus_height+plus_padding);
		mPreviewBarHolder.setLayoutParams(lpp);
		mPreview = new TwoWayView(this);
		mPreview.setOrientation(Orientation.HORIZONTAL);
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-1, -1);
		mPreview.setLayoutParams(lp);
		pdfPreviewPagerAdapter = new ToolbarPreviewAdapter(this, core);
		mPreview.setAdapter(pdfPreviewPagerAdapter);
		mPreview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> pArg0, View pArg1,
					int position, long id) {
				hideButtons();
				mDocView.setDisplayedViewIndex((int)id);
			}
		});
		mPreviewBarHolder.addView(mPreview);
		audioView = (LinearLayout)mButtonsView.findViewById(R.id.audioView);
		audioBar = (SeekBar)mButtonsView.findViewById(R.id.seekBar1);
		playPauseAudio = (ImageButton)mButtonsView.findViewById(R.id.playButton);
		closeAudio = (ImageButton)mButtonsView.findViewById(R.id.closeButton);
		audioView = (LinearLayout)mButtonsView.findViewById(R.id.audioView);
		currentTime = (TextView)mButtonsView.findViewById(R.id.audioCurrent);
		totalTime = (TextView)mButtonsView.findViewById(R.id.audioDuration);
		mPageNumberView = (TextView) mButtonsView.findViewById(R.id.pageNumber);
		mInfoView = (TextView) mButtonsView.findViewById(R.id.info);
		mSearchButton = (ImageButton) mButtonsView.findViewById(R.id.searchButton);
		//mReflowButton = (ImageButton) mButtonsView.findViewById(R.id.reflowButton);
		//mOutlineButton = (ImageButton) mButtonsView.findViewById(R.id.outlineButton);
		//mAnnotButton = (ImageButton)mButtonsView.findViewById(R.id.editAnnotButton);
		//mAnnotTypeText = (TextView)mButtonsView.findViewById(R.id.annotType);
		mTopBarSwitcher = (ViewAnimator) mButtonsView.findViewById(R.id.switcher);
		mSearchBack = (ImageButton) mButtonsView.findViewById(R.id.searchBack);
		mSearchFwd = (ImageButton) mButtonsView.findViewById(R.id.searchForward);
		mSearchText = (EditText) mButtonsView.findViewById(R.id.searchText);
		//mLinkButton = (ImageButton) mButtonsView.findViewById(R.id.linkButton);
		//mMoreButton = (ImageButton) mButtonsView.findViewById(R.id.moreButton);
		mTopBarSwitcher.setVisibility(View.INVISIBLE);
		mPageNumberView.setVisibility(View.INVISIBLE);
		mInfoView.setVisibility(View.INVISIBLE);
		///mPageSlider.setVisibility(View.INVISIBLE);
		mPreviewBarHolder.setVisibility(View.INVISIBLE);
		mButtonsView.findViewById(R.id.btn_info).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showInfo();
			}
		});
		mButtonsView.findViewById(R.id.imageButton4).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				final Dialog share = new Dialog(MuPDFActivity.this,R.style.MyDialog);
				share.setContentView(R.layout.share_view);
				share.getWindow().setGravity(Gravity.TOP | Gravity.RIGHT);
				//share.getWindow().set
				//fav.getWindow().setAttributes(a)
				WindowManager.LayoutParams params2 = share.getWindow().getAttributes();
				params2.y = 60;
				share.getWindow().setAttributes(params2);
				share.show();

				String pkg = getPackageName();

				String appPath = Environment.getExternalStorageDirectory().getPath()
						.concat("/Revistas/")
						.concat(getIntent().getStringExtra("pkn"))
						.concat("/file");
				File folder = new File(appPath + "/" + URLEncoder.encode(getIntent().getStringExtra("edition_id")) + "/Thumb");
				File[] thumbList = folder.listFiles();

				HashMap<Integer, String> allFiles = new HashMap<Integer, String>();
				for (int i = 0; i < thumbList.length; i++) {
					String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_") + 1, thumbList[i].getName().lastIndexOf("."));
					allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
				}
				Uri[] thumbUriFiles = new Uri[allFiles.size()];
				for (int i = 0; i < allFiles.size(); i++) {
					thumbUriFiles[i] = Uri.parse(allFiles.get(i));
				}
				final Uri image = thumbUriFiles[mDocView.getDisplayedViewIndex()];
				Log.i("Imagen a compartir", " " + image.getPath());

				share.findViewById(R.id.btn_twitter).setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						Log.i("share", "Share page on twitter clicked");
						Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
						shareIntent.setType("image/*");
						shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, getIntent().getStringExtra("social_text") + " " + getIntent().getStringExtra("social_link"));
						//shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(new File("/storage/emulated/0/Android/data/com.jetskinworld/N%C3%BAmero+12+-+Dic%2Fene+2012/Thumb/gand_20121219175417_n12-app_0.jpg")));
						shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(new File(image.getEncodedPath())));

						final PackageManager pm = v.getContext().getPackageManager();
						final List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
						for (final ResolveInfo app : activityList) {
							if ("com.twitter.android.PostActivity".equals(app.activityInfo.name)) {
								final ActivityInfo activity = app.activityInfo;
								final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
								shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
								shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
								shareIntent.setComponent(name);
								if(shareIntent.resolveActivity(getPackageManager())!= null)
									v.getContext().startActivity(shareIntent);
								share.hide();
								break;
							}
					      /*else{
					    	  AlertDialog alert = new AlertDialog.Builder(getContext()).create();
					    	  alert.setTitle(getContext().getResources().getString(R.string.alert));
					    	  alert.setMessage(getContext().getResources().getString(R.string.no_twitter_app));
					    	  alert.show();
					    	  share.hide();
					      }*/
						}

					}
				});
				share.findViewById(R.id.btn_facebook).setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						Log.i("share", "Share page on facebook clicked");

						Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
						shareIntent.setType("text/plain");
						shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, getIntent().getStringExtra("social_text") + " " + getIntent().getStringExtra("social_link"));
						//shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "sdksdk");
						PackageManager pm = v.getContext().getPackageManager();
						List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
						for (final ResolveInfo app : activityList) {
							if ((app.activityInfo.name).contains("facebook")) {
								final ActivityInfo activity = app.activityInfo;
								final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
								shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
								shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
										Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
								shareIntent.setComponent(name);
								if(shareIntent.resolveActivity(getPackageManager())!= null)
									v.getContext().startActivity(shareIntent);
								share.hide();
								break;
							}
						}
					}
				});
				share.findViewById(R.id.btn_mail).setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						Log.i("share", "Share page by mail clicked");

						final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

						//emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, Constant.mailText);

						emailIntent.setType("image/*");

						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getIntent().getStringExtra("mailsub"));

						emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,getIntent().getStringExtra("mailtxt") + " " + getIntent().getStringExtra("social_link"));

						emailIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(new File(image.getEncodedPath())));

						//emailIntent.putExtra(android.content.Intent.EXTRA_STREAM, image);

						v.getContext().startActivity(Intent.createChooser(emailIntent, "Send mail..."));
						share.hide();
					}
				});
				share.findViewById(R.id.btn_print).setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {

						Log.i("share", "Print page clicked");

						/*String intentToCheck = "org.androidprinting.intent.action.PRINT"; //can be any other intent
						final PackageManager packageManager = getPackageManager();
						final Intent intent = new Intent(intentToCheck);
						List list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
						final boolean isAvailable = list.size() > 0;*/

						//if(isAvailable){
						Intent i = new Intent("org.androidprinting.intent.action.PRINT");
						i.addCategory(i.CATEGORY_DEFAULT);
						i.setDataAndType(Uri.fromFile(new File(image.getEncodedPath())), "image/*");

						if(i.resolveActivity(getPackageManager())!=null)
							v.getContext().startActivity(i);
						share.hide();
						//}
					}
				});

			}

		});
		mButtonsView.findViewById(R.id.imageButton5).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				Display disp2 =((WindowManager)getSystemService(MuPDFActivity.this.WINDOW_SERVICE)).getDefaultDisplay();
				int width = disp2.getWidth()/3;
				int height = disp2.getHeight()/2;

				if(disp2.getWidth()<600)
					width=disp2.getWidth()/2;
				if(!((MuPDFActivity.this.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE))
					width=disp2.getWidth()/2;


				fav.setContentView(R.layout.favs_view);

//                if(mOrientation==Configuration.ORIENTATION_LANDSCAPE)
//                    fav.getWindow().setLayout(height,width);
//                else
				fav.getWindow().setLayout(width,height);
				fav.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
				//fav.getWindow().setAttributes(a)
				WindowManager.LayoutParams params = fav.getWindow().getAttributes();
				params.y=60;
				fav.getWindow().setAttributes(params);
				fav.show();

				final SharedPreferences preferences = getSharedPreferences("MyPreferences", MODE_PRIVATE);

				fav.findViewById(R.id.btn_addfav).setOnClickListener(new View.OnClickListener(){
					public void onClick(View v) {
						//IssueActivity.favClicked(preferences);

						//IssueActivity issue = new IssueActivity();
						//issue.favClicked(preferences, docView.getDisplayedViewIndex(), URLEncoder.encode(getIntent().getStringExtra("name")));

						//  if(Configuration.ORIENTATION_PORTRAIT==mOrientation)
						favClicked(preferences, mDocView.getDisplayedViewIndex(), URLEncoder.encode(getIntent().getStringExtra("name")));
//                        else
//                            favClicked(preferences, (docView.getDisplayedViewIndex()*2)-1, URLEncoder.encode(getIntent().getStringExtra("name")));
					}
				});

				String appPath = Environment.getExternalStorageDirectory().getPath()
						.concat("/Revistas/")
						.concat(getIntent().getStringExtra("pkn"))
						.concat("/file");
				File folder = new File(appPath + "/" + URLEncoder.encode(getIntent().getStringExtra("edition_id")) + "/Thumb");
				File[] thumbList = folder.listFiles();

				HashMap<Integer, String> allFiles = new HashMap<Integer, String>();
				for (int i = 0; i < thumbList.length; i++) {
					String id = thumbList[i].getName().substring(thumbList[i].getName().lastIndexOf("_")+1, thumbList[i].getName().lastIndexOf("."));
					allFiles.put(Integer.parseInt(id), thumbList[i].getAbsolutePath());
				}

				Uri[] thumbUriFiles = new Uri[allFiles.size()];
				for (int i = 0; i < allFiles.size(); i++) {
					thumbUriFiles[i] = Uri.parse(allFiles.get(i));
				}

				LinearLayout layout = (LinearLayout) fav.findViewById(R.id.scroll);
				LayoutInflater inflat = (LayoutInflater) getSystemService(fav.getContext().LAYOUT_INFLATER_SERVICE);

				for(int i=1;i<=thumbList.length;i++){

					int index = preferences.getInt(URLEncoder.encode(getIntent().getStringExtra("name"))+"_"+i, 0);
					if(index==1){
						View placeHolder = inflat.inflate(R.layout.favs_placeholder, null);
						placeHolder.setTag(i-1);
						ImageView img = (ImageView) placeHolder.findViewById(R.id.img_fav);
						img.setImageURI(thumbUriFiles[i-1]);
						TextView txt = (TextView) placeHolder.findViewById(R.id.txt_fav);
						txt.setText("" + (i));
						Log.i("info","Hago esto, index: "+URLEncoder.encode(getIntent().getStringExtra("name"))+"_"+(i-1));

						layout.addView(placeHolder);

						placeHolder.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								Integer id = (Integer) v.getTag();
								mDocView.setDisplayedViewIndex(id);
								//thumb.dismiss();

								/*if(((MuPDFPageView)(docView.getViewAtIndex(id-1)))!=null)
									((MuPDFPageView)(docView.getViewAtIndex(id-1))).deleteMedia();
								if(((MuPDFPageView)(docView.getViewAtIndex(id+1)))!=null)
									((MuPDFPageView)(docView.getViewAtIndex(id+1))).deleteMedia();

								if(((MuPDFPageView)(docView.getViewAtIndex(id)))!=null){
									((MuPDFPageView)(docView.getViewAtIndex(id))).deleteMedia();
									((MuPDFPageView)(docView.getViewAtIndex(id))).loadMedia();
								}*/
							}
						});

						Button b = (Button) placeHolder.findViewById(R.id.delete_fav);
						b.setTag(i);
						b.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Integer id = (Integer) v.getTag();
								Log.i("delete", "a borrar "+URLEncoder.encode(getIntent().getStringExtra("name"))+"_"+id);
								SharedPreferences.Editor editor = preferences.edit();
								editor.putInt(URLEncoder.encode(getIntent().getStringExtra("name"))+"_"+id, 0);
								editor.commit();
								fav.hide();

							}
						});
					}
				}
			}
		});
	}

	public MuPDFReaderView getmDocView() {
		return mDocView;
	}

	public void showAudioControls(String [] audios){
		View but_con= findViewById(R.id.but_cont);
		but_con.measure(0,0);
		int height= but_con.getMeasuredHeight();
		File audioFile= new File(audios[0]);
		audioView.setVisibility(View.VISIBLE);
		audioFileString=audioFile.toString();
		audioFromUrl=false;
		if(!new File(audioFileString).exists()){
			if(media==null){
				audioFromUrl=true;
				media= new MediaPlayer();
				media.setAudioStreamType(AudioManager.STREAM_MUSIC);
				media.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mediaPlayer) {
						Toast.makeText(MuPDFActivity.this,"Stream ready",Toast.LENGTH_SHORT).show();
						streamingProgres.dismiss();
						media.start();
					}
				});
				media.setOnErrorListener(new MediaPlayer.OnErrorListener() {
					@Override
					public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
						Toast.makeText(MuPDFActivity.this,"Error reading stream",Toast.LENGTH_SHORT).show();
						mediaPlayer.release();
						return false;
					}
				});
				try {
					media.setDataSource(audios[1]);
					media.prepareAsync();
					streamingProgres.show();
				} catch (IOException e) {
					e.printStackTrace();
					media.release();
				}
			}}else
		if(media==null){
			media = MediaPlayer.create(this, Uri.fromFile(audioFile));
			audioBar.setMax(media.getDuration()/1000);
			media.start();
			playPauseAudio.setBackgroundResource(R.drawable.pause_audio_button);
			SimpleDateFormat df = new SimpleDateFormat("mm:ss");
			totalTime.setText(df.format(media.getDuration()));
		}

		System.out.println("Llego aqui 2");

		media.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				media.seekTo(0);
				media.pause();
				playPauseAudio.setBackgroundResource(R.drawable.play_audio_button);
			}
		});

		System.out.println("Llego aqui 3");

		playPauseAudio.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if(media.isPlaying()){
					media.pause();
					playPauseAudio.setBackgroundResource(R.drawable.play_audio_button);
				}
				else{
					media.start();
					playPauseAudio.setBackgroundResource(R.drawable.pause_audio_button);
				}

			}
		});

		closeAudio.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				media.stop();
				media=null;
				audioView.setVisibility(View.GONE);

			}
		});

		audioBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
										  boolean fromUser) {
				if(fromUser)
					media.seekTo(progress*1000);

			}
		});
		if(!audioFromUrl)
			updateAudioBar();

	}

	public void updateAudioBar(){
		if(media!=null){
			audioBar.setProgress(media.getCurrentPosition()/1000);
			SimpleDateFormat df = new SimpleDateFormat("mm:ss");
			currentTime.setText(df.format(media.getCurrentPosition()));

			mHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					//Do something after 100ms
					updateAudioBar();
				}
			}, 1000);
		}
	}

	public void playFullScreenVideo(Uri path){
		full_scree_video_container.setVisibility(View.VISIBLE);
		full_scree_video_container.removeView(videoHolder);
		full_scree_video_container.addView(videoHolder);
		videoHolder.setVideoURI(path);
		videoHolder.requestFocus();
		videoHolder.start();
	}

	void favClicked(SharedPreferences preferences, int index, String issueName){
		Log.i("btn","button add pressed y index: "+index);
		//preferences = this.getSharedPreferences("MyPreferences", MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(issueName+"_"+(index+1), 1);

		Log.i("Guardaro","nombre: "+issueName+"_"+(index+1));
		/*int size = preferences.getInt(issueName+"_fav_size", 0);
		size++;
		editor.putInt(issueName+"_fav_size", size);*/
		editor.commit();
		//ManagerGoogleAnalytics.getInstance().sendEvent(MuPDFActivity.this,getString(R.string.readingTrackedName)+": "+getIntent().getStringExtra("name"), getString(R.string.favoritesTrackedName), ""+(index+1), null);

		/*View placeHolder = layoutForHorizontalList.findViewWithTag(index);
		ImageView star = (ImageView) placeHolder.findViewById(R.id.favStar);
		star.setImageResource(R.drawable.fav_star);*/

		fav.hide();
	}

	public void OnMoreButtonClick(View v) {
		
		
		if (core != null) {
			int i = mDocView.getDisplayedViewIndex();
			if (core.getDisplayPages() == 2) {
				i = (i * 2) - 1;
			}
			PDFPreviewGridActivityData.get().core = core;
			PDFPreviewGridActivityData.get().position = i;
			//PDFPreviewGridActivity prevAct = new PDFPreviewGridActivity();
			//Intent intent = prevAct.getIntent();
			Intent intent = new Intent(MuPDFActivity.this, PDFPreviewGridActivity.class);
			startActivityForResult(intent, PAGE_CHOICE_REQUEST);
			
			MuPDFCallbackClass.sendGaiView(String.format("documentThumbView (%1$s)", mDocName));

		}
		
		/////////////mTopBarMode = TopBarMode.More;
		/////////////mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
	}

	public void OnCancelMoreButtonClick(View v) {
		mTopBarMode = TopBarMode.Main;
		mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
	}

	public void OnPrintButtonClick(View v) {
		printDoc();
	}
	
	//  start a proof activity with the given resolution.
	public void proofWithResolution (int resolution)
	{
		mProofFile = core.startProof(resolution);
		Uri uri = Uri.parse("file://"+mProofFile);
		Intent intent = new Intent(this, MuPDFActivity.class);
		intent.setAction(Intent.ACTION_VIEW);
		intent.setData(uri);
		// add the current page so it can be found when the activity is running
		intent.putExtra("startingPage", mDocView.getDisplayedViewIndex());
		startActivityForResult(intent, PROOF_REQUEST);
	}
	
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public void OnProofButtonClick(final View v)
	{
		//  set up the menu or resolutions.
		final PopupMenu popup = new PopupMenu(this, v);
		popup.getMenu().add(0, 1,    0, "Select a resolution:");
		popup.getMenu().add(0, 72,   0, "72");
		popup.getMenu().add(0, 96,   0, "96");
		popup.getMenu().add(0, 150,  0, "150");
		popup.getMenu().add(0, 300,  0, "300");
		popup.getMenu().add(0, 600,  0, "600");
		popup.getMenu().add(0, 1200, 0, "1200");
		popup.getMenu().add(0, 2400, 0, "2400");

		//  prevent the first item from being dismissed.
		//  is there not a better way to do this?  It requires minimum API 14
		MenuItem item = popup.getMenu().getItem(0);
		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		item.setActionView(new View(v.getContext()));
		item.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
			@Override
			public boolean onMenuItemActionExpand(MenuItem item) {
				return false;
			}

			@Override
			public boolean onMenuItemActionCollapse(MenuItem item) {
				return false;
			}
		});

		popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				int id = item.getItemId();
				if (id != 1) {
					//  it's a resolution.  The id is also the resolution value
					proofWithResolution(id);
					return true;
				}
				return false;
			}
		});

		popup.show();
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public void OnSepsButtonClick(final View v)
	{
		if (isProofing()) {

			//  get the current page
			final int currentPage = mDocView.getDisplayedViewIndex();

			//  buid a popup menu based on the given separations
			final PopupMenu menu = new PopupMenu(this, v);

			//  This makes the popup menu display icons, which by default it does not do.
			//  I worry that this relies on the internals of PopupMenu, which could change.
			try {
				Field[] fields = menu.getClass().getDeclaredFields();
				for (Field field : fields) {
					if ("mPopup".equals(field.getName())) {
						field.setAccessible(true);
						Object menuPopupHelper = field.get(menu);
						Class<?> classPopupHelper = Class.forName(menuPopupHelper
								.getClass().getName());
						Method setForceIcons = classPopupHelper.getMethod(
								"setForceShowIcon", boolean.class);
						setForceIcons.invoke(menuPopupHelper, true);
						break;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			//  get the maximum number of seps on any page.
			//  We use this to dimension an array further down
			int maxSeps = 0;
			int numPages = core.countPages();
			for (int page=0; page<numPages; page++) {
				int numSeps = core.getNumSepsOnPage(page);
				if (numSeps>maxSeps)
					maxSeps = numSeps;
			}

			//  if this is the first time, create the "enabled" array
			if (mSepEnabled==null) {
				mSepEnabled = new boolean[numPages][maxSeps];
				for (int page=0; page<numPages; page++) {
					for (int i = 0; i < maxSeps; i++)
						mSepEnabled[page][i] = true;
				}
			}

			//  count the seps on this page
			int numSeps = core.getNumSepsOnPage(currentPage);

			//  for each sep,
			for (int i = 0; i < numSeps; i++) {

//				//  Robin use this to skip separations
//				if (i==12)
//					break;

				//  get the name
				Separation sep = core.getSep(currentPage,i);
				String name = sep.name;

				//  make a checkable menu item with that name
				//  and the separation index as the id
				MenuItem item = menu.getMenu().add(0, i, 0, name+"    ");
				item.setCheckable(true);

				//  set an icon that's the right color
				int iconSize = 48;
				int alpha = (sep.rgba >> 24) & 0xFF;
				int red   = (sep.rgba >> 16) & 0xFF;
				int green = (sep.rgba >> 8 ) & 0xFF;
				int blue  = (sep.rgba >> 0 ) & 0xFF;
				int color = (alpha << 24) | (red << 16) | (green << 8) | (blue << 0);

				ShapeDrawable swatch = new ShapeDrawable (new RectShape());
				swatch.setIntrinsicHeight(iconSize);
				swatch.setIntrinsicWidth(iconSize);
				swatch.setBounds(new Rect(0, 0, iconSize, iconSize));
				swatch.getPaint().setColor(color);
				item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
				item.setIcon(swatch);

				//  check it (or not)
				item.setChecked(mSepEnabled[currentPage][i]);

				//  establishing a menu item listener
				item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						//  someone tapped a menu item.  get the ID
						int sep = item.getItemId();

						//  toggle the sep
						mSepEnabled[currentPage][sep] = !mSepEnabled[currentPage][sep];
						item.setChecked(mSepEnabled[currentPage][sep]);
						core.controlSepOnPage(currentPage, sep, !mSepEnabled[currentPage][sep]);

						//  prevent the menu from being dismissed by these items
						item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
						item.setActionView(new View(v.getContext()));
						item.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
							@Override
							public boolean onMenuItemActionExpand(MenuItem item) {
								return false;
							}

							@Override
							public boolean onMenuItemActionCollapse(MenuItem item) {
								return false;
							}
						});
						return false;
					}
				});

				//  tell core to enable or disable each sep as appropriate
				//  but don't refresh the page yet.
				core.controlSepOnPage(currentPage, i, !mSepEnabled[currentPage][i]);
			}

			//  add one for done
			MenuItem itemDone = menu.getMenu().add(0, 0, 0, "Done");
			itemDone.setOnMenuItemClickListener(new OnMenuItemClickListener() {
				@TargetApi(Build.VERSION_CODES.HONEYCOMB)
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					//  refresh the view
					mDocView.refresh(false);
					return true;
				}
			});

			//  show the menu
			menu.show();
		}

	}
	
	public void OnCloseReaderButtonClick(View v) {
		finish();
	}
/*
	public void OnCancelAcceptButtonClick(View v) {
		MuPDFView pageView = (MuPDFView) mDocView.getDisplayedView();
		if (pageView != null) {
			pageView.deselectText();
			pageView.cancelDraw();
		}
		mDocView.setMode(MuPDFReaderView.Mode.Viewing);
		
		//switch (mAcceptMode) {
		//case CopyText:
		//	mTopBarMode = TopBarMode.More;
		//	break;
		//default:
			//mTopBarMode = TopBarMode.Annot;
		//	break;
		//}
		
		mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
	}
	*/

	public void OnCancelSearchButtonClick(View v) {
		searchModeOff();
	}

	private void showKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null)
			imm.showSoftInput(mSearchText, 0);
	}

	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null)
			imm.hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);
	}

	private void search(int direction) {
		hideKeyboard();
		int displayPage = mDocView.getDisplayedViewIndex();
		SearchTaskResult r = SearchTaskResult.get();
		int searchPage = r != null ? r.pageNumber : -1;
		mSearchTask.go(mSearchText.getText().toString(), direction, displayPage, searchPage);
	}

	@Override
	public boolean onSearchRequested() {
		if (mButtonsVisible && mTopBarMode == TopBarMode.Search) {
			hideButtons();
		} else {
			showButtons();
			searchModeOn();
		}
		return super.onSearchRequested();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (mButtonsVisible && mTopBarMode != TopBarMode.Search) {
			hideButtons();
		} else {
			showButtons();
			searchModeOff();
		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	protected void onStart() {
		if (core != null) {
			core.startAlerts();
			createAlertWaiter();
		}

		super.onStart();
	}

	@Override
	protected void onStop() {
		if (core != null) {
			destroyAlertWaiter();
			core.stopAlerts();
		}

		super.onStop();
	}

	@Override
	public void onBackPressed() {
		//if (core.hasChanges()) {
		if (core != null && core.hasChanges()) {
			DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if (which == AlertDialog.BUTTON_POSITIVE)
						core.save();

					finish();
				}
			};
			AlertDialog alert = mAlertBuilder.create();
			alert.setTitle("MuPDF");
			alert.setMessage(getString(R.string.document_has_changes_save_them_));
			alert.setButton(AlertDialog.BUTTON_POSITIVE,
					getString(R.string.yes), listener);
			alert.setButton(AlertDialog.BUTTON_NEGATIVE,
					getString(R.string.no), listener);
			alert.show();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void performPickFor(FilePicker picker) {
		mFilePicker = picker;
		//Intent intent = new Intent(this, ChoosePDFActivity.class);
		//startActivityForResult(intent, FILEPICK_REQUEST);
	}
}
