// Generated code from Butter Knife. Do not modify!
package com.muevaelvolante.qiumagazine.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class GalleryActivity$$ViewBinder<T extends com.muevaelvolante.qiumagazine.activity.GalleryActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624161, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624161, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131624162, "field 'pager'");
    target.pager = finder.castView(view, 2131624162, "field 'pager'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.pager = null;
  }
}
