// Generated code from Butter Knife. Do not modify!
package com.muevaelvolante.qiumagazine.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class GalleryItem$$ViewBinder<T extends com.muevaelvolante.qiumagazine.activity.GalleryItem> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624045, "field 'image'");
    target.image = finder.castView(view, 2131624045, "field 'image'");
    view = finder.findRequiredView(source, 2131624170, "field 'next'");
    target.next = view;
    view = finder.findRequiredView(source, 2131624171, "field 'prev'");
    target.prev = view;
  }

  @Override public void unbind(T target) {
    target.image = null;
    target.next = null;
    target.prev = null;
  }
}
