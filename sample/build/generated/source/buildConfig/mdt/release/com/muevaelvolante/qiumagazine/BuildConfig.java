/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.muevaelvolante.qiumagazine;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.mdt";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "mdt";
  public static final int VERSION_CODE = 8;
  public static final String VERSION_NAME = "1.08";
}
