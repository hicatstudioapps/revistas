package com.lacostra.utils.notification;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by CQ on 16/01/2016.
 */
public class PackageUtil {

    public static List<String> getIntalledApps(Context context) {
        List<String> result = new LinkedList<String>();
        List<PackageInfo> infos = context.getPackageManager().getInstalledPackages(PackageManager.GET_ACTIVITIES);
        if (infos != null && infos.size() > 0) {
            for (PackageInfo info : infos) {
                result.add(info.packageName);
            }
        }
        return result;
    }

    public static boolean isAppInstalled(Context c, String packageName) {
        PackageManager pm = c.getPackageManager();
        boolean installed = false;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            installed = false;
        }
        return installed;
    }

    public static    void open(Context c,String packageName) {
        Intent LaunchIntent = c.getPackageManager().getLaunchIntentForPackage(
                packageName);
        c.startActivity(LaunchIntent);
    }

    public static void openMarket(String packageName,  Context context){
        Intent var1 = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + "com.battery.cal"));
        try {
            context.startActivity(var1);
        } catch (ActivityNotFoundException var2) {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + "com.battery.cal")));
        }
    }
}
