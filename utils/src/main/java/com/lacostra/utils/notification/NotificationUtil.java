package com.lacostra.utils.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

/**
 * Created by La Costra on 28/09/2015.
 */
public class NotificationUtil {

    public NotificationManager notificationManager;
    public NotificationCompat.Builder notificationText;
    public NotificationCompat.Builder notificationProgress;

    public void getInstance(Context context) {
        if (notificationManager == null)
            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    /**
     * Created by La Costra on 28/09/2015.
     *
     * @param drawable       icono a mostrar junto a la notificacion
     * @param intent         accion a ejecutar cuando se presione sobre la notificacion
     * @param notificationID id de la notificacion
     * @param context        texto de la notificacion
     */
    public void notifyText(Context context,Resources res, int notificationID, int drawable,int drawableL, String text, String content, PendingIntent intent) {
        getInstance(context);
        notificationText = new NotificationCompat.Builder(context);
        notificationText.setSmallIcon(drawable)
                .setLargeIcon(BitmapFactory.decodeResource(res,drawableL))
                .setContentTitle(text)
                .setContentText(content)
//
//
//        .setVibrate(new long[]{200L,300L,500L})
        .setOngoing(true)
        ;

        if (intent != null)
            notificationText.setContentIntent(intent);
        notificationManager.notify(notificationID, notificationText.build());
    }

    /***
     * @param context
     * @param notificationID id de la notificacion
     * @param drawable icono de la notificacion
     * @param text titulo de la notificacion
     * @param content texto de la notificacion
     * @param maxProgress maximo progreso del progressbar
     * @param progress progreso actual
     * @param indeterminate 
     */
    public void notifyProgress(Context context, int notificationID, int drawable, String text, String content, int maxProgress, int progress, boolean indeterminate) {
        getInstance(context);
        if (notificationProgress == null) {
            notificationProgress = new NotificationCompat.Builder(context);
            notificationProgress.setSmallIcon(drawable)
                    .setContentTitle(text)
                    .setContentText(content)
                    .setProgress(maxProgress, progress, indeterminate);
        } else {
            notificationProgress.setProgress(maxProgress, progress, false);
        }
        notificationManager.notify(notificationID, notificationProgress.build());
    }

    public void notifyProgressFinish(int notificationID, String finishText, PendingIntent intent) {
        notificationProgress.setContentText(finishText);
        notificationProgress.setProgress(0, 0, false);
        if (intent != null)
            notificationProgress.setContentIntent(intent);
        notificationManager.notify(notificationID, notificationProgress.build());
    }

    public void ressetNotification() {
        notificationText = null;
        notificationProgress = null;
    }
}
